#!/bin/env pipecl

string	pipemon, switchbd

servers

if (defvar("NHPPS_PORT_PM")) {

    pipemon = envget ("NHPPS_PORT_PM")
    switchbd = envget("NHPPS_PORT_SB") // ":" // envget("NHPPS_NODE_SB")

    # Direct connectiong to pipemon without switchboard.
    #cpmsgs (input=pipemon, output="NHPPS$/src/iraf/guis/pipemon.gui",
    #    switchbd="", nlines=100, sleep=5, wait=yes,
    #    reporterr=no, prefix="")

    # Connection to pipemon through the switchboard.
    cpmsgs (input=switchbd, output="NHPPS$/src/iraf/guis/pipemon.gui",
	    switchbd=pipemon, nlines=100, sleep=5, wait=yes,
	    reporterr=no, prefix="")

    # Send messages to cpmsgs.gui for debugging.
    #cpmsgs (input=switchbd, output="NHPPS$/src/iraf/guis/cpmsgs.gui",
    #	switchbd=pipemon, nlines=100, sleep=5, wait=yes,
    #	reporterr=no, prefix="")

} else {

    !!obmsh $NHPPS/src/iraf/guis/pipemon.gui

}

logout
