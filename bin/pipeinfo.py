#!/bin/env python

import warnings
warnings.filterwarnings ('ignore')

import getopt
import os
import sys
from nhpps import *


# Constants
PREFIX = 'pipeadmin'
USAGE = """-f xmlFile -o fields
  field is one (or more) of the following:
    name
    errorDirectory
    rootDirectory
    inputDirectory
    obsDirectory
    pollingTime
    description
  Multiple fields may be specified. In this case, enclose them in quotes
  and separate them with spaces.
"""



def usage (usageString):
    """
    Print the usageString to STDERR.
    
    If usageString is null, print a generic message saying that some
    of the command line arguments had syntax errors in them.
    """
    command = os.path.basename (sys.argv[0])
    if (usageString):
        sys.stderr.write ('usage: %s %s\n' % (command, usageString))
    else:
        sys.stderr.write ('Error in parsing command line arguments.\n')
    return


def main ():
    xmlFile = None
    fields = None
    
    # Parse the command line args
    try:
        opts, args = getopt.getopt(sys.argv[1:], "f:o:", [])
    except:
        usage (USAGE)
        return (1)
    
    # Extract the variable values fron opts
    for flag, val in opts:
        if(flag == '-f'):
            xmlFile = val
        elif(flag == '-o'):
            fields = val.split ()
        else:
            usage (USAGE)
            return (1)
    # <-- end for
    
    if(not xmlFile or not fields):
        usage (USAGE)
        return (1)
    
    # Parse the pipeline XML file.
    dirname = os.path.dirname  (xmlFile)
    xmlname = os.path.basename (xmlFile)
    pipe = loadNHPPSxml (xmlname, path=dirname)
    
    # Extract information on fields
    result = ''
    for field in fields:
        if field == 'name':
            result += '%s ' % pipe.name
        elif field == 'errorDirectory':
            result += '%s ' % pipe.Directories.error
        elif field == 'rootDirectory':
            result += '%s ' % pipe.Directories.root
        elif field == 'inputDirectory':
            result += '%s ' % pipe.Directories.input
        elif field == 'obsDirectory':
            result += '%s ' % pipe.Directories.obs
        elif field == 'pollingTime':
            result += '%.2f' % pipe.poll
        elif field == 'description':
            result += '%s ' % pipe.Description
        else:
            result += '"" '
    print(result[:-1])
    return (0)

if (__name__ == '__main__'):
    sys.exit (main ())
