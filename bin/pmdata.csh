#!/bin/csh -f
#
# Update the osf_test output.  This may be run as a server.

set args = `getPipeApp $*`
set pipeApp = $args[1]
shift args

set dir = $NHPPS_LOGS
set server = 0
set s = 0
while ($#args > 0)
    switch ("$args[1]")
    case -s:
	shift args
        set s = $args[1]
	breaksw
    case -server:
        set server = 1
    endsw

    shift args
end

cd $dir

# Check for server possibly already running.
if ( $server && -e serverpm.pid ) exit

# Any repeated updates may be handled externally or by this script.
while (1)

    # Check whether to run update.
    if (  -e pmdata.dat ) then
	find . -name pmdata.dat -mmin -1 | grep -q pmdata.dat
	set stat = $status
    else
	set stat = 1
    endif

    #  Update.
    if (! -e pmdata.tmp) then
	if ($stat) osf_test > pmdata.tmp
	if (-e pmdata.tmp) mv pmdata.tmp pmdata.dat
    endif

    # Sleep and repeat if run as a server.
    if (! $server ) break
    sleep $s

end
