#!/bin/csh -f
#
## plrstdoc
## """"""""
## internal script for plman
## '''''''''''''''''''''''''
##
## :Manual group:    nhpps/bin
## :Manual section:  1
##
## Usage
## +++++
## ``plrstdoc file [stage]``

# Set input arguments.
set fname = $1
set stage = $2

# Determine type of processing based on the extension.
set extn = `echo $fname | sed 's/^.*\././'`
if ("$extn" == ".xml" && "$stage" != "") set extn = ".xmlstage"

# Set defaults for other installations.
if (! $?NHPPS) set NHPPS = "none"
if (! $?pipeshared) set pipeshared = "none"
if (! $?MarioSrc) set MarioSrc = "none"
if (! $?NHPPS_PIPESRC) set NHPPS_PIPESRC = "none"
if (! $?NHPPS_PIPEAPPSRC) set NHPPS_PIPEAPPSRC = "none"


# Unfortunately we can't count on SED being able to read commands from
# stdin (i.e. -f - ).  This is the case for Mac OSX.
# So we need to create the sed scripts in a temporary file.

set sedscript = `mktemp plrstdocXXXXXX`

# Extract rst documentation.
switch ($extn)


case .rst:
cat > $sedscript << EOF
# Transform paths in include directives to absolute paths.
/include::/ {
    s+\$pipeshared+$pipeshared+
    s+\$NHPPS+$NHPPS+
    s+\$MarioSrc+$MarioSrc+
    s+\$NHPPS_PIPESRC+$NHPPS_PIPESRC+
    s+\$NHPPS_PIPEAPPSRC+$NHPPS_PIPEAPPSRC+
}
EOF
sed -f $sedscript $fname; rm $sedscript
breaksw


case .xml:
cat > $sedscript << EOF
# Print pipeline and application header.

/<Pipeline/,/<\/Description>/ {

    # Extract application and pipeline name and print pipeline title.
    /<Pipeline/ {
	s/ENV(NHPPS_SYS_NAME)/$NHPPS_SYS_NAME/
	s/^.*system="\([^"]*\)".*name="\([^"]*\)".*\$/\1 \2/
	h
	s/\(.*\) \(.*\)/\2/p
	s/./"/gp
	d
    }

    # Print additional pipeline and application header.

    /<Description>/,/<\/Description>/ {
	# Transform paths in include directives to absolute paths.
	/include::/ {
	    s+\$pipeshared+$pipeshared+
	    s+\$NHPPS+$NHPPS+
	    s+\$MarioSrc+$MarioSrc+
	    s+\$NHPPS_PIPESRC+$NHPPS_PIPESRC+
	    s+\$NHPPS_PIPEAPPSRC+$NHPPS_PIPEAPPSRC+
	}

	# Print the synopsis line (the first line of the description),
	# the manual section info, and the main description header.

	/<Description>/ {
	    n
	    p
	    s/./'/gp
	    g
	    s/\(.*\) \(.*\)/\
:Manual section:  PDL\
:Manual group:  \1\
	    /
	    a\
Description\
+++++++++++\

	}

	/<\/Description>/ {
	   s/^.*\$/\
Stages\
++++++\
/
	}

	p
    }
}

# Print stage header.

/<Module/ {
    s/^.*name="\([^"]*\)".*\$/\
\1/
    p
    s/\n//
    s/./=/g
    a\

    p
    }

# Print stage description.

/<Description>/ {
    n
    p
}
EOF
sed -n -f $sedscript $fname; rm $sedscript
breaksw


case .xmlstage:
cat > $sedscript << EOF
# Extract application and pipeline.

/<Pipeline/ {
    s/^.*system="\([^"]*\)".*name="\([^"]*\)".*\$/\1 \2/
    h
}

# Generate the stage documentation.

/<Module.*name="\($stage\)"/,/<\/Module>/ {

    # Print stage title.

    /<Module/ {
        s/^.*name="\($stage\)".*\$/\1/p
	s/./"/gp
	d
    }

    # Print stage description.

    /<Description>/,/<\/Description>/ {
	# Transform paths in include directives to absolute paths.
	/include::/ {
	    s+\$pipeshared+$pipeshared+
	    s+\$NHPPS+$NHPPS+
	    s+\$MarioSrc+$MarioSrc+
	    s+\$NHPPS_PIPESRC+$NHPPS_PIPESRC+
	    s+\$NHPPS_PIPEAPPSRC+$NHPPS_PIPEAPPSRC+
	}

	# Print the synopsis line (the first line of the description),
	# the manual section info, and the main description header.

	/<Description>/ {
	    n
	    p
	    s/./'/gp
	    g
	    s/\(.*\) \(.*\)/\
:Manual section:  \2\
:Manual group:  \1\
	    /
	    a\
Description\
+++++++++++
	}

	/<\/Description>/ d

	p
    }
}
EOF
sed -n -f $sedscript $fname; rm $sedscript
breaksw


default:
cat > $sedscript << EOF
    # Extract double sharp documentation.
    /^##/!d
    /^## /s///
    /^##/s///

    # Transform paths in include directives to absolute paths.
    /include::/ {
	s+\$pipeshared+$pipeshared+
	s+\$NHPPS+$NHPPS+
	s+\$MarioSrc+$MarioSrc+
	s+\$NHPPS_PIPESRC+$NHPPS_PIPESRC+
	s+\$NHPPS_PIPEAPPSRC+$NHPPS_PIPEAPPSRC+
    }
EOF
sed -f $sedscript $fname; rm $sedscript
breaksw


endsw
