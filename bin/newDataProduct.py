#!/bin/env python

"""
Data Manager interface

    NewDataProduct -
        Creates a new entry within the DM PMAS data product table for
        the indicated dataId. A status code is returned (below) and the
        behavior of this method is dependent on whether the DM is being
        run in 'clobber' mode or not.
        
    Description 

    newDataProduct always returns a status code. The status code is either
    0 for success or
    1 for failure.

    Usage
       newDataProduct.py -d <dataId> -m <observeMJD> -c <class> [-u <url> -p <procid>]

    Communication Protocol

    Pipeline RPC

"""

import warnings
warnings.filterwarnings ('ignore')

import sys
import os

import string
import socket
import time
from pipeutil import *

VERBOSE = False

def create_product (dataid=None, obsMJD=None, dataClass=None, url='', procid=None):
    
    if (not dataid or not obsMJD or not dataClass):
        usage()
        return -1

    ret_status = 1
    #localhost = string.split (socket.gethostname (), '.')[0]
    # our preference is to try to get the IP value rather
    # than the host name. IF we fail, its likely to be a real short
    # trip (e.g. no networking running on our machine) but I suppose
    # there are cases there the pipeline may be run sans connections,
    # and so we will want to put in the hostname, at least
    localhost = socket.gethostname()
    try:
        localhost = socket.gethostbyname(localhost)
    except:
        pass

    envVars = {
        'module': 'NHPPS_MODULE_NAME',
        'dataset': 'OSF_DATASET',
        'port': 'NHPPS_PORT_DM',
        'addr': 'NHPPS_NODE_DM'
        }
    
    for key in envVars.keys():
        try:
            envVars [key] = os.environ [envVars [key]]
        except:
            print "Unable to access environment variable: (%s)" % envVars [key]
            return -1

    dm_addr = envVars ['addr']
    dm_port = (string.split (envVars ['port'], ':'))[1]

    if (not procid):
        procid = '%s_%s' % (envVars ['dataset'], envVars ['module'])
    
    command = {'COMMAND': 'newdataproduct',
               'dataid': dataid,
               'producedby': procid,
               'observemjd': obsMJD,
               'url': url,
               'class': dataClass
               }

    try:
        sock = connect (dm_addr, dm_port)
        result = execute (sock, command)
        disconnect (sock)

        if (not result):
            raise TypeError, "response is returned null" 

        # capture, and return our status
        # however, in the special case that we tried to clobber an existing process
        # and failed, lets not return a failure
        if (not result):
            raise TypeError, "response result is not returned null" 

        status = result['status']

        if (not status): 
            raise TypeError, "result status is not returned as a list, got null" 

        try:
            ret_status = int(status[0])
        except:
            raise sys.exc_type, "status had error. Is value:%s" % status
        """
        # Dont use regex here.. while it is more flexible, its slower too.
        if (ret_status and status[2:] == '_sqlite.IntegrityError:column id_str is not unique'):
            # process DID already exist. Lets not fill up the logs, this will
            # happen alot, and is expected, so lets change to 'success' to quiet
            # things down.
            ret_status = 0
        """

    except:
         # pass along the error
         raise sys.exc_type, sys.exc_value

    return ret_status


def usage (progName = ""):

    print "usage: %s <dataId> <observeMJD> <class> [-u < | url> -p <procid>]" % progName 
    return

if __name__ == '__main__':

    VERBOSE = True
    dataid = None
    obsMJD = None
    dClass = None
    url = ''
    procid = None

    maxArg = len (sys.argv)
    arg = 4
    try:
        dataid = sys.argv[1]
        obsMJD = sys.argv[2]
        dClass = sys.argv[3]
        while (arg < maxArg):
            if (sys.argv[arg] == '-u'):
                arg += 1
                if (arg < maxArg):
                    if (sys.argv[arg] != '-p'):
                        url = sys.argv[arg]
                    else:
                        arg += 1
                        procid = sys.argv[arg]
            elif (sys.argv[arg] == '-p'):
                arg += 1
                procid = sys.argv[arg]
            arg += 1
    except:
       usage()
       sys.exit(-1)

    # now create the product, and return the code
    # of this method (note: this will be opposite of
    # usual exit code value) 
    exitCode = create_product (dataid, obsMJD, dClass, url, procid)
    sys.exit( exitCode )
