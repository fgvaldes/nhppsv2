# NOAO High-Performance Pipeline System Configuration File
#
# The NHPPS requires many environment variables to be set specific
# to your particular system and needs.  Additionally, several standard
# environment variables must be modified, such as the PYTHONPATH in order
# to take advantage of the nhpps package.

# THe NHPPS environment variable defines the root directory for the
# NHPPS package; e.g.
#
#      $HOME/nhpps

# The NHPPS release in use
setenv NHPPS_VERSION 2.0

# Most NHPPS scripts used /bin/env.  If this is not available on your system
# it may be at /usr/bin and you will need to make a link.

if (! -e /bin/env) then
    if (-e /usr/bin/env) then
	echo "ERROR: NHPPS requires /usr/bin/env to be linked to /bin/env"
    else
	echo "ERROR: NHPPS requires /bin/env to be installed"
    endif
    exit 1
endif

if (! $?NHPPS_HOST) setenv NHPPS_HOST `hostname -s`

if (! $?path) set path = ()
if (! $?cdpath) set cdpath = ()
if ($?home) then
set -f path = ($path . ~/bin)
set -f cdpath = (~ $cdpath)
endif
set -f path = ($path /bin /usr/bin /usr/local/bin)

if (! $?NHPPS) then
    echo NHPPS environment variable must be defined
    exit (1)
endif
set -f path = ($NHPPS/bin $NHPPS/extern/bin $path)
set -l cdpath = ($cdpath `dirname $NHPPS`)

# PipelineMode defines an execution framework.  Note that this may be
# a combination for frameworks such as NHPPS pipeline services wrapped
# for Grid execution.

if (! $?PipelineMode) setenv PipelineMode NHPPS

# NHPPS_SYS_NAME is the pipeline application.  This is used to allow
# multiple pipeline applications.  This name is used in many places
# including pipeline application specific directories.

if (! $?NHPPS_SYS_NAME) setenv NHPPS_SYS_NAME BCW

# NHPPS_DATA is the directory where processing data is managed.  Below
# this are application directories (i.e. $NHPPS_SYS_NAME) and below that
# pipeline root directories (.e.g. ${NHPPS_SYS_NAME}_${NHPPS_SUBPIPE_NAME}.
# These directories may be part of a "shared" or "local" file system on
# each node.

if (! $?NHPPS_FSTYPE) setenv NHPPS_FSTYPE shared
if (! $?NHPPS_DATA) then
    setenv NHPPS_DATA $NHPPS/examples/data
    if ($?outputData) then
	if (`echo $outputData | wc -c` > 80) then
	    set short = `dirname $outputData`
	    set short = `dirname $short`
	    echo $short
	    set short = `mktemp -d ${short}/NHPPS_XXXXXX`
	    setenv NHPPS_DATA $short
	    echo $NHPPS_DATA
	    if (! -e $outputData) mkdir -p $outputData
	    if (! -e $outputData/nhpps) ln -s $short nhpps
	    ls -l $outputData/nhpps
	else
	    setenv NHPPS_DATA $outputData
	endif
    else if ($?HOME) then
        if (-e $HOME/NHPPS_DATA) setenv NHPPS_DATA $HOME/NHPPS_DATA
    endif
endif
if (-l $NHPPS_DATA) setenv NHPPS_DATA `readlink -f $NHPPS_DATA`
if (! $?NHPPS_DATAAPP) setenv NHPPS_DATAAPP $NHPPS_DATA/$NHPPS_SYS_NAME
if (! -e $NHPPS_DATAAPP) mkdir -p $NHPPS_DATAAPP
set -f cdpath = ($cdpath $NHPPS_DATA $NHPPS_DATAAPP)

# NHPPS_TRIG is a potential directory for trigger files.  This variable
# can be used in the global, application, or pipeline PDL (xml) files
# as part of defining the trig directory attribute(s).  When multiple
# nodes are used the directory should either be a local directory
# or host dependent.  The code below ensures this by appending
# the host name.

if (! $?NHPPS_TRIG) then
    if ($NHPPS_FSTYPE == "shared") then
        setenv NHPPS_TRIG $NHPPS_DATAAPP
    else
        setenv NHPPS_TRIG '${Pipeline.Directories.root}'
    endif
endif
if ($NHPPS_FSTYPE == "shared") then
    if (`basename $NHPPS_TRIG` != $NHPPS_HOST) setenv NHPPS_TRIG "$NHPPS_TRIG/$NHPPS_HOST"
    if (! -e $NHPPS_TRIG) mkdir -p $NHPPS_TRIG
endif

# NHPPS_OUTPUT is a root directory for final data products..  This variable
# can be used in the global, application, or pipeline PDL (xml) files
# as part of defining the output directory attribute(s).  The default
# is simply the same as the working root directory.  It is particularly
# useful for specifying an output that is on a different partition and,
# even more useful, a shared file system in a mixed mode where both
# a shared and local file systems are used.

if (! $?NHPPS_OUTPUT) setenv NHPPS_OUTPUT '${Pipeline.Directories.root}'

# NHPPS_LOGS should contain a directory into which nhpps log files are
# written.

if (! $?NHPPS_LOGS) then
    setenv NHPPS_LOGS $NHPPS/examples/logs
    if ($?NHPPS_DATA) then
	setenv NHPPS_LOGS $NHPPS_DATA/logs
    endif
    if ($?outputData) then
        setenv NHPPS_LOGS $outputData
    else if ($?HOME) then
        if (-e $HOME/NHPPS_LOGS) setenv NHPPS_LOGS $HOME/NHPPS_LOGS
    endif
endif
if ($NHPPS_FSTYPE == "shared") then
    if (`basename $NHPPS_LOGS` != $NHPPS_HOST) setenv NHPPS_LOGS $NHPPS_LOGS/$NHPPS_HOST
endif
if (! -e $NHPPS_LOGS) mkdir -p $NHPPS_LOGS
if (! $?NHPPS_PROC_LOGS) setenv NHPPS_PROC_LOGS datadir
set -f cdpath = ($cdpath `dirname $NHPPS_LOGS` $NHPPS_LOGS)

# NHPPS_PIPESRC is the root directory for the various Pipeline Application
# source directories...  The specific Pipeline Application source is
# given by NHPPS_PIPEAPPSRC.

if (! $?NHPPS_PIPESRC) setenv NHPPS_PIPESRC $NHPPS/examples/src
if (! $?NHPPS_PIPEAPPSRC) setenv NHPPS_PIPEAPPSRC $NHPPS_PIPESRC/$NHPPS_SYS_NAME
if (-e $NHPPS_PIPESRC/BIN) set -f path = ($path $NHPPS_PIPESRC/BIN)
if (-e $NHPPS_PIPEAPPSRC/${NHPPS_SYS_NAME}BIN) set -f path = ($path $NHPPS_PIPEAPPSRC/${NHPPS_SYS_NAME}BIN)
set -f cdpath = ($cdpath $NHPPS_PIPESRC $NHPPS_PIPEAPPSRC)

# Set the architecture.
if ($?ARCH) then
    setenv NHPPS_ARCH $ARCH
endif
if (! $?NHPPS_ARCH) then
    setenv NHPPS_ARCH `getArch`
    setenv ARCH $NHPPS_ARCH
endif

# NHPPS_MIN_DISK specifies the minimum number of 1K blocks (on disk) which must
# be available in order for a node to be a candidate for processing a dataset.

if (! $?NHPPS_MIN_DISK) setenv NHPPS_MIN_DISK 2000000

# Primary node for servers.

if (! $?ServerNode) setenv ServerNode $NHPPS_HOST

# Setup default ports.
if (! $?PortStart) setenv PortStart 7010
@ port = $PortStart

# Directory Server (required component):
#    NHPPS_NODE_DS contains the name of the node which runs the DS
#    NHPPS_PORT_DS contains the port number on which the DS runs

if (! $?NHPPS_NODE_DS) setenv NHPPS_NODE_DS $ServerNode
if (! $?NHPPS_PORT_DS) setenv NHPPS_PORT_DS $port
@ port++

# Node Manager (required component):
# NHPPS_PORT_NM contains the port on which the Node Managers will operate.

if (! $?NHPPS_PORT_NM) setenv NHPPS_PORT_NM $port
@ port++

# Message Monitor (semi-optional component):
#    NHPPS_NODE_MM contains the name of the node which runs the MM
#    NHPPS_PORT_MM contains the port on which the MM runs

if (! $?NHPPS_NODE_MM) setenv NHPPS_NODE_MM $ServerNode
if (! $?NHPPS_PORT_MM) setenv NHPPS_PORT_MM $port
@ port++

# Pipeline Scheduling Agent (optional component):
#    NHPPS_NODE_PS contains the name of the node which runs the PS
#    NHPPS_PORT_PS contains the port number on which the PS runs

if (! $?NHPPS_NODE_PS) setenv NHPPS_NODE_PS $ServerNode
if (! $?NHPPS_PORT_PS) setenv NHPPS_PORT_PS $port
@ port++

# Data Manager (optional component):
#    NHPPS_NODE_DM contains the name of the node which runs the DM
#    NHPPS_PORT_DM contains the port on which the DM runs

if (! $?NHPPS_NODE_DM) setenv NHPPS_NODE_DM $NHPPS_HOST
if (! $?NHPPS_PORT_DM) setenv NHPPS_PORT_DM $port
@ port++

# Calibration Manager (optional component):
#    NHPPS_NODE_CM contains the name of the node which runs the CM
#    NHPPS_PORT_CM contains the port on which the CM runs

if (! $?NHPPS_NODE_CM) setenv NHPPS_NODE_CM $ServerNode
if (! $?NHPPS_PORT_CM) setenv NHPPS_PORT_CM $port
@ port++

# Switchboard Server (optional component):
#    NHPPS_NODE_SB contains the name of the node which runs the SB
#    NHPPS_PORT_SB contains the port on which the SB listens for messages.
#    NHPPS_PORT_PM contains the port on which the SB writes messages.

if (! $?NHPPS_NODE_SB) setenv NHPPS_NODE_SB    $ServerNode
if (! $?NHPPS_PORT_SB) setenv NHPPS_PORT_SB    $port
@ port++

# Pipeline Monitor (optional component):
if (! $?NHPPS_PM_SHOW) setenv NHPPS_PM_SHOW ""
if (! $?NHPPS_PM_HIDE) setenv NHPPS_PM_HIDE ""
if (! $?NHPPS_PORT_PM) setenv NHPPS_PORT_PM    $port
@ port++

setenv PortStart $port

# Each computer in a processing cluster is known as a 'node'. The first part of
# the hostname is its node-name (the part before the first '.').

setenv NHPPS_NODE_NAME $NHPPS_HOST

# Some of the NHPPS_PORT_xx environment variables need to be in the IRAF format
# of: inet:xyz where xyz is a port number. We update those variables here.

if ($?NHPPS_PORT_DM) then
    echo $NHPPS_PORT_DM | grep -q 'inet:'
    if ($status) then
	setenv NHPPS_PORT_DM inet:$NHPPS_PORT_DM
    endif
endif

if ($?NHPPS_PORT_CM) then
    echo $NHPPS_PORT_CM | grep -q 'inet:'
    if ($status) then
	setenv NHPPS_PORT_CM inet:$NHPPS_PORT_CM
    endif
endif

if ($?NHPPS_PORT_SB) then
    echo $NHPPS_PORT_SB | grep -q 'inet:'
    if ($status) then
	setenv NHPPS_PORT_SB inet:$NHPPS_PORT_SB
    endif
endif

# Update the PLMANPATH to include the nhpps package documentation.

if (! $?PLMANPATH) then
    setenv PLMANPATH ${NHPPS}/doc
else
    setenv PLMANPATH ${NHPPS}/doc:$PLMANPATH
endif


# Updating the MANPATH to include the nhpps package documentation.

if (! $?MANPATH) then
    setenv MANPATH ${NHPPS}/doc:`manpath`
else
    echo $MANPATH | grep -q ${NHPPS}/doc
    if ($status) then
	setenv MANPATH ${NHPPS}/doc:`manpath`
    endif
endif

# PYTHON

# Updating the PYTHONPATH variable in order to include the python sources in
# the search path in Python.

if (! $?PYTHONPATH) then
    setenv PYTHONPATH ${NHPPS}/src/python:${NHPPS}/extern/site-packages
else
    echo $PYTHONPATH | grep -q ${NHPPS}/src/python
    if ($status) then
	setenv PYTHONPATH ${NHPPS}/src/python:${PYTHONPATH}
    endif
    echo $PYTHONPATH | grep -q ${NHPPS}/extern/site-packages
    if ($status) then
	setenv PYTHONPATH ${NHPPS}/extern/site-packages:$PYTHONPATH
    endif
endif

# Make PYTHON output unbuffered.
setenv PYTHONUNBUFFERED yes

# PYRO
if (! $?NHPPS_PORT_PYRO) setenv NHPPS_PORT_PYRO 9001
if (! $?NHPPS_NODE_PYRO) setenv NHPPS_NODE_PYRO $ServerNode
if (! $?PYRO_STORAGE) setenv PYRO_STORAGE	$NHPPS_LOGS
alias nsc "nsc -h $NHPPS_NODE_PYRO -p $NHPPS_PORT_PYRO"

# IRAF
if ($?iraf) then
    if (! $?NHPPS_IRAF) setenv NHPPS_IRAF $NHPPS/iraf
    if (! $?IRAFARCH) setenv IRAFARCH $NHPPS_ARCH
    if (! $?arch) setenv arch .$IRAFARCH
    if (! $?host) setenv host $iraf/unix/
    if (! $?tmp) setenv tmp /tmp/
    if (! $?hlib) setenv hlib $NHPPS_IRAF/
    if (! $?uparm) setenv uparm $NHPPS_IRAF/uparm/
    if ($NHPPS_FSTYPE == "shared") then
	if (`basename $uparm` != $NHPPS_HOST) setenv uparm $NHPPS_DATA/uparm/$NHPPS_HOST/
    endif
    if (! -e $uparm) mkdir -p $uparm
    if (! $?irafextern) setenv irafextern $iraf/extern/
    if (! $?pipes) setenv pipes $tmp
    if ($NHPPS_FSTYPE == "shared") then
	#if ($?irafhnt) unsetenv irafhnt
    else
	if (! $?irafhnt) setenv irafhnt `irafhnt`
    endif
    if ($?irafhnt) then
	if (! $?RSH) setenv RSH ssh
	if (! $?KSRSH) setenv KSRSH $RSH
	if (! $?KS_RETRY) setenv KS_RETRY 3600
	if (! $?KS_NO_RETRY) setenv KS_NO_RETRY 1
    endif
    limit stacksize unlimited

    # NHPPS IRAF
    alias mknhppsiraf "mkpkg \!:* NHPPS_IRAF=$NHPPS_IRAF"
    set path = ($NHPPS_IRAF/bin $path)

endif

# Source anything from the pipeline sources.  Note that file may then
# source something like $NHPPS_PIPEAPPSRC/cshrc.

if (-e $NHPPS_PIPESRC/cshrc) then
    if ($?SSH_TTY) then
	if ($SSH_TTY != "--nopipeapp") source $NHPPS_PIPESRC/cshrc
    endif
endif
