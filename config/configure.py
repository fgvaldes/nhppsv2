#!/bin/env python
# 
# NOAO High-Performance Pipeline System configuration script.
# Authour: F. Pierfederici <fpierfed@noao.edu>
# 

import os
import select
import socket
import sys
import threading
import time

from textwrap import wrap



# Constants
BCPORT = 50000            # Broadcast port
REQUIRED_VARS = ['NHPPS', 'NHPPS_VERSION', 'PYTHONPATH', 'NHPPS_PIPESRC', ]
OPTIONAL_VARS = ['NHPPS_LOGS', ]
PYTHON_MIN_VERSION = (2, 3)        # Major, minor version numbers
PYRO_MIN_VERSION = (3, 4)          # Major, minor version numbers
TEXT_WIDTH = 54                    # Width of text messages


class Broadcaster (threading.Thread):
    def __init__ (self):
        super (Broadcaster, self).__init__ ()
        
        self.s = socket.socket (socket.AF_INET, socket.SOCK_DGRAM)
        self.s.bind (('', 0))
        self.s.setsockopt (socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        
        self.done = False
        return
    
    def run (self):
        while (not self.done):
            data = repr (time.time ()) + '\n'
            self.s.sendto (data, ('<broadcast>', BCPORT))
            time.sleep(1)
        return
    
    def join (self, timeout=0):
        self.done = True
        super (Broadcaster, self).join (timeout)
        return


def printWrapped (s, indent=0):
    global TEXT_WIDTH
    lines = wrap (s, TEXT_WIDTH)
    print (lines[0])
    for line in lines[1:]:
        print (' ' * indent + line)
    return


def which (cmdName):
    """
    Python implementation of the UNIX which command.
    """
    try:
        PATH = os.environ['PATH'].split (':')
    except:
        # Standard PATH on UNIX
        PATH = ('/bin', 
                '/sbin', 
                '/usr/bin', 
                '/usr/sbin', 
                '/usr/local/bin')
    
    for dir in PATH:
        cmdPath = os.path.join (dir, cmdName)
        if (os.path.isfile (cmdPath)):
            return (cmdPath)
    return (None)


def banner ():
    print ('*-----------------------------------------------------*')
    print ('*              NHPPS Configuration Script             *')
    print ('*-----------------------------------------------------*')
    return


def summaryBanner ():
    print ('*-----------------------------------------------------*')
    print ('*              NHPPS Configuration Summary            *')
    print ('*-----------------------------------------------------*')
    return


def testEnv ():
    sane = True
    # Test for the required env vars
    for v in REQUIRED_VARS:
        try:
            vv = os.environ[v]
        except:
            sane = False
            printWrapped (' **** The required environment variable %s is NOT defined.' \
                   % (v), 6)
    # Test for the recommended env vars
    missing = []
    for v in OPTIONAL_VARS:
        try:
            vv = os.environ[v]
        except:
            printWrapped (' **** The recommended environment variable %s is NOT defined.' \
                   % (v), 6)
            missing.append (v)
    return (sane, missing)


def testSocket ():
    """
    Test the socket's ability do do a UDP broadcast (copied from the
    Demo/socket examples in the standard Python source distribution).
    """
    # Spawn a thread that doe sthe broadcasting
    b = Broadcaster ()
    b.start ()
    
    # Try and get the UDP broadcasted message
    s = socket.socket (socket.AF_INET, socket.SOCK_DGRAM)
    s.bind (('', BCPORT))
    # Is there data?
    (i, o, e) = select.select ([s], [], [], 1)    # 1 sec timeout
    if (i):
        data, wherefrom = s.recvfrom (1500, 0)
    else:
        data = None
    
    # Kill the Broadcaster
    b.join ()
    
    if (not data):
        # Add Pyro's PYRO_NS_HOSTNAME var to REQUIRED_VARS
        REQUIRED_VARS.append ('PYRO_NS_HOSTNAME')
        return (False)
    return (True)


def testHasNameServer ():
    ns = which ('ns')
    if (not ns):
        return (False)
    return (True)


def testNameServerRunning ():
    import Pyro.naming
    import Pyro.core
    
    Pyro.core.initClient(0)
    
    running = True
    try:
        host = os.environ['PYRO_NS_HOSTNAME']
    except:
        host = None
    if (host):
        try:
            port = int (os.environ['PYRO_NS_PORT'])
        except:
            port = None
    
    if (host):
        if (port != None):
            try:
                ns = Pyro.naming.NameServerLocator ().getNS (host=host,
                                                             port=port)
            except:
                running = False
        else:
            try:
                ns = Pyro.naming.NameServerLocator ().getNS (host=host)
            except:
                running = False
    else:
        try:
            ns = Pyro.naming.NameServerLocator ().getNS ()
        except:
            running = False
    return (running)


def testPythonVersion ():
    (major, minor, micro, releaselevel, serial) = sys.version_info
    requiredMajor = PYTHON_MIN_VERSION[0]
    requiredMinor = PYTHON_MIN_VERSION[1]
    
    if (major < requiredMajor):
        sane = False
    elif (major == requiredMajor and minor < requiredMinor):
        sane = False
    else:
        sane = True
    return (sane)


def testPyroVersion ():
    try:
        import Pyro
    except:
        return (False)
    
    (major, minor) = Pyro.constants.VERSION.split ('.')
    major = int (major)
    minor = int (minor)
    requiredMajor = PYRO_MIN_VERSION[0]
    requiredMinor = PYRO_MIN_VERSION[1]
    
    if (major < requiredMajor):
        sane = False
    elif (major == requiredMajor and minor < requiredMinor):
        sane = False
    else:
        sane = True
    return (sane)


def testStartNameServer ():
    # Start the Name Server
    err = os.system ('ns >& /dev/null &')
    time.sleep (1)
    if (not err):
        return (True)
    return (False)


def main ():
    # Print the banner
    banner ()
    
    # Test Python's version (and configuration)
    printWrapped ('Checking Python installation')
    if (testPythonVersion ()):
        printWrapped (' * Python installation looks OK.', 3)
    else:
        printWrapped (' *** Python installation does NOT look OK.', 5)
        printWrapped (' *** NHPPS requires Python %d.%d or greater.' \
               % (PYTHON_MIN_VERSION[0], PYTHON_MIN_VERSION[1]), 5)
        return (1)
    
    # Test Pyro's version (and configuration)
    printWrapped ('Checking Pyro installation')
    if (testPyroVersion ()):
        printWrapped (' * Pyro installation looks OK.', 3)
    else:
        printWrapped (' *** Pyro installation does NOT look OK.', 5)
        printWrapped (' *** NHPPS requires Pyro %d.%d or greater.' \
               % (PYRO_MIN_VERSION[0], PYRO_MIN_VERSION[1]), 5)
        return (2)
    
    # Test for Pyro's Name Server
    printWrapped ('Testing (Pyro) Name Server availability')
    hasNS = False
    if (testHasNameServer ()):
        printWrapped (' * (Pyro) Name Server is available.', 3)
        hasNS = True
    else:
        printWrapped (' *** (Pyro) Name Server is NOT available.', 5)
        return (3)
    
    # Test the socket's broadcase capabilities
    printWrapped ('Testing UDP broadcast availability')
    hasBroadcast = False
    if (testSocket ()):
        printWrapped (' * UDP broadcast is available.')
        hasBroadcast = True
    else:
        printWrapped (' *** UDP broadcast is NOT available.', 5)
    
    # Test the environment
    printWrapped ('Checking if environment is sane')
    (isSane, missingVars) = testEnv ()
    if (isSane):
        printWrapped (' * Environment looks OK.', 3)
    else:
        printWrapped (' *** Environment does NOT look OK.', 5)
        return (4)
    
    # Check is the Name Server is running
    printWrapped ('Checking if (Pyro) Name Server is running')
    isRunning = False
    if (testNameServerRunning ()):
        printWrapped (' * (Pyro) Name Server is running.', 3)
        isRunning = True
    else:
        printWrapped (' *** (Pyro) Name Server is either NOT running or ' +
               'CANNOT be discovered.', 5)
    
    # Try to connect to the Name Server (starting it if necessary).
    if (not isRunning):
        printWrapped ('Checking if (Pyro) Name Server can be started')
        canBeStarted = False
        if (testStartNameServer ()):
            printWrapped (' * (Pyro) Name Server CAN be started.', 3)
            canBeStarted = True
            isRunning = True
        else:
            printWrapped (' *** (Pyro) Name Server CANNOT be started.', 5)
        
        if (canBeStarted):
            printWrapped ('Checking if (Pyro) Name Server can be discovered')
            canBeDiscovered = False
            if (testNameServerRunning ()):
                printWrapped (' * (Pyro) Name Server CAN be discovered.', 3)
                canBeDiscovered = True
            else:
                printWrapped (' *** (Pyro) Name Server CANNOT be discovered.', 5)
    else:
        canBeStarted = True
        canBeDiscovered = True
    
    # Summarise the results
    summaryBanner ()
    
    # If we are here, no serious problems were found
    printWrapped ('NHPPS appears to be correctly installed.')
    printWrapped ('The following recommended variables should be defined:')
    for v in missingVars:
        printWrapped ('  %s' % (v), 2)
    printWrapped ('Network supports UDP broadcast: %s' % (hasBroadcast))
    if (hasBroadcast):
        printWrapped ('  No need to define $PYRO_NS_HOSTNAME')
    else:
        printWrapped ('  You need to define $PYRO_NS_HOSTNAME')
        if ('PYRO_NS_HOSTNAME' in os.environ.keys ()):
            printWrapped ('  You already defined $PYRO_NS_HOSTNAME')
    printWrapped ('The Name Server can be started: %s' % (canBeStarted))
    printWrapped ('The Name Server can be discovered: %s' % (canBeDiscovered))
    printWrapped ('The Name Server is running: %s' % (isRunning))
    # Exit
    return (0)




if (__name__ == '__main__'):
    sys.exit (main ())
