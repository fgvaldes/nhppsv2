## =======
## BCW.CSH
## =======
## ------------------------------------------
## set environment to run the BCW application
## ------------------------------------------
## 
## :Manual section:  NHPPS/BCW
## :Manual group:    Files
## 
## Description
## ===========
## This file is ``sourced`` to set the NHPPS environment.
## The only required environment variable is ``$NHPPS`` which gives the
## path to the the NHPPS source.

# Set the system name.
setenv NHPPS_SYS_NAME	BCW

# To work with multiple nodes you must set ServerNode and PROCNODES.
if (! $?NHPPS_HOST)	  setenv NHPPS_HOST 	`hostname -s`
if (! $?ServerNode)	  setenv ServerNode	$NHPPS_HOST
if (! $?PROCNODES)	  setenv PROCNODES	$NHPPS_HOST

source	$NHPPS/config/Setup.csh
if (-e $NHPPS_PIPESRC/cshrc) source $NHPPS_PIPESRC/cshrc
