from nhpps.dtd.dtd import *
from BlackBoard import *
from TriggerMgr import *
from ActionMgr import *

# Define STATES of the execution...
MM_PROCESSING = 'mm_processing'
MM_COMPLETE   = 'mm_complete'

def timedeltaSeconds (tdObj):
    
    """
    returns the total number of seconds represented by a timedelta
    object (the maxExec.time value for instance.
    """
    
    if isinstance (tdObj, datetime.timedelta):
        daySeconds = (tdObj.days*60*60*24)
        return tdObj.seconds + daySeconds
    else:
        return None
    pass


class ModuleMgr (object):
    
    """
    Module Manager (ModuleMgr)
    """
    
    PREFIX = 'ModuleMgr' # STDERR message prefix
    
    def __init__ (self, pipeline, modIndex, parent, pid=None, instID=0):
        
        """
        Standard constructor/initializer.
        """
        
        self.parent      = parent
        self.blackboard  = parent.bb
        self.log         = parent.log
        self.pipe        = pipeline.name
        self.module      = pipeline.Modules [modIndex]
        self.name        = self.module.name
        self.minDisk     = self.module.MinDisk.space
        self.maxTime     = timedeltaSeconds (self.module.MaxExec.time)
        self.instID      = instID
        self.pid         = pid
        self.stop        = False
        
        # Create an entry in the PSTAT BB
        # and open the LOG file for writing
        self.LOG = self.blackboard.pstatCreate (
            process = self.name,
            pid     = self.pid,
            status  = 'starting',
            instID  = self.instID)
        
        self.eventEnv = {}
        
        # Setup managers for triggers and actions
        self._setup_TriggerMgrs ()
        self._setup_ActionMgrs ()
        
        return
    
    
    def getModuleName (self):
        
        return self.name
    
    
    def getModuleDescription (self):
        
        return self.module.description
    
    
    def shutDown (self):
        
        """
        Kill any process we might have spawn.
        """
        
        self.preProcActionManager.killActiveProcess ()
        self.procActionManager.killActiveProcess ()
        self.postProcActionManager.killActiveProcess ()
        
        return
    
    
    def run (self):
        
        self._setStatus_Idle ()
        
        while True:
            if self._readyForProcessing ():
                #yield MM_PROCESSING
                # Execute the pre-proc, or Setup, actions keeping the
                # relevant, i.e., final, exit code.
                self.preProcActionManager.prepareToExecute (env=self.eventEnv)
                exitCode = None
                try:
                    while True:
                        exitCode = self.preProcActionManager.execute ()
                        pass
                    pass
                except StopIteration:
                    # All the actions have been executed, yield
                    pass
                yield MM_PROCESSING
                
                # Execute the proc, or Primary, action, keeping its
                # exit code. Note that the execution of the Primary
                # action is constrained by the values set in the
                # maxTime and minDisk elements.
                if self._enoughDiskSpace ():
                    # We have enough disk space to execute the 'ProcAction'
                    self.procActionManager.prepareToExecute (
                        env      = self.eventEnv,
                        exitCode = exitCode,
                        maxTime  = self.maxTime)
                    exitCode = None
                    try:
                        while True:
                            exitCode = self.procActionManager.execute ()
                            yield MM_PROCESSING
                            pass
                        pass
                    except StopIteration:
                        # All the actions have been executed
                        pass
                    except TimeOut:
                        # Execution exceeded the specified time limit
                        exitCode = self.module.MaxExec.status
                        pass
                    pass
                else:
                    # Minimum available disk space requirement not meet
                    exitCode = self.module.MinDisk.status
                    pass
                yield MM_PROCESSING
                
                # Execute the post-proc, or Cleanup, actions based on
                # the value of the exit code from the proc stage.
                self.postProcActionManager.prepareToExecute (
                    env=self.eventEnv, exitCode=exitCode)
                try:
                    while True:
                        exitCode = self.postProcActionManager.execute ()
                        pass
                    pass
                except StopIteration:
                    # All the actions have been executed
                    pass
                yield MM_PROCESSING
                
                # Perform module processing cleanup actions
                self._finishProcessing ()
                pass
            
            # We may now go to sleep because we either:
            # 1) Didn't have a dataset to process, or
            # 2) just finished processing a dataset
            yield MM_COMPLETE
            pass
        
        return # Only happens when self.stop is True...
    
    
    def _checkTriggers (self):
        
        # Loop over all the TriggerManager instances
        # to check if there is any work to do.
        for triggerMgr in self.triggerManagers:
            event = triggerMgr.checkRequirements ()
            if event:
                return event
            pass
        
        return None
    
    
    def _setStatus_Idle (self):
        
        self.blackboard.pstatUpdate (
            process = self.name,
            pid     = self.pid,
            status  = 'idle',
            instID  = self.instID)
        
        return
    
    
    def _setStatus_Processing (self):
        
        self.blackboard.pstatUpdate (
            process = self.name,
            pid     = self.pid,
            status  = 'processing',
            instID  = self.instID)
        
        return
    
    
    def _readyForProcessing (self):
        
        """
        Provides basic setup functionality when there is a dataset
        which is ready for the module to process. If the setup actions
        are performed, then the function returns True, specifying that
        the module will branch into the execution mode, while a return
        value of False specifies that the module will go to sleep.
        """
        
        # Check if there is any work to do.
        self.eventEnv = self._checkTriggers ()
        
        # If self.eventEnv, then the required triggering events occurred.
        if self.eventEnv:
            
            if self.log:
                self.startTime = time.time ()
                pass
            
            # Update the status of the module to be active
            self._setStatus_Processing ()
            
            # Setup the environment
            self.eventEnv['NHPPS_MODULE_PID'] = str (self.pid)
            self.eventEnv['NHPPS_MODULE_NAME'] = str (self.name)
            
            # Return True so that the generator function will
            # move into the processing loop.
            return True
        
        # If we are here, there were no datasets ready for processing.
        # So, return False to tell the generator function to go to
        # sleep for a period of time.
        return False
    
    
    def _finishProcessing (self):
        
        # Print out timing information
        if self.log:
            self.log.write ('TIME\t%s\t%.02f\n' % (
                self.name, (time.time () - self.startTime)))
            pass
        self._setStatus_Idle ()
        
        return
    
    
    def _enoughDiskSpace (self):
        
        if self.minDisk:
            try:
                dir = self.parent.pipeline.Directories.root
                freespace = get_freespace (dir, self.parent.system)
                if self.minDisk > freespace:
                    exitCode = self.module.MinDisk.status
                    return False
                pass
            except:
                pass
            pass
        
        return True
    
    
    def _setStatus_Processing (self):
        
        self.blackboard.pstatUpdate (
            process = self.name,
            pid     = self.pid,
            status  = 'processing',
            instID  = self.instID)
        
        return
    
    
    def _setStatus_Idle (self):
        
        self.blackboard.pstatUpdate (
            process = self.name,
            pid     = self.pid,
            status  = 'idle',
            instID  = self.instID)
        
        return
    
    
    def _setup_TriggerMgrs (self):
        
        """
        Creates the TriggerMgr objects which will monitor the system
        for the events which will cause the module to begin execution.
        """
        
        self.triggerManagers = [
            TriggerMgr (trigger, self.name, self.blackboard)
            for trigger in self.module.Triggers]
        
        return
    
    
    def _setup_ActionMgrs (self):
        
        """
        Creates the ActionMgr objects which will execute the modules
        actions when needed.
        """
        
        self.preProcActionManager  = PreProcActionMgr  (
            blackboard=self.blackboard, LOG=self.LOG)
        self.procActionManager     = ProcActionMgr     (
            blackboard=self.blackboard, LOG=self.LOG)
        self.postProcActionManager = PostProcActionMgr (
            blackboard=self.blackboard, LOG=self.LOG)
        
        self.preProcActionManager.addActions  (self.module.PreProcAction)
        self.procActionManager.addActions     (self.module.ProcAction)
        self.postProcActionManager.addActions (self.module.PostProcActions)
        
        return
    
    pass
