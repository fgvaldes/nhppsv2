from nhpps.dtd.dtd import *
from pipeutil import *
from BlackBoard import *

# Import the Native Actions plugin
import PlugIns
import sys
import datetime

try:
    import NHPPS_PlugIns
except:
    pass


# Constants
PREFIX = 'ActionMgr'                    # STDERR message prefix

DEFAULT = 'default' # exitCode in XML file which represents default action
                    # to perform if the actual exit code is not one
                    # we handle explicitly.

# The convention for native methods names is to prepend 'native' to the 
# XML tag name (e.g. OSFUpdate -> nativeOSFUpdate).
NATIVE_PREFIX = 'native'

ADDON_PREFIX = 'addon'

NOUPDATE = ['OSFUpdate',
	    'OSFWait',
	    'OSFClose',
	    'OSFUpdateParent',
	    'OSFClose',
	    'OSFOpen',
	    'OSFConditRemove',
	    'OSFFinal',
	    'RenameTrigger',
	    'UpdateTrigger',
	    'RemoveTrigger',
	    'Dummy',
	    'NewProcess',
	    'FinishProcess']

# FIXME: Handle the case where an action does not have a command!

class TimeOut (Exception):
    """
    This is raised when the ActionMgr takes too long to execute
    in order to break out of the generator while True loop.
    """
    pass

def getExeArgs (command):
    exe = command.getName ()
    if hasattr (command, 'argv'):
        args = command.argv.split ()
	if (exe == 'Foreign' or
	    exe == 'StartPipe' or
	    exe == 'CallPipe' or
	    exe == 'WaitPipe' or
	    exe == 'DonePipe'):

            cmd = ''
            for arg in args:
                cmd += arg + ' '
                cmd = cmd.replace ('"', '\\"')
            args = cmd
    else:
        args = None
    return (exe, args)

class ActionMgr (object):
    
    """
    Action Manager

    self.gCommand contains the global processing commands. They are
    NOT included in self.command and therefore the two must be joined
    during execution of the pipeline.
    """
    
    def __init__ (self, blackboard=None, LOG=None, verbose=False):
        """
        self.command is a dictionary of exit code -> commands entries. 
        The dictionary keys are strings, either the string representaiton
        of an integer, or 'default' to represent a 'catch-all' default
        set of commands to execute if the exit code does not match any
        which are explicitly specified.
            [command, [arg1, arg2, ...]], [command, [arg1, arg2, ...]]
        
        If the action does not have an exit code associated to it (as in
        the case of pre-proc and processing actions), self.command will 
        simply be an array of
            [command, [arg1, arg2, ...]], [command, [arg1, arg2, ...]]
        
        The same holds for self.gCommand. The difference between 
        self.command and self.gCommand is that the latter holds 
        information about the global (i.e. pipeline wide) actions.
        """
        self.blackboard = blackboard
        self.verbose = verbose
        self.LOG = LOG
        
        self.command = {}
        self.gCommand = {}
        
        self.commandQueue = None
        self.status = None
        
        self.activeProcess = None
        self.activeProcessGID = None
        return
    
    def addActions (self, actions):
        pass
    
    # TODO: Implement a dispatcher method insead of individual pass-throughs
    
    def osfUpdate (self, dataset, module, stage, flag):
        """
        Just a pass-through method.
        """
        return (self.blackboard.osfUpdate(dataset, module, stage, flag))
    
    def osfCall (self, dataset, module, flag):
        """
        Just a pass-through method.
        """
        return (self.blackboard.osfCall(dataset, module, flag))
    
    def osfWait (self, dataset, module, flag):
        """
        Just a pass-through method.
        """
        return (self.blackboard.osfWait(dataset, module, flag))
    
    def osfOpen (self, dataset):
        """
        Just a pass-through method.
        """
        return (self.blackboard.osfOpen(dataset))
    
    def osfClose (self, dataset):
        """
        Just a pass-through method.
        """
        return (self.blackboard.osfClose(dataset))

    def osfConditRemove (self, dataset):
        """
        Just a pass-through method.
        """
        return (self.blackboard.osfConditRemove(dataset))

    def osfFinalClose (self, dataset):
        """
        Just a pass-through method.
        """
        return (self.blackboard.osfFinalClose(dataset))


    def _initCommandQueue (self, exitCode=None):
        """
        
        """
        # self.gCommand is a list
        globalCommands = copy.copy (self.gCommand)

        # self.command is a command list
        self.commandQueue = copy.copy (self.command)
            
        # Merge the Global and Local commands appropriately given the
        # order they are to be executed as either pre or post actions
        if globalCommands:
            # global commands are to be executed before the local commands
            # and since we use pop() they must be in the queue after local commands
            self.commandQueue = self.commandQueue + globalCommands
        return

    def prepareToExecute (self, env=None, exitCode=None, maxTime=None):
        """
        Creates the generator function (which is called indirectly via the execute function.
        """

        self._genFunc = self._executeActions (env=env, exitCode=exitCode)
        if maxTime:
            self.timeOut = time.time () + maxTime
        else:
            self.timeOut = None
        return

    def execute (self):
        return self._genFunc.next ()
        
    def _executeActions (self, env, exitCode=None):
        """
        This is a generator function which manages the modules Actions.
        """
        # Ensure that the exit code we are dealing with is a string
        if exitCode is not None:
            exitCode = str (exitCode)

        # Gather the commands to execute:
        self._initCommandQueue (exitCode)

        # Now we are ready to process the commandQueue in order.
        while self.commandQueue:
            # Fetch the next command to execute, if any.
            try:
                exe, args = self.commandQueue.pop ()
                
                if (exe == 'Foreign' or
		    exe == 'StartPipe' or
		    exe == 'CallPipe' or
		    exe == 'WaitPipe' or
		    exe == 'DonePipe'):

                    # We have a UNIX executable!
                    # Setup the environment
                    for var in env.keys ():
                        os.environ[var] = str (env[var])
		    if ('NHPPS_DIR_DATA' in os.environ and
		       'OSF_DATASET' in os.environ):
		        os.environ['NHPPS_DIR_DATASET'] = ("%s%s/" %
		            (os.environ['NHPPS_DIR_DATA'],
			    os.environ['OSF_DATASET']))

                    # Set the default log directory
                    logfile = self.LOG

                    # Send logs to datadir or parent of datadir if requested
		    # Parent means stripping off the last hyphen
		    # separated component
                    try:
                        proclog = os.environ[ "NHPPS_PROC_LOGS" ]
                    except:
                        pass
                    else:
                        if proclog.lower() == "datadir":
			    logpath = "%s/%s" % (os.environ['NHPPS_DIR_DATA'],
				os.environ['OSF_DATASET'])
			    if not os.path.exists ( logpath ):
			        i = logpath.rfind ( '-' )
				if i > 0 and os.path.exists ( logpath [:i]):
					logpath = logpath[:i]
                            logfile = "%s/%s_%s.plog" % ( logpath,
			        os.environ[ "NHPPS_MODULE_NAME" ],
				os.environ[ "OSF_DATASET" ] )
                            if not os.path.exists ( logpath ):
                                try:
                                    os.mkdir ( logpath )
                                except:
                                    logfile = self.LOG

		    # Write a banner to the log file.
		    if self.LOG:
			banner = '\n%s' % os.environ ['NHPPS_MODULE_NAME']
			if 'OSF_DATASET' in os.environ:
			    banner += ' (%s)' % os.environ ['OSF_DATASET']
			banner += (': %s\n\n' %
			    datetime.datetime.now().strftime (
			    '%a %H:%M:%S %d-%b-%Y'))
			f = open (logfile, 'a', 0)
			f.write (banner)
			f.close ()

                    # Start up the executable.
		    cmd = args.split()[0]
                    if self.LOG:
                        self.activeProcess = popen2.Popen4('%s >> %s 2>&1' %
			    (args, logfile))
                    else:
                        self.activeProcess = popen2.Popen4('%s' % (args))

                    # Put self.activeProcess in its own process group
                    try:
                        err = os.setpgid (self.activeProcess.pid, self.activeProcess.pid)
                        self.activeProcessGID = self.activeProcess.pid
                    except:
                        self.activeProcessGID = None
                        
                    # Check whether the process finished already
                    self.status = self.activeProcess.poll ()
                    while (self.status == -1):
                        # We executed the external command and it has not 
                        # returned yet. We can sleep for a bit and check 
                        # back later.
                        yield (None)
                        self.status = self.activeProcess.poll ()
                    self.status = os.WEXITSTATUS (self.status)
                    
                    # cleanup
                    self.activeProcess.fromchild.close ()
                    self.activeProcess.tochild.close ()
                    self.activeProcess = None
                    
                elif exe == 'PlugIn':
                    try:
			cmd = args[0]
                        methodName = '%s%s' % (ADDON_PREFIX, args[0])
                        
                        # trim off first argument as that is the method name
                        args = args[1:]
                        
                        # call the (plugin) method from our library
                        method = getattr (NHPPS_PlugIns.actions, methodName)
                        
                    except:
                        self.status = -1
                        method = None
                        warning (PREFIX, 'unsupported plugin method (%s)(%s:%s).' % (methodName, sys.exc_type, sys.exc_value))
                        
                    if (method):
                        self.status = method (self, args, env, exitCode)
                        
                else:
                    # We have a native method!
		    cmd = exe
                    methodName = '%s%s' % (NATIVE_PREFIX, exe)
                    try:
                        method = getattr (PlugIns.actions, methodName)
			self.status = None
                    except:
                        self.status = -1
                        method = None
                        warning (PREFIX, 'unsupported native method (%s).' % (methodName))
                        
                    # Execute the method
                    if method:
                        self.status = method (self, args, env, exitCode)
                    # <-- end if

		# Set the status in the blackboard and environment.
		if cmd not in NOUPDATE:
		    self.osfUpdate (env['OSF_DATASET'],
			env['NHPPS_MODULE_NAME'], None, '.:'+str(self.status))
		    env['OSF_EXITCODE'] = self.status
		    try:
			env['NCALL'] = int (self.status) - 20
		    except:
		        pass

		    # Map the status to identifiers.
		    if self.status in self.blackboard.exitCode:
			id = self.blackboard.exitCode[self.status].id
			severity = self.blackboard.exitCode[self.status].severity
		    else:
			id = str (self.status)
			if self.status > 20:
			    severity = 'FATAL'
			else:
			    severity = 'OK'
		    env['OSF_EXITID'] = id

		    #if severity in ['HALT', 'FATAL'] and exe == 'Foreign':
		    #    self.standalone (args, logfile)
		else:
		    id = str (self.status)
                    
                # Return the exitCode to the ModuleMgr
                yield (id)
            except StopIteration:
                # Signal that the iterator is empty
                raise (StopIteration)
            # <-- end try:except
        # <-- end while
        
        # Return control to the Module Manager (should never happen)
        return
    
    def killActiveProcess (self):
        """
        Handle with care!
        
        This method kills the processes associated to self.activeProcess, if 
        any. It should be called when shutting down the whole pipeline only.
        """
        if self.activeProcess != None:
            import signal
            # Send a SIGKILL to the process group to which self.activeProcess
            # belongs. Remember that the PID of the group is set to the PID of
            # self.activeProcess in executeActions().
            if (self.activeProcess.pid != None):
                os.killpg (self.activeProcessGID, signal.SIGKILL)
            else:
                os.kill (self.activeProcess.pid, signal.SIGKILL)
        return

    def standalone (self, args, logfile):
        """
	Write information to the logfile for standalone execution.
	"""

	VARS = ['EVENT_FULL_NAME',   'EVENT_NAME',         'EVENT_TYPE',
		'NHPPS_DIR_DATA',    'NHPPS_DIR_DATASET',  'NHPPS_DIR_ERROR',
		'NHPPS_DIR_INPUT',   'NHPPS_DIR_OBS',      'NHPPS_DIR_OUTPUT',
		'NHPPS_DIR_ROOT',    'NHPPS_DIR_TRIG',     'NHPPS_STAGE',
		'NHPPS_MODULE_NAME', 'NHPPS_SUBPIPE_NAME', 'OSF_COUNT',
		'OSF_DATASET',       'OSF_FLAG',           'OSF_FULL_DATASET']

	f = open (logfile, 'a')
	f.write ('\n#!/bin/csh -f\n')
	for var in VARS:
	    f.write ('setenv %s %s\n' % (var, os.environ[var]))
	f.write ('%s\n' % (args))
	f.close ()
	return


class PreProcActionMgr (ActionMgr):
    """
    There is only ONE PreProcAction per module, however it may have
    several commands. These commands are provided to the NHPPS
    system in the order that they will be executed with
    the Global PreProcActions at the very beginning of the list so
    that we must reverse this list in order to use the pop command
    and get the proper order of commands from the list.
    """
    def __init__ (self, blackboard=None, LOG=None, verbose=0):
        ActionMgr.__init__ (self, blackboard=blackboard, LOG=LOG, verbose=verbose)
        
        # PreProcAction objects do not have exit codes to mess with...
        self.command = []
        return
    
    def addActions (self, action):
        if isinstance (action, PreProcAction):
            action.Commands.reverse ()
            for command in action.Commands:
                # Extract command name and arguments
                exe, args = getExeArgs (command)
                self.command.append ((exe, args))
        elif self.verbose:
            warning (PREFIX,
                     'wrong action type for pre proc action (%s).'
                     % (type (action)))
        return

class ProcActionMgr (ActionMgr):
    """
    There is only ONE ProcAction per module, with only ONE command.
    """
    def __init__ (self, blackboard=None, LOG=None, verbose=0):
        ActionMgr.__init__ (self, blackboard=blackboard, LOG=LOG, verbose=verbose)
        
        # ProcAction objects do not have exit codes to mess with.
        self.command = []
        return
    
    def addActions (self, action):
        if isinstance (action, ProcAction):
            exe, args = getExeArgs (action.Command)
            self.command.append ((exe, args))
        elif self.verbose:
            warning (PREFIX, 
                     'wrong action type for proc actions (%s).' 
                     % (type (action)))
        return

    def execute (self):
        """
        Ensures that the execution does not exceed the specified time limits.
        """

        if self.timeOut and time.time () > self.timeOut:
            # We must terminate the execution as we have taken too long.
            self.killActiveProcess ()
            self._genFunc = None
            raise TimeOut
        else:
            # We are within our time limits...
            return self._genFunc.next ()


class PostProcActionMgr (ActionMgr):
    """
    The PostProcActions are given to the ActionMgr as a group of actions
    (commands) to execute for the case of a SINGLE exit code (even though
    multiple exit codes may do the same task). So all we do, is store the
    commands in a list in a hash which is referenced by the exit code.
    """
    def __init__ (self, blackboard=None, LOG=None, verbose=0):
        ActionMgr.__init__ (self, blackboard=blackboard, LOG=LOG, verbose=verbose)
        
        self.command = {}
        return
    
    def addActions (self, actions):
        for action in actions:
            if isinstance (action, PostProcAction):
                cmds = []
                for command in action.Commands:
                    cmds.append (getExeArgs (command))
                # We `pop` the commands from the list, so we will reverse it.
                cmds.reverse ()

                for exitCode in action.exitCodes:
                    self.command [exitCode.val] = cmds
            elif self.verbose:
                warning (PREFIX, 
                         'wrong action type for post proc actions (%s).' 
                         % (type (action)))
                continue
        return

    def _initCommandQueue (self, exitCode=None):
        # self.command is a hash from exitCode to a command list
        if self.command.has_key (exitCode):
            self.commandQueue = copy.copy (self.command[exitCode])
        elif self.command.has_key (DEFAULT.lower()):
            self.commandQueue = copy.copy (self.command[DEFAULT.lower()])
        elif self.command.has_key (DEFAULT.upper()):
            self.commandQueue = copy.copy (self.command[DEFAULT.upper()])
        else:
            self.commandQueue = []
        
        return
