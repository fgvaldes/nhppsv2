import re
from nhpps.dtd.dtd  import OSFRequirement, FileRequirement, TimeRequirement
from pipeutil   import *
from BlackBoard import *

# Globals
PREFIX       = 'TriggerMgr'               # STDERR message prefix
TIME_DATASET = 'TIME_DATASET'
EVENT_TYPES  = {
     'osf': 'EVENT_OSF_EVENT',
    'file': 'EVENT_FILE_EVENT', 
    'time': 'EVENT_TIME_EVENT'}


class TriggerMgr (object):
    
    """
    Trigger Manager
    """
    
    def __init__ (self, trigger, name, blackboard, max=None, verbose=0):
        
        """
        Standard constructor/initializer.
        """
        
        super (TriggerMgr, self).__init__ ()
        self.trigger     = trigger
        self.conditional = self.trigger.conditional.lower ()
        self.verbose     = verbose
	self.name        = name
        self.blackboard  = blackboard
        self.max         = max
        
        self.osfReqs     = []
        self.fileReqs    = {}
        self.timeReqs    = []
        
        # Populate the hash tables
        for req in self.trigger.Requirements:
            if isinstance (req, OSFRequirement):
                # The osfReqs is a simple array of requirements.
                # It has the form:
                # [(moduleName, OP, flag), ]
                # Where OP could be ==, !=, etc.
                m, o, f = req.argv.split ()
                self.osfReqs.append ([m, o, f])
            elif isinstance (req, TimeRequirement):
                # The timeReqs array has the form
                # [startDate, endDate, timeInterval, lastRun]
                self.timeReqs.append ([req.start, req.end, req.interval, None])
            elif isinstance (req, FileRequirement):
                # The fileReqs hash table has the form
                # {'directory name': ['filename pattern', ], logicalOp}
                dirName = req.directory
                fName   = req.fnPattern
                
                # Update the dict (and create a new entry if necessary)
                if dirName in self.fileReqs:
                    self.fileReqs [dirName][0].append (fName)
                else:
                    self.fileReqs [dirName] = ([fName], self.conditional)
                    pass
                pass
            else:
                # Unsupported requirement type!
                msg = 'unsupported requirement type (%s).' % (reqType)
                warning (PREFIX, msg)
                pass
            pass
        
        return
    
    def checkRequirements (self):
        
        """
        Check if the requirements specified by self.trigger (and hence
        by self.osfReqs + self.fileReqs + self.timeReqs) are satisfied.
        
        Return
        env = {'EVENT_TYPE': EVENT_TYPES[...] EVENT_TYPES[...] ..., 
        'EVENT_NAME': fileDataset, 'OSF_DATASET': osfDataset,
        'OSF_FLAG': osfFlag, 'OSF_COUNT': osfCount,
	'NHPPS_MODULE_NAME': moduleName}
        """
        
        result = { 'EVENT_TYPE': '',
                   'EVENT_NAME': None,
              'EVENT_FULL_NAME': None,
                  'OSF_DATASET': None,
                     'OSF_FLAG': None,
                    'OSF_COUNT': None,
             'OSF_FULL_DATASET': None,
             'NHPPS_MODULE_PID': None,
            'NHPPS_MODULE_NAME': None}

        if self.max:
            openDatasets = self.blackboard.getOpenOSFDatasets ()
            n = len (openDatasets)
            if self.max < n:
                return None
            elif self.max > n:
                openDatasets = None
                pass
            pass
        else:
            openDatasets = None
            pass

	osfDataset = None
	fileDataset = None
        
        # Handle the case of self.conditional = 'OR'
        if self.conditional == 'or':
            if self.osfReqs:
                osfDataset = self.checkOSFRequirements ()
                if osfDataset:
		    osfDataset = osfDataset [0][1]
                    result ['EVENT_TYPE']       = EVENT_TYPES ['osf']
                    result ['OSF_FULL_DATASET'] = osfDataset
                    result ['OSF_DATASET']      = os.path.splitext (
                        os.path.basename (osfDataset))[0]
                    
                    result ['OSF_FLAG'] = self.blackboard.getOSFBBFlag (
                        osfDataset, self.moduleName)
                    result ['OSF_COUNT'] = self.blackboard.getOSFBBCount (
                        osfDataset, self.moduleName)
                    
                    if openDatasets and result ['OSF_DATASET'] in openDatasets:
                        return result
                    elif not openDatasets:
                        return result
                    else:
                        return None
                    pass
                pass
            
            if self.fileReqs:
                fileDataset = self.checkFileRequirements ()
                if fileDataset:
                    result ['EVENT_TYPE']      = EVENT_TYPES ['file']
                    result ['EVENT_FULL_NAME'] = str (fileDataset)
                    result ['EVENT_NAME']      = os.path.splitext (
                        os.path.basename (str (fileDataset)))[0]
		    if re.search ('rtn', os.path.splitext (str (fileDataset))[1]):
			result ['OSF_DATASET'] = '-'.join (result ['EVENT_NAME'].split ('-')[:-1])
		    else:
			result ['OSF_DATASET'] = result ['EVENT_NAME']
                    
                    if openDatasets and result ['OSF_DATASET'] in openDatasets:
                        return result
                    elif not openDatasets:
                        return result
                    else:
                        return None
                    pass
                pass
            
            if self.timeReqs:
                timeDataset = self.checkTimeRequirements ()
                if timeDataset:
                    result ['EVENT_TYPE'] = EVENT_TYPES ['time']
                    return result
                else:
                    return None
                pass
            
            # If we are here, no matches were found, return None
            # to represent that it is not time to trigger
            return None
        
        elif self.conditional == 'and':
            # Which ones are relevant? And, most of
            # all, do we have all the ones we need?
            evtType = ''
	    if self.osfReqs and self.fileReqs:
		osfDatasets = self.checkOSFRequirements ()
		if not osfDatasets:
		    return None

		for osfDataset in osfDatasets:
		    osfDataset = osfDataset[1]
		    fileDataset = self.checkFileRequirements (
		        os.path.splitext (os.path.basename (osfDataset))[0])
		    if fileDataset:
		        break

		if not fileDataset:
		    return None

	    elif self.osfReqs:
		osfDatasets = self.checkOSFRequirements ()
		if not osfDatasets:
		    return None
		osfDataset = osfDatasets[0][1]
		
	    elif self.fileReqs:
		fileDataset = self.checkFileRequirements ()
		if not fileDataset:
		    return None
		
	    if osfDataset:
		evtType += EVENT_TYPES ['osf']
		result ['OSF_FULL_DATASET'] = osfDataset
		result ['OSF_DATASET']      = os.path.splitext (
		    os.path.basename (osfDataset))[0]
		result ['OSF_FLAG'] = self.blackboard.getOSFBBFlag (
		    osfDataset, self.name)
		result ['OSF_COUNT'] = self.blackboard.getOSFBBCount (
		    osfDataset, self.name)
		
	    if fileDataset:
		evtType += EVENT_TYPES ['file']
		result ['EVENT_FULL_NAME'] = str (fileDataset)
		result ['EVENT_NAME'] = os.path.splitext (
		    os.path.basename (str (fileDataset)))[0]
		if re.search ('rtn', os.path.splitext (str (fileDataset))[1]):
		    result ['OSF_DATASET'] = '-'.join (result ['EVENT_NAME'].split ('-')[:-1])
		else:
		    result ['OSF_DATASET'] = result ['EVENT_NAME']
            
            if self.timeReqs:
                timeDataset = self.checkTimeRequirements ()
                if not timeDataset:
                    return None
                
                evtType += EVENT_TYPES ['time']
                pass
            
            # If we are still here, it means that we have everything we  need!
            result ['EVENT_TYPE'] = evtType
            if not result ['OSF_DATASET']:
                result ['OSF_DATASET'] = result ['EVENT_NAME']
                pass
            if openDatasets and result ['OSF_DATASET'] in openDatasets:
                return result
            elif not openDatasets:
                return result
            else:
                return none
            pass
        
        # If we are here, the conditional operator was invalid...
        msg = 'unsupported logical operator: %s' % (self.conditional)
        warning (PREFIX, msg)
        
        return None
    
    def checkOSFRequirements (self):
        
        # Remember that self.osfReqs is an array table of the form
        # [(moduleName, OP, flag), ]
        
        # Extract the Python logical operator from self.conditional
        op = self.conditional
        
        d = self.blackboard.selectOSFDatasets (where=self.osfReqs, logicalOp=op)
        
        # Return what we found
        return d
    
    def checkFileRequirements (self, root=None):
        
        trigFiles = []
        for dir in self.fileReqs.keys ():
            for mask in self.fileReqs [dir][0]:
	        if root:
		    mask = root + mask
                searchPattern = os.path.expandvars (os.path.join (dir, mask))
                list = glob.glob (searchPattern)
                if list:
                    trigFiles.extend (list)
                    pass
                pass
            pass
        
        if len (trigFiles) > 1:
            # Get the ctime and order the files by it...
            trigFiles = [(os.stat (file)[-1], os.path.basename (file), file)
                         for file in trigFiles]
            trigFiles.sort ()
            trigFiles = [trigFiles [0][-1]]
            pass
        
        if trigFiles:
            return string.strip (trigFiles [0])
        
        return None
    
    def checkTimeRequirements (self):
        
        now = datetime.datetime.today ()
        
        for i in xrange (len (self.timeReqs)):
            startDate, endDate, timeInterval, lastRun = self.timeReqs[i]
            
            if now >= startDate and now <= endDate:
                if timeInterval:
                    if ((lastRun and (now - lastRun) >= timeInterval) or
                        not lastRun):
                        
                        # Update lastRun (i.e. self.timeReqs[i][3])
                        self.timeReqs[i][3] = now
                        return (TIME_DATASET)
                    else:
                        # Nothing to do
                        return (None)
                    pass
                else:
                    # Update lastRun (i.e. self.timeReqs[i][3])
                    self.timeReqs[i][3] = now
                    return (TIME_DATASET)
                pass
            pass
        
        return (None)
    
    pass
