""" Pipeline mesage & error display functions.

"""

from sys import argv, stderr, exit
from os.path import basename
from responses import responses

__ERRMSG__ = 'something bad happened'

def usage (usageString):
    
    """
    Print the usageString to STDERR.
    
    If usageString is null, print a generic message saying that some
    of the command line arguments had syntax errors in them.
    """
    
    command = basename (argv[0])
    if (usageString):
        stderr.write ('usage: %s %s\n' % (command, usageString))
    else:
        stderr.write ('Error in parsing command line arguments.\n')
    return


def fatalError (prefix='', err=__ERRMSG__, exitCode=1):
    
    """
    Print err to STDERR.
    Exit with exit_code (default is 1)
    """
    
    if (prefix):
        stderr.write ('%s Fatal Error: %s\n' % (prefix, err))
    else:
        stderr.write ('Fatal Error: %s\n' % (err))
    exit (exitCode)


def warning (prefix='', msg=__ERRMSG__):

    """
    Prints msg to STDERR prepended with 'Warning: '.
    """
    
    if (prefix):
        stderr.write ('%s Warning: %s\n' % (prefix, msg))
    else:
        stderr.write ('Warning: %s\n' % (msg))
    return


def toDoWarning (prefix='', msg=__ERRMSG__):

    """
    Prints msg to STDERR.
    Formats message in a way that it is obvious that there
    is work to be done in correcting whatever caused the
    error to occur.
    """
    
    if (prefix):
        stderr.write ('*** %s: %s ***\n' % (prefix, msg))
    else:
        stderr.write ('*** %s ***\n' % (msg))
    return
