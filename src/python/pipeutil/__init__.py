from network import *
from strng import *
from message import *
from timedate import *
from config import *
from pyroUtils import *

from os import mkdir, environ, rename
from os.path import isdir, join, isfile

from stdlib import *

try:
    USER = environ['USER']
except:
    fatalError (err='$USER is not defined.')

def makeDirs (dirs, dry_run=False):
    
    """
    Given an array of strings (dirs)
    it creates a directory named <dirs>[0],
    then, inside it, a directory named
    <dirs>[1] and so on and so forth.
    
    Returns
    path    for success. path is the final
            directory path.
    None    for error.
    """
    
    if isinstance (dirs, str):
        # dirs is a simple string, not an
        # array. turn it into a 1-D array
        dirs = [dirs]
    
    path = ''
    for dir in dirs:
        if not dir:
            continue
        
        path += dir
        if dir[-1] != '/':
            path += '/'
        
        if (not isdir (path) and not dry_run):
            # path does not exists, try and create it
            try:
                mkdir (path)
            except:
                return None
    return path


def parseArgs (args, style='--'):
    
    """
    Do a quick command line argument parsing. 
    
    The expected syntax is either --var=val or -var val but not both. 
    The choice is made using the style parameter: style='--' or 
    style='-' respectively.
    No check on the semantic is performed (it is left to the colling 
    code).
    
    Return a dictionary of var=val entries. If args is None, an empty
    dictionary is returned.
    
    Raise an exception of type SyntaxError in case of syntax errors 
    in the command line args.
    """
    
    result = {}
    
    if style == '--':
        for arg in args:
            try:
                (var, val) = arg.split ('=', 1)
            except:
                raise (SyntaxError, '%s is in the wrong format' % (arg))
            
            if var[:2] != '--':
                raise (SyntaxError, 'variable names must start with a '+
                                    'double dash (%s)' % (var))
            
            result[var[2:]] = val
    elif (style == '-' and len (args) % 2 == 0):
        for i in range (0, len (args), 2):
            var = args[i]
            val = args[i+1]
            
            if var[0] != '-':
                raise (SyntaxError, 'variable names must start with a '+
                                    'dash (%s)' % (var))
            
            result[var[1:]] = val
    else:
        raise (SyntaxError, 'unsupported argument syntax.')
    return result


def which (cmdName):
    
    """
    Python implementation of the UNIX which command.
    """
    
    try:
        PATH = environ['PATH'].split (':')
    except:
        # Standard PATH on UNIX
        PATH = ('/bin', 
                '/sbin', 
                '/usr/bin', 
                '/usr/sbin', 
                '/usr/local/bin')
    
    for dir in PATH:
        cmdPath = join (dir, cmdName)
        if isfile (cmdPath):
            return cmdPath
    return None


def updateFile (dir, old, new):
    
    """
    Given a file mask (old), it finds the file and
    renames it to new. If more than one file is 
    found, then an error is raised. Files are searched
    in the input directory (dir).
    
    If the old file does not exists, then the new one is
    created and the error silently ignored.
    """
    
    list = glob.glob (join (dir, old))
    newName = join (dir, new)
    if not list:
        try:
            f = file (newName, 'w')
            f.close ()
        except:
            return 2
    else:
        oldName = list[0].strip ()
        try:
            rename (oldName, newName)
        except:
            return 2
    return None


def get_freespace (directory, pipeApp=None):
    
    """
    Executes 'df -k directory' in a subshell, parses the output and
    Returns the available space on that directory/mount point.
    
    The input parameters are:
    
    directory   The directory we want to check the free space of
    
    Returns:
    free space in Kb     if successful
    'No such directory.' in case directory does not exist
    -1                   in case of error parsing result.
    """

    if not os.access ('%s%s' % (directory, os.path.sep), os.F_OK):
        return 'No such directory.'
    try:
        data = os.popen ('df -k %s%s' % (directory, os.path.sep)).readlines ()
        free = long (data[-1].split ()[-3])
    except:
        free = -1
        pass
    return free

