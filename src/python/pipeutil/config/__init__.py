""" Pipeline configuration utilities

"""

from string import replace, split, strip

def readConfFile (filename):
    
    """
    struct read_conf_file (char* filename)
    
    Reads the configuration file specified by filename
    Comments (marked by the '#' carachter are ignored.
    All returned keys are arrays.

    3/29/12: convert comma separated values to single space separated value.
    The previous version treated this as different words.  But I don't think
    that feature has been used in pipeselect.config.
    
    Rerturns:
    a dictionary (hash) of key = value couples
    """
    
    # the author of this function is Norbert Pirzkal
    # modified by F. Pierfederici
    conf = {}
    lines = open (filename, 'r').readlines ()
    for line in lines:
        if line[0]=='#': 
            continue
        # Remove end comment
        good = split (line,'#', 1)[0]
        # Remove all leading/ending spaces
        good = strip (good)
        # split on the first = sign
        words = split (good, '=', 1)
        if len (words)==2:
            key,val = words[0],words[1]
            key = strip (key)
            val = strip (val)
            if not conf.has_key (key):
                conf[key]=[]
            val = replace (val, ',', ' ')
            vals = split (val,',')

            for i in range (len (vals)):
                v = replace (vals[i],"\012","")
                v = replace (v,"'","")
                v = strip (v)
		conf[key].append (v)
    
    return conf


def writeConfFile (filename):
    
    """
    int write_conf_file (char* filename)
    
    Writes a default configuration into filename
    
    Rerturns:
     0 - no error
     1 - cannot open filename for writing
     2 - unknown error
    """
    
    try:
        f = open (filename, 'w')
    except:
        return 1
    
    # write out the default configuration file
    f.write (defaults)
    f.close ()
    
    return 0


def updateConfFile (filename, cfg):
    
    """
    int update_conf_file (char* filename)
    
    Writes the cfg dictionary into filename
    Comments are omitted.
    
    Rerturns:
     0 - no error
     1 - cannot open filename for writing
     2 - unknown error
    """
    
    if (not isinstance (cfg, dict)):
        return 2
    
    try:
        f = open (filename, 'w')
    except:
        return 1
    
    # write out the default configuration file
    for key in cfg.keys():
        val = cfg[key]
        k = str (key)
        if (isinstance (val, list)):
            for v in val:
                f.write (k + ' = ' + str (v) + '\n')
        else:
            f.write (k + ' = ' + str (val) + '\n')
    f.close ()
    
    return 0
