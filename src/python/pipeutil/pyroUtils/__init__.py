""" Pipeline Pyro NameServer utilities

"""

import warnings
warnings.filterwarnings ('ignore')

import os
import Pyro.core, Pyro.naming
import popen2, time

from pipeutil.message import fatalError

NSGROUP = ':pipeline'
PREFIX = 'pyroUtils'

# Name of the executable to start Pyro's NameServer
NS_EXE = 'ns'

# Pyro utility functions:

def findNameServer (timeout=1.):
    
    """
    Find the Pyro Name Server by means of a NameServerLocator instance.
    First check for a direct host specification and then try anywhere on
    the network.

    Input
    timeout    timeout time in seconds.
    
    Output
    NameSever handle or None in case of error.
    
    Raise
    Pyro.errors.PyroError
    """
    
    # First try and connect to the Name Server
    host = None
    if 'NHPPS_NODE_PYRO' in os.environ:
        host = os.environ ['NHPPS_NODE_PYRO']
        pass
    port = None
    if 'NHPPS_PORT_PYRO' in os.environ:
        port = int (os.environ ['NHPPS_PORT_PYRO'])
        pass
    
    found = True
    try:
	if host and port:
	    #print "Try nameserver at %s:%s" % (host, port)
	    ns = Pyro.naming.NameServerLocator ().getNS (host=host, port=port)
	    pass
	elif host:
	    #print "Try nameserver at %s" % (host)
	    ns = Pyro.naming.NameServerLocator ().getNS (host=host)
	    pass
	else:
	    #print "Try network locator"
	    ns = Pyro.naming.NameServerLocator ().getNS ()
	    pass
    except:
        if host or port:
            try:
                #print "Using network locator"
                ns = Pyro.naming.NameServerLocator ().getNS ()
		pass
            except:
                found = False
		pass
        else:
            found = False
	    pass

    return ns


def getNSlist (group=None, pipeApp=None, host=None, pipe=None, user=None, nm=None, prefix=PREFIX):

    # Init the Pyro client and contact the NameServer
    Pyro.core.initClient (0)
    try:
        ns = findNameServer (timeout=2)
    except:
        fatalError (prefix, 'the Name Server cannot be either started or discovered.')
    
    if group and pipeApp and pipe and host and user:
        name = '%s^%s^%s^%s^%s^%s' % (group, pipeApp, pipe, host, user, nm)
        try:
            uri = ns.resolve (name)
        except:
            return []
        all = [(name, uri)]
    else:
        all = ns.flatlist()
        all = [(name.split ('^'), uri) for (name, uri) in all if len (name.split ('^')) == 6]
        if group:
            all = [((g,a,p,h,u,n),uri) for ((g,a,p,h,u,n),uri) in all if g==group]
        if pipeApp:
            all = [((g,a,p,h,u,n),uri) for ((g,a,p,h,u,n),uri) in all if a==pipeApp]
        if pipe:
            all = [((g,a,p,h,u,n),uri) for ((g,a,p,h,u,n),uri) in all if p==pipe]
        if host:
            all = [((g,a,p,h,u,n),uri) for ((g,a,p,h,u,n),uri) in all if h==host]
        if user:
            all = [((g,a,p,h,u,n),uri) for ((g,a,p,h,u,n),uri) in all if u==user]
        if nm:
            all = [((g,a,p,h,u,n),uri) for ((g,a,p,h,u,n),uri) in all if n==nm]
            pass
        pass
    
    # In the future, we may filter the results further by dataset,
    # module, status, etc...
    
    return all


def getNSproxies (group=None, pipeApp=None, host=None, pipe=None, user=None, nm=None, prefix=PREFIX):
    
    """
    Returns an array of Pyro proxies to the objects listed in the nameserver
    which match any of the specific criteria [group,host,pipe,user,nm].
    """
    
    nsList = getNSlist (group=group, pipeApp=pipeApp, host=host, pipe=pipe, user=user, nm=nm, prefix=prefix)
    
    proxies = []
    
    for (name, uri) in nsList:
        try:
            obj = Pyro.core.getProxyForURI (uri)
        except:
            continue
        
        proxies.append (obj)
    
    return proxies


def getNSattrProxies (group=None, pipeApp=None, host=None, pipe=None, user=None, nm=None, prefix=PREFIX):
    
    """
    Returns an array of Pyro Attribute proxies to the objects listed in the nameserver
    which match any of the specific criteria [group,host,pipe,user,nm].
    """
    
    nsList = getNSlist (group=group, pipeApp=pipeApp, host=host, pipe=pipe, user=user, nm=nm, prefix=prefix)
    
    proxies = []
    
    for (name, uri) in nsList:
        try:
            obj = Pyro.core.getAttrProxyForURI (uri)
        except:
            continue
        
        proxies.append (obj)
    
    return proxies
