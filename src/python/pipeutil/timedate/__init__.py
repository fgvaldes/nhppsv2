""" Pipeline Time & Date conversion functions

"""

import time

def toDecimal (time):
    
    """
    Converts (hour,minute,second) format to decimal hours
    From RO.py package.
    """
    
    (hour,min,sec) = time
    if hour < 0:
        mult = -1
    else:
        mult = 1
    return mult*(abs(hour)+((sec/60.0)+min)/60.0)


def MJD ():
    
    """
    Get the MJD for the current instant
    """
    
    utctime = toUTC (time.time())
    return toMJD (utctime)


def toMJD (time):
    
    """
    Converts a (utc) time tuple to modified julian day
    Pulled from RO.py package
    """
    
    year,month,day,hour,min,sec=time
    d=day+toDecimal(time[-3:])/24.0
    if month<3:
        y=year-1
        m=month+12
    else:
        y=year
        m=month
    if (year>1582 or (year==1582 and month>10) or
        (year==1582 and month==10 and day >=15)):
        A=int(y/100)
        B=2-A+int(A/4.)
    else:
        B=0
    if y<0:
        C=int((365.25*y)-0.75)
    else:
        C=int(365.25*y)
    D=int(30.6001*(m+1))
    julian = B+C+D+d+1720994.5
    return (julian - 2400000.5)


def toUTC (ltime):
    
    """
    Get the UTC for the passed clock time
    """
    
    utctime = ltime - time.timezone
    utctime = time.localtime(utctime)[0:6]
    return utctime
