""" Pipeline String manipulation functions.

"""

from string import find, strip, split, whitespace

# Globals for the String functions
__QUOTES__ = ('"', "'")


def quote (istr, trim=True):
    
    """
    Encloses the input string in quotes, in
    case it contains white spaces. The choice of
    single or double quotes depends on other
    quotes being present in the string.
    
    Returns:
    ''      in case of error
    qstr    quoted version of the input string
    
    INTERNAL
    """
    
    if trim:
        istr = strip (istr)
    tokens = split (istr)
    if (len (tokens) == 0):
        return ('')
    elif (len (tokens) == 1):
        return (istr)

    # we have white spaces so set quote type.
    q0 = find (istr, __QUOTES__[0])
    q1 = find (istr, __QUOTES__[1])
    if (q0 != -1 and q1 != -1):
    	# if there is a mixture of quotes punt by not quoting.
	q = ''
    elif (q0 != -1):
        # use a single quote if there are double quotes.
        q = __QUOTES__[1]
    else:
        # use a double quote if there is no quote or only single quotes.
        q = __QUOTES__[0]
    
    # quote the string and spit it out
    return (q + istr + q)


def forceQuote (istr, trim=True):
    
    """
    Same as quote above, but quotes the
    input string no matter what.
    
    INTERNAL
    """
    
    if trim:
        istr = strip (istr)
    if isQuoted (istr):
        return istr
    
    tokens = split (istr)
    if (len (tokens) == 0):
        return ('')
    
    if (find (istr, '"') != -1):
        # we have a double quote already.
        # use single quotes
        q = "'"
    elif (find (istr, "'") != -1):
        # we have a single quote already.
        # use double quotes
        q = '"'
    else:
        # we do not have any quote
        # use double quotes
        q = '"'
    
    # quote the string and spit it out
    return (q + istr + q)


def dequote (istr, trim=True):
    
    """
    Removes quotes from a string.
    
    Input
    istr    a text string.
    
    Output
    ostr    istr without quotes (read the
            Description).
    -1      in case of error.
    
    
    Description
    Not a general purpose de-quoter: it 
    is tailored to handle strings following
    the Pipeline RPC Protocol. The idea is
    that a string can be enclosed in quotes
    (either single or double). If so, the
    quote enclosed part of the string is a
    word. In any case, we are only interested 
    in the FIRST word, and we discard anything
    else. Quotes might be escaped.    
    """
    
    # istr might be enclosed in quotes and it might 
    # be a list. if we do not find quotes, we assume 
    # that istr is not a list and grab the first word, 
    # only. In any case, we only return the first
    # word.
    
    if trim:
        istr = strip (istr)

    if istr[0] in __QUOTES__:
        for quoteStyle in __QUOTES__:
            if istr[0] == quoteStyle:
                # We know the quote style to look for, now find
                # the end quote
                pos = findNextCh (istr, quoteStyle, 1)
                if pos == -1:
                    # The end quote was not found
                    return -1
                # extract the unquoted text
                ostr = istr[1:pos]
    else:
        # The string didn't begin with a quote, grab the first word
        ostr = split (istr)[0]
        
    # at this point ostr is without quotes and
    # it is what we consider being a single word
    return strip (ostr)


def findNextCh (strng, ch = ' ', start=0):
    
    """
    Finds the next occurence of character ch
    in the string strng, starting from position start.
    Understands that \ch doesn't count.
    
    Returns:
    -1 of ch is not found
    absolute position of ch in strng (> start)
    
    INTERNAL
    """
    
    found = 0
    next = -1
    strng_len = len (strng)
    while (not found and start < strng_len):
        next  = find (strng[start:], ch)
        if (next != -1):
            if (strng[start+next-1] == '\\'):
                # ch is escaped, keep looking
                start += next + 1
            else:
                found = 1
        else:
            return (-1)
    return (next + start)


def isQuoted (istr):
    
    """
    Returns True if the input string (istr)
    is enclosed in (matching) quotes. False 
    otherwise.
    """
    
    s = strip (istr)
    return (s[0] == s[-1]) and (s[0] in __QUOTES__)


def hasWhitespace (istr):
    
    """
    Returns True if the input string (istr)
    has white spaces. False otherwise.
    """
    
    for ch in whitespace:
        if ch in istr:
            return True
    return False


def getWords (istr):
    
    """
    This routine parses a string and extracts
    words from it. A word is defined as either a
    contiguous sequence of ono white space 
    characters or as a sequence of characters
    (including white spaces) enclosed in quotes.
    The enclosing quotes can be either single
    quotes or double quotes. The quotes enclosing
    a world must match.
    
    get_words returns an array of words (dequoted
    if needed) or an empty array if no words are found.
    """
    
    # do we have white spaces? If not,
    # simply return the dequoted string.
    if not hasWhitespace (istr):
        return [dequote (istr)]
    
    # otherwise parse it and extract
    # words.
    words = []
    dummy = ''
    q = None
    have_token = False
    prev_ch = None
    for i in range (len (istr)):
        ch = istr[i]
        if ch in whitespace:
            if not have_token:
                # we are between words.
                prev_ch = ch
                continue
            else:
                # we are inside a word
                # did the current word start with a quote?
                if q:
                    # yes: the space is part of the word
                    prev_ch = ch
                    dummy += ch
                else:
                    # no. we need to close the word and
                    # add it to the words array.
                    words.append (dummy)
                    dummy = ''
                    have_token = False
                    prev_ch = ch
            continue
        elif ch in __QUOTES__:
            if not have_token:
                # we are between words
                if (not prev_ch or 
                    prev_ch in whitespace):
                    # and the previous ch was a
                    # whitespace: we have a word!
                    prev_ch = ch
                    have_token = True
                    dummy = ''
                    q = ch
                elif ch == q:
                    # we have an empty string
                    words.append ('')
                    prev_ch = ch
                    have_token = False
                    dummy = ''
                else:
                    # ummm... we are between words
                    # and prev_ch is not a white 
                    # space... there is something
                    # wrong here!
                    warning ('check this: ' + istr)
                    return (None)
            else:
                # we are inside a word
                if ch == q:
                    # this is the closing quote!
                    words.append (dummy)
                    dummy = ''
                    prev_ch = ch
                    have_token = False
                    q = None
                else:
                    # the quote is part of the word
                    dummy += ch
                    prev_ch = ch
            continue
        else:
            # ch is neither a white space nor a quote.
            # are we between words?
            if have_token:
                # yes. keep building the string
                dummy += ch
                prev_ch = ch
                if (i == len (istr) - 1):
                    # we are at the end of the string!
                    if not q:
                        # close the word.
                        words.append (dummy)
                        dummy = ''
                        have_token = False
                    else:
                        # we have an opening quote but not
                        # a closing one! Syntax error.
                        warning ('check this: ' + istr)
                        return (None)
            else:
                # no. we start a token, unless we are at the
                # end of the string!
                if (i == len (istr) - 1):
                    words.append (ch)
                    have_token = False
                else:
                    dummy = ch
                    have_token = True
                    prev_ch = ch
                    q = None
            continue
        # this should be never used, in practice
        continue
    # return the words array
    return words


def shellEscape (cmd):
    
    """
    Given a string (cmd) it escapes quotes and the like
    so that the shell is happy.
    """
    
    escaped = ''
    if isinstance (cmd, str):
        for i in range (len (cmd)):
            if (cmd[i] in __QUOTES__ and i > 0 and cmd[i-1] != '\\'):
                escaped += '\\' + cmd[i]
            else:
                escaped += cmd[i]
        return escaped
    else:
        return cmd
