""" Pipeline RPC Network utilities

"""

from pipeutil.strng import quote, dequote
from pipeutil.message import fatalError

from os import environ
import socket, sys, re

try:
    NHPPS_PORT_NM = int (environ['NHPPS_PORT_NM'])
except:
    fatalError (err='$NHPPS_PORT_NM is not defined.')

# Global definitions which impact the networking functions
__MAX_SIZE__ = 1024000          # max amount of data on a socket
                                # connection
               
__PKT_EOF_REGEX__ = re.compile('^.*EOF\s*$', re.S) #def of packet End Of File

NODE = socket.gethostname ().split ('.')[0]

def composePacket (words):
    
    """
    Turns an input dictionary into a string using
    the Pipeline RPC Protocol.
    
    Input
    words       a dictonary of the form key:value
                which is the decoded representation of
                the input string (istr). The 'EOF'
                control sequence is discarded.
                keys are turned lowercase, vals
                are kept as given.
    
    Output
    istr        a text string
    ''          (empty string) in case of error.
    
    Description
    composePacket is the exact complement to parsePacket.
    
    In case of error, '' is returned.
    """
    
    ostr = ''
    
    # make sure that we have a proper dictionary
    if len (words) and isinstance (words, dict):
        for key in words.keys ():
            key = str (key).strip ()
            val = str (words[key]).strip ()
            # remember to quote val if it has spaces
            oval = quote (val)
            if not oval:
                oval = '""'
        
            # we should also make sure that key is
            # a single word. no spaces allowed, in 
            # this case!
            okey = key.split ()[0]
            ostr += '%s = %s\n' % (okey, oval)
    
        # close the packet with an 'EOF' string
        ostr += 'EOF\n'
    
    return (ostr)


def parsePacket (istr, deQuote=True):
    
    """
    Turns a string of text (which uses the Pipeline RPC
    Protocol) into a dictionary, i.e., request packet.
    
    Input
    istr        a text string
    
    Output
    words       a dictonary of the form key:value
                which is the decoded representation of
                the input string (istr). The 'EOF'
                control sequence is discarded.
                keys are turned lowercase, vals
                are kept as given.
    -1          in case of error.
    
    Description
    parsePacket accepts in input a string (istr) written
    using the Pipeline PRC Protocol. It stop reading the 
    string as soon as the 'EOF' control sequence is 
    encountered. At that point, it simply discard both the 
    'EOF' string and any text that follows.
    
    The remaining text is then analyzed and a dictionary
    of key:value pairs (words) extracted. Once the parsing is 
    done, if no errors were encountered, the final dictionary 
    is returned.
    
    A note on strings and how they are parsed. According to the
    Pipeline RPC Protocol, a string may be enclosed in quotes.
    Quotes can be either single quotes or double quotes. A 
    string HAS to be enclosed in quotes if it contains white
    spaces (e.g. a list). In parsing strings, therefore, we
    strip the enclosing quotes.
    
    It is important to remember that the Pipeline RPC Protocol
    only allows key and values composed of a SINGLE word.
    Quotes enclosing a value string define a word. Otherwise, 
    the first white spaces marks the end of a word. Keys cannot
    have white spaces in them.
    
    In parsing, only the first word is returmed.
    
    
    In case of error, -1 is returned.
    """
    
    words = {}
    
    # do the actual parsing
    lines = istr.split ('\n')
    for i in range (len (lines)):
        line = lines[i].strip ()
                
        # Ignore blank and comment lines.
        if not line or line[0] == '#':
            continue

        # Do not process lines past the 'EOF' string
        if line.upper () == 'EOF':
            break
        
        # Split the key = value pairs
        try:
            key, val = line.split ('=', 1)
        except:
            continue
        
        key = key.strip ()
        key = key.lower ()
        val = val.strip ()
        
        # Ignore line if either key or val is null
        if not key or not val:
            continue
        
        # val must be a single word. If it has white spaces, quotes 
        # mark the word boundaries. If no quotes are found, we take
        # the first word.
        if deQuote: val = dequote (val)
        
        # Store key:val in the words dictionary.
        words[key] = val
    
    if len (words) == 0:
        return -1
    else:
        return words


def connect (host, port):
    
    """
    Opens a socket connection to host:port
    
    Input
    host    the address of the remote machine
    port    the port the server on the remote host 
            is listening to.
    
    Output
    s       a socket connection to host:port
    None    in case of error
    """
    
    try:
        s = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
        s.connect ((host, int (port)))
    except:
        s = None
    
    return s


def disconnect (skt):
    
    """
    Closes the input socket
    """
    
    try:
        skt.close ()
    except:
        return -1
    
    return 0


def execute (skt, cmd):
    
    """
    Given a socket connection and a dictionary, it
    generates a packet from the dictionary and sends it
    to the socket. The packet is written in the Pipeline 
    RPC protocol syntax. The result is then parsed and
    a dictionary is returned.
    """

    msg = composePacket (cmd)
    if msg:
        skt.send (msg)
        response = ''
        try:
            while 1:
                recievedData = skt.recv (__MAX_SIZE__)
                if (not recievedData): break
                response += recievedData
                
                # check for end of packet send
                if (__PKT_EOF_REGEX__.match(recievedData)):
                    break
        except:
            pass
        if response:
            return parsePacket (response)

    return -1


def sendPacketTo (pkt, addr, port, will_respond=False, timeout=1, verbose=False):
    
    """
    Send pkt to the server at a given address and port
    """

    if not pkt:
       raise MissingAttributeError, "sendPacketTo can't send null pkt"

    response = ''
    try:
        # create the socket
        target = socket.socket (socket.AF_INET, socket.SOCK_STREAM)

        # configure the socket
        target.settimeout(timeout)
        #target.setblocking(False)

        # connect the socket
        if verbose: print " sendPacketTo: attempt connect to %s:%d" % (addr, port),
        target.connect ((addr, port))
        if verbose: print "...success"

        # send the sql request
        target.sendall (pkt)

        # if a response is requested, wait for it
        if will_respond:
            try:
                while 1:
                    recievedData = target.recv (__MAX_SIZE__)
                    if not recievedData: break
                    if verbose: print (" SEND_PKT_GETS_DATA:[%s]" % recievedData)
                    response += recievedData

                    # check for end of packet send
                    if __PKT_EOF_REGEX__.match (recievedData):
                        break

            except socket.timeout:
                # thats OK
                if verbose: print (" SEND_PKT_DATA times out...connection terminated with no response")
                pass

            except:
                raise sys.exc_type, sys.exc_value

        # fin: disconnect our socket
        target.close ()

    except:
        raise sys.exc_type, "sendPacketTo %s:%s failed on exception %s" % (addr,port, sys.exc_value)

    return response


def composeMultilineListPacket (words):
    
    """
    Turns an input dictionary into a string using
    the Pipeline RPC Protocol.
    
    Input
    words       a dictonary of the form key:value
    which is the decoded representation of
    the input string (istr). If value is a list, each member
    of that list get seperated onto its own line and prefixed
    with the key.  Behaves as compose_packet otherwise.
    The 'EOF' control sequence is discarded.
    keys are turned lowercase, vals
    are kept as given.
    
    Output
    istr        a text string
    ""          (empty string) in case of error.
    
    Description
    compose_packet is the exact complement to parse_packet
    above.
    
    In case of error, "" is returned.
    """

    ostr = ''

    # make sure that we have a proper dictionary
    if (len (words) == 0 or not isinstance (words, dict)):
        return ('')

    for key in words.keys ():
        key = key.strip ()
        if isinstance(words[key], str):
            val = str (words[key]).strip ()
	    if (val[0] == "("): # tuple
		val = val[1:len(val)-2]
		val = str (val).strip()

            ## remember to quote val if it has spaces
            #oval = quote (val)
	    oval = val
            if (oval == ''):
                oval = '""'

            # we should also make sure that key is
            # a single word. no spaces allowed, in 
            # this case!
            okey = key.split ()[0]
            ostr = "%s%s = %s\n" % (ostr, str (okey), str (oval))
        elif isinstance(words[key], list):
            for entry in words[key]:
		val = str (entry).strip ()
	        if (val[0] == "("): # tuple
		    val = val[1:len(val)-2]
		    val = str(val).strip ()

		## remember to quote val if it has spaces
		#oval = quote (val)
		oval = val
		if (oval == ''):
		    oval = '""'

		# we should also make sure that key is
		# a single word. no spaces allowed, in 
		# this case!
                okey = key.split ()[0]
		ostr = "%s%s = %s\n" % (ostr, str (okey), str (oval))

    # close the packet with an 'EOF' string
    ostr = "%sEOF\n" % (ostr)

    return (ostr)


def iraf2unix (iraf_path):
    
    """
    Transform an IRAF path in network notation 
    to a UNIX path in network notation (for use
    with SSH or RSYNC).
    
    Takes into account the fact that, if the 
    hostname is given in the long form, IRAF will
    truncate it. This is why we ALWAYS revert to 
    the short form.
    """
    
    try:
        host, path = iraf_path.split ('!', 1)
    except:
        return iraf_path

    # long form -> short form
    host = host.split ('.', 1)[0]
    
    if host == NODE:
        return path
    return host + ':' + path


def getNodeManagers (host=None):
    
    """
    Checks to see if the local node manager is running.
    If it is, it is queried for the list of node managers.
    The result is returned.
    SHORTCUT:
       if the optional host is specified, and it is the current
       node, we only find the mgr port and return.
    """
    
    if host == NODE:
        return {NODE:NHPPS_PORT_NM}

    output = {}
    packet = composePacket ({'COMMAND':'get_node_list'})
    try:
        result = parsePacket (sendPacketTo (packet, NODE, NHPPS_PORT_NM, True, 10., False))
        # OK, now we have a dictionary with the results
        # The dictionary has two entries: status and list.
        try:
            status = result['status']
            list = result['list'].split ()
        except:
            return result
        
        # parse the list
        for node_port in list:
            try:
                node, port = node_port.split (':')
                port = int (port)
            except:
                continue
            output[node] = port
    except:
        raise sys.exc_type, sys.exc_value
    return output


#def parse_packet_obsolete (istr):
#    
#    """
#    Parses a string of text that uses the Pipeline RPC
#    Protocol.
#    
#    Input
#    istr        a text string
#    
#    Output
#    words       a dictonary of the form key:value
#                which is the decoded rapresentation of
#                the input string (istr). The 'EOF' 
#                control sequence is discarded.
#                keys are turned lowercase, vals
#                are kept as given.
#    -1          in case of error.
#    
#    Description
#    parse_packet accepts in input a string (istr) written
#    using the Pipeline PRC Protocol. It stop reading the 
#    string as soon as the 'EOF' control sequence is 
#    encountered. At that point, it simply discard both the 
#    'EOF' string and any text that follows.
#    
#    The remaining text is then analyzed and a dictionary
#    of key:value pairs (words) extracted. Once the parsing is 
#    done, if no errors were encountered, the final dictionary 
#    is returned.
#    
#    In case of error, -1 is returned.
#    
#    OBSOLETE
#    """
#    
#    words = {}
#    
#    # do the actual parsing
#    lines = split (istr, '\n')
#    for i in range (len (lines)):
#        # Remember: the deal is that we
#        # parse until the 'EOF' string.
#        # The rest is not our business.
#        line = strip (lines[i])
#                
#        if (upper (line) == 'EOF'):
#            break
#        # Ignore blank lines.
#        if (line == ''):
#            continue
#        # Ignore comments: they are identified 
#        # by a hash ('#') as FIRST character.
#        if (line[0] == '#'):
#            continue
#        
#        # split the key = value pairs
#        try:
#            [key, val] = split (lines[i], '=', 1)
#        except:
#            continue
#        # ignore line if either key or val is null
#        if (not key or not val):
#            continue
#        
#        key = strip (key)
#        lower_key = lower (key)
#        val = strip (val)
#        val_len = len (val)
#        
#        # val must be a single word. If it has white spaces, quotes 
#        # mark the word boundaries. If no quotes are found, we take
#        # the first word.
#        min_wp = find (val, " ")
#        min_sp  = find (val, "'")
#        min_dp = find (val, '"')
#        # do we have a quotes? if not, we happily accept val
#        # otherwise we might have to do some parsing and quote 
#        # matching.
#        if ((min_sp != -1 and (min_sp < min_wp or min_wp == -1)) 
#            or (min_dp != -1 and (min_dp < min_wp  or min_wp == -1))):
#            if (min_sp < min_dp and min_sp != -1):                  
#                # the first quote is a single quote. in order not to
#                # reject val, we have to have another single quote!
#                start = min_sp + 1
#                next_sp = find_next_ch (val, "'", start)
#                # did we find the closing quotes?
#                if (next_sp != -1):
#                    min_sp += 1
#                    val = val[min_sp:next_sp]
#                else:
#                    # skip the line altogether
#                    continue
#            elif (min_dp < min_sp and min_dp != -1):
#                # the first quote is a double quote. in order not to
#                # reject val, we have to have another double quote!
#                start = min_dp + 1
#                next_dp = find_next_ch (val, '"', start)
#                # did we find the closing quotes?
#                if (next_dp != -1):
#                    min_dp += 1
#                    val = val[min_dp:next_dp]
#                else:
#                    # skip the line altogether
#                    continue
#            elif (min_sp != -1 and min_dp == -1):
#                # we only have single quote(s). we either
#                # find another single quote or we skip val.
#                next_sp  = find (val[min_sp+1:], "'")
#                if (next_sp != -1):
#                    min_sp += 1
#                    next_sp += min_sp
#                    val = val[min_sp:next_sp]
#                else:
#                    # skip the line altogether
#                    continue
#            elif (min_dp != -1 and min_sp == -1):
#                # we only have double quote(s). we either
#                # find another double quote or we skip val.
#                next_dp  = find (val[min_dp+1:], '"')
#                if (next_dp != -1):
#                    min_dp += 1
#                    next_dp += min_dp
#                    val = val[min_dp:next_dp]
#                else:
#                    # skip the line altogether
#                    continue
#        else:
#            # we have no quotes before a space. just split val 
#            # on the first white space and keep the first elements.
#            val = split (val)[0]
#        
#        # store key:val in the words dictionary.
#        words[lower_key] = val
#    
#    if (len (words) == 0):
#        return -1
#    else:
#        return (words)


# TAILORED ERROR TYPES
#
class MissingAttributeError (AttributeError):
    "Used to indicate that a needed keyword/parameter is missing"
# <-- end class MissingAttributeError
