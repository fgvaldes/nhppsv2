
# Some extra widget classes we use

from Tkinter import *

class MultiListBox(Frame):

    def __init__(self, master, lists):
	Frame.__init__(self, master)
        self._LastRowSelected = None
	self.labels = []
	self.lists = []
	for l,w in lists:
	    frame = Frame(self); frame.pack(side=LEFT, expand=YES, fill=BOTH)
	    label=Label(frame, text=l, borderwidth=1, relief=RAISED)
	    lb = Listbox(frame, width=w, borderwidth=0, selectborderwidth=0,
			 relief=FLAT, exportselection=FALSE)
            label.pack(fill=X)
	    lb.pack(expand=YES, fill=BOTH)
	    self.labels.append(label)
	    self.lists.append(lb)
	    lb.bind('<Shift-Button-1>', lambda e, s=self: s._select(e.y, shiftPressed=True))
	    lb.bind('<Control-Button-1>', lambda e, s=self: s._select(e.y, cntlPressed=True))
            lb.bind('<Control-B1-Motion>', lambda e, s=self: s._select(e.y, cntlPressed=True, inMotion=True))
	    lb.bind('<B1-Motion>', lambda e, s=self: s._select(e, inMotion=True))
	    lb.bind('<Button-1>', lambda e, s=self: s._select(e.y))
	    lb.bind('<Leave>', lambda e: 'break')
	    lb.bind('<B2-Motion>', lambda e, s=self: s._b2motion(e.x, e.y))
	    lb.bind('<Button-2>', lambda e, s=self: s._button2(e.x, e.y))
	frame = Frame(self); frame.pack(side=LEFT, fill=Y)
	Label(frame, borderwidth=1, relief=RAISED).pack(fill=X)
	sb = Scrollbar(frame, orient=VERTICAL, command=self._scroll)
	sb.pack(expand=YES, fill=Y)
	self.lists[0]['yscrollcommand']=sb.set

    def clear_selection(self, first=0, last=END):
	self.selection_clear(first, last)
	self._LastRowSelected = None
        return

    def _select(self, y, cntlPressed=False, shiftPressed=False, inMotion=False):
	row = self.lists[0].nearest(y)
        if (not cntlPressed and not shiftPressed):
	    self.clear_selection()

        # under these conditions, the user wants to unselect 
        # only the current row 
        if (cntlPressed and not inMotion and self.selection_includes(row)):
            self.clear_selection(first=row, last=row)
            return 'break'

        # if shift is pressed, then they want to select a whole 
        # raft of stuff
        if (shiftPressed):
            self.selection_set(row, last=self._LastRowSelected)
        else:
            # just select the current row
            self.selection_set(row)

	self._LastRowSelected = row
	return 'break'

    def _button2(self, x, y):
	for l in self.lists: l.scan_mark(x, y)
	return 'break'

    def _b2motion(self, x, y):
	for l in self.lists: l.scan_dragto(x, y)
	return 'break'

    def _scroll(self, *args):
	for l in self.lists:
	    apply(l.yview, args)

    def curselection(self):
	return self.lists[0].curselection()

    def delete(self, first, last=None):
	for l in self.lists:
	    l.delete(first, last)

    def config(self, **kw):
        self.configure (kw)

    def configure(self, **kw):
	for l in self.labels:
            l.configure(kw)

	for lb in self.lists:
            lb.configure(kw)

    def get(self, first, last=None):
	result = []
	for l in self.lists:
	    result.append(l.get(first,last))
	if last: return apply(map, [None] + result)
	return result
	    
    def index(self, index):
	self.lists[0].index(index)

    def insert(self, index, *elements):
	for e in elements:
	    i = 0
	    for l in self.lists:
		l.insert(index, e[i])
		i = i + 1

    def size(self):
	return self.lists[0].size()

    def see(self, index):
	for l in self.lists:
	    l.see(index)

    def selection_anchor(self, index):
	for l in self.lists:
	    l.selection_anchor(index)

    def selection_clear(self, first, last=None):
	for l in self.lists:
	    l.selection_clear(first, last)

    def selection_includes(self, index):
	return self.lists[0].selection_includes(index)

    def selection_set(self, first, last=None):
	for l in self.lists:
	    l.selection_set(first, last)

# <--- end class MultiListBox

class SortedMultiListBox(MultiListBox):

    def __init__(self, master, lists):
        Frame.__init__(self, master)
        self.sortedBy = -1
        self.labels = []
        self.lists = []
        frame = Frame(self)
        frame.pack(side=LEFT, expand=NO, fill=BOTH)
        sb = Scrollbar(frame, orient=VERTICAL, command=self._scroll)
        sb.pack(side=LEFT,expand=YES, fill=Y)
        counter = 0
        for l,w in lists:
            frame = Frame(self); frame.pack(side=LEFT, expand=YES, fill=BOTH)
            button=Button(frame, text=l, borderwidth=1, relief=RAISED, 
                          command=lambda counter=counter:self._sortBy(counter))
            lb = Listbox(frame, width=w, borderwidth=0, selectborderwidth=0,
                         relief=FLAT, exportselection=FALSE)
            button.pack(fill=X)
            lb.pack(expand=YES, fill=BOTH)
            self.labels.append(button)
            self.lists.append(lb)
	    lb.bind('<Shift-Button-1>', lambda e, s=self: s._select(e.y, shiftPressed=True))
	    lb.bind('<Control-Button-1>', lambda e, s=self: s._select(e.y, cntlPressed=True))
            lb.bind('<Button-1>', lambda e, s=self: s._select(e.y))
            lb.bind('<Control-B1-Motion>', lambda e, s=self: s._select(e.y, cntlPressed=True, inMotion=True))
            lb.bind('<B1-Motion>', lambda e, s=self: s._select(e.y, inMotion=True))
            lb.bind('<Leave>', lambda e: 'break')
            lb.bind('<B2-Motion>', lambda e, s=self: s._b2motion(e.x, e.y))
            lb.bind('<Button-2>', lambda e, s=self: s._button2(e.x, e.y))
            counter = counter + 1
#        frame = Frame(self); frame.pack(side=LEFT, fill=Y)
#        # main label?
#        Label(frame, borderwidth=1, relief=RAISED).pack(fill=X)
#        sb = Scrollbar(frame, orient=VERTICAL, command=self._scroll)
#        sb.pack(side='left',expand=YES, fill=Y)
        self.lists[0]['yscrollcommand']=sb.set

    def _null(self): return

    def _sortBy(self, column):
        """ Sort by a given column. """

        if column == self.sortedBy:
            direction = -1 * self.direction
        else:
            direction = 1

        elements = self.get(0, END)
        self.delete(0, END)
        elements.sort(lambda x, y: self._sortAssist(column, direction, x, y))
        self.insert(END, *elements)

        self.sortedBy = column
        self.direction = direction

    def _sortAssist(self, column, direction, x, y):
        c = cmp(x[column], y[column])
        if c:
            return direction * c
        else:
            return direction * cmp(x, y)


# <--- end class SortedMultiListBox

#if __name__ == '__main__':
#    tk = Tk()
#    Label(tk, text='MultiListBox').pack()
#    mlb = MultiListBox(tk, (('Subject', 40), ('Sender', 20), ('Date', 10)))
#    for i in range(1000):
#	mlb.insert(END, ('Important Message: %d' % i, 'John Doe', '10/10/%04d' % (1900+i)))
#    mlb.pack(expand=YES,fill=BOTH)
#    tk.mainloop()
