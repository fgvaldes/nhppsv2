# Standard Python imports
import getopt
from sys import exit, argv, stderr
from string import split
import socket, os

# Pipeline specific imports
from pipeutil import *
#from pipeutil import usage, getNSproxies, USER, NSGROUP, NODE

#import utils

VERBOSE = False

def update (pipeApp, pipe, dataset, stage, state, module, host, user, prefix='osf_update'):
    
    """
    update ...
    """
    
    # The only required vars are dataset and state AS WELL AS:
    # at least one of stage or module, we better have those!
    if (not dataset or not state or not (stage or module)):
        return -1
    
    nm = os.environ ['NHPPS_PORT_NM']
    proxies = getNSproxies (group=NSGROUP, pipeApp=pipeApp, pipe=pipe, host=host, user=user, nm=nm, prefix=prefix)
    
    err = 1
    for proxy in proxies:
        osfs = None
        try:
            # Check if the pipeline is working on the dataset
            # of it is, then osfs will not be empty
            osfs = proxy.osfBB (dataset, module, '', '', False, False, 'OSFFMT')
        except:
            continue
        
        if osfs:
            err = proxy.osfUpdate (dataset, module, stage, state)
            break
        
        else:
            if (nm and pipe and pipeApp and NSGROUP and (host or len (proxies) == 1)):
                err = proxy.osfUpdate (dataset, module, stage, state)
                break
            pass
        pass
    
    if err:
        print ("Unable to find or create a matching dataset on the requested pipeline")
        pass
    
    return err


def create (pipeApp, pipe, dataset, stage, state, module, host, user, prefix='osf_create'):

    """
    create - Creates or updates a specified dataset.

    A call to osf_create guarantees that the host, user, pipe, nm, and NSGROUP
    values are specified. This means that a call to osf_update will lookup
    the Pyro Proxy to only the pipeline on the specified host.
    When that proxy is accessed, if the dataset is not currently in the
    pipelines blackboard, it is inserted.
    """

    # The only required vars are pipe, dataset and state we better have those!
    if not pipeApp or not pipe or not dataset or not state or not host or not user:
        return -1

    """
    Use update to do the work
    NOTE: because we are passing the host and user, if the osf entry does
    not already exist, osf_update will be able to create the entry
    OSF_UPDATE needs a unique host, user, pipe, nm, and NSGROUP to create an
    entry, the regular osf_update call does not gurantee a unique
    host though.
    """
    
    return update (pipeApp, pipe, dataset, stage, state, module, host, user, prefix=prefix)


def wait (pipeApp, pipe, dataset, stage, state, module, host, user, prefix='osf_wait'):
    
    """
    wait ...
    """
    
    # The only required vars are dataset and state AS WELL AS:
    # at least one of stage or module, we better have those!
    if (not dataset or not state or not (stage or module)):
        return -1
    
    nm = os.environ ['NHPPS_PORT_NM']
    proxies = getNSproxies (group=NSGROUP, pipeApp=pipeApp, pipe=pipe, host=host, user=user, nm=nm, prefix=prefix)
    
    err = 1
    for proxy in proxies:
        osfs = None
        try:
            # Check if the pipeline is working on the dataset
            # of it is, then osfs will not be empty
            osfs = proxy.osfBB (dataset, module, '', '', False, False, 'OSFFMT')
        except:
            continue
        
        if osfs:
            err = proxy.osfWait (dataset, module, state)
            break
        
        else:
            if (nm and pipe and pipeApp and NSGROUP and (host or len (proxies) == 1)):
                err = proxy.osfWait (dataset, module, state)
                break
            pass
        pass
    
    if err:
        print ("Unable to find or create a matching dataset on the requested pipeline")
        pass
    
    return err


def bb (pipeApp, pipe, dataset, stage, state, module, host, open, closed, user, format='OSFFMT1', prefix='BB'):

    """
    bb - dump the blackboard
    """

    nm = os.environ ['NHPPS_PORT_NM']
    proxies = getNSproxies (group=NSGROUP, pipeApp=pipeApp, pipe=pipe, host=host, user=user, nm=nm, prefix=prefix)

    for proxy in proxies:
        osfs = []
        try:
            osfs = proxy.osfBB (dataset, module, stage, state, open, closed, format)
        except:
            # It could be that the ns has stale entries for pipelines which 
            # have crashed and not cleaned up their entries in time.
            continue
        
        # Print out the result
        for osf in osfs:
            print (osf)
            pass
        pass
    
    return 0


def test (pipeApp, pipe, dataset, stage, state, module, host, open, closed, user, prefix='osf_test'):

    """
    test - this is depricated, use bb
    """

    nm = os.environ ['NHPPS_PORT_NM']
    proxies = getNSproxies (group=NSGROUP, pipeApp=pipeApp, pipe=pipe, host=host, user=user, nm=nm, prefix=prefix)

    for proxy in proxies:
        osfs = []
        try:
            osfs = proxy.osfBB (dataset, module, stage, state, open, closed, 'OSFFMT')
        except:
            # It could be that the ns has stale entries for pipelines which 
            # have crashed and not cleaned up their entries in time.
            continue
        
        # Print out the result
        for osf in osfs:
            print (osf)
            pass
        pass
    
    return 0


def open (pipeApp, pipe, dataset, stage, state, module, host, user, prefix='osf_open'):

    """
    open
    """
    
    # The only required var is dataset and we better have it!
    if not dataset:
        return -1

    # Get a list of the Pyro NameServer proxies to objects matching our criteria
    nm = os.environ ['NHPPS_PORT_NM']
    proxyList = getNSproxies (group=NSGROUP, pipeApp=pipeApp, pipe=pipe, user=user, host=host, nm=nm, prefix=prefix)

    err = 1
    for proxy in proxyList:
        osfs = None
        try:
            # Check if the object is working on the dataset and is open
            osfs = proxy.osfBB (dataset, None, None, None, False, False, 'OSFFMT')
        except:
            continue

        if osfs:
            # If it is, open the dataset
            err = proxy.osfOpen (dataset)
            break

    if err:
        print "Unable to open the specified dataset"

    return err


def close (pipeApp, pipe, dataset, stage, state, module, host, user, prefix='osf_close'):

    """
    close
    """
    
    # The only required var is dataset and we better have it!
    if (not dataset):
        return -1
    
    # Get a list of the Pyro NameServer proxies to objects matching our criteria
    nm = os.environ ['NHPPS_PORT_NM']
    proxyList = getNSproxies (group=NSGROUP, pipeApp=pipeApp, pipe=pipe, user=user, host=host, nm=nm, prefix=prefix)

    err = 1
    for proxy in proxyList:
        osfs = None
        try:
            # Check if the object is working on the dataset and is open
            osfs = proxy.osfBB (dataset, None, None, None, True, False, 'OSFFMT')
        except:
            continue

        if osfs:
            # If it is, close the dataset
            err = proxy.osfClose (dataset)
            break

    if err:
        print "Unable to close the specified dataset"

    return err


def remove (pipeApp, pipe, dataset, stage, state, module, host, user, prefix='osf_remove'):

    """
    remove
    """
    
    # The only required var is dataset and we better have it!
    if (not dataset):
        return -1
    
    # Get a list of the Pyro NameServer proxies to objects matching our criteria
    nm = os.environ ['NHPPS_PORT_NM']
    proxyList = getNSproxies (group=NSGROUP, pipeApp=pipeApp, pipe=pipe, user=user, host=host, nm=nm, prefix=prefix)

    err = 1
    for proxy in proxyList:
        osfs = None
        try:
            # Check if the object is working on the dataset and is open
            osfs = proxy.osfBB (dataset, None, None, None, False, True, 'OSFFMT')
        except:
            continue

        if osfs:
            # If it is, remove the dataset
            err = proxy.osfFinal (dataset)
            break

    if err:
        print "Unable to remove the specified dataset"

    return err

def getArgs (cmdLineArgs = argv[1:]):

    """
    Parses the command line arguments and returns a tuple containing
    the values as follows:
    (pipe, dataset, stage, state, module, host, user)
    """
    
    try:
        opts, args = getopt.getopt (cmdLineArgs, "a:p:f:c:s:m:h:u:P", [])
    except:
        return -1

    pipeApp = None
    pipe    = None
    dataset = None
    stage   = None
    state   = None
    module  = None
    host    = None
    user    = None

    for flag, val in opts:
        if flag == '-a':
            pipeApp = val
        elif flag == '-p':
            pipe = val
        elif flag == '-f':
            dataset = val
        elif flag == '-c':
            try:
                stage = int (val)
            except:
                pass
        elif flag == '-s':
            state = val
        elif flag == '-m':
            module = val
        elif flag == '-h':
            host = val
        elif flag == '-u':
            user = val

    # Do minor processing on the arguments:

    # For historical reasons, pipe might have a spurious .path extension!
    if pipe: pipe = pipe.split ('.')[0]

    # Make sure the node name only contains the node name, not
    # the domain name
    if host: host = host.split ('.')[0]

    # If module is specified, it takes precedence over stage number
    if module: stage = None
    
    return (pipeApp, pipe, dataset, stage, state, module, host, user)


def printUsage (useString):

    """
    Prints the usage string and exits with status -1
    """

    usage (useString)

    exit (-1)


#def pyro_Helper (pipe, host, user, func):
#    nm = os.environ ['NHPPS_PORT_NM']
#    proxies = getNSproxies (group=NSGROUP, pipe=pipe, host=host, user=user, nm=nm, prefix='osf_%s:Pyro' % func)
#    funcCall = 'proxy.osf%s ()' % (func.strip ()).capitalize ()
#    retVal = 0
#    for proxy in proxies:
#        try:
#            retVal = retVal + eval (funcCall)
#        except:
#            continue
#    return retVal
#
#def halt_Pyro (pipe, host, user, prefix='osf_halt:Pyro'):
#    return pyro_Helper (pipe, host, user, 'halt')
#
#def resume_Pyro (pipe, host, user, prefix='osf_resume:Pyro'):
#    return pyro_Helper (pipe, host, user, 'resume')
#
#def step_Pyro (pipe, host, user, prefix='osf_step:Pyro'):
#    return pyro_Helper (pipe, host, user, 'step')


def NM_Command (pipeApp, pipe, user, node, port, command):

    if not command['PIPELINES']:
        command['PIPELINES'] = ''

    osf_cmd = 'osf_' + command['COMMAND'].split ('_')[0]

    packet = composePacket (command)
    if VERBOSE: print packet, node, port
    try:
        response = parsePacket (sendPacketTo (packet, node, port, True, 10.,False))
        if VERBOSE: print response
        try:
            status = response['status']
            list = response['list'].split ()
        except:
            return -3
        retVal = 0
        for pds in list:
            p, ds = pds.split (':')
            ds = int (ds)
            if ds > 0:
                retVal += ds
        return retVal
    except:
        print sys.exc_type, sys.exc_value
        return -6


def resume_NM (pipeApp, pipe, user, node, port):

    COMMAND = {  'COMMAND':'resume_pipes',
               'PIPELINES':pipe,
                 'PIPEAPP':pipeApp,
                    'USER':user
              }

    return NM_Command (pipeApp=pipeApp, pipe=pipe, user=user, node=node, port=port, command=COMMAND)


def halt_NM (pipeApp, pipe, user, node, port):

    COMMAND = {  'COMMAND':'halt_pipes',
               'PIPELINES':pipe,
                 'PIPEAPP':pipeApp,
                    'USER':user
              }
    
    return NM_Command (pipeApp=pipeApp, pipe=pipe, user=user, node=node, port=port, command=COMMAND)


def step_NM (pipeApp, pipe, user, node, port):

    COMMAND = {  'COMMAND':'step_pipe',
               'PIPELINES':pipe,
                 'PIPEAPP':pipeApp,
                    'USER':user
              }
    
    return NM_Command (pipeApp=pipeApp, pipe=pipe, user=user, node=node, port=port, command=COMMAND)


def DSCount_NM (pipeApp, pipe, user, node, port):

    COMMAND = {  'COMMAND':'getOpenDatasetCount_pipes',
               'PIPELINES':pipe,
                 'PIPEAPP':pipeApp,
                    'USER':user
              }

    return NM_Command (pipeApp=pipeApp, pipe=pipe, user=user, node=node, port=port, command=COMMAND)


def NMhelper (pipeApp, pipe, host, user, func):
    """
    Calls the node manager helper function func to perform a command on the pipeline
    """

    # Get the node managers...
    try:
        NP = getNodeManagers (host)
    except:
        # We aren't able to access the local node manager (it's down)
        fatalError (err="Local node manager appears to be down.", exitCode=-1)
    if not NP.keys ():
        fatalError (err="Node Manager reports no nodes are running.", exitCode=-2)

    # Check that the specified host is known
    if host:
        if host not in NP.keys ():
            stderr.write ("Node Manager did not report information for node: %s\n" % host)
            return -5
        else:
            return func (pipeApp=pipeApp, pipe=pipe, user=user, node=host, port=NP[host])
    retVal = 0
    for node in NP.keys ():
        result = func (pipeApp=pipeApp, pipe=pipe, user=user, node=node, port=NP[node])
        if result > 0:
            retVal += result
    return retVal


def dsCount (pipeApp, pipe, dataset, stage, state, module, host, user, prefix='osf_dsCount'):
    return NMhelper (pipeApp=pipeApp, pipe=pipe, host=host, user=user, func=DSCount_NM)


def halt (pipeApp=None, pipe=None, dataset=None, stage=None, state=None, module=None, host=None, user=None, prefix='osf_halt'):
    
    """
    halt.
    Contacts the local Node Manager, if one is running, the node manager
    version (which keeps track of which pipelines are active on a node)
    of osfHalt (halt_pipes) will be called.
    """
    return NMhelper (pipeApp=pipeApp, pipe=pipe, host=host, user=user, func=halt_NM)


def resume (pipeApp=None, pipe=None, dataset=None, stage=None, state=None, module=None, host=None, user=None, prefix='osf_resume'):

    """
    Resume a halted pipeline.
    Contacts the local Node Manager. If it is running, the node manager
    version (which keeps track of which pipelines are active on a node)
    of osfResume (resume_pipes) will be called.
    """

    return NMhelper (pipeApp=pipeApp, pipe=pipe, host=host, user=user, func=resume_NM)


def step (pipeApp=None, pipe=None, dataset=None, stage=None, state=None, module=None, host=None, user=None, prefix='osf_step'):

    """
    Step a pipeline.
    Contacts the local Node Manager. If it is running, the node manager
    version (which keeps track of which pipelines are active on a node)
    of osfStep (step_pipe) will be called.
    """

    return NMhelper (pipeApp=pipeApp, pipe=pipe, host=host, user=user, func=step_NM)

