# 
# Native Action Plugin
# 

"""
Native Action API

Each routine name has to have the form

    native<XML Tag name>    (e.g. nativeOSFUpdate)

Each routine will be invoked with at least 3 and not more than 4 
arguments:
    actionMgr
    argList
    env
    successExitCode    (optional)

Each routine has to return an exit code, making sure that the code for 
success has to be equal to successExitCode (defaults to 0).

"""


import copy
import os
import sys

PREFIX = 'Plugins::actions'

def subEnvVars (instream, env):
    
    """
    returns the instream string with all $XYZ 'XYZ' environment
    variables substituted with the value of XYZ in the env hash.
    """
    
    index = 0
    while True:
        index = instream.find ('$', index)
        if index >= 0:
            envVar = None
            for var in env:
                varLen = len (var)
                if instream [index+1:index+1+varLen] == var:
                    if not envVar:
                        envVar = var
                    elif varLen > len (envVar):
                        envVar = var
            if envVar:
                # We found a matching environment variable with maximal length
                instream = instream.replace ('$%s' % envVar, str (env [envVar]))
            else:
                index += 1
        else:
            break
    return instream

def warning (prefix='', msg='something bad happened'):
    if (prefix):
        sys.stderr.write ('%s Warning: %s\n' % (prefix, msg))
    else:
        sys.stderr.write ('Warning: %s\n' % (msg))
    return

def nativeOSFUpdate (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard.
    
    Input
    A list of arguments of the format:
        [module_name, flag]
    
    Return
    0   success
    1   failure
    """
    moduleName = str (argList[0])
    flag = subEnvVars (str (argList[1]), env)
    d = env ['OSF_DATASET']
    
    #print ('nativeOSFUpdate (%s, %s, %s)' % (d, moduleName, flag))
    
    # Update the OSF BB
    actionMgr.osfUpdate (dataset=d, 
                         module=moduleName, 
                         stage=None,
                         flag=flag)
    return (successExitCode)

def nativeOSFWait (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard.
    
    Input
    A list of arguments of the format:
        [module_name, flag]
    
    Return
    0   success
    1   failure
    """

    d = env ['OSF_DATASET']
    module = str (argList[0])
    flag = subEnvVars (str (argList[1]), env)
    
    #print ('nativeOSFWait (%s, %s, %s)' % (d, module, flag))
    
    # Update the OSF BB
    actionMgr.osfWait (dataset=d, 
                         module=module, 
                         flag=flag)
    return (successExitCode)

def nativeOSFClose (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard.
    
    Input
    A list of arguments of the format:
        [module_name]
    
    Return
    0   success
    1   failure
    """
    d = env['OSF_DATASET']
    
    # Update the OSF BB
    actionMgr.osfClose (dataset=d)
    return (successExitCode)

def nativeOSFUpdateParent (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard. using the
    parent dataset name.  This is tied to the NOAO dataset
    conventions.  The parent dataset is defined by stripping the final
    hyphen in the current dataset.
    
    Input
    A list of arguments of the format:
        [module_name, flag]
    
    Return
    0   success
    1   failure
    """
    moduleName = str (argList[0])
    flag = subEnvVars (str (argList[1]), env)

    d = env ['OSF_DATASET']
    p = d.split('-')
    n = len(p)
    if n > 1:
        d = '-'.join(p[:n-1])
    
    #print ('nativeOSFUpdate (%s, %s, %s)' % (d, moduleName, flag))
    
    # Update the OSF BB
    actionMgr.osfUpdate (dataset=d, 
                         module=moduleName, 
                         stage=None,
                         flag=flag)
    return (successExitCode)

def nativeOSFClose (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard.
    
    Input
    A list of arguments of the format:
        [module_name]
    
    Return
    0   success
    1   failure
    """
    d = env['OSF_DATASET']
    
    # Update the OSF BB
    actionMgr.osfClose (dataset=d)
    return (successExitCode)

def nativeOSFOpen (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard.
    
    Input
    A list of arguments of the format:
        []
    
    Return
    0   success
    1   failure
    """
    d = env['OSF_DATASET']
    
    # Update the OSF BB
    actionMgr.osfOpen (dataset=d)
    return (successExitCode)

def nativeOSFConditRemove (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard.

    Input
    A list of arguments of the format:
        [module_name]

    Return
    0   success
    1   failure
    """
    d = env['OSF_DATASET']

    # Update the OSF BB
    exitCode = 0
    actionMgr.osfConditRemove (dataset=d)
    return (successExitCode)

def nativeOSFFinal (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to interact with the OSF blackboard.

    Input
    A list of arguments of the format:
        [module_name]

    Return
    0   success
    1   failure
    """
    d = env['OSF_DATASET']

    # Update the OSF BB
    exitCode = 0
    actionMgr.osfFinalClose (dataset=d)
    return (successExitCode)
    
def nativeRenameTrigger (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to change the name of the trigger file. We know 
    where the active trigger file is located!
    
    Input
    A list of arguments of the format:
        [module_name, flag]
    
    Return
    0   success
    1   failure
    """
    # TODO: keep track of the trigger names as they are renamed so that users are not forced to specify the oldName in the XML file
    
    # Fetch the full trigger path from env
    try:
        triggerPath = env ['EVENT_FULL_NAME']
    except:
        warning (PREFIX, 'the environment does not have the EVENT_FULL_NAME.')
        try:
            retVal = int (successExitCode) - 1
        except:
            retVal = 0
        return retVal
    
    if not triggerPath:
        warning (PREFIX, 'RenameTrigger called with null EVENT_FULL_NAME.')
    
    # Extract the directory name
    triggerDir = os.path.dirname (triggerPath)
    
    # Most people are confused by $EVENT_NAME and $OSF_DATASET...
    env = copy.copy (env)
    if not env ['OSF_DATASET']:
        env ['OSF_DATASET'] = env ['EVENT_NAME']
    
    # Do variable substitution on argList. Remember that in this 
    # case argList = [oldName, newName]
    oldName, newName = subEnvVars (
        '%s\n%s' % (argList [0], argList [1]), env).split ('\n')
    #warning(PREFIX,'Renaming trigger file '  + oldName +' -> ' + newName)
    
    # Now rename the trigger file.  Leave exception handling to higher level.
    # An exception may happen when using shared disks.
    oldPath = os.path.join (triggerDir, oldName)
    newPath = os.path.join (triggerDir, newName)
    os.rename (oldPath, newPath)

    return (successExitCode)

def nativeUpdateTrigger (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to change the name of the trigger file. We know
    where the active trigger file is located!
   
    Input
    A list of arguments of the format:
        [module_name, flag]
   
    Return
    0   success
    1   failure
    """
   
    # Fetch the full trigger path from env
    try:
        trigDir = os.environ ['NHPPS_DIR_TRIG']
    except:
        warning (PREFIX, 'the environment does not have the NHPPS_DIR_TRIG.')
        try:
            retVal = int (successExitCode) - 1
        except:
            retVal = 0
        return retVal
   
    ds = env['OSF_DATASET']
   
    if not trigDir:
        warning (PREFIX, 'UpdateTrigger called with null NHPPS_DIR_TRIG.')
        try:
            retVal = int (successExitCode) - 1
        except:
            retVal = 0
        return retVal
   
    oldName = ds + argList[0]
    newName = ds + argList[1]

    warning(PREFIX,'Updateting trigger file '  + oldName +' -> ' + newName);

    # Now rename the trigger file
    oldPath = os.path.join (trigDir, oldName)
    newPath = os.path.join (trigDir, newName)
    os.rename (oldPath, newPath)

    return (successExitCode)


def nativeRemoveTrigger (actionMgr, argList, env, successExitCode=0):
    """
    Native routine to remove the trigger file. We know 
    where the active trigger file is located!
    
    Input
    A list of arguments of teh format:
        [module_name, flag]
    
    Return
    0   success
    1   failure
    """
    # TODO: keep track of the trigger names as they are renamed so that users are not forced to specify the oldName in the XML file
    
    # Fetch the full trigger path from env
    try:
        triggerPath = env['EVENT_FULL_NAME']
    except:
        warning (PREFIX, 
                 'the environment does not have the EVENT_FULL_NAME.')
        try:
            retVal = int (successExitCode) - 1
        except:
            retVal = 0
        return retVal
        #return (successExitCode - 1)
    
    if (not triggerPath):
        warning (PREFIX, 
                'RenameTrigger called with null EVENT_FULL_NAME.')
    
    # Extract the directory name
    triggerDir = os.path.dirname (triggerPath)
    
    # Most people are confused bt $EVENT_NAME and $OSF_DATASET...
    env = copy.copy (env)
    if not env ['OSF_DATASET']:
        env ['OSF_DATASET'] = env ['EVENT_NAME']
    
    oldName = subEnvVars ('%s' % (argList [0]), env)
    
    # Now rename the trigger file
    oldPath = os.path.join (triggerDir, oldName)
    try:
        os.remove (oldPath)
    except:
        warning (PREFIX, 'failed removing trigger file %s' % (oldPath))
        try:
            retVal = int (successExitCode) - 1
        except:
            retVal = 0
        return retVal
    return (successExitCode)
