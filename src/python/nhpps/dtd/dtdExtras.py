from EaseXML import *

import datetime
from time import strptime

class NHPPS_XMLObject (XMLObject):
    """
    Using _unicodeOutput = False prevents the string data within the
    NHPPS XML object trees from being output in unicode. This is
    desireable as the NHPPS package desires plain str objects.
    """
    _unicodeOutput = False
    pass

def validFlag (flag):
    """
    Checks if a flag is valid:
    1) single character
    2) only letters or the _ character
    """
    return (flag.isalpha () and len (flag) == 1) or flag == '_' or flag == '!'

def validModule (module):
    """
    Checks if a module is valid:
    1) only letters and numbers
    2) OR the variable ${self.name}
    """
    return module.isalnum () or module == '${self.name}' or module == 'self.name'

def validOp (op):
    """
    Checks if the operator is valid:
    1) ==, <=, >=, !=, <, >
    """
    return op in ['==', '<=', '>=', '<', '>', '!=']

class attr_OSFUpdate (StringAttribute):
    def checkType (self, val):
        result = StringAttribute.checkType (self, val)
        if result is not False:
            # Check that we have:
            # 1) a module name (letters and numbers)
            # 2) a flag (single letter or _)
            try:
                (m,f) = result.split ()
                if not validModule(m) or not validFlag(f):
                    result = False
                    pass
                pass
            except:
                result = False
                pass
            pass
        return result
    pass

class attr_OSFUpdateParent (StringAttribute):
    def checkType (self, val):
        result = StringAttribute.checkType (self, val)
        if result is not False:
            # Check that we have:
            # 1) a module name (letters and numbers)
            # 2) a flag (single letter or _)
            try:
                (m,f) = result.split ()
                if not validModule(m) or not validFlag(f):
                    result = False
                    pass
                pass
            except:
                result = False
                pass
            pass
        return result
    pass

class attr_OSFWait (StringAttribute):
    def checkType (self, val):
        result = StringAttribute.checkType (self, val)
        if result is not False:
            # Check that we have:
            # 1) a module name (letters and numbers)
            # 2) a flag (single letter or _)
            try:
                (m,f) = result.split ()
                if not validModule(m) or not validFlag(f):
                    result = False
                    pass
                pass
            except:
                result = False
                pass
            pass
        return result
    pass

class attr_OSFRequirement (StringAttribute):
    def checkType (self, val):
        result = StringAttribute.checkType (self, val)
        if result is not False:
            # Check if the string matches the format specified
            # and if the operator is =, change it to ==.
            try:
                (m,o,f) = result.split ()
                if o == '=':
                    o = '=='
                    pass
                if not validModule (m) or not validOp (o) or not validFlag (f):
                    result = False
                else:
                    result = '%s %s %s' % (m,o,f)
                    pass
                pass
            except:
                result = False
                pass
            pass
        return result
    
    def xmlrepr(self, parentInstance=None):
        val = self.getValue()
        if self.isOptional() and val == self.getDefaultValue():
            result = ''
        elif val is not None:
            (m,o,f) = val.split ()
            if o == '=':
                o = '=='
                pass
            result = '%s="%s %s %s"' % (self.getName (), m, o, f)
        else:
            result = ''
            pass
        if parentInstance:
            result = utils.customUnicode(result, parentInstance._encoding)
            pass
        return result
    pass

class attr_String (StringAttribute):
    def xmlrepr(self, parentInstance=None):
        val = self.getValue ()
        #if self.isOptional() and val == self.getDefaultValue():
        #    result = ''
        #el
        if val is not None:
            result = '%s="%s"' % (self.getName (), val)
        else:
            result = ''
            pass
        if parentInstance:
            result = utils.customUnicode(result, parentInstance._encoding)
            pass
        return result
    pass

class RawTypeAttribute (CDATAttribute):
    def setValue (self, value):
        if isinstance (value, self._dataType) or value == None:
            CDATAttribute.setValue (self, value)
            return
        else:
            result = self.cast (value)
            if isinstance (result, self._dataType):
                CDATAttribute.setValue (self, result)
                return
            pass
        raise TypeError ('Incorrect type for attribute value. Needs %s, got %s.' % (self._dataType, type(value)))
    
    def setDefaultValue (self, default):
        if isinstance (default, self._dataType) or default == None:
            CDATAttribute.setDefaultValue (self, default)
            return
        else:
            result = self.cast (default)
            if isinstance (result, self._dataType):
                CDATAttribute.setDefaultValue (self, result)
                return
            pass
        raise TypeError ('Incorrect type for default attribute value. Needs %s, got %s.' % (self._dataType, type(default)))
    
    def checkType (self, value):
        result = value
        if not isinstance (value, self._dataType):
            result = self.cast (value)
            if result is None:
                result = False
                pass
            pass
        return result
    
    def xmlrepr(self, parentInstance=None):
        #val = self.getValue ()
        #if self.isOptional() and val == self.getDefaultValue():
        #    result = ''
        #elif val is not None:
        #    result = '%s="%s"' % (self.getName (), self.toString ())
        #else:
        #    result = ''
        result = self.toString ()
        if parentInstance:
            result = utils.customUnicode(result, parentInstance._encoding)
            pass
        return result
    
    def getValueFromDom (self, dom, attrName, **kw):
        """
        needed to overwrite this function to remove
        the string.strip call as the value we store
        is not a string.
        """
        xmlAttrNode = dom.getAttributeNode(attrName)
        try:
            value = xmlAttrNode.value
        except AttributeError:
            if not self.isOptional():
                raise RequiredNodeError(self.getName())
            else:
                value = self.getDefaultValue()
                pass
            pass
        return value
    pass

class attr_Float (RawTypeAttribute):
    
    _dataType = float
    
    def __init__ (self, default=None, optional=False):
        if default is None:
            default = 0.0
            pass
        RawTypeAttribute.__init__(self, default=default, optional=optional)
        return
    
    def cast (self, value):
        try:
            result = self._dataType (value)
        except:
            result = None
            pass
        return result
    
    def toString (self):
        return '%s="%.2f"' % (self.getName (), self.getValue ())
    pass

class attr_Integer (RawTypeAttribute):
    
    _dataType = int
    
    def __init__ (self, default=None, optional=False):
        if default is None:
            default = 0
            pass
        RawTypeAttribute.__init__ (self, default=default, optional=optional)
        return
    
    def cast (self, value):
        try:
            result = self._dataType (value)
        except:
            result = None
            pass
        return result
    
    def toString (self):
        return '%s="%d"' % (self.getName (), self.getValue ())
    pass

class attr_Bool (RawTypeAttribute):
    """
    Stores Boolean values as True or None.
    
    The implementation of EaseXML (on which these custom attributes are based)
    requires that an attribute not be False, therefore we cannot store False,
    and instead use None to represent a False value. Within the code then, we
    simply check is the attribute is True.
    """
    
    _dataType = bool
    
    def __init__ (self, default=None, optional=False):
        RawTypeAttribute.__init__ (self, default=default, optional=optional)
        return
    
    def cast (self, value):
        result = False
        if str(value) in ['True','T','Yes','Y','true','t','yes','y']:
            result = True
        elif value == False or str(value) in ['False','F','No','N','false','f','no','n']:
            result = None
            pass
        return result
    
    def toString (self):
        if self.getValue ():
            return '%s="True"' % (self.getName ())
        else:
            return '%s="False"' % (self.getName ())
        pass
    
    def setValue (self, value):
        if value == True or value == None:
            CDATAttribute.setValue (self, value)
            return
        else:
            result = self.cast (value)
            if result == True or result == None:
                CDATAttribute.setValue (self, result)
                return
            pass
        raise TypeError ('Incorrect type for attribute value. Needs %s, got %s.' % (self._dataType, type(value)))
    
    def setDefaultValue (self, default):
        if default == True or default == None:
            CDATAttribute.setDefaultValue (self, default)
            return
        else:
            result = self.cast (default)
            if result == True or result == None:
                CDATAttribute.setDefaultValue (self, result)
                return
            pass
        raise TypeError ('Incorrect type for default attribute value. Needs %s, got %s.' % (self._dataType, type(default)))
    
    def checkType (self, value):
        result = value
        if not (value == True or value == None):
            result = self.cast (value)
            pass
        return result
    pass

class attr_DiskSpace (RawTypeAttribute):
    
    _dataType = long
    
    def __init__ (self, default=None, optional=False):
        if default is None:
            default = 0
            pass
        RawTypeAttribute.__init__ (self, default=default, optional=optional)
        return
    
    def cast (self, val):
        if isinstance (val, int):
            return self._dataType (val)
        elif isinstance (val, (str, unicode)):
            factor = 1
            if val[-1].isalpha () and val[-1].lower () in ['k', 'm', 'g']:
                if val[-1].lower () == 'm':
                    factor = 1024
                elif val[-1].lower () == 'g':
                    factor = 1024*1024
                    pass
                result = val[:-1]
            else:
                result = val
                pass
            try:
                result = abs (long (eval (result))) * factor
            except:
                result = None
                pass
            return result
        pass
    
    def toString (self):
        return '%s="%dk"' % (self.getName (), self.getValue ())
    pass

class attr_Interval (RawTypeAttribute):
    """
    Custom Interval Attribute ensures that the format of the interval
    attribute of the TimeRequirement and MaxExec elements are valid.
    """
    
    _dataType = datetime.timedelta
    
    def __init__ (self, default=None, optional=False):
        if default is None:
            default = '0:0:0:0'
            pass
        RawTypeAttribute.__init__ (self, default=default, optional=optional)
        return
    
    def cast (self, val):
        try:
            (d,h,m,s) = val.split (':')
            result = datetime.timedelta (days=int(d), hours=int(h), minutes=int(m), seconds=int(s))
        except:
            result = None
            pass
        return result
    
    def toString (self):
        val = self.getValue ()
        secs = val.seconds
        hours = (secs - (secs % (60*60)))
        secs -= hours
        mins = (secs - secs % 60)
        secs -= mins
        return '%s="%d:%d:%d:%d"' % (self.getName (), val.days, hours / (60*60), mins / 60, secs)
    
    def seconds (self):
        val = self.getValue ()
        seconds = val.seconds + (60*60*24*val.days)
        return seconds
    
    def getDefaultValue (self):
        val = self._defaultValue
        secs = val.seconds
        hours = (secs - (secs % (60*60)))
        secs -= hours
        mins = (secs - secs % 60)
        secs -= mins
        return '%d:%d:%d:%d' % (val.days, hours / (60*60), mins / 60, secs)
    pass

class DateTimeAttribute (RawTypeAttribute):
    
    _dataType = datetime.datetime
    
    def __init__ (self, default=None, optional=False):
        RawTypeAttribute.__init__ (self, default=default, optional=optional)
        return
    
    def cast (self, val):
        # Check if the string matches the format specified
        try:
            (yy, mm, dd, h, m, s) = strptime (val, '%Y-%m-%dT%H:%M:%S')[:-3]
            result = datetime.datetime (yy, mm, dd, h, m, s)
        except:
            result = None
            pass
        return result
    
    def toString (self):
        return '%s="%s"' % (self.getName (), self.getValue ().isoformat ().split ('.')[0])
    pass

class attr_StartDate (DateTimeAttribute):
    """
    Custom class to define a default value for the start
    attribute of the TimeRequirement class
    """
    def __init__ (self, default=None, optional=False):
        if default is None:
            default = datetime.datetime.today ()
            pass
        DateTimeAttribute.__init__ (self, default=default, optional=optional)
        return
    pass

class attr_EndDate (DateTimeAttribute):
    """
    Custom class to define a default value for the end
    attribute of the TimeRequirement class
    """
    def __init__ (self, default=None, optional=False):
        if default is None:
            default=datetime.datetime.max
            pass
        DateTimeAttribute.__init__(self, default=default, optional=optional)
        return
    pass

class NHPPS_ArgvXMLObject (NHPPS_XMLObject):
    """
    Defines a base class for an object which requires an argument.
    """
    argv = attr_String (optional=False)
    
    _attrsOrder = ['argv']
    pass
