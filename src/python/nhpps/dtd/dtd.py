"""
NHPPS 2.0 Processing Model

This is the EaseXML version of the PDL DTD.
"""

import os
from EaseXML import *
from dtdExtras import * # attr_XYZ...

__all__ = [ 'System',
           'Directories',
	   'MaxExec',
	   'MinDisk',
           'ExitCodes',
           'Template',
           'Pipeline',
           'Module',
           'Trigger',
           'TimeRequirement',
           'FileRequirement',
           'OSFRequirement',
           'PreProcAction',
           'ProcAction',
           'PostProcAction',
           'ExitCode',
           'Foreign',
           'PlugIn',
           'StartPipe',
           'CallPipe',
           'WaitPipe',
           'DonePipe',
           'OSFUpdate',
           'OSFUpdateParent',
           'OSFWait',
           'OSFClose',
           'OSFConditRemove',
	   'OSFFinal',
           'OSFMessage',
           'OSFOpen',
           'RemoveTrigger',
	   'UpdateTrigger',
           'RenameTrigger']


########################### System ################################

class System (NHPPS_XMLObject):
    # Children
    Description     = TextNode (optional=False)
    Directories     = ItemNode (optional=False, itemType='Directories')
    MaxExec         = ItemNode (optional=True,  itemType='MaxExec')
    MinDisk         = ItemNode (optional=True,  itemType='MinDisk')
    ExitCodes       = ListNode (optional=True,  itemType='ExitCodes')
    Template	    = ListNode (optional=True,  itemType='Template')
    
    _nodesOrder = ['Description', 'Directories', 'MaxExec', 'MinDisk', 'ExitCodes', 'Template']

class Directories (NHPPS_XMLObject):
    # Attributes
    root  = attr_String (optional=True, default='ENV(NHPPS_DATA)/CAPS(${Pipeline.system}/${Pipeline.system}_${Pipeline.name})')
    if 'NHPPS_TRIG' in os.environ:
	default = os.environ ['NHPPS_TRIG']
    else:
	default = '${Pipeline.Directories.root}/trig'
    trig = attr_String (optional=True, default=default)
    input = attr_String (optional=True, default='${Pipeline.Directories.root}/input')
    if 'NHPPS_OUTPUT' in os.environ:
	default = os.environ ['NHPPS_OUTPUT']
    else:
	default = '${Pipeline.Directories.root}/output'
    output = attr_String (optional=True, default=default)
    data = attr_String (optional=True, default='${Pipeline.Directories.root}/data')
    obs   = attr_String (optional=True, default='${Pipeline.Directories.root}/obs')
    error = attr_String (optional=True, default='${Pipeline.Directories.root}/err')
    
    _attrsOrder = ['root', 'trig', 'input', 'output', 'data', 'obs', 'error']

class MaxExec (NHPPS_XMLObject):
    # Attributes
    time   = attr_Interval (optional=False, default='0:0:0:0')
    status = attr_String   (optional=True,  default='0')
    
    _attrsOrder = ['time', 'status']

class MinDisk (NHPPS_XMLObject):
    # Attributes
    space  = attr_DiskSpace (optional=False, default='0')
    status = attr_String    (optional=True, default='0')
    
    _attrsOrder = ['space', 'status']

class Template (NHPPS_XMLObject):
    # Attributes
    type  = attr_String (optional=False)
    
    # Children
    Description     = TextNode (optional=True)
    Triggers        = ListNode (optional=True,  itemType='Trigger')
    MaxExec         = ItemNode (optional=True,  itemType='MaxExec')
    MinDisk         = ItemNode (optional=True,  itemType='MinDisk')
    PreProcAction   = ItemNode (optional=True,  itemType='PreProcAction')
    ProcAction      = ItemNode (optional=True,  itemType='ProcAction')
    PostProcActions = ListNode (optional=True,  itemType='PostProcAction')
    
    _attrsOrder = ['type']

class ExitCodes (NHPPS_XMLObject):
    # Attributes
    id   = attr_String (optional=False)
    code  = attr_Integer (optional=False)
    flag = attr_String (optional=True)
    severity = attr_String (optional=False)
    desc = attr_String (optional=True)
    
    _attrsOrder = ['id', 'code', 'flag', 'severity', 'desc']

########################### Pipeline ################################

class Pipeline (NHPPS_XMLObject):
    # Attributes
    system    = attr_String  (optional=False)
    name      = attr_String  (optional=False)
    poll      = attr_Float   (optional=False)
    maxActive = attr_Integer (optional=True, default=0)
    
    # Children
    Description     = TextNode (optional=False)
    Directories     = ItemNode (optional=True,  itemType='Directories')
    MaxExec         = ItemNode (optional=True,  itemType='MaxExec')
    MinDisk         = ItemNode (optional=True,  itemType='MinDisk')
    ExitCodes       = ListNode (optional=True,  itemType='ExitCodes')
    Template        = ListNode (optional=True,  itemType='Template')
    Modules         = ListNode (optional=False, itemType='Module')
    
    _nodesOrder = ['Description', 'Directories', 'MaxExec', 'MinDisk', 'ExitCodes', 'Template', 'Modules']
    _attrsOrder = ['system', 'name', 'poll', 'maxActive']

########################### Modules #################################

class Module (NHPPS_XMLObject):

    # Attributes
    name      = attr_String  (optional=False)
    type      = attr_String  (optional=True)
    var       = attr_String  (optional=True)
    maxActive = attr_Integer (optional=True, default=0)
    isActive  = attr_Bool    (optional=True, default=True)
    #isHidden  = attr_Bool    (optional=True, default=False)
    
    # Children
    Description     = TextNode (optional=True)
    Triggers        = ListNode (optional=False, itemType='Trigger')
    ProcAction      = ItemNode (optional=False, itemType='ProcAction')
    MaxExec         = ItemNode (optional=True,  itemType='MaxExec')
    MinDisk         = ItemNode (optional=True,  itemType='MinDisk')
    PreProcAction   = ItemNode (optional=True,  itemType='PreProcAction')
    PostProcActions = ListNode (optional=True,  itemType='PostProcAction')
    
    _nodesOrder = ['Description', 'Triggers', 'MaxExec', 'MinDisk', 'PreProcAction', 'ProcAction', 'PostProcActions']
    _attrsOrder = ['name', 'type', 'maxActive', 'isActive']

########################### Triggers #################################

class Trigger (NHPPS_XMLObject):
    # Attributes
    conditional = attr_String (optional=True, default='AND', permittedValues=['AND','OR'])
    
    # Children
    Requirements = ChoiceNode (
        ['TimeRequirement','FileRequirement', 'OSFRequirement'],
        optional=False, noLimit=True, main=True
        )
    
    _attrsOrder = ['conditional']
    _nodesOrder = ['Requirements']

class TimeRequirement (NHPPS_XMLObject):
    # Attributes
    interval = attr_Interval  (optional=False)
    start    = attr_StartDate (optional=True)
    end      = attr_EndDate   (optional=True)
    
    _attrsOrder = ['start', 'end', 'interval']

class FileRequirement (NHPPS_XMLObject):
    # Attributes
    fnPattern = attr_String (optional=False)
    directory = attr_String (optional=True, default='${Pipeline.Directories.trig}')
    
    _attrsOrder = ['directory', 'fnPattern']

class OSFRequirement (NHPPS_XMLObject):
    # Attributes
    argv = attr_OSFRequirement (optional=False)
    
    _attrsOrder = ['argv']

########################### Actions #################################

class ProcAction (NHPPS_XMLObject):
    
    # Children
    Command = ChoiceNode (['Foreign', 
                            'PlugIn',
			    'StartPipe',
			    'CallPipe',
			    'WaitPipe',
			    'DonePipe',
                            'OSFUpdate',
                            'OSFUpdateParent',
                            'OSFWait',
                            'OSFClose',
                            'OSFConditRemove',
			    'OSFFinal',
			    'OSFMessage',
                            'OSFOpen',
                            'RenameTrigger',
			    'UpdateTrigger',
                            'RemoveTrigger'], 
                           optional=False,
                           noLimit=False,main=True)
    
    _attrsOrder = ['none']
    _nodesOrder = ['Command']

class PreProcAction (NHPPS_XMLObject):
    
    # Children
    Commands = ChoiceNode (['Foreign', 
                            'PlugIn',
                            'OSFUpdate',
                            'OSFUpdateParent',
                            'OSFWait',
                            'OSFClose',
                            'OSFConditRemove',
			    'OSFFinal',
			    'OSFMessage',
                            'OSFOpen',
                            'RenameTrigger',
			    'UpdateTrigger',
                            'RemoveTrigger'], 
                           optional=False,
                           noLimit=True, main=True)
    
    _attrsOrder = ['none']
    _nodesOrder = ['Commands']

class PostProcAction (NHPPS_XMLObject):
    
    # Attributes
    isFailure = attr_Bool (optional=True, default=False)
    isGlobal = attr_Bool (optional=True, default=False)
    
    # Children
    exitCodes = ListNode ('ExitCode', optional=False)
    Commands = ChoiceNode (['Foreign', 
                            'PlugIn',
                            'OSFUpdate',
                            'OSFUpdateParent',
                            'OSFWait',
                            'OSFClose',
                            'OSFConditRemove',
			    'OSFFinal',
			    'OSFMessage',
                            'OSFOpen',
                            'RenameTrigger',
			    'UpdateTrigger',
                            'RemoveTrigger'], 
                           optional=False,
                           noLimit=True)
    
    _attrsOrder = ['none']
    _nodesOrder = ['exitCodes', 'Commands']

class ExitCode (NHPPS_XMLObject):
    # Attributes
    val = attr_String (optional=False)
    
    _attrsOrder = ['val']

class Foreign (NHPPS_ArgvXMLObject):
    pass

class PlugIn (NHPPS_ArgvXMLObject):
    pass

class StartPipe (NHPPS_ArgvXMLObject):
    pass

class CallPipe (NHPPS_ArgvXMLObject):
    pass

class WaitPipe (NHPPS_ArgvXMLObject):
    pass

class DonePipe (NHPPS_ArgvXMLObject):
    pass

class RenameTrigger (NHPPS_ArgvXMLObject):
    pass

class UpdateTrigger (NHPPS_ArgvXMLObject):
    pass

class RemoveTrigger (NHPPS_ArgvXMLObject):
    pass

class OSFUpdate (NHPPS_ArgvXMLObject):
    pass

class OSFUpdateParent (NHPPS_ArgvXMLObject):
    pass

class OSFWait (NHPPS_ArgvXMLObject):
    pass

class OSFClose (NHPPS_XMLObject):
    pass

class OSFConditRemove (NHPPS_XMLObject):
    pass

class OSFFinal (NHPPS_XMLObject):
    pass

class OSFMessage (NHPPS_XMLObject):
    pass

class OSFOpen (NHPPS_XMLObject):
    pass
