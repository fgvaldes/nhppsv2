from parsingTools import *
from EaseXML import *
import string, os
import cPickle

__all__ = ['loadNHPPSxml']

def replaceXMLvars (xmldata, modList):
    
    """
    Replaces references to the XML tree, '${xyz}', values and
    executes the ENV, CAPS, and INDEX macros.
    """
    
    index = findXMLvar (xmldata, 'Pipeline')
    while index >= 0:
        variableName  = xmldata[index+2:].split ('}')[0]
        variableValue = getAttribute (variableName, xmldata)
        if variableValue:
            xmldata = xmldata.replace ('${%s}' % (variableName), variableValue)
            index = findXMLvar (xmldata, 'Pipeline')
        else:
            index = findXMLvar (xmldata, 'Pipeline', index + 10)
    
    # Run the macros...
    xmldata = xmldata.split ('\n')
    for lineNum in xrange (len (xmldata)):
        xmldata [lineNum] = evalMACROS (xmldata [lineNum], modList=modList)

    return string.join (xmldata, '\n')

def loadNHPPSxml (pipe, pipeApp=None, path=None):
    
    """
    Loads and parses an XML file which contains the
    description of a pipeline in the NHPPS format.
    """

    # Set the system path:
    # $NHPPS_PIPEAPPSRC/xml/, $NHPPS_PIPESRC/xml/ $NHPPS_PIPESRC/{pipeApp}/xml/
    if not pipeApp and 'NHPPS_SYS_NAME' in os.environ:
	pipeApp = os.environ ['NHPPS_SYS_NAME']
    if pipeApp:
	#print os.eviron ['NHPPS_PIPEAPPSRC']
	syspath = os.path.join (
	    os.environ ['NHPPS_PIPEAPPSRC'], 'xml')
	if not os.path.exists (syspath):
	    syspath = os.path.join (
		os.environ ['NHPPS_PIPESRC'], 'xml')
	if not os.path.exists (syspath):
	    syspath = os.path.join (
		os.environ ['NHPPS_PIPESRC'], pipeApp, 'xml')
    else:
	return None
    
    # Check that a path to the file was specified. If it
    # wasn't use the system path.
    if not path:
        path = syspath
    pipe = pipe.split ('.')[0] # In case there's an extension on the filename.
    xmlFile = os.path.join (path, '%s.xml' % (pipe))

    try:
        pipeline = readXML (xmlFile)
	pipeline = re.sub ('(<Module[^>]*?)/>', '\\1>\n</Module>', pipeline)
	pipeline = re.sub ('>[ 	]*<', '>\n<', pipeline)
        pipeline = pipeline.split ('<Module')
        if not pipeline:
            print 'Unable to read the contents of', xmlFile
    except:
        pipeline = None
    else:
        # Merge the contents of the system level XML file
        # into the pipeline XML files `global` space,
        # including the `Directories` element...
        globalSpace = mergeSystemPipelineFiles (pipeline [0], path, syspath)

	# Replace references to 'pipe' with the pipeline name
	pipe = getAttribute ('Pipeline.name', pipeline[0])
	pipeline[0] = pipeline[0].replace ('${pipe}', pipe)
        
        modList = {}
        index = 1
        for module in pipeline [index:]:
            
            # Restore the '<Module' we took out during the splitting...
            module = '<Module' + module
            
            # Determine the modules name
            modName = moduleName (module)
	    
	    # Extract the vars.
	    var = getAttribute ('Module.var', module)
	    if var:
		varargs = var.split()
            
            # Remove inactive modules
            if not moduleIsActive (module):
                print 'Skipping inactive module:', modName
                pipeline.pop (index)
                continue
            
            # Merge the 'global' values into the module
            module = mergeParentChild (module, globalSpace,
	        closeTag='</Module>', childElement='Module')

	    # Replace references to 'pipe' with the pipeline name
	    module = module.replace ('${pipe}', pipe)

	    # Replace references to 'var'
	    if var:
		module = module.replace ('${var}', var)
		for i in range (len(varargs)):
		    module = module.replace ('${var[%d]}'%(i+1), varargs[i])

            # Replace references to self.name with the modules name
            module = module.replace ('${name}', modName)
            module = module.replace ('${self.name}', modName)
            
            # Store the modified module data back into the pipeline
            pipeline [index] = module
            
            # Store the module index in a hash based on its name
            modList [modName] = index
            
            # Increment the index counter
            index += 1
        
        # Remove the globals from the XML description as they are no
	# longer needed.
        pipeline [0] = removeGlobals (globalSpace)
        
        # Join the pipelines back into one source.
        pipeline = string.join (pipeline, '\n').replace ('="None"', '="False"')

        # PASS 1:
        # Replace the existing XML vars and evaluate the MACROS in order
        # to expand the XML tree (include in the printout the default
        # values of all XML elements).
        pipeline = replaceXMLvars (pipeline, modList)

        # Get the 'full' printout of the XML code (so that defaults are included)
        pipeline = XMLObject.instanceFromXml (pipeline).toXml ()

        # PASS 2:
        # Now that the XML has been expanded, replace any remaining
        pipeline = replaceXMLvars (pipeline, modList)

        # Create the XML object tree
        pipeline = XMLObject.instanceFromXml (pipeline)
        
	# Remove duplicate exit code handlers.  This assumes the
	# global ones appear after the local ones.
        for module in pipeline.Modules:
	    ppa = {}
            while module.PostProcActions:
                postProc = module.PostProcActions.pop ()
		for ec in postProc.exitCodes:
		    ecval = ec.val.upper()
		    if ecval == 'DEFAULT':
		        ecval = 'default'
		    ppa[ecval] = postProc
	    for ecval in ppa:
		if ppa[ecval] not in module.PostProcActions:
		    module.PostProcActions.insert (0, ppa[ecval])

    return pipeline
