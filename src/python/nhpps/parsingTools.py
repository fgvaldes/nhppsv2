from dtd.dtd import *

import os, re

DTDNAME = 'NHPPS.dtd'

def readXML (fName):
    
    """
    Reads the contents of an XML file stripping out the comments.
    """
    
    try:
        xmlFile = file (fName, 'r')
        xmlData = xmlFile.read ()
        xmlFile.close ()
    except:
        xmlData = None
    else:
        # Strip out comments
        sIndex = xmlData.find ('<!--')
        while sIndex >= 0:
            eIndex = xmlData.find ('-->', sIndex)
            if eIndex > sIndex:
                xmlData = xmlData [:sIndex] + xmlData [eIndex+3:]
                sIndex = xmlData.find ('<!--', sIndex)
            else:
                print "Poorly formed XML comment string in %s. Please correct it." % (fName)
                xmlData = None
                break
            pass
        pass
    return xmlData

def mergeSystemPipelineFiles (pipeline, path, syspath):
    
    """
    Merges the contents of the System level and Pipeline
    level XML files into one `file` (string)...
    """
    
    system = getAttribute ('Pipeline.system', pipeline)
    if system:
	# We are here before the macros have been expanded. There is
	# probably a more general way but for now do it brute force.
	if system == 'ENV(NHPPS_SYS_NAME)':
	    system = os.environ ['NHPPS_SYS_NAME']
        sysFile = os.path.join (path, '%s.xml' % (system))
	if not os.path.exists (sysFile):
	    sysFile = os.path.join (syspath, '%s.xml' % (system))
        if os.path.exists (sysFile):
            sysXml  = readXML (sysFile)
            if sysXml:
                # Merge the contents of the system level XML file
                # into the pipeline XML files `global` space,
                # including the `Directories` and `ExitCodes` elements...
                pipeline = mergeParentChild (pipeline, sysXml, 'System',
		    Directories=True, ExitCodes=True, Templates=True)
            else:
                print 'Unable to load the system level xml file: %s' % (sysFile)
        else:
            # The file does not exist...
            pass
    else:
        print 'Could not get the system name'
    return pipeline

def xmlElementReplace (xmlObject, xmlData, rootElement='Pipeline'):
    index = findXMLvar (xmlData, rootElement)
    while index >= 0:
        variableName  = xmlData[index+2:].split ()[0].split ('}')[0]
        variableValue = eval ('xmlObject.%s' % (variableName[variableName.find ('.')+1:]))
        xmlData = xmlData.replace ('${%s}' % (variableName), variableValue)
        index = findXMLvar (xmlData, rootElement)
        pass
    return xmlData

def evalSelf (input, currentObj):
    return  xmlElementReplace (currentObj, input, rootElement='self')

def uniqueCommands (xProcAction):
    """
    Removes duplicated commands from the xProcAction
    """
    lines = xProcAction.toXml ().split ('\n')[3:-1]
    index = 0
    newlines = []
    for command in lines:
        if command not in newlines:
            newlines.append (command)
            index += 1
        else:
            xProcAction.Commands.pop (index)
            pass
        pass
    return

def findXMLvar (xmlData, rootElement='Pipeline', startIndex=0):
    return findVar (xmlData, sp='${%s.' % (rootElement), ep='}', startIndex=startIndex)

def findVar (xmlData, sp='${', ep='}', startIndex = 0):
    """
    Searches the input text, xmlData, to find sp...ep
    """
    sIndex = xmlData.find (sp, startIndex)
    if sIndex >= 0 and ep in xmlData[sIndex:].split ()[0]:
        return sIndex
    return -1

def findMacro (xmlData):
    macro = xmlData.find ('CAPS(')
    if macro < 0:
        macro = None
        pass
    tmp = xmlData.find ('ENV(')
    if tmp > -1:
        if macro is None or tmp < macro:
            macro = tmp
            pass
        pass
    tmp = xmlData.find ('INDEX(')
    if tmp > -1:
        if macro is None or tmp < macro:
            macro = tmp
            pass
        pass
    return macro, xmlData.find (')')

def evalMACROS (line, modList = None):
    """
    Evaluates the MACROS in the xml pipeline definition in a recursive
    manner so that the pipeline architect is able to for instance write:
    x='ENV(CAPS(${Pipeline.name}DATA))/input' to specify the input
    directory which is the environment variable $XXXDATA + '/input'
    where XXX is the name of the pipeline in caps.
    """
    fIndex, fEnd = findMacro (line)
    if fIndex is not None:
        # We have a macro in the line to evaluate
        if fIndex < fEnd:
            # recurse into the current macro...
            pre = line [0:fIndex]
            x = line[fIndex:].find ('(') + fIndex
            func = line [fIndex:x]
            post = evalMACROS (line[x+1:], modList=modList)
            fEnd = post.find (')')
            fText = post [:fEnd]
            
            if func == 'CAPS':
                fText = fText.upper ()
            elif func == 'ENV':
                if fText[0] == '$':
                    fText = fText[1:]
                    pass
                if fText in os.environ:
                    fText = os.environ [fText]
                else:
                    fText = '$' + fText
                    pass
                pass
            elif func == 'INDEX':
                if modList and fText in modList:
                    fText = '%d' % (modList [fText])
                    pass
                pass
            return pre + fText + post [fEnd+1:]
        else:
            # recurse into the NEXT macro...
            return line [:fEnd+1] + evalMACROS (line [fEnd+1:], modList=modList)
        pass
    else:
        # at the end of the macro definition
        return line
    return

def getAttribute (attr, xmlData):
    tree = attr.split ('.')

    index = 0
    for element in tree[:-2]:
        xmlData1 = re.search ('<%s.*?</%s>' % (element, element),
	    xmlData, flags=re.DOTALL)
	if not xmlData1:
            return None
	xmlData = xmlData1.group (0)

    xmlData1 = re.search ('<%s[^>]*%s="([^"]*)"' % (tree[-2], tree[-1]),
	xmlData, flags=re.DOTALL)
    if xmlData1:
        return xmlData1.group (1)
    else:
	return None

def findEndOf (pattern, xmlData):
    """
    Returns the index of the character AFTER the first occurrence
    of `pattern` in xmlData.
    """
    sIndex = xmlData.find (pattern)
    if sIndex >= 0:
        sIndex += len (pattern)
    else:
        sIndex = -1
    return sIndex

def moduleIsActive (module):
    """
    Looks for the isActive attribute in the module and returns
    False if the isActive attribure is not a True value.
    """
    nIndex = module.find ('isActive="')
    if nIndex >= 0:
        nIndex += len ('isActive="')
        eIndex = module.find ('"', nIndex)
        if eIndex >= 0:
            if module[nIndex:eIndex].lower () not in ['yes', 'y', 'true', 't']:
                return False
    return True

def moduleName (module):
    """
    Returns the modules name attribute.
    """
    return getAttribute ('Module.name', module)

def mergeElementAttributes (element, child, parent, closeTag=None, parentElement=None):
    """
    Merges the attributes of the parent into the child, if necessary.
    """

    exec 'mObj = %s ()' % (element)
    
    newTag = '<%s' % (element)
    if not parentElement:
        parentElement = element
    for attr in mObj._attrsOrder:
        childAttr  = getAttribute ('%s.%s' % (element, attr), child)
        parentAttr = getAttribute ('%s.%s' % (parentElement, attr), parent)
        
        node = mObj.getNodeWithName (attr)
        if parentAttr:
            if not childAttr:
                childAttr = parentAttr
            elif node.isOptional () and childAttr == node.getDefaultValue ():
                childAttr = parentAttr
        elif not childAttr:
            childAttr = node.getDefaultValue ()
        newTag += ' %s="%s"' % (attr, childAttr)

    if mObj._nodesOrder:
        newTag += '>'
    else:
        newTag += '/>'
    
    childStart = child.find ('<%s' % (element))
    if childStart >= 0:
        # Child included the tag, replace it with the new one
        childEnd = child.find ('>', childStart) + 1
        child = child[:childStart] + newTag + child[childEnd:]
    elif closeTag:
        insertIndex = child.find (closeTag)
        child = child[:insertIndex] + newTag + child[insertIndex:]
    else:
        child += newTag + '\n'
    return child

def merge_Directories (child, parent, closeTag=None):
    pipeline = mergeElementAttributes ('Directories', child, parent, closeTag=closeTag)
    return pipeline

def merge_ExitCodes (child, parent, closeTag=None):
    # There is probably a better way to do this.
    parentElements = re.findall ('<ExitCodes[^>]*>', parent)
    for parentElement in parentElements:
        parentId = re.search ('id="[^"]*"', parentElement)
        parentCode = re.search ('code="[^"]*"', parentElement)
	if not (parentId and parentCode):
	    continue
	if (re.search ('<ExitCodes[^>]*'+parentId.group(), child) or
	    re.search ('<ExitCodes[^>]*'+parentCode.group(), child)):
	    continue
	child += parentElement + '\n'
    return child

def splitExitCodes (postProc):
    exitCodes = re.findall ('<ExitCode.*?/>', postProc)
    if len (exitCodes) <= 1:
        return postProc

    tags = postProc.split ('<')

    newPostProc = ""
    for exitCode in exitCodes:
        for tag in tags:
	    if not tag:
	        continue
            if exitCode[1:] in tag:
	        newPostProc += exitCode
	    elif 'ExitCode' not in tag:
	        newPostProc += '<' + tag
	newPostProc += '\n'

    return newPostProc

def merge_Templates (child, parent, closeTag=None):
    Templates = re.findall ('<Template.*?</Template>', parent, re.DOTALL)

    # Check for older file without Templates.
    if len(Templates) == 0:
        ppParent = re.findall (
	    '<PostProcAction.*?</PostProcAction>', parent, re.DOTALL)
	if ppParent:
	    ppChild = re.findall (
		'<PostProcAction.*?<ExitCode[^>]*(val="[^"]*").*?</PostProcAction>',
		child, re.DOTALL)
	    child += '<Template type="Module">\n'
	    for pp in ppParent:
		val = None
		for val in ppChild:
		    if val in pp:
		        break
		if val and val in pp:
		    continue
		child += pp + '\n'
	    child += '</Template>\n'
	return child
    
    # Merge the templates.
    for Template in Templates:
        type = re.findall ('type=".*?"', Template)
	if type[0] in child:
	    continue
	child += Template + '\n'
    return child

def merge_MaxExec (child, parent, closeTag=None):
    return mergeElementAttributes ('MaxExec', child, parent, closeTag=closeTag)

def merge_MinDisk (child, parent, closeTag=None):
    return mergeElementAttributes ('MinDisk', child, parent, closeTag=closeTag)

def merge_Trigger (child, parent):

    # Only merge if the child is a module and does not contain a Trigger.
    if '<Module' not in child or '<Trigger' in child:
        return child

    # Find the type and Trigger.
    type = re.findall ('type=".*?"', child)
    if type:
        Template = re.findall ('<Template[^>]*%s.*?</Template>' % type[0],
	    parent, re.DOTALL)
    else:
        Template = re.findall ('<Template[^>]*type="Module".*?</Template>',
	    parent, re.DOTALL)
    if not Template:
	return child

    element = re.findall ('<Trigger>.*?</Trigger>',
        Template[0], re.DOTALL)
    if not element:
        return child

    child += element[0]

    return child

def merge_PreProc (child, parent):

    # Only merge if the child is a module and does not contain a PreProcAction.
    if '<Module' not in child or '<PreProcAction' in child:
        return child

    # Find the type and PreProcAction.
    type = re.findall ('type=".*?"', child)
    if type:
        Template = re.findall ('<Template[^>]*%s.*?</Template>' % type[0],
	    parent, re.DOTALL)
    else:
        Template = re.findall ('<Template[^>]*type="Module".*?</Template>',
	    parent, re.DOTALL)
    if not Template:
	return child

    element = re.findall ('<PreProcAction>.*?</PreProcAction>',
        Template[0], re.DOTALL)
    if not element:
        return child

    child += element[0]

    return child

def merge_Proc (child, parent):

    # Only merge if the child is a module and does not contain a ProcAction.
    if '<Module' not in child or '<ProcAction' in child:
        return child

    # Find the type and ProcAction.
    type = re.findall ('type=".*?"', child)
    if type:
        Template = re.findall ('<Template[^>]*%s.*?</Template>' % type[0],
	    parent, re.DOTALL)
    else:
        Template = re.findall ('<Template[^>]*type="Module".*?</Template>',
	    parent, re.DOTALL)
    if not Template:
	return child

    element = re.findall ('<ProcAction>.*?</ProcAction>',
        Template[0], re.DOTALL)
    if not element:
        return child

    child += element[0]

    return child

def merge_PostProc (child, parent):

    # Only merge if the child is a module.
    if '<Module' not in child:
        return child

    # Find the type.
    Template = re.findall ('type=".*?"', child)
    if Template:
        parent = re.search ('<Template[^>]*%s.*?</Template>' % Template[0],
	    parent, re.DOTALL)
    else:
        parent = re.search ('<Template[^>]*type="Module".*?</Template>',
	    parent, re.DOTALL)
    if not parent:
	return child

    # Merge.
    parent = parent.group(0)
    ppParent = re.findall (
	'<PostProcAction.*?</PostProcAction>', parent, re.DOTALL)
    newParent = ""
    for pp in ppParent:
	newParent += splitExitCodes (pp)
    ppParent = re.findall (
	'<PostProcAction.*?</PostProcAction>', newParent, re.DOTALL)
    ppChild = re.findall (
	'<PostProcAction.*?<ExitCode[^>]*(val="[^"]*").*?</PostProcAction>',
	child, re.DOTALL)
    for pp in ppParent:
	val = None
	for val in ppChild:
	    if val in pp:
		break
	if val and val in pp:
	    continue
	child += pp
    return child

def merge_maxActive (child, parent, parentElement='Pipeline', childElement='Pipeline', closeTag=None):
    return mergeElementAttributes (childElement, child, parent, parentElement=parentElement, closeTag=closeTag)

def mergeParentChild (child, parent, parentElement='Pipeline', childElement='Pipeline', closeTag=None,
    Directories=False, ExitCodes=False, Templates=False):

    end = None
    if '</Module>' in child:
        index = child.find ('</Module>')
        end = child [index:]
        child = child[:index]
    if Directories:
        child = merge_Directories (child, parent, closeTag=closeTag)
    if ExitCodes:
        child = merge_ExitCodes (child, parent)
    if Templates:
        child = merge_Templates (child, parent)

    child = merge_MaxExec (child, parent, closeTag=closeTag)
    child = merge_MinDisk (child, parent, closeTag=closeTag)
    child = merge_Trigger (child, parent)
    child = merge_PreProc (child, parent)
    child = merge_Proc (child, parent)
    child = merge_PostProc (child, parent)
    child = merge_maxActive (child, parent, parentElement=parentElement, childElement=childElement, closeTag=closeTag)
    if end:
        child += end
    return child

def removeElement (tag, xmlData):
    changed = False
    sIndex = xmlData.find ('<%s' % (tag))
    if sIndex >= 0:
        eIndex = xmlData.find ('/>', sIndex)
        if eIndex >= 0 and '<' not in xmlData[sIndex+1:eIndex]:
            eIndex += 2
            xmlData = xmlData [:sIndex] + xmlData [eIndex:]
            changed = True
        else:
            eIndex = xmlData.find ('</%s>' % (tag), sIndex)
            if eIndex >= 0:
                eIndex += len ('</%s>' % (tag))
                xmlData = xmlData [:sIndex] + xmlData[eIndex:]
                changed = True
            else:
                print 'globals are not well-formed. Missing close of %s element' % (tag)
    return xmlData, changed

def removeGlobals (xmlData):
    """
    Removes the MaxExec, MinDisk, and Template elements
    from the globals space in the pipeline definition in order to minimize
    the additional wasted space storing such data AFTER those were propogated
    to the individual modules.
    """
    
    # Remove the MaxExec tag...
    xmlData, changed = removeElement ('MaxExec', xmlData)

    # Remove the MinDisk tag...
    xmlData, changed = removeElement ('MinDisk', xmlData)

    # Remove the Template tag(s)...
    while True:
	xmlData, changed = removeElement ('Template', xmlData)
        if not changed:
            break
        
    return xmlData
