# The Scheduler component of the PipelineMgr is responsible for determining
# if and when the pipelines module threads are to execute. To execute means
# a module thread will, if not currently processing a dataset, check if a
# dataset is ready for it to process, otherwise it will continue to perform
# the processing as specified in the pipelines XML description.
# 
# The scheduler executes in different states:
# PL_NORMAL
#   The module threads are all executed in turn (subject to a sleep period).
# PL_HALTING   
#   The module threads which are currently processing are executed until
#   they have completed processing the current dataset.
# PL_HALTED
#   No module threads are executed.
# PL_STEPPING
#   Inactive module threads are executed until one of them becomes active.
# PL_STOPPED
#   No module threads are executed and/or the scheduler has not been started.

import select

from ModuleMgr import *

class modThread (object):
    
    """
    Wraps the ModuleMgr objects as micro-`threads`
    which are maintained by the scheduler.
    """
    
    def __init__ (self, module):
        self._genFunc = module.run ()
        self.status = MM_COMPLETE
        self.activated = False
        return
    
    def execute (self):
        self.status = self._genFunc.next ()
        return
    

# Constants
POLLING_SPEEDUP = 3.            # Speedup factor in the polling loop

PL_NORMAL   = 1001
PL_HALTING  = 1002
PL_HALTED   = 1003
PL_STEPPING = 1004
PL_STOPPED  = 1005

__plstatus__ = [x for x in dir () if x [0:3] == 'PL_']

class Scheduler (object):
    def __init__ (self, threads, pollingTime = 1, daemon = None,
                  pipeName = '', log = None, fast = False):
        
        super (Scheduler, self).__init__ ()
        
        # Record the time we wake up
        self.startTime    = time.time ()
        self.daemon       = daemon
        self.pipeName     = pipeName
        self.log          = log
        self.stop         = False
        self.poll         = pollingTime
        self.activeStages = 0
        self.status       = PL_STOPPED
        self.fast         = fast
        
        # Just to keep track of processing time vs sleep time
        self.timeSlept = 0

        # Create a list of generators from the input threads.
        self.threads = [modThread (module) for module in threads]

        # Store the number of module instances in total...
        self.instances = len (self.threads)

        # Create an empty list to store new threads if the
        # operator adds instances later...
        self.newThreads = []
        
        # Keed the blackboard reference
        self.blackboard = threads [0].blackboard
        
        # self.pollingTime is the ammount of time (in seconds) that we 
        # are supposed to sleep. See the discussion in self.run () for 
        # more details. We divide that by the number of modules.
        if fast:
            speedup = float (POLLING_SPEEDUP * self.instances)
            self.pollingTime = pollingTime / speedup
        else:
            self.pollingTime = pollingTime
        
        # since different threads might choose/have to sleep
        # for different ammounts of time, we need a "wake up 
        # call list" to know when we should re-activate them.
        # the call list is a hash table of the form:
        # {wakeUpTime: [thread1, thread2...]}
        # times are in UNIX time format (i.e. the output of 
        # time.time ()).
        self.callList = {}
        
        return

    def addInstances (self, threads):
        
        """
        Adds new module manager instances in the event the
        operator 'starts' a pipeline a second time with a
        different value for ninst
        """

        self.newThreads += [modThread (module) for module in threads]
        
    def halt (self):
        """
        Halts the execution of the pipeline module threads.
        Returns the number of datasets which were in the
        pipelines blackboard.
        """
        
        retVal = 0
        self.blackboard.setStatus (0) # HALTED
        self.status = PL_HALTING
        # The pipeline is not currently halted, halt it!
        for ds in self.blackboard.OSFBlackBoard:
            # lock the capability to update the osf flag for stage 1
            self.blackboard.lockOSF = True
            self.blackboard.osfUpdate (ds, None, 1, 'h', True)
            retVal += 1
        return retVal
    
    def resume (self):
        """
        Resume the pipeline after a halt
        """
        
        retVal = 0
        self.blackboard.setStatus (1) # ACTIVE
        if self.status == PL_HALTED:
            # The pipeline is halted, resume it!
            for ds in self.blackboard.OSFBlackBoard:
                # unlock the capability to update the osf flag for stage 1
                self.blackboard.lockOSF = False
                self.blackboard.osfUpdate (
                    ds, None, 1, self.blackboard.OSFBlackBoard [ds].firstFlag)
                retVal+=1
        if not self.status == PL_STEPPING:
            self.status = PL_NORMAL
        return retVal
    
    def step (self):
        """
        Set the 'stepping' flag, which will tell the next module
        which starts to call osfHalt (). Then, we must ensure that
        the pipeline is in it's running state.
        
        We call this function in the osf_resume system call because
        we want to be able to keep track of pipelines which are
        'active' and 'halted'. By calling the 'system' version of
        osf_resume, rather than the blackboard osfResume, we are
        contacting the Node Manager and telling it that the pipeline
        is now running, or in its 'active' state, once again.
        """

        self.status = PL_STEPPING
        self.resume ()
        return 0
    
    def shutdown (self, n, frame):
        return self.join (timeout=0)
    
    def updateThreadList (self):
        # let's see if we need to wake anybody up.
        now = time.time ()
        for wakeUpTime in self.callList.keys ():
            if wakeUpTime < now:
                # wake them all up
                for thread in self.callList [wakeUpTime]:
                    self.threads.append (thread)
                # and remove them from the call list
                del self.callList[wakeUpTime]
        return
    
    def stopThread (self, i):
        """
        Removes a thread from the execution queue because it
        has either finished processing a dataset or it has no
        datasets available to process at this time.
        """
        if self.threads [i].activated:
            self.activeStages -= 1
        self.threads [i].activated = False
        t = time.time () + self.poll
        if t not in self.callList:
            self.callList [t] = []
        self.callList [t].append (self.threads.pop (i))
        return
    
    def launchThread (self, i):
        self.threads [i].execute ()
        self.threads [i].activated = True
        self.activeStages += 1
        if self.threads [i].status == MM_PROCESSING:
            # Check if we were stepping through the pipeline,
            # if so we need to 'halt' the pipeline by calling
            # osfHalt to begin halting the pipeline
            if self.status == PL_STEPPING:
                self.halt ()
        return
    
    def runThreads (self):
        N = len (self.threads)
        i = 0
        while i < N:
            startingState = self.threads [i].status
            # Execute threads until all of the threads in
            # self.threads are in the `PROCESSING` state.
            if startingState == MM_PROCESSING:
                self.threads [i].execute ()
            elif self.status in [PL_NORMAL, PL_STEPPING]:
                # The thread was not processing already, and the pipeline is in
                # a state such that the thread should be given the oportunity
                # to process...
                self.launchThread (i)
            
            if self.threads [i].status == MM_COMPLETE:
                N -= 1
                self.stopThread (i)
                if self.activeStages == 0 and self.status == PL_HALTING:
                    # The pipeline was in the process of being halted, and
                    # there are no longer any modules processing in the
                    # pipeline, we want to update the halt signal to 'H'
                    self.status = PL_HALTED
                    for ds in self.blackboard.OSFBlackBoard.keys ():
                        self.blackboard.osfUpdate (ds, None, 1, 'H', True)
            else:
                i += 1
        return
    
    def handlePyroRequests (self):
        # If we have a daemon, run its event loop
        # dt0 = time.time ()
        if self.daemon:
            socks=self.daemon.getServerSockets()
            ins,outs,exs=select.select (socks,[],[],0)   # 'foreign' event loop
            for s in socks:
                if s in ins:
                    self.daemon.handleRequests (timeout=0)
                    break    # no need to continue with the for loop
        else:
            if self.log:
                self.log.write ('no daemon!\n')
        return
    
    def sleep (self):
        time.sleep (self.pollingTime)
        self.timeSlept += self.pollingTime
        return
    
    def run (self):
        """
        Executes all the (micro) threads, one at the time. Every time 
        each thread does some atomic operation, it yields. Control comes
        back at us and we give it to the next thread.
        """
        self.status = PL_NORMAL
        while not self.stop:
            if self.status in [PL_NORMAL, PL_STEPPING]:
                self.updateThreadList () # wake up sleeping threads
            if self.status in [PL_NORMAL, PL_STEPPING, PL_HALTING]:
                self.runThreads () # execute threads
            if self.newThreads:
                # We have added instances of the modules to the
                # pipeline. Add them to the 'threads'
                if self.fast:
                    self.pollingTime *= self.instances
                self.threads += self.newThreads
                self.instances += len (newThreads)
                self.newThreads = []
                if self.fast:
                    self.pollingTime /= self.instances
            #if self.status not PL_STOPPED:
            self.handlePyroRequests () # deal with pyro requests
            self.sleep ()
        # <-- end while
        return # DESTROYS THE PIPELINE!
    
    def join (self, timeout=None):
        # Print out some timimg information
        now = time.time ()
        self.stop = True
        
        tot = now - self.startTime
        work = tot - self.timeSlept
        sleep = self.timeSlept
        if self.log:
            self.log.write (
                '\nTIME\t%s (total)\t%.02f\n' % (self.pipeName, tot))
            self.log.write (
                'TIME\t%s (sleep)\t%.02f\n' % (self.pipeName, sleep))
            self.log.write (
                'TIME\t%s (work)\t%.02f\n' % (self.pipeName, work))
        return
