"""
Data Manager interface which registers the pipeline module

    NewProcess - A Script to create a new process with indicated ID, should
    it not presently exist with the Data Manager.

    Description 

    newProcess always returns a status code. The status code is either 0 for
    success or 1 for failure.

    Usage
       newProcess.py [processId] [parentId] [dm_addr] [dm_port]
   
       The values of 'processId' 'parentId' 'dm_addr' and 'dm_port' may also be
       passed to the program via the 5 environment variables of 'OSF_DATASET',
       'NHPPS_MODULE_PID', 'NHPPS_SUBPIPE_ID', 'NHPPS_NODE_DM', and
       'NHPPS_PORT_DM' should they not be explicitly given on the commandline.

    Communication Protocol

    Pipeline RPC

"""

import sys
import os

import string
import socket
import time
from pipeutil import *

global VERBOSE

def newDMProcess (processId=None, parentId=None, dm_addr=None, dm_port=None, logUrl='', uparmUrl=''):
    
    """
    Configure the processing environment to have the desired
    processId, parentId, and osf_dataset variables.
    """

    # 1.  grab environment variables if they are presently missing
    try:
        if (not dm_addr):
            dm_addr = os.environ['NHPPS_NODE_DM']

        if (not dm_port):
            arg = os.environ['NHPPS_PORT_DM']
            (type, port) = string.split (arg, ':')
            dm_port = int (port)

        if (not processId):
            osf_dataset = os.environ['OSF_DATASET']
            processId = os.environ['NHPPS_MODULE_PID'] + "_" + osf_dataset

        if (not parentId):
            try:
                # not sure we want to have the osf_dataset appended here..
                # but there is no other way to guarrentee "uniqueness" of the parentId otherwise
                parentId = os.environ['NHPPS_SUBPIPE_ID'] + "_" + osf_dataset
            except KeyError:
                if (VERBOSE): print "No pipeline ID, using none as parent id"
                else:
                    pass

    except KeyError:
        print "Missing %s environment variable" % sys.exc_value
        #usage(sys.argv[0])
        return 0

    except:
        raise sys.exc_type, sys.exc_value


    # 2. Now see if we have such a process in our Data Manager
    return create_process(processId, parentId, dm_addr, dm_port, logUrl=logUrl, uparmUrl=uparamUrl, verbose=VERBOSE)


def create_process (processId, parentId, dm_addr, dm_port, env={}, logUrl='', uparmUrl='', verbose=False):

    start_time = time.time()
    ret_status = 1
    #localhost = string.split (socket.gethostname (), '.')[0]
    # our preference is to try to get the IP value rather
    # than the host name. IF we fail, its likely to be a real short
    # trip (e.g. no networking running on our machine) but I suppose
    # there are cases there the pipeline may be run sans connections,
    # and so we will want to put in the hostname, at least
    localhost = socket.gethostname()
    try:
        localhost = socket.gethostbyname(localhost)
    except:
        pass

    
    envVars = {
        'user_id': 'USER',
        'system_name': 'NHPPS_SYS_NAME',
        'pipe_name': 'NHPPS_SUBPIPE_NAME',
        'pipe_module': 'NHPPS_MODULE_NAME',
        'osf_flag': 'OSF_FLAG',
        'event_type': 'EVENT_TYPE',
        'dataset': 'OSF_DATASET'
        }
    
    for key in envVars.keys():
        try:
            envVars [key] = env [envVars [key]]
        except KeyError:
            try:
                envVars [key] = os.environ [envVars [key]]
            except:
		envVars [key] = None
    
    pkt = composePacket ({'COMMAND': 'newprocess',
                           'procid': processId,
                           'nodeip': localhost,
                           'logurl': logUrl,
                           'uparmurl': uparmUrl,
                           'parentprocid': parentId,
                           'host': string.split (socket.gethostname(), '.')[0],
                           'start_time': start_time,
                           'user_id': envVars ['user_id'],
                           'system_name': envVars ['system_name'],
                           'pipe_name': envVars ['pipe_name'],
                           'pipe_module': envVars ['pipe_module'],
                           'osf_flag': envVars ['osf_flag'],
                           'event_type': envVars ['event_type'],
                           'dataset': envVars ['dataset']
                          })
                           # 'clobber': False, \ # dont pass this.. let the DM decide if OK to clobber
    if (verbose): print "PKT: %s" % pkt
    try:

        response = sendPacketTo (pkt, dm_addr, dm_port, timeout=300, will_respond=True, verbose=verbose)

        if (not response):
            raise TypeError, "response is returned null" 

        result = parsePacket (response)

        # capture, and return our status
        # however, in the special case that we tried to clobber an existing process
        # and failed, lets not return a failure
        if (not result):
            raise TypeError, "response result is not returned null" 

        status = result['status']

        if (not status): 
            raise TypeError, "result status is not returned as a list, got null" 

        try:
            ret_status = int(status[0])
        except:
            raise sys.exc_type, "status had error. Is value:%s" % status

        # Dont use regex here.. while it is more flexible, its slower too.
        if (ret_status and status[2:] == '_sqlite.IntegrityError:column id_str is not unique'):
            # process DID already exist. Lets not fill up the logs, this will
            # happen alot, and is expected, so lets change to 'success' to quiet
            # things down.
            ret_status = 0 

    except:
         # pass along the error
         raise sys.exc_type, sys.exc_value

    return ret_status
