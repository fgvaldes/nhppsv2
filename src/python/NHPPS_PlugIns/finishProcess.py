""" FinishProcess - A Script to update the DM about a successfully completed 
                    process with indicated ID.

    Description 

    finishProcess always returns a status code. The status code is either 0 for
    success or 1 for failure.

    Usage
       finishProcess.py -status_flag <0|1> [processId] [dm_addr] [dm_port]
   
       The values of 'processId' 'dm_addr' and 'dm_port' may also be passed to
       the program via the 4 environment variables of OSF_DATASET,
       NHPPS_MODULE_PID, NHPPS_NODE_DM, and NHPPS_PORT_DM should they not be
       explicitly given on the command line.

    Communication Protocol

    Pipeline RPC
"""

import sys
import os
import time

import string
import socket
from pipeutil import *


global VERBOSE

def finishDMProcess ( processId=None, success_flag="1", dm_addr=None, dm_port=None):
    """ Configure the processing environment to have the desired
        processId and success_flag variables.
    """

    # 1.  grab environment variables if they are presently missing
    try:
        if (not dm_addr):
            dm_addr = os.environ['NHPPS_NODE_DM']

        if (not dm_port):
            arg = os.environ['NHPPS_PORT_DM']
            (type, port) = string.split (arg, ':')
            dm_port = int (port)

        if (not processId):
            osf_dataset = os.environ['OSF_DATASET']
            processId = os.environ['NHPPS_MODULE_PID'] + "_" + osf_dataset

    except KeyError:
        print "Missing %s environment variable" % sys.exc_value
        #usage(sys.argv[0])
        return 1

    except:
        raise sys.exc_type, sys.exc_value


    # 2. Now see if we have such a process in our Data Manager
    return finish_process(processId, success_flag, dm_addr, dm_port, 0, VERBOSE)


def finish_process (processId, success_flag, dm_addr, dm_port, exitCode, verbose=False):

    end_time = time.time()
    ret_status = 0
    #localhost = string.split (socket.gethostname (), '.')[0]
    # our preference is to try to get the IP value rather
    # than the host name. IF we fail, its likely to be a real short
    # trip (e.g. no networking running on our machine) but I suppose
    # there are cases there the pipeline may be run sans connections,
    # and so we will want to put in the hostname, at least
    localhost = socket.gethostname()
    try:
        localhost = socket.gethostbyname(localhost)
    except:
        pass

    pkt = composePacket ({
        'COMMAND': 'finishprocess',
        'id_str': processId,
        'end_time': end_time,
        'exit_code': exitCode,
        'success': success_flag
        })
    if (verbose): print "PKT: %s" % pkt
    try:
        response = sendPacketTo (pkt, dm_addr, dm_port, timeout=300, will_respond=True, verbose=verbose)

        if (not response):
            raise TypeError, "response is returned null" 

        result = parsePacket (response)

        # capture, and return our status however, in the special case that
        # we tried to clobber an existing process and failed, lets not return
        # a failure
        if (not result):
            raise TypeError, "response result is not returned null" 

        status = result['status']

        if (not status): 
            raise TypeError, "result status is not returned as a list, got null" 

        try:
            ret_status = int(status[0])
        except:
            raise sys.exc_type, "status had error. Is value:%s" % status

        # Dont use regex here.. while it is more flexible, its slower too.
        if (ret_status and status[2:] == '_sqlite.IntegrityError:column id_str is not unique'):
            # process DID already exist. Lets not fill up the logs, this will
            # happen alot, and is expected, so lets change to 'success' to quiet
            # things down.
            ret_status = 0 

    except:
         # pass along the error
         raise sys.exc_type, sys.exc_value

    return ret_status
