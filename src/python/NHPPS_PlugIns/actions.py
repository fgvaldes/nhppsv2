"""
Addon Action Plugins

Action Plugin API

Each routine name has to have the form

    addon<XML Tag name>    (e.g. addonNewProcess)

Each routine will be invoked with at least 3 and not more than 4
arguments:
    actionMgr
    argList
    env
    successExitCode    (optional)

Each routine has to return an exit code, making sure that the code for
success has to be equal to successExitCode (defaults to 0).
"""

import newProcess, finishProcess
from os.path import expandvars

def addonDummy (actionMgr, argList, env, successExitCode=0):

    """ just prints 'dummy' to standard out. """

    print "DUMMY\n"

    return 1

def addonNewProcess (actionMgr, argList, env, successExitCode=0):

    """
    Add a new process to the data manager.
    """

    # 1. Grab environment variables and build up info we need
    osf_dataset = env['OSF_DATASET']
    #procId = "%s_%s" % (env['NHPPS_MODULE_PID'], osf_dataset) 
    procId = "%s_%s" % (osf_dataset, env['NHPPS_MODULE_NAME']) 
    parentId = None
    try:
        parentId = "%s_%s" % (env['NHPPS_SUBPIPE_ID'], osf_dataset)
    except: 
        pass

    # 2. Parse argList to get following info
    args = __parseArgList (argList)

    dm_addr = args ['addr']
    if ':' in args ['port']:
        junk, dm_port = args ['port'].split (':')
    else:
        dm_port = args ['port']
        pass
    
    dm_port = int (dm_port)

    logUrl = None #argList['']
    try:
        logUrl = args['logUrl']
    except: pass

    uparmUrl = None
    try:
        uparmUrl = args['uparmUrl']
    except: pass

    # 3. Run
    try:
	exitCode = newProcess.create_process(procId, parentId, dm_addr, dm_port, env, logUrl, uparmUrl)
    except TypeError, e:
        print e
	exitCode = 1

    # 4. Exit
    return exitCode

 
def addonFinishProcess (actionMgr, argList, env, successExitCode=0):

    """
    Update the (exit/finish) status of process in the data manager.
    """

    # 1. Grab environment variables and build up info we need
    osf_dataset = env['OSF_DATASET']
    #procId = "%s_%s" % (env['NHPPS_MODULE_PID'], osf_dataset)
    procId = "%s_%s" % (osf_dataset, env['NHPPS_MODULE_NAME'])
    parentId = None
    try:
        parentId = "%s_%s" % (env['NHPPS_SUBPIPE_ID'], osf_dataset)
    except:
        pass

    # 2. Parse argList to get following info
    args = __parseArgList (argList)

    dm_addr = args ['addr']
    if ':' in args ['port']:
        junk, dm_port = args ['port'].split (':')
    else:
        dm_port = args ['port']
        pass
    
    dm_port = int (dm_port)

    success_flag = 1
    try:
        success_flag = args['success']
    except: pass

    # 3. Run
    try:
	exitCode = finishProcess.finish_process(procId, success_flag, dm_addr, dm_port, successExitCode)
    except TypeError, e:
        print e
	exitCode = 1

    # 4. Exit
    return exitCode

def __parseArgList (argList):
    """ a little resued bit of code """

    dict = {}

    for arg in argList:
       (key, value) = arg.split('=')
       dict[key] = expandvars (value)

    return dict

