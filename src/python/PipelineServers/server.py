__version__ = "0.1"


import socket
import SocketServer
from pipeutil import *

# Default error message
DEFAULT_ERROR_MESSAGE = """\
STATUS = "%(code)d %(message)s"
EOF
"""

class PipeSocketServer (SocketServer.TCPServer):

    allow_reuse_address = True    # Seems to make sense in testing environment
    done = False
    
    def server_bind (self):
        
        """
        Override server_bind to store the server name.
        """
        
        SocketServer.TCPServer.server_bind (self)
        host, port = self.socket.getsockname ()
        self.server_name = socket.getfqdn (host)
        self.server_port = port


class PipeRequestHandler (SocketServer.StreamRequestHandler):
    
    """
    Generic Base class for the Pipeline Request Handler.

    Provides the 'core' code which reads and parses incoming
    packets, handles errors, and calls the function which
    services the request.
    """
    
    def readline (self):
        
        """
        Reads a line from self.rfile and strips leading and trailing
        whitespace from it.
        """
        
        return self.rfile.readline().strip ()
    
    
    def readRequest (self):
        
        """
        Reads lines from rfile, which form a request, and returns
        a string, '\n' delimited, containing the lines.
        """
        
        requestLines = []
        line = self.readline()
        while line[0:4].upper () != 'EOF':
            requestLines.append (line)
            line = self.readline()
        return '\n'.join (requestLines)
    
    
    def handle (self):
        
        """
        Handle a single request.
        """
        
        # Read the request
        request = self.readRequest ()
        
        # Convert the input Pipeline RPC Protocol string
        # input into a dictionary.
        packet = parsePacket (request)
        
        # Extract the command and params from the packet
        if packet.has_key ('command'):
            self.command = packet['command']
            del packet['command']
        else:
            self.send_error (203, "Partial information: 'command' parameter is missing.")
            return
        
        mname = 'mgr_' + self.command
        if hasattr (self, mname):
            method = getattr (self, mname)
            
            self.params = packet
            if self.params.has_key('pipelines') and self.params['pipelines']:
                #self.params['pipelines'] = self.params['pipelines'].lower ().split ()
                self.params['pipelines'] = self.params['pipelines'].split ()
            method ()
        else:
            self.send_error (501, "Unsupported method (%s)" % `self.command`)
        
        return
    
    
    def send_error (self, code, message=None):
        
        """
        Send an error reply.
        
        Arguments are the error code, and a detailed message.
        The detailed message defaults to the short entry matching the
        response code.
        """
        
        try:
            short, long = responses[code]
        except KeyError:
            short, long = '???', '???'
        if not message:
            message = short
        explain = long
        self.send_response (code, message)
        self.wfile.write (DEFAULT_ERROR_MESSAGE %
                         {'code': code,
                          'message': message,
                          'explain': explain})
        return
    
    
    def send_response (self, code, message=None):
        
        """
        Send the response.
        """
        
        if message == None:
            if responses.has_key (code):
                message = responses[code][0]
            else:
                message = ''
        return
    
    
    def address_string (self):
        
        """
        Return the client address formatted for logging.
        
        This version looks up the full hostname using gethostbyaddr(),
        and tries to find a name that contains at least one dot.
        """
        
        host, port = self.client_address
        return socket.getfqdn (host)
    
    
    def mgr_echo (self):
        
        """
        Simple debugging method (internal)
        """
        
        out_msg = 'STATUS = 0\n'
        for key in self.params.keys ():
            val = self.params[key]  
            if (val.find (' ') != -1):
                if (findNextCh (val, '"', 0) != -1):
                    val = "'" + val + "'"
                else:
                    val = '"' + val + '"'
            out_msg += '%s = %s\n' % (key.upper (), val.upper ())
        out_msg += 'EOF\n'
        
        # write the out_msg back to the client
        self.wfile.write (out_msg)
        return
    
    
    def getParam (self, param):
        
        """
        Attempts to access param in self.params. If successful, it
        is returned, otherwise an error message is sent and None
        is returned.
        """
        
        retVal = None
        try:
            retVal = self.params[param]
        except:
            self.send_error (203, "Partial information: '%s' parameter is missing." % param)
        return retVal
    
    
    def hasParam (self, param, CheckEmpty=False, warn=True):
        
        """
        Checks if params has the key specified by param.
        It optionally will check that params[param] is not empty.
        Setting warn=False will surpress the warning message that
        the key does not exist or it's value is empty.
        """
        
        retVal = False
        if self.params.has_key (param):
            retVal = True
            if CheckEmpty and not self.params[param]:
                retVal = Fasle

        if not retVal and warn:
            self.send_error (203, "Partial information: '%s' parameter is missing." % param)
        return retVal
