import os
from psautil import *
import datetime

# These are editable parameters for testing and control.
showpending = False		# Show pending entries?
listdata    = False		# List data?

NEWEST = True
OLDEST = False


IQSQUERY      = "SELECT pathname FROM dtkpct WHERE %s AND pathname LIKE '%%gz'"
IQSQUERY     += " ORDER BY SUBSTR(pathname,1,8) LIMIT %d;\n"
ENDDATE       = "SELECT end FROM %s WHERE name='%s';\n"
UPDATE        = "UPDATE %s SET status='%s' WHERE dataset='%s';\n"
UPDATE_NODATA = UPDATE % ('%s', 'nodata', '%s')
UPDATE_SUBMIT = UPDATE % ('%s', "submitted', submitted='%s", '%s')
UPDATE_STAGE  = UPDATE % ('%s', 'lock', '%s')

if os.environ ['NHPPS_VERSION'][0] == '1':
    MK_RETURN     = """plssh -sn %s 'echo "%s" > %sreturn/%s'"""
else:
    MK_RETURN     = """plssh -sn %s 'echo "%s" "%s" > %s/%srtn'"""
MK_ENIDS      = """rsync -qz %s %s:%s"""
MK_TRIGGER    = """plssh -sn %s 'touch %s%strig'"""

class pipeInfo (object):
    
    def __init__ (self, pipeline, application, execute):
        self.queues     = []
        self.nSubmit    = 0
        self.nPending   = 0
        self.nSubmitted = 0
        self.nPipes     = -1
        self.app        = application
        self.execute    = execute
        self.pipeline   = pipeline
        self.entries    = []
        if execute:
            self.nPipes = len (pipeselect (application, pipeline))
            if self.nPipes == 0:
                msg = 'WARNING: pipeline %s:%s unavailable'%(self.app,pipeline)
                print msg
                pass
            pass
        return
    
    def addQueue (self, psqname, queue, data, query, ds, execute=False):
        
        # Insert queue information into pipedata
        self.queues.append ((psqname, queue, data, query))
        
        # Update number of submitted and pending datasets
        self.nPending   += getPendingCount (psqname, ds, data)
        self.nSubmitted += getSubmittedCount (psqname, ds)
        if not execute:
            total = self.nPending + self.nSubmitted
            if total > 0:
                self.nPipes = total
                pass
            pass
        elif self.nPipes - self.nSubmitted <= 0:
            msg  = 'All available resources for pipeline '
            msg += '%s:%s are busy, skipping.' % (self.app, self.pipeline)
            print msg
            self.nPipes = 0
            pass
        return
    
    def computeMaxSubmit (self, submit):
        
        # Determine maximum to submit...
        if not self.execute:
            if submit > 0:
                self.nSubmit = min (submit, self.nPending)
            else:
                self.nSubmit = self.nPending
                pass
            pass
        elif submit == 0:
            self.nSubmit = self.nPipes - self.nSubmitted
        else:
            self.nSubmit = min (submit, self.nPipes - self.nSubmitted)
            pass
        return
    
    def selectDatasets (self, ds, orderByNewest, rtQueue=None, verbose=False):
        
        """
        Processes a pipeInfo object to select the datasets from the queue which
        are highest in priority and most relevant based on date.
        In this case, rtQueue is the queue for a specific target pipeline.
        """
        
        today = datetime.datetime.utcnow ()
        hours, minutes = INGEST_TIME.split (':')
        hours, minutes = int(hours), int(minutes)
        latestDate = today - datetime.timedelta (hours=hours, minutes=minutes)
        today = latestDate.strftime ('%Y%m%d')
        datasets = {}
        
        enabledQueues = []
        while self.queues:
            (psqname, queue, data, query) = self.queues.pop ()
            if rtQueue:
                enabledQueues.append ((psqname, data))
            queueData = getDATASETS (
                psqname, data, ds, orderByNewest, self.nSubmit, today)
            while queueData:
                (dataset, priority, end, subquery, status) = queueData.pop ()
                if priority not in datasets:
                    datasets [priority] = {}
                if end not in datasets [priority]:
                    datasets [priority][end] = []
                subquery = '%s and %s' % (query, subquery)
                datasets [priority][end].append (
                    (psqname, queue, data, dataset, subquery, status))
                pass
            pass

        # All of the relevant information from the PSQ database is now in the
        # datasets structure. We will step through the data in it to determine
        # which datasets will be queued based upon the following factors:
        # Priority (lowest number to highest)
        # End Date (either newest first [orderByNewest=True] or
        #           oldest first [orderByNewest=False])
        # End Date (only those before today, EXCEPT in the case that
        #           ALLOW_TODAY=True AND the current UTC hour and minute
        #           are equal to or greater than those specified in INGEST_TIME
        
        # FIRST we check for suitable datasets in the rtQueue...
        # Give datasets in the rtQueue (real-time) priority over ANY other
        # datasets because we wish to keep up with the datasets as they come
        # off the mountain-top. This rtQueue is provided the necessary infor-
        # mation needed to launch a dataset on the pipeline.
        # This particular strategy allows the operator to 'disable' a queue
        # and keep pending 'realtime' datasets from being submitted...
        if rtQueue:
            getInfo  = "SELECT queue, query, subquery, priority, end FROM PSQ, %s, %s "
            getInfo += "WHERE psqname='%s' AND data='%s' AND "
            getInfo += "dataset=name AND dataset='%s'"
            index = 0
            while index < len (rtQueue):
                (psqname, data, dataset, status) = rtQueue [index]
                if (psqname, data) in enabledQueues:
                    if self.nSubmit:
                        sql_query = getInfo % (psqname, data, psqname, data, dataset)
                        queue, query, subquery, priority, end = mysql (sql_query)[0]
                        if end <= today:# or (end == today and PROCESS_TODAY):
                            info = (psqname, queue, data, dataset, '%s and %s' % (
                                query, subquery), status)
                            self.entries.append (info)
                            if verbose:
                                msg  = '\tGiving priority to (%s) from '
                                msg += 'the real-time queue'
                                msg %= ('%s:%s' % (info[1], info[3]))
                                print msg
                                pass
                            self.nSubmit -= 1
                            del rtQueue [index]
                            # Remove the dataset from the list of candidate datasets
                            # (if it made it into that list) because we know that it
                            # is being submitted.
                            if priority in datasets:
                                if end in datasets [priority]:
                                    if info in datasets [priority][end]:
                                        datasets [priority][end].remove (info)
                                        if not datasets [priority][end]:
                                            del datasets [priority][end]
                                            if not datasets [priority]:
                                                del datasets [priority]
                                                pass
                                            pass
                                        pass
                                    pass
                                pass
                            pass
                        else:
                            index += 1
                            pass
                        pass
                    else:
                        break
                    pass
                else:
                    index += 1
                    pass
                pass
            pass
        elif verbose:
            msg = '\tNo real-time datasets queued at the moment'
            print msg
            pass
        # Select entries to submit.
        if self.nSubmit:
            priorities = datasets.keys ()
            priorities.sort ()
            for priority in priorities:
                if self.nSubmit:
                    dates = datasets [priority].keys ()
                    dates.sort ()
                    if orderByNewest:
                        dates.reverse ()
                        pass
                    for date in dates:
                        if self.nSubmit:
                            for entry in datasets [priority][date]:
                                if self.nSubmit:
                                    self.entries.append (entry)
                                    self.nSubmit -= 1
                                else:
                                    break
                                pass
                            pass
                        else:
                            break
                        pass
                    pass
                
                else:
                    break
                pass
            pass
        return
    pass

def readCL (redo, limit, queue, queueSettings, ds):
    
    """
    Read queue and qname configuration file for redo and
    limit. This was not read when the CL started up because
    this pipeline may not have a dataset associated with it.
    """
    
    if queue not in queueSettings:
        r = getPSQ_REDO (infile = queue)
        l = getPSQ_LIMIT (infile = queue)
        queueSettings [queue] = (r, l)
    redoNOW, limitNOW = queueSettings [queue]
    if redoNOW == None:
        redoNOW = redo
    if limitNOW == None:
        limitNOW = limit
        pass
    
    #if ds not in ['pending', 'error', 'submitted']:
    if ds != None:
#        redoTMP = getPSQ_REDO (infile = ds)
        redoTMP = getPSQ_REDO (infile = queue+'_'+ds)
        if redoTMP != None:
            redoNOW = redoTMP
#        limitTMP = getPSQ_LIMIT (infile = ds)
        limitTMP = getPSQ_LIMIT (infile = queue+'_'+ds)
        if limitTMP != None:
            limitNOW = limitTMP
            pass
        pass
    return redoNOW, limitNOW

def getENIDS (query, queue, dsname, limitNOW):
    
    """
    Query the IQS for ENIDs.
    """
    
    if 'PipeConfig' in os.environ:
	fname = os.path.join (os.environ['PipeConfig'],
	    '%s_%s.enids' % (queue, dsname))
	if os.path.exists (fname):
	    enids = file (fname, 'r').readlines ()
	else:
	    enids = get_enids (query, limitNOW)
    else:
	enids = get_enids (query, limitNOW)
    return ['%s\n' % (enid.strip ()) for enid in enids if enid.strip ()]
