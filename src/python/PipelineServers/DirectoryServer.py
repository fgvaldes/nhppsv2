"""DIRECTORY server base class.


Description

The Directory Server provides a means to maintain a listing of the Node
Managers within the Pipeline system. There is only one Directory Server in the
Pipeline system (per user of course).

When a Node Manager (mgrsrv.py) is started, it looks up the node and port of
the Directory Server (dirsrv.py i.e, this file) and registers with it.

Both the Data Manager (dmsrv.py) and the Node Manager (mgrsrv.py) use the
'get_list' utility of the Directory Server to get a list of the active Node
Managers running in the Pipeline system.


Operation

The Directory Server runs as a server accepting connection through a given
port. The mode of operation is essentially that of a web server. Connections
are stateless. The Server always return a value. Communications follow the
Pipeline Team Protocol.


Usage

The Directory Server is controlled via a script (dirsrv). This script can be
used to start the Directory Server automatically at boot time. The syntax is as
follows:

dirsrv.py [--port port] [--pid-file file] [--verbose]

The optional parameter port specifies the port the Directory Server listens to
(for incoming control connections). If not present, a default port is used
[TBD].

dirsrv always returns a status code. The status code is either 0 for success or
1 for failure.


Communication Protocol

Pipeline RPC
"""

from server import *

class NodeInfo:
    def __init__ (self, port, pipeApp):
        self.port = port
        self.pipeApps = [pipeApp]
        return
    pass

class dsData (object):
    
    def __init__ (self):
        self.nodeData = {}
        return
    
    def add (self, node, port, pipeApp):
        if node not in self.nodeData:
            self.nodeData [node] = NodeInfo (port, pipeApp)
        else:
            if pipeApp not in self.nodeData [node].pipeApps:
                self.nodeData [node].pipeApps.append (pipeApp)
                pass
            if self.nodeData [node].port != port:
                self.nodeData [node].port = port
                pass
            pass
        return
    
    def remove (self, node, pipeApp):
        if node in self.nodeData:
            # Remove the pipeApp from the pipeApp list
            if pipeApp in self.nodeData [node].pipeApps:
                self.nodeData [node].pipeApps.remove (pipeApp)
                pass
            if not self.nodeData [node].pipeApps:
                # Empty list, remove the node from the list of nodes...
                del self.nodeData [node]
                pass
            pass
        return
    
    def getHostPorts (self, pipeApp=None):
        if pipeApp:
            ret = [(node, self.nodeData [node].port)
                   for node in self.nodeData
                   if pipeApp in self.nodeData [node].pipeApps]
        else:
            ret = [(node, self.nodeData [node].port)
                   for node in self.nodeData]
            pass
        return ret
    
    def getPipeApps (self, node):
        if node in self.nodeData:
            return self.nodeData [node].pipeApps
        return None
    
    def printContents (self):
        for node in self.nodeData:
            print 'Node:', node, '(%s)' % (self.nodeData [node].port)
            print '\t',
            for pipeApp in self.nodeData [node].pipeApps:
                print pipeApp,
                pass
            print
            pass
        return
    pass

# Globals
PREFIX      = 'DirectoryServer'      # error/warning message prefix
alive_nodes = dsData ()

class DIRRequestHandler (PipeRequestHandler):
    
    """
    DIR request handler base class.
    """
    
    def mgr_get_list (self):
        
        """
        Returns the full list of known running Node Managers.
        Each list entry is in the form
        
        <host name>:<port>
        
        Entries are separated by a white space. If more than one entry
        is returned, the list will be enclosed in double quotes.
        
        The input parameters are:
        
        pipeapp  OPTIONAL. Specifies that we are only interested in
                 nodes which are running processes under the specified
                 Pipeline Application, pipeapp.
        
        Returns:
        STATUS   = 'exit_code [message]'
        LIST     = '<host name>:<port> [...]'
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success
        1          failure: no data
        """
        
        if 'pipeapp' in self.params:
            pipeApp = self.params ['pipeapp']
        else:
            pipeApp = None
            pass
        
        host_ports = alive_nodes.getHostPorts (pipeApp)
        if host_ports:
            list = '%s:%s' % host_ports [0]
            for (host, port) in host_ports [1:]:
                list += ' %s:%s' % (host, port)
                pass
            packet = {'STATUS': '0 OK', 'LIST': list}
            pass
        else:
            packet = {'STATUS': '1 No data available.'}
            pass
        
        packet = composePacket (packet)
        self.wfile.write (packet)
        return
    
    def mgr_get_list_node (self):
        
        """
        Returns the full list of Pipeline Applications running processes
        on a specified host.
        Each list entry is in the form
        
        <pipeapp>
        
        Entries are separated by a white space. If more than one entry 
        is returned, the list will be enclosed in double quotes.
        
        The input parameters are:
        
        NODE     = host name
        
        Returns:
        STATUS   = 'exit_code [message]'
        LIST     = '<pipeapp> [...]'
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success.
        1          failure: no data for given host name.
        """
        
        host = self.getParam ('node')
        if not host:
            return
        
        pipeApps = alive_nodes.getPipeApps (host)
        if pipeApps:
            list = pipeApps [0]
            for pipeApp in pipeApps [1:]:
                list += ' %s' % (pipeApp)
                pass
            packet = {'STATUS': '0 OK (host: %s)' % (host), 'LIST': list}
            pass
        else:
            packet = {'STATUS': '1 No data available.'}
            pass
        
        # compose the packet
        packet = composePacket (packet)
        self.wfile.write (packet)
        return
    
    def mgr_add_pipeapp (self):
        
        """
        Adds an entry to the nodes database.
        
        The input parameters are:
        
        HOST      =
        PORT      =
        PIPEAPP   =
        
        Returns:
        STATUS    = 'exit_code [message]'
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0           success.
        >0          failure.
        """
        
        host    = self.getParam ('node')
        port    = self.getParam ('port')
        pipeApp = self.getParam ('pipeapp')
        if not (host and port and pipeApp):
            return
        
        alive_nodes.add (host, port, pipeApp)
        
        packet = composePacket ({'STATUS': '0 OK'})
        self.wfile.write (packet)
        return
    
    def mgr_del_pipeapp (self):
        
        """
        Removes an entry from the nodes database.
        
        <user>:<node>:<port>
        
        The input parameters are:
        
        NODE      =
        PIPEAPP   =
        
        Returns:
        STATUS    = 'exit_code[ message]
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0           success.
        >0          failure.
        """
        
        host    = self.getParam ('node')
        pipeApp = self.getParam ('pipeapp')
        if not (host and pipeApp):
            return
        
        alive_nodes.remove (host, pipeApp)
        
        packet = composePacket ({'STATUS': '0 OK'})
        self.wfile.write (packet)
        return
    pass


def start_dirsrv (  HandlerClass = DIRRequestHandler,
                    ServerClass = PipeSocketServer,
                    server_address = ('', 9000),
                    verbose = 0,
                    pidFile = None):
    
    """
    This runs a DIR server on port 9000 (or the first command line argument).
    """
    
    ## read the config file
    #try:
    #    config_file = os.environ['hlib'] + '/nodemanagers'
    #    conf = readConfFile (config_file)
    #    nodes = conf['nodemanagers']
    #except:
    #    nodes = None
    #
    #if nodes:
    #    for node in nodes:
    #        try:
    #            user, host_port = node.split (':', 1)
    #        except:
    #            continue
    #        if user in alive_nodes:
    #            alive_nodes[user].append (host_port)
    #        else:
    #            alive_nodes[user] = [host_port]
    
    try:
        httpd = ServerClass (server_address, HandlerClass)
    except:
        fatalError (
            PREFIX,
            'error starting DirectoryServer (port %d already in use?)' % (
            server_address[1]))
        pass
    
    if VERBOSE:
        if not len (alive_nodes.keys ()):
            warning (PREFIX, 'No known Node Manager.')
            pass
        print 'DS Node Information:'
        alive_nodes.printContents ()
        pass
    
    if pidFile:
        try:
            pid = file (pidFile, 'w')
            pid.writelines ('%d' % os.getpid())
            pid.close ()
        except:
            print "Unable to write pid:", os.getpid()
            pass
        pass
    
    print ('%s started on %s:%d' % (PREFIX, NODE, server_address[1]))
    while not httpd.done:
        try:
            httpd.handle_request ()
        except:
            httpd.done = 1
            pass
        pass
    return


VERBOSE = 0

def start (port=9000, pidFile=None, verbose=False):
    
    try:
        port = int (port)
    except:
        port = int (9000)
        pass
    
    global VERBOSE
    if verbose:
        VERBOSE = 1
        pass
    
    server_address = ('', port)
    
    start_dirsrv (
        server_address=server_address, verbose=VERBOSE, pidFile=pidFile)
    return
