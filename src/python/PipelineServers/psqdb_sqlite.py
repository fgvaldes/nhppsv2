# Version for SQLITE3.

import sys, os
import sqlite3

def psqdb_query (sql):
    
    """
    Connects to the SQLite database and executes the SQL statement
    contained in sql.  Returns a list which contains the entries for each
    row retrieved from the database.
    """
    
    try:
        db = sqlite3.connect (os.environ ['NHPPS_PSQDB'])
    except:
        sys.stderr.write ('ERROR: Cannot open PSQDB\n' % psqdb)
	sys.exit (1)

    a = db.cursor ()
    a.execute (sql)
    c = a.fetchall ()
    db.close ()

    try:
	columns = len (a.description)
    except:
	columns = 0
	pass

    if columns > 1:
	# Multiple items per row...
	return [row for row in c]
    elif columns == 1:
	# Only one item per row...
	return [row [0] for row in c]
    return []

def psqdb_update (sql):
    
    """
    Connects to the SQLite database and executes the SQL statement
    contained in sql.
    """

    try:
	db = sqlite3.connect (os.environ ['NHPPS_PSQDB'])
    except:
        sys.stderr.write ('ERROR: Cannot open PSQDB\n' % psqdb)
	sys.exit (1)
    
    a = db.cursor ()
    a.execute (sql)
    db.commit ()
    db.close ()
