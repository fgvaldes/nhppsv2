"""Data Manager server base class.

Description

TO BE WRITTEN

Operation

The Data Manager runs as a server accepting connection through a given port.
The mode of operation is essentially that of a web server. Connections are
stateless. The Data Manager always return a value. Communications follow the
Pipeline Team RPC Protocol.

Usage

The Data Manager is controlled via a script (dmsrv). This script can be used to
start the Data Manager automatically at boot time. The syntax is as follows:

dmsrv.py [port]

The optional parameter port specifies the port the Data Manager listens to (for
incoming control connections). If not present, a default port is used
(specified by the environment variable $NHPPS_PORT_DM).

dmsrv always returns a status code. The status code is either 0 for success or
1 for failure.


Communication Protocol

Pipeline RPC
"""


__version__ = '1.5'


import os
import stat
import sys
import time
import string
import socket
import popen2
import mimetools
import re
import AsyncWrapperServer
from pipeutil import *
import sqlite

# Default error message
DEFAULT_ERROR_MESSAGE = """
STATUS = "%(code)d %(message)s %(explain)s"
EOF
"""


# Globals
DBTable = 'catalog'         # name of the database table
                            # with the calibration metadata.

FREP_HAS_STRUCTURE = True   # does the File Repository have
                            # structure (class/MJD)?

DMMASTERHOST = None         # Hostname of the optional master DM 
DMMASTERPORT = None         # Port of the optional master Data Monitor. 
                            # If the master DM exists, the
                            # DM will refer to that DM if it can't find
                            # information locally.

DMMONITOR = None            # Connection to the optional 
                            # monitor. Internal use only.

TRANSACTION = 0             # Transaction number. 
                            # Internal use only.

TIMEOUT = 30                # Timeout in seconds for DB 
                            # connections.

START_TIME = None           # Start time of an instance of the DM. Used
                            # to determine if a Master DM should be 
                            # queried.

RETRY = 0.1                 # If can't connect, retry after this interval

PREFIX = 'DataManager'      # error/warning message prefix

DAYS_AGO_TO_UPDATE = 3      # how many days back to sync DM to other DM


class DataManagerServer (AsyncWrapperServer.TCPServer):
    
    """
    The DM server class. Currently implemented on
    top of the AsyncWrapperServer.TCPServer class.
    """ 
    
    # class variables
    
    DISABLE_REQUEST_HANDLER = False
    TEST_MODE               = False
    VERBOSE_MODE            = False
    CLOBBER_MODE            = False
    NO_COL_REGEX            = re.compile ('.*no such column: (.*)$')
    DB_CLASS_INFO           = {}
    DONE                    = False
    # allow_reuse_address = True  # Seems to make sense in testing environment
    #request_queue_size = 5
    
    def __init__ (self,
                  server_address,
                  HandlerClass,
                  db,
                  repository,
                  VERBOSE_MODE       = False,
                  CLOBBER_MODE       = False,
                  MASTER_MODE        = True,
                  TEST_MODE          = False,
                  days_ago_to_update = DAYS_AGO_TO_UPDATE):
        
        # base class init
        AsyncWrapperServer.TCPServer.__init__ (
            self, server_address, HandlerClass)
        
        self.__debug__    = True
        self.VERBOSE_MODE = VERBOSE_MODE
        self.CLOBBER_MODE = CLOBBER_MODE
        self.MASTER_MODE  = MASTER_MODE
        self.TEST_MODE    = TEST_MODE
        
        self.repository   = repository
        self.database     = db
        self.connection   = self.open_connection (self.database)
        if not self.connection:
            # ops! something went wrong.
            msg = 'unable to establish a connection to the DB (%s)'
            fatalError (PREFIX, msg % (self.database))
            pass
        
        self.__init_repository (self.repository)
        self.__init_class_info (days_ago_to_update)
        
        # initialize handler class
        self.RequestHandlerClass.VERBOSE_MODE = VERBOSE_MODE
        self.RequestHandlerClass.CLOBBER_MODE = CLOBBER_MODE
        
        return
    
    
    def __init_class_info (self, days_ago_to_update):
        
        """
        init class/table lookup information for present database
        """
        
        # access meta-data from database
        self.DB_CLASS_INFO = {}
        
	try:
	    results = self.query_db (
		'SELECT id, class, viewname, tablename FROM ClassDictionary;')
	except:
	    pass
	else:
	    if 'data' in results:
		result = results ['data']
	    else:
		result = None
		pass
	    
	    # if we get a result, record and break out of loop
	    if not result:
		msg = ' Can\'t initialize class location information from database'
		fatalError (PREFIX, msg)
		pass
	    
	    # now store in convenient form for later use
	    for (id, cname, vname, tname) in result:
		cname = str (cname)
		self.DB_CLASS_INFO [cname] = (id, vname, tname)
		pass
        
        # how many days back to sync DM to other DMs
        sinceMJD = MJD () - days_ago_to_update
        
        # now try to sync our information to other DMs we know of
        dmList = self.get_master_dm_list ()
        for (node, port) in dmList:
            if self.VERBOSE_MODE:
                print_msg (' Try to sync to dm %s:%s' % (node, port), True)
                pass
            
            # first this inits from remote DM
            self.initFrom (node, port, sinceMJD=sinceMJD)
            
            # now, we init back to remote DM any info we may
            # have that it doesn't have, that is, IF we are
            # another master node ourselves
            if self.MASTER_MODE: 
                # is a master, so sync back
                self.initTo (node, port, sinceMJD=sinceMJD)
            else:
                # is NOT a master, so terminate now as all master nodes alike
                # and we don't gain anything further from more syncing 
                break
            pass
        return
    
    def __init_repository (self, frep):
        
        """
        Just makes sure that the File Repository is there 
        and has the right directory structure.
        """
        
        if not makeDirs (frep):
            msg =  'the File Repository (%s) does not '
            msg += 'exist and cannot be created!'
            fatalError (PREFIX, msg % (frep))
            pass
        return
    
    
    def get_master_dm_list (self):
        
        """
        Returns a list of address port tuples
        of Master mode DM we can connect to.
        """
        
        dm_list = []
        
        if DIRHOST:
            pkt = composePacket ({'COMMAND': 'get_list',})
            try:
                response = sendPacketTo (
                    pkt, DIRHOST, DIRPORT, will_respond=True)
                result = parsePacket (response)
                if 'list' in result:
                    list = result ['list']
                    if list:
                        # parse list entries into our list of addresses
                        ignore_addr = NODE
                        ignore_port = str (self.server_address [1])
                        dm_list = parse_dm_list_str (
                            list, ignore_addr, ignore_port)
                        pass
                    pass
                pass
            except socket.error:
                # can't connect...directory server is down
                msg =  'DM can\'t connect to the directory server at %s:%s.\n'
                msg += 'Returning empty list of alternate data managers'
                warning (PREFIX, msg % (DIRHOST, DIRPORT))
            except:
                # pass along the error
                raise sys.exc_type, sys.exc_value
            pass
        
        if self.VERBOSE_MODE:
            print_msg ('GOT DM_LIST:%s' % str (dm_list), True)
            pass
        
        return dm_list
    
    
    def get_connection (self):
        
        connection = self.connection
        if not connection:
            raise DBConnectionError, 'Error in connecting to the DB.'
        return connection
    
    
    def get_repository (self):
        
        return self.repository
    
    
    def handle_request (self):
        
        """
        Start handling requests from to the server
        """
        
        if self.DISABLE_REQUEST_HANDLER:
            msg = ' handle_requests called - but DM is currently disabled. '
            print_msg (msg, True)
        else:
            AsyncWrapperServer.loop ()
            pass
        return
    
    
    def initTo (self, addr, port, sinceMJD=None):
        
        """
        Sync the local database to a remote DM, with
        an option to set how far back in time we wish to sync,
        and whether or not we wish to clobber current, remote
        information which conflicts with that in this DM.
        """
        
        if self.VERBOSE_MODE:
            print_msg (' Init DM TO DM at %s:%s' % (addr, port), True)
            pass
        
        # if no time limit is specified make it X days ago
        if not sinceMJD:
            sinceMJD = MJD () - DAYS_AGO_TO_UPDATE
            pass
        
        try:
            connect = self.get_connection ()
            initTo (
                addr, port, connect, self.get_repository(), sinceMJD=sinceMJD,
                verbose=self.VERBOSE_MODE, clobber=self.CLOBBER_MODE)
        except socket.error:
            msg  = '** Unable to initTo DataManager at %s:%s! **\n'
            msg += '  because: error_type:%s msg:%s.'
            warning (PREFIX, msg % (addr, port, sys.exc_type, sys.exc_value))
            return
        except:
            raise sys.exc_type, sys.exc_value
        
        return
    
    
    def initFrom (self, addr=None, port=None, sinceMJD=None):
        
        """
        Sync the database of the DM to another one, with
        an option to set how far back in time we wish to sync,
        and whether or not we wish to clobber current, local  
        information which conflicts with that in the other DM.
        """ 
        
        if self.VERBOSE_MODE:
            print_msg (' Init DM FROM DM at %s:%s' % (addr, port), True)
            pass
        
        # disable handler
        self.DISABLE_REQUEST_HANDLER = True
        
        # safety: if no time limit is specified, lets make it X days ago
        if not sinceMJD:
            sinceMJD = MJD () - DAYS_AGO_TO_UPDATE
            pass
        
        try:
            connect = self.get_connection ()
            initFrom (
                addr, port, sinceMJD=sinceMJD, dbconnection=connect,
                verbose=self.VERBOSE_MODE, clobber=self.CLOBBER_MODE,
                caldir=self.get_repository ())
        except socket.error:
            msg  = '** Unable to initFrom DataManager at %s:%s! **\n  '
            msg += 'because: error_type:%s msg:%s.'
            warning (PREFIX, msg % (addr, port, sys.exc_type, sys.exc_value))
            self.DISABLE_REQUEST_HANDLER = False
            return
        except:
            raise sys.exc_type, sys.exc_value
        
        # return to former state
        self.DISABLE_REQUEST_HANDLER = False
        
        return
    
    
    def open_connection (self, database):
        
        """
        Opens a connection to the DB database and
        returns the connection object.
        self.connection
        """
        
        return open_connection (database)
    
    
    def server_bind (self):
        
        """
        Override server_bind to store the server name.
        """
        
        AsyncWrapperServer.TCPServer.server_bind (self)
        
        host, port       = self.socket.getsockname ()
        self.server_name = socket.getfqdn (host)
        self.server_port = port
        
        return
    
    
    def query_db (self, sql, fetchone=False, verbose=None):
        
        """
        send a query to the database
        """
        
        if verbose == None:
            verbose = self.VERBOSE_MODE
            pass
        
        return query_db (
            self.get_connection (), sql, fetchone=fetchone, verbose=verbose)
    
    
    def update_db (self, sql, prefix=PREFIX, MasterSync=False):
        
        """
        send a request to update the database
        """
        
        startTime = MJD () # record start of update
        try:
            #if (self.VERBOSE_MODE):
            #    msg = ' *** master sync is: %s ***********'
            #    print_msg (msg % MasterSync, True)
            #    pass
            update_db (
                self.get_connection (), sql,
                verbose=self.VERBOSE_MODE, prefix=prefix)
        except:
            raise sys.exc_type, sys.exc_value
        
        # looks like everything went OK, therefore we need to update any
        # sibling data managers running in master mode IF we are in master
        # mode ourselves
        if MasterSync and self.MASTER_MODE:
            
            # 1. determine if we have any New, additional files to ship
            sql = 'select * from catalog where ( datatype == \'file\' '
            sql += 'or datatype == \'image\' or datatype == \'directory\' ) '
            sql += 'and mjdstart > %f ' % startTime
            new_cal_results = self.query_db (sql)
            new_cal_files = find_cal_files_from_results (
                new_cal_results, check_exist=False)
            
            # 2. find list of master dm's, then update
            dmList = [] #self.get_master_dm_list ()
            for (node, port) in dmList:
                if self.VERBOSE_MODE:
                    print_msg (' Try to update dm %s:%s' % (node, port), True)
                    pass
                
                # 1. Transfer Cal files, if possible
                if len (new_cal_files) > 0:
                    remote_repository = get_remote_repository (node, port)
                    remote_loc = '%s:%s' % (node, repository)
                    # FIX? : we could check if these files already exist
                    # first... and ONLY copy them if 1. don't exist or 2.
                    # in clobber mode
                    move_files (
                        new_cal_files, remote_loc, True, self.get_repository(),
                        True, self.VERBOSE_MODE)
                    pass
                
                # 2. update remote table
                pkt = composePacket ({
                       'COMMAND': 'sql_update',
                           'SQL': sql,
                    'MASTERSYNC': False})
                
                response = sendPacketTo (
                    pkt, node, int (port), timeout=TIMEOUT, will_respond=True)
                
                result = parsePacket (response)
                status = string.split (result ['status'])
                
                if int (status [0]):
                    msg  = 'Can\'t update remote table using sql:[%s]\n '
                    msg += 'error:%s'
                    warning (PREFIX, msg % (sql, str (response)))
                elif self.VERBOSE_MODE: 
                    print_msg (' Updated with:%s' % sql, True)
                    pass
                pass
            pass
        return
    pass
# <-- end class DataManagerServer


class DataManagerRequestHandler (AsyncWrapperServer.StreamRequestHandler):
    
    """
    DataManager request handler base class.
    """
    
    # Essentially static class variables
    CLOBBER_MODE = False
    VERBOSE_MODE = False
    
    # The version of the Pipeline protocol we support.
    # Don't override unless you know what you're doing (hint: incoming
    # requests are required to have exactly this version string). Is it
    # really true? Bah!
    protocol_version = '1.0'
    
    # The Message-like class used to parse headers
    MessageClass = mimetools.Message
    
    def __init__ (self, request, client_addr, server, terminator='EOF\n'):
        
        """
        Standard constructor: setup instance variables.
        """
        
        AsyncWrapperServer.StreamRequestHandler.__init__ (
            self, request, client_addr, server, terminator=terminator)
        
        return
    
    
    def __parse_request (self, request):
        
        """
        Parse a request (internal).
        
        The request parsed will be formatted as a request packet.
        Results are returned in self.command and self.params attributes.
        
        Return value is 1 for success, 0 for failure.
        """
        
        self.command = None
        self.params  = {}
        
        iterator = request.iterkeys()
        for key in iterator:
            self.params [string.lower (key)] = request.get (key)
            pass
        try:
            self.command = self.params ['command']
            del self.params ['command']
        except:
            self.send_error (
                203, 'Partial information: \'command\' parameter is missing.')
            return 0
        
        return 1
    
    
    def handle (self):
        
        """
        Handle a single request.
        """
        
        global TRANSACTION

        if self.VERBOSE_MODE:
            start_time = time.time ()
            print 'RAW LINES: %s' % self.raw_requestlines
            pass
        
        # increment the TRANSACTION counter
        TRANSACTION += 1
        
        # Parse the packet
        packet = parsePacket (self.terminator.join (self.raw_requestlines))
        
        if self.VERBOSE_MODE:
            print_msg ('dm gets packet: %s' % packet, True)
            now = time.time ()
            dt  = now - start_time
            print_msg ('Time (read): %.3fs' % dt, True)
            pass
        
        # now it is time to parse the packet and respond approprately.
        if not self.__parse_request (packet):
            return
        
        if self.VERBOSE_MODE:
            new_now = time.time ()
            dt      = new_now - now
            now     = new_now
            print_msg ('Time (parse): %.3fs' % dt, True)
            pass
        
        # invoke the appropriate method
        mname = 'mgr_' + self.command
        if hasattr (self, mname):
            method = getattr (self, mname)
        else:
            self.send_error (501, 'Unsupported method (%s)' % (self.command))
            return
        
        # invoke the method. 
        #if (self.VERBOSE_MODE):
        #    print_msg ('%s CALLED @ MJD %.3f' % (self.command, MJD()), True)
        #    pass
        try:
            header, return_pkt = method ()
        except:
            header = 'Error in %s' % (self.command)
            return_pkt = composePacket ({'STATUS': '1 %s' % (self.command)})
            pass
        
        if self.VERBOSE_MODE:
            # calculate benchmark
            new_now = time.time ()
            dt      = new_now - now
            now     = new_now
            print_msg ('Time (handler:%s): %.3fs' % (self.command, dt), True)
            
            # write some information about our activity to the DM monitor, 
            # if we have a monitor, that is.
            
            # log a message to the local log, IF verbose mode is used
            # label = string.upper(self.command) + ' (%.3fs)' % dt
            # print_msg ('%s\n%s\n%s' % (label, header, body), True)
            
            dt = new_now - start_time
            print_msg ('Time (total): %.3fs\n' % dt, True)
            pass
        
        # Input packet info to return to the client
        
        # KLUDGE : dont return empty packet (getcal hangs if you do)
        # this condition may occur if an error happened (not the right
        # way to do this IMO).
        if len (return_pkt) > 0:
            if self.VERBOSE_MODE:
                print_msg (' DMSERV sends return packet:[%s]' % return_pkt)
                pass
            self.push (return_pkt)
            pass
        return
    
    
    def send_error (self, code, message=None):
        
        """
        Send an error reply.
        
        Arguments are the error code, and a detailed message.
        The detailed message defaults to the short entry matching the
        response code.
        """
        
        try:
            short, long = responses [code]
        except KeyError:
            short, long = '???', '???'
            pass
        if not message:
            message = short
            pass
        explain = long
        
        # queue the response
        self.push (DEFAULT_ERROR_MESSAGE % (code, message, explain))
        return
    
    
    def send_response (self, code, message=None):
        
        """
        Send the response.
        """
        
        if message == None:
            if responses.has_key (code):
                message = responses [code][0]
            else:
                message = ''
                pass
            pass
        print 'DM.send_response sends:[%s]' % (message)
        return
    
    
    def address_string (self):
        
        """
        Return the client address formatted for logging.
        
        This version looks up the full hostname using gethostbyaddr(),
        and tries to find a name that contains at least one dot.
        """
        
        host, port = self.client_address
        return socket.getfqdn (host)
    
    
    def compose_sql_getcal (self, dict):
        
        """
        Convenience routine for better 
        source code readibility.
        
        INTERNAL USE
        """

	# Patterns for operators.
	ops = ('=', '!=', 'isnull', 'notnull', 'like', 'glob')

        mjd = str (float (dict ['mjd']))
        if dict ['exptime']:
	    exptime = str (float (dict ['exptime']))
	    if dict ['dexptime']:
		dexptime = str (max (0.001, float (dict ['dexptime'])))
	    else:
		dexptime = str (0.001)
	    dexptime = str (float (dexptime) * float (exptime))

        # compose the SQL query so that we get a full list of
        # reasonable candidates with the closest in time
        # listed first. If the delta MJD is the same, we give 
        # precedence to the calibrations acquired BEFORE our 
        # observations (hence the weird double sort).

        sql  = 'SELECT value, datatype, class, mjdstart, '
	sql += '%s AS mjd, %s - (mjdstart + mjdend) / 2 AS dmjd, '
	if dict ['exptime']:
	    sql += 'ABS (IFNULL (exptime, 0.0) - %s) AS dexptime ' % exptime
	else:
	    sql += '0.0 AS dexptime '
        sql += 'FROM %s WHERE '
        sql  = sql % (mjd, mjd, DBTable)

	w = string.split (dict ['class'], ' ', 1)
	sql += 'class like "%s" ' % (w[0])
	if len(w) > 1:
	    classord = 'class %s, ' % (w[1])
	else:
	    classord = 'class, '
        if dict ['detector']:
	    w = string.split (dict ['detector'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND detector %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND detector %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND detector = "%s" ' % dict ['detector']
            pass
        if dict ['imageid']:
	    w = string.split (dict ['imageid'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND imageid %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND imageid %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND imageid = "%s" ' % dict ['imageid']
            pass
        if dict ['filter']:
	    w = string.split (dict ['filter'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND filter %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND filter %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND filter like "%s" ' % dict ['filter']
            pass
	if dict ['dmjd']:
	    sql += 'AND ABS (dmjd) <= %s ' % str (float (dict ['dmjd']))
	    pass
        if dict ['exptime']:
            sql += 'AND dexptime <= %s '   % dexptime
            pass
	if dict ['quality']:
            sql += 'AND quality >= "%s" '    % dict ['quality']
            pass
	if dict ['match']:
	    w = string.split (dict ['match'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND match %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND match %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND match like "%s" ' % dict ['match']
            pass
        
        # add the constraints
        sql += 'AND mjd >= mjdstart+0 AND mjd < mjdend+0 '
        
        # order
	sql += 'ORDER BY '
	sql += 'quality desc, '
        #sql += 'min(10,ncombine) desc, '
	sql += classord
        #sql += 'min(5,ncombine) desc, '
        sql += 'min(2,ncombine) desc, '
        if dict ['exptime']:
	    sql += 'dexptime, '
        sql += 'ABS (dmjd), dmjd'

        return sql
    
    
    def compose_sql_rmcal (self, dict):
        
        """
        Convenience routine for better 
        source code readibility.
        
        INTERNAL USE
        """

	# Patterns for operators.
	ops = ('=', '!=', 'isnull', 'notnull', 'like', 'glob')

	sql  = 'DELETE FROM %s WHERE ' % DBTable
	w = string.split (dict ['class'], ' ', 1)
	if w[0] == 'isnull' or w[0] == 'notnull':
	    sql += 'AND class %s ' % (w[0])
	elif w[0] in ops:
	    sql += 'class %s "%s" ' % (w[0],w[1])
	else:
	    sql += 'class = "%s" ' % dict ['class']
	if dict ['mjd']:
	    mjd = str (float (dict ['mjd']))
	    sql += 'AND mjdstart <= %s AND mjdend > %s ' % (mjd, mjd)
	    pass
        if dict ['detector']:
	    w = string.split (dict ['detector'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND detector %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND detector %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND detector = "%s" ' % dict ['detector']
            pass
        if dict ['imageid']:
	    w = string.split (dict ['imageid'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND imageid %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND imageid %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND imageid = "%s" ' % dict ['imageid']
            pass
        if dict ['filter']:
	    w = string.split (dict ['filter'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND filter %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND filter %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND filter = "%s" ' % dict ['filter']
            pass
        if dict ['exptime']:
	    exptime = str (float (dict ['exptime']))
	    if dict ['dexptime']:
		dexptime = str (max (0.001, float (dict ['dexptime'])))
	    else:
		dexptime = str (0.001)
	    dexptime = str (float (dexptime) * float (exptime))
            sql += 'AND ABS (exptime - %s) <= %s '   % (exptime, dexptime)
            pass
	if dict ['quality']:
            sql += 'AND quality >= "%s" '    % dict ['quality']
            pass
	if dict ['match']:
	    w = string.split (dict ['match'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND match %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND match %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND match = "%s" ' % dict ['match']
            pass
	if dict ['value']:
	    w = string.split (dict ['value'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND value %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND value %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND value = "%s" ' % dict ['value']
	    pass
	if dict ['datatype']:
	    w = string.split (dict ['datatype'], ' ', 1)
	    if w[0] == 'isnull' or w[0] == 'notnull':
		sql += 'AND datatype %s ' % (w[0])
	    elif w[0] in ops:
		sql += 'AND datatype %s "%s" ' % (w[0],w[1])
	    else:
		sql += 'AND datatype = "%s" ' % dict ['datatype']
	    pass
        
        return sql
    
    
    def do_search_for_keyword (self, keyword, returnOneResult=True):
        
        """
        Internal routine to initiate a search for a keyword
        """
        
        # these are all terms we want to ignore in the param list
        ignoreTerms = {
            'telescope':None, 
             'detector':None, 
               'filter':None, 
                'multi':None,
              'keyword':None,
                'class':None}
        
        # quick check
        if not self.params.has_key ('class'):
            raise MissingAttributeError, 'missing \'class\' parameter'
        
        # Create our search term dictionary
        # and include a check for the required keywords
        limit_terms = self.winnow_dictionary (ignoreTerms, self.params)
        
        # Determine the observing configuration, if any
        observeConfigs = self.find_obs_configs (self.params)
        if len (observeConfigs) > 0:
            limit_terms ['obsConfig'] = observeConfigs
            pass
        
        # Where are we going to search: does the declared
        # class exist in the database?
        obj_class, viewName, tableName = self.find_class_info (
            self.params ['class'])
        
        # create the sql
        sql = compose_sql_query (
            keyword, viewName, limit_terms, self.VERBOSE_MODE)
        
        # query the database
        result = self.server.query_db (sql, returnOneResult)
        
        return (sql, result ['data'])
    
    
    def get_process_id (self, procid):
        
        """
        Find the process type (int) id using the passed string procid
        """
        
        return self.get_object_id (procid, 'ProcessingProcesses')
    
    
    def get_object_id (self, id_str, tableName):
        
        """
        Find the object of type (int) id using the passed string id
        """
        
        sql = 'SELECT ROWID FROM %s WHERE id_str = "%s";' % (tableName, id_str)
        results = self.server.query_db (sql, True)
        item = results ['data']
        if not item or len (item) < 1:
            return long (-1)
        
        return long (item [0])
    
    
    def get_rule_params (self, limit_terms):
        
        params = {}
        limit = {}
        
        # get space delimited list of id's
        sql = compose_sql_query (
            'parameterList', 'ProcessingRules', limit_terms, self.VERBOSE_MODE)
        result = self.server.query_db (sql, fetchone=True)
        data = result ['data']
        
        plistStr = data [0]
        for p in string.split (plistStr): 
            limit ['id'] = p
            sql = compose_sql_query (
                'name,datatype,nullallowed',
                'ProcessingRuleParams',
                limit,
                self.VERBOSE_MODE)
            result = self.server.query_db (sql, fetchone=True)
            attribs = convert_sql_result_to_simple_dict (result)
            params [result ['data'][0]] = attribs
            pass
        
        return params
    
    
    # probably should be static..
    def winnow_dictionary (self, ignoreTerms, dict):
        
        """
        winnow down a table of terms based on a list
        we wish to ignore. Also cast's any strings
        which are numbers as floats, if it can do so
        """
        
        # Create our search term dictionary
        # and include a check for the required keywords
        terms = {}
        for item in dict.keys():
            if ignoreTerms.has_key (item):
                # skip these items.. 
                continue
            else:
                val = None
                # try to put it in as a float, if that fails,
                # then capture as a string
                try:
                    val = float (dict [item])
                except:
                    val = dict [item]
                    pass
                
                terms [item] = val
                pass
            pass
        
        return terms
    
    
    def find_obj_class (self, tableName, classname):
        
        """
        Find the object class (int) id using the passed string table, classname
        """
        
        classId = -1
        
        sql = 'SELECT id FROM %s WHERE class = "%s";' % (tableName, classname)
        results = self.server.query_db (sql, True)
        item = results ['data']
        
        if item and len (item) > 0:
            classId = item [0]
        else:
            raise MissingAttributeError, 'class %s not valid' % classname
        
        return classId
    
    
    def find_class_info (self, classname):
        
        """
        Find the information about a classname in our database
        which we may use to access the class.
        """
        
        try:
            return self.server.DB_CLASS_INFO [classname]
        except:
            # FIX: not the appropriate error
            msg = 'Requested class: (%s) is undefined/invalid'
            raise MissingAttributeError, msg % classname
        
        return
    
    
    def find_obs_configs (self, dict):
        
        """
        Find the observing configureation ids that match
        the passed parameter string values
        """
        
        observeIds      = tuple ()
        nrof_conditions = False
        telescope       = 'unknown'
        detector        = 'unknown'
        filter          = 'unknown'
        
        if 'telescope' in dict:
            telescope = dict ['telescope']
            pass
        if 'detector' in dict:
            detector = dict ['detector']
            pass
        if 'filter' in dict:
            filter = dict ['filter']
            pass
        
        sql = 'select id from ObservationConfig where'
        
        tel_id = self.find_obs_component_id ('Telescopes', 'name', telescope)
        if tel_id > -1:
            sql += ' telescope_id="%s"' % (str (tel_id))
            nrof_conditions = True
            pass
        
        det_id = self.find_obs_component_id ('Detectors', 'name', detector)
        if det_id > -1:
            if nrof_conditions:
                sql += ' and'
                pass
            sql += ' detector_id="%s"' % (str (det_id))
            nrof_conditions = True
            pass
        
        filter_id = self.find_obs_component_id ('Filters', 'sn', filter)
        if filter_id > -1:
            if nrof_conditions:
                sql += ' and'
                pass
            sql += ' filter_id="%s"' % (str (filter_id))
            nrof_conditions = True
            pass
        
        if nrof_conditions:
            # ONLY query if we have constraints! 
            try:
                results = self.server.query_db (sql)
                if results ['data']:
                    result = results ['data']
                    observeIds = tuple (result [0])
                    pass
                pass
            except:
                raise sys.exc_type, sys.exc_value
            pass
        
        if self.VERBOSE_MODE:
            msg = 'returning obs config(s) of :[%s] for %s:%s:%s'
            print_msg (msg % (observeIds, telescope, detector, filter), True)
            pass
        
        return observeIds
    
    
    def find_obs_component_id (self, tableName, col, name):
        
        """
        internal method for quickly finding the id of an observation
        component from its name
        """
        
        compId = -1
        
        sql = 'select id from %s where %s="%s";' % (tableName, col, name)
        
        try:
            results = self.server.query_db (sql)
            result = results ['data']
            if result:
                compId = int ((result [0])[0])
                pass
            pass
        except:
            raise sys.exc_type, sys.exc_value
        
        if self.VERBOSE_MODE:
            msg = ' got component id:%d for table:%s'
            print_msg (msg % (compId, tableName), True)
            pass
        
        return compId
    
    
    def mgr_echo (self):
        
        """
        Simple debugging method (internal)
        """
        
        out_msg = 'STATUS = 0\n'
        for key in self.params.keys ():
            val = self.params[key]
            if string.find (val, ' ') != -1:
                if findNextCh (val, '"', 0) != -1:
                    val = "'%s'" % (val)
                else:
                    val = '"%s"' % (val)
                    pass
                pass
            out_msg += '%s = %s\n' % (string.upper (key), string.upper (val))
            pass
        #out_msg += 'EOF\n'
        
        return ('echo', out_msg)
    
    
    def mgr_findid (self):
        
        """
        Find the 'best' id of the requested object 
        
        The input parameters (stored in self.params) are:
        
        CLASS       The type of object we want to find an id for.
                    e.g. 'ZeroImage'
                    
        And one or more groupings of the following:
        
        <PARAM>     The exact value of a parameter we want. 
        
        <PARAM>_MIN Indicates a minimum in the value of a parameter 
                    we want. In this case, the prior <PARAM> value 
                    becomes the centriod of the range and <PARAM>_MAX
                    must be sent as well.
                    
        <PARAM>_MAX The maximum value of a parameter we want
                    In this case, the prior <PARAM> value
		    becomes the centriod of the range and 
                    <PARAM>_MIN must be sent as well.
                    
        Returns:
        
        STATUS      'exit_code findid'
        VALUE       The value of the object id 
                    e.g. 'Mosaic2_CAL9906_6_bpm.fits'
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success
        1          failure: no data
        """
        
        # set up
        value    = None
        sql, pkt = '',''
        
        # main body..catch all possible error conditions in try
        try:
            sql, result = self.do_search_for_keyword ('id_str', True)
            
            # query the database
            results = self.server.query_db (sql, True)
            result = results ['data']
            
            if result:
                value = str (result [0])
            elif self.VERBOSE_MODE:
                print_msg ('NO result!', True)
                pass
            
            # hmm. everything OK...make a packet
            pkt = composePacket ({'STATUS': '0 FindID', 'VALUE': value})
            
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt   = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_getcal_new (self):
        
        pkt = composePacket ({'STATUS': '0 GETCAL'})
        return ('', pkt)
    
    
    def mgr_getcal (self):
        
        """
        Get calibrations
        
        The input parameters (stored in self.params) are:
        
        CLASS       The type of calibration we want.
                    e.g. 'Zero'
        MJD         Modified Julian Day.
                    e.g. 52642.4
        DETECTOR    The string identifying the detector
                    e.g. 'Mosaic2'
        IMAGEID     Image/Amp ID
                    e.g. 'SITe #98164FACR10-02 (NOAO 21), lower left (Amp13)'
        FILTER      Filter name
                    e.g. '815 815_v1 k1026	'
        EXPTIME     Exposure time
                    e.g. 120.
        DMJD        DMJD window
                    e.g. 10 for +- 10 days
        DEXPTIME    Exposure time window as fraction of EXPTIME
                    e.g. 0.1 for within 10% of requested exposure time
	QUALITY     Minimum desired quality value
	            e.g. 1
	MATCH       Match pattern string
	            e.g. 'glob "DA=4 FS=[12]"'
        VALUE       Destination directory path (in IRAF network notation)
                    e.g. 'pipedevn!/pipelines/fpierfed/MarioCal/'
        
        Returns:
        STATUS      'exit_code getcal'
        VALUE       The value of the catalog entry (depends on the data type)
                    e.g. 'Mosaic2_CAL9906_6_bpm.fits[1]'
        TYPE        The type of the item one of { 'file', 'image', 'directory',
                    etc. }
        PATH        The IRAF path to the item
        MTIME       The last modification time of the item
        SIZE        The size of the item in bytes
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success
        1          failure: no data
        """
        
        global FREP_HAS_STRUCTURE
        
        # get the required keywords
        keywords = {
               'class': '<<REQUIRED>>', 
                 'mjd': '<<REQUIRED>>', 
            'detector': None, 
             'imageid': None, 
              'filter': None, 
             'exptime': None, 
                'dmjd': None, 
            'dexptime': None, 
             'quality': None, 
               'match': None, 
               'value': None}
        
        keywords = self.getKeywords (keywords)
        
        # compose_sql_xxx is just a macro; it is NOT 
        # a general purpose routine. It is here for 
        # code readibility reasons alone...
        sql = self.compose_sql_getcal (keywords)
        
        result = self.server.query_db (sql, True)
        
        if len (result ['fields']) == 7:
            (value, datatype, fclass, fmjd) = result ['data'][0:4]
        elif len (result ['fields']) == 0:
	    # check for default for unknown detectors/filters.
	    if len(result['fields'])==0 and keywords['filter']:
	        keywords['filter'] = 'any'
		sql = self.compose_sql_getcal (keywords)
		result = self.server.query_db (sql, True)
	    if len(result['fields'])==0 and keywords['detector']:
	        keywords['detector'] = 'any'
		sql = self.compose_sql_getcal (keywords)
		result = self.server.query_db (sql, True)

	    if len(result['fields'])==0:
		# no match ! Not sure I agree with this interface.. 
		if self.VERBOSE_MODE:
		    print_msg ('The query produced no data (%s).' % (sql), False)
		    pass
		
		pkt = composePacket ({'STATUS': '2 the query produced no data'})
		return (sql, pkt)

	    (value, datatype, fclass, fmjd) = result ['data'][0:4]
        else:
            print_msg ('Error in sending the query (%s)' % (sql), False)
            pkt = composePacket ({'STATUS': '1 getcal error in the query'})
            #self.send_error (1, 'error in the query (%s).' % (sql))
            return (sql, pkt);
        
        if datatype == 'keyword':
            # no need to do anything further; just return value of the keyword.
            pkt = composePacket ({'STATUS': '0 getcal', 'VALUE': value})
        else:
            frep = self.server.get_repository ()
            
            if frep [-1] == '/':
                # remove the trailing slash!
                frep = frep [:-1]
                pass
            
            # does the frep have structure?
            if FREP_HAS_STRUCTURE:
                # round fmjd to the nearest int
                fmjd = str (int (round (float (fmjd))))
                frep += '/%s/%s' % (fclass, fmjd)
                pass
            
            irafpath = '%s!%s' % (NODE, frep)
            
            #print ' * FILE REPOSITORY IS : %s' % frep
            #print ' * FILE IS : %s' % value
            #print ' * SERVER ADDR IS : %s' % NODE
            #print ' * iraf path is: %s' % irafpath
            
            try:
                stats = os.stat ('%s/%s' % (frep, value))
                mtime = stats [stat.ST_MTIME]
                size  = stats [stat.ST_SIZE]
                #print ' * mtime is: %s' % mtime
                #print ' * size is: %s' % size
            except:
                print 'Could not open file %s/%s' % (frep, value)
                pass
            
            # FIX : missing some stuff here
            pkt = composePacket ({
                'STATUS': '0 getcal',
                 'VALUE': value,
                  'TYPE': datatype,
                 'PATH' : irafpath,
                 'MTIME': mtime,
                  'SIZE': size})
            
            # for getcal..the extra EOF appears important??
            pass
        
        return (sql, pkt)
    
    
    def mgr_rmcal (self):
        
        """
        Delete calibrations
        
        The input parameters (stored in self.params) are:
        
        CLASS       The type of calibration
                    e.g. 'Zero'
        MJD         Modified Julian Day.
                    e.g. 52642.4
        DETECTOR    The string identifying the detector
                    e.g. 'Mosaic2'
        IMAGEID     Image/Amp ID
                    e.g. 'SITe #98164FACR10-02 (NOAO 21), lower left (Amp13)'
        FILTER      Filter name
                    e.g. '815 815_v1 k1026	'
        EXPTIME     Exposure time
                    e.g. 120.
        DEXPTIME    Exposure time window as fraction of EXPTIME
                    e.g. 0.1 for within 10% of requested exposure time
	QUALITY     Minimum desired quality value
	            e.g. 1
	MATCH       Match pattern string
	            e.g. 'glob "DA=4 FS=[12]"'
        VALUE       Match value string
                    e.g. 'glob "*ABC*.fits"'
        DATATYPE    Datatype
                    e.g. 'keyword'
        
        Returns:
        STATUS      'exit_code rmcal'
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success
        1          failure
        """
        
        # get the required keywords
        keywords = {
               'class': '<<REQUIRED>>',
                 'mjd': None, 
            'detector': None, 
             'imageid': None, 
              'filter': None, 
             'exptime': None, 
            'dexptime': None, 
             'quality': None, 
               'match': None, 
               'value': None,
	     'datatype': None}

        error  = None
        
        keywords = self.getKeywords (keywords)
        
        # compose_sql_xxx is just a macro; it is NOT 
        # a general purpose routine. It is here for 
        # code readibility reasons alone...

        sql = self.compose_sql_rmcal (keywords)
	if self.VERBOSE_MODE:
	    print_msg ('sql: (%s)' % (sql), False)
	    pass
        
        # delete from the database
        connection = self.server.get_connection ()
        if connection and not error:
            # Send off the query...
            # The database could be locked!
            querySent = False
            start     = time.time ()
            while not querySent and (time.time () - start) < TIMEOUT:
                try:
                    # get a cursor
                    dbc = connection.cursor ()
                    # send the query
                    dbc.execute (sql)
                    querySent = True
                except:
                    msg = 'rmcal: the DB is busy. Retrying in %fs.\n'
                    warning (PREFIX, msg % RETRY)
                    time.sleep (RETRY)
                    pass
                pass
            
            if not querySent:
                error  = '1 Error in sending the query (%s)' % (sql)
                pass
            pass
        else:
            error  = '1 Error in connecting to the DB.'
            pass
        
        # compose the packet
        if not error:
            # close the transaction
            connection.commit ()
            pkt = composePacket ({'STATUS': '0 rmcal'})
        else:
            pkt = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_getkeywordvalue (self):
        
        """
        Find all of the values of a given keyword from
        the 'best' object which matches in the database.
        
        The input parameters (stored in self.params) are:
        
        CLASS       The type of object we want to find the keyword for.
                    e.g. 'ZeroImage'
                    
        KEYWORD     The name of the keyword to search for the value of.
        
        ID_STR      The string id of the object we are looking for.
        
        Returns:
        
        STATUS      'exit_code getkeywordvalue'
        VALUE       The 'best' value of the object
                    e.g. 'Mosaic2_CAL9906_6_bpm.fits'
        EOF
        
        In case of no match the  VALUE is empty.
        
        Exit codes:
        0          success
        1          failure: no data
        """
        
        # set up
        value = None
        sql, pkt = '',''
        keywords = {
             'id_str': '<<REQUIRED>>',
              'class': '<<REQUIRED>>',
            'keyword': '<<REQUIRED>>'}
        
        # main body..catch all possible error conditions in try
        try:
            # get keywords from passed parameters
            # and include a check for the required keywords
            # FIX: Sloppy! All we are using this for is to
            # check that keyword is present!
            keywords = self.getKeywords (keywords)
            
            # query the database
            sql, result = self.do_search_for_keyword (
                self.params ['keyword'], True)
            
            if result:
                value = str (result [0])
            elif self.VERBOSE_MODE:
                print_msg ('NO result!', True)
                pass
            
            # hmm. everything OK...make a packet
            pkt = composePacket ({
                'STATUS': '0 GetKeyWordValue', 'VALUE': value})
            
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt   = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_getkeywordvalues (self):
        
        """
        Find all of the values of a given keyword from
        objects which match in the database.
        
        The input parameters (stored in self.params) are:
        
        CLASS       The type of object we want to find the keyword for.
                    e.g. 'ZeroImage'
                    
        KEYWORD     The name of the keyword to search for values of.
        
        MULTI       0/1 specifies multiple or single result to be returned.
        
        And one or more groupings of the following that specify the
        search terms:
        
        <PARAM>     The exact value of a parameter we want.
        
        <PARAM>_MIN Indicates a minimum in the value of a parameter
                    we want. In this case, the prior <PARAM> value
                    becomes the centriod of the range and <PARAM>_MAX
                    must be sent as well.
                    
        <PARAM>_MAX The maximum value of a parameter we want
                    In this case, the prior <PARAM> value
                    becomes the centriod of the range and
                    <PARAM>_MIN must be sent as well.
                    
        Returns:
        
        STATUS      'exit_code getkeywordvalues'
        VALUE       The value of the object ids returned in newline ('\n')
                    delimited list.  
                    e.g.:
                    Mosaic2_CAL9906_6_bpm.fits\nMosaic2_CAL9906_7_bpm.fits\n...
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success
        1          failure: no data
        """
        
        # set up
        values = []
        sql, pkt = '',''
        keywords = {
              'class': '<<REQUIRED>>',
            'keyword': '<<REQUIRED>>',
              'multi': '<<REQUIRED>>'}
        
        # main body..catch all possible error conditions in try
        try:
            # get keywords from passed parameters
            # and include a check for the required keywords
            # FIX: Sloppy! All we are using this for is
            # to check that keyword is present!
            keywords = self.getKeywords (keywords)
            
            returnOneResult = False
            if keywords ['multi'] == '0':
                returnOneResult = True
                pass
            
            # query the database
            sql, result = self.do_search_for_keyword (
                self.params ['keyword'], returnOneResult)
            
            if result:
                if isinstance (result, tuple) or isinstance (result, list):
                    # parse result list...tack into our return list
                    for item in result:
                        values.append (item [0])
                        pass
                    pass
                else: # only one value 
                    values.append (result [0])
                    pass
                pass
            elif self.VERBOSE_MODE:
                print_msg ('NO result!', True)
                pass
            
            # hmm. everything OK...make a packet
            pkt = composePacket ({
                'STATUS': '0 GetKeyWordValues', 'VALUE': values})
            
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt   = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    # FIX: document this .. 
    def mgr_get_repository (self):
        
        pkt = composePacket ({
            'STATUS': '0 get_repository',
             'VALUE': self.server.get_repository ()})
        
        return ('', pkt)
    
    
    def mgr_findrule (self):
        
        """
        Find all of the values of a given keyword from
        objects which match in the database.
        
        The input parameters (stored in self.params) are:
        
        ID_STR      The (string) ID of the code to retrieve.
        
        CODETYPE    The type of code we want to find. If not specified, 'CL'
                    is the default.
        
        MJD         The MJD date of the code to search for. If not specified,
                    the present date is the default.
        
        VALIDATE    A value of 1 or 0. If '1' (True) then the returned code
                    will be wrapped by a validating script. As this slows
                    performance, it should only be used to debug rule code.
                    This parameter may be overridden by the DM itself, so that
                    if the DM is already in 'test' mode, all returned rule code
                    will be wrapped with validation code anyway.
        
        Returns:
        
        STATUS      'exit_code getrulecode'
        VALUE       The code which corresponds to the requested rule (location 
                    is in the local $DMDATA/rules dir)
        EOF
        
        In case of error, VALUE is empty.
        """
        
        sql, pkt    = '',''
        limit_terms = {}
        keywords    = {
              'id_str': '<<REQUIRED>>',
                 'mjd': MJD (),
            'codetype': 'CL',
            'validate': '0'}
        
        # main body..catch all possible error conditions in try
        try:
            # get keywords from passed parameters
            # and include a check for the required keywords
            keywords = self.getKeywords (keywords)
            
            # FIX: need to put in time limits on search
            limit_terms ['id_str']        = keywords ['id_str']
            limit_terms ['codetype']      = keywords ['codetype']
            limit_terms ['createMJD_max'] = keywords ['mjd']
            limit_terms ['expireMJD_min'] = keywords ['mjd']
            
            # create the sql
            sql = compose_sql_query (
                'codelocation',
                'ProcessingRules',
                limit_terms,
                self.VERBOSE_MODE)
            
            # query the database
            result       = self.server.query_db (sql, fetchone=True)
            data         = result ['data']
            codelocation = data [0]
            
            if not codelocation or codelocation == '':
                error = 'No %s code found for %s at %s' % (
                    keywords ['codetype'],
                    keywords ['id_str'],
                    keywords ['mjd'])
                
                pkt = composePacket ({'STATUS': '1 get_rule_code %s' % error})
            else:
                # return iraf path to code. FUTURE: we should have
                # an option to return standard URL instead
                path_to_code = '%s!%s/rules/%s' % (
                    NODE, self.server.get_repository (), codelocation)
                
                # DISABLE THIS STUFF FOR NOW
                #if (self.server.TEST_MODE or keywords['validate']!='0'):
                #    # wrap code in validation wrapper, based on supported
                #    # languages (e.g. 'CL') To do this, we have to 
                #    if keywords ['codetype'] == 'CL':
                #        # for CL, we have to create a new, temp file with the
                #        # code in question
                #        loc = '%s/rules/%s' % (
                #            self.server.get_repository (), codelocation)
                #        fileobj = open (loc)
                #        code_contents = open (loc).read ()
                #        fileobj.close ()
                #        
                #        # FIX: check if the existing validation code is older 
                #        # than existing code. IF SO, re-build
                #        validateLoc = '%s/rules/_val_%s' % (
                #            self.server.get_repository (),codelocation)
                #
                #        # Partly so that this is more efficient, and so that
                #        # multiple nearly simultaneous requests for the same
                #        # validating rule don't collide, we need to check if
                #        # the validating version of the rule script is older
                #        # than its parent rule script. IF so, then we will
                #        # have to rebuild
                #        t1 = os.access (validateLoc, os.F_OK)
                #        t2 = os.path.getmtime (validateLoc)
                #        t3 = os.path.getmtime (loc)
                #        if (not t1 or (t2 < t3)):
                #            # get the params for this rule
                #            params = self.get_rule_params (limit_terms)
                #            
                #            # either it doesnt exist, or its older so we 
                #            # build new code with validation wrapper in it
                #            build_new_validating_rule_cl_script (
                #                code_contents, validateLoc, params,
                #                NODE, self.server.get_repository (),
                #                codelocation)
                #            pass
                #        # build an IRAF path
                #        path_to_code = '%s!%s' % (NODE, validateLoc)
                #        pkt = composePacket ({
                #            'STATUS': '0 get_rule_code',
                #             'VALUE': path_to_code})
                #    else:
                #        response = 'Can\'t create %s validation code for %s.'
                #        response = response % (
                #            keywords ['codetype'], keywords['id_str'])
                #        pkt = composePacket ({
                #            'STATUS': '1 get_rule_code %s' % error })
                #        pass
                #    pass
                #else:
                # unwrapped, vannilla code
                pkt = composePacket ({
                    'STATUS': '0 get_rule_code', 'VALUE': path_to_code})
                pass
            pass
            #pass
        except:
            # may need to have additional conditions specified here..
            error = '%s:%s' % (sys.exc_type, sys.exc_value)
            pkt   = composePacket ({'STATUS': '1 get_rule_code %s' % error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_is_mastermode (self):
        
        """
        Allow a remote client to dtermine if this DM is being run
        in master mode.
        """
        
        pkt = composePacket ({
            'STATUS': '0 is_mastermode', 'VALUE': self.server.MASTER_MODE})
        
        return ('', pkt)
    
    
    def mgr_newdataproduct (self):
        
        """
        Create a new data product entry in the database
        
        The input parameters (stored in self.params) are:
        
        DATAID         The (Unique) ID of the data product
        
        PROCID         The (Unique) ID of the process which created this data
        
        CLASS          The type of data we want to record.
                       e.g. 'ZeroImage'
        
        CREATEMJD      The time at which the data product was created as
                       Modified Julian Day. e.g. 52642.4
        
        OBSERVEMJD     The time at which the data product was observed
                       (aquired) as Modified Julian Day. e.g. 52642.4
        
        URL            The URL pointing to the data product
        
        CLOBBER        Whether or not to allow clobbering of existing entry
                       (may override test mode)
        
        Returns:
        STATUS      'exit_code newdataproduct'
        
        Exit codes:
        0          success
        1          failure: no new data product entry created
        """
        
        # set up
        sql, pkt = '',''
	utctime = toUTC (time.time())
	year, month, day, hr, min, sec = utctime
        keywords = {
                'id_str': '<<REQUIRED>>',
            'producedby': None,
             'createmjd': toMJD (utctime),
                  'year': year,
                 'month': month,
                   'day': day,
                    'hr': hr,
                   'min': min,
                   'sec': sec,
            'observemjd': None,
                   'url': '',
                 'class': 'dataproduct',
             'nelements': 1,
                  'size': None}#, 'usedby': 'none'}
        
        # main body..catch all possible error conditions in try
        try:
            self.params ['id_str'] = self.params ['dataid']
            
            clobber = self.server.TEST_MODE
            if 'clobber' in self.params:
                if self.params ['clobber'] == 'True':
                    clobber = True
                else:
                    clobber = False
                    pass
                pass
            
            # get keywords from passed parameters
            # and include a check for the required keywords
            keywords = self.getKeywords (keywords)
            
            # Sanity check1 : does the declared class exist?
            obj_class, viewName, tableName = self.find_class_info (
                keywords ['class'])
            
            if obj_class < 0:
                msg = 'class:%s is unknown/not allowed'
                raise UnknownDBObjectError, msg % keywords['class']
            else:
                keywords ['class'] = obj_class
                pass
            
            ## Sanity check2 : does the parent process exist in our db?
            #proc_id = self.get_process_id (keywords ['producedby'])
            #if proc_id < 0:
            #    msg = 'procid:%s is unknown. Cannot create data product.'
            #    raise UnknownDBObjectError, msg % keywords ['producedby']
            #else:
            #    keywords ['producedby'] = proc_id
            #    pass
            
            # create the sql insert statement 
            sql = compose_sql_insert (
                tableName, keywords, CLOBBER_MODEOK=clobber)
            
            # ingest the file info in the database
            # and compose the packet as appropriate
            self.server.update_db (sql, MasterSync=True)
            
            # FIX: If the above went well, then we update our entry
            # in the processing table
            # self.add_dataprod_to_process(lastId, keywords['procid'])
            
            # hmm. everything OK...make a packet
            pkt = composePacket ({'STATUS': '0 NewDataProduct'})
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt   = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_newprocess (self):
        
        """
        Create a new process 
        
        The input parameters (stored in self.params) are:
        
        PROCID         The (Unique) ID of the process
        CLASS          The type of process we want to record.
                       e.g. 'Zero'
        startMJD       The start time of the process as Modified Julian Day.
                       e.g. 52642.4
        
        parentProcId   The id of the parent process
        
        logURL         The URL for the log of this process
        
        nodeIP         The IP address of the machine this process ran on
        
        clobber        Whether or not to allow clobbering of existing entry
                       (may override test mode)
        
        Returns:
        STATUS      'exit_code newprocess'
        
        Exit codes:
        0          success
        1          failure: no new process entry created
        """
        
        # set up 
        sql, pkt = '',''
	utctime = toUTC (time.time())
	year, month, day, hr, min, sec = utctime
        keywords = {
                  'id_str': '<<REQUIRED>>',
                'startmjd': toMJD (utctime),
                    'year': year,
                   'month': month,
                     'day': day,
                      'hr': hr,
                     'min': min,
                     'sec': sec,
                  'logurl': '',
            'parentprocid': '-1',
                  'nodeip': '0.0.0.0',
                   'class': 'process',
                    'host': '<<REQUIRED>>',
              'start_time': '<<REQUIRED>>',
                 'user_id': '<<REQUIRED>>',
             'system_name': '<<REQUIRED>>',
               'pipe_name': '<<REQUIRED>>',
             'pipe_module': '<<REQUIRED>>',
                'osf_flag': '<<REQUIRED>>',
              'event_type': '<<REQUIRED>>',
                 'dataset': '<<REQUIRED>>'}#'class': 'process'}
        
        # main body..catch all possible error conditions in try
        try:
            self.params ['id_str'] = self.params ['procid']
            
            clobber = self.server.TEST_MODE
            if 'clobber' in self.params:
                if self.params ['clobber'] == 'True':
                    clobber = True
                else:
                    clobber = False
                    pass
                pass
            
            # get keywords from passed parameters
            # and include a check for the required keywords
            keywords = self.getKeywords (keywords)
            
            # Sanity check1: does the declared class exist?
            obj_class, viewName, tableName = self.find_class_info (
                keywords ['class'])
            
            if obj_class < 0:
                msg = 'class:%s is unknown/not allowed'
                raise UnknownDBObjectError, msg % keywords ['class']
            else:
                keywords ['class'] = obj_class
                pass
            
            # create the sql insert statement 
            sql = compose_sql_insert (
                tableName, keywords, CLOBBER_MODEOK=clobber)
            
            # ingest the file info in the database
            # and compose the packet as appropriate
            self.server.update_db (sql, MasterSync=True)
            
            # hmm. everything OKEY..make a packet
            pkt = composePacket ({'STATUS': '0 NewProcess'})
            
            # FIX: debug msg
            #if self.VERBOSE_MODE:
            #    self.server.dump_table (tableName, 10)
            #    pass
            
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt   = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    def mgr_finishprocess (self):
        
        """
        Finish a process 
        
        The input parameters (stored in self.params) are:
        
        PROCID         The (Unique) ID of the process
        CLASS          The type of process we want to record.
                       e.g. 'Zero'
        clobber        Whether or not to allow clobbering of existing entry
                       (may override test mode)
        
        Returns:
        STATUS      'exit_code finishprocess'
        
        Exit codes:
        0          success
        1          failure: no new process entry created
        """
        
        # set up 
        sql, pkt = '',''
        keywords = {
               'id_str': '<<REQUIRED>>',
                'class': 'process',
             'end_time': '<<REQUIRED>>',
            'exit_code': '<<REQUIRED>>',
              'success': '<<REQUIRED>>'}
        
        terms = {}
        
        # main body..catch all possible error conditions in try
        try:
            clobber = self.server.TEST_MODE
            if 'clobber' in self.params:
                if self.params ['clobber'] == 'True':
                    clobber = True
                else:
                    clobber = False
                    pass
                pass
            
            # get keywords from passed parameters
            # and include a check for the required keywords
            keywords = self.getKeywords (keywords)
            
            # Sanity check1 : does the declared class exist?
            (obj_class, viewName, tableName) = self.find_class_info (
                keywords ['class'])
            
            if obj_class < 0:
                msg = 'class:%s is unknown/not allowed'
                raise UnknownDBObjectError, msg % keywords ['class']
            else:
                keywords ['class'] = obj_class
                pass
            
            """
            # Sanity check2 : does the object exist in our db?
            obj_id = self.get_object_id (keywords ['id_str'], tableName)
            if obj_id < 0:
                msg = 'id:%s is unknown. Cannot set object.'
                raise UnknownDBObjectError, msg % (keywords ['id_str'])
            #else:
            #     keywords ['id'] = obj_id
            """
            
            # FIX: sloppy!
            terms ['id_str'] = keywords ['id_str'] # should use 'id'
            
            # create the sql insert statement
            sql = compose_sql_col_update (
                tableName, 'end_time', keywords ['end_time'], terms)
            
            if self.VERBOSE_MODE:
                print_msg (' SetKeyWordValue GOT SQL: (%s)' % sql, True)
                pass
            
            # ingest the file info in the database
            # and compose the packet as appropriate
            self.server.update_db (sql, MasterSync=True)
            
            # create the sql insert statement
            sql = compose_sql_col_update (
                tableName, 'exit_code', keywords ['exit_code'], terms)
            
            if self.VERBOSE_MODE:
                print_msg (' SetKeyWordValue GOT SQL: (%s)' % sql, True)
                pass
            
            # ingest the file info in the database
            # and compose the packet as appropriate
            self.server.update_db (sql, MasterSync=True)
            
            # create the sql insert statement
            sql = compose_sql_col_update (
                tableName, 'success', keywords ['success'], terms)
            
            if self.VERBOSE_MODE:
                print_msg (' SetKeyWordValue GOT SQL: (%s)' % sql, True)
                pass
            
            # ingest the file info in the database
            # and compose the packet as appropriate
            self.server.update_db (sql, MasterSync=True)
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt   = composePacket ({'STATUS': error})
            pass
        
        # If we are here, all of the values were updated.
        pkt = composePacket ({'STATUS': '0 FinishProcess'})
        
        return (sql, pkt)
    
    
    def mgr_putcal (self):
        
        """
        Put calibrations
        
        The input parameters (stored in self.params) are:
        
        CLASS       The type of calibration we want to ingest.
                    e.g. 'Zero'
        MJDSTART    Modified Julian Day.
                    e.g. 52642.4
        MJDEND      Modified Julian Day.
                    e.g. 52642.4
        DETECTOR    The string identifying the detector (optional)
                    e.g. 'Mosaic2'
        IMAGEID     Image/Amp ID (optional)
                    e.g. 'SITe #98164FACR10-02 (NOAO 21), lower left (Amp13)'
        FILTER      Filter name (optional)
                    e.g. '815 815_v1 k1026	'
        EXPTIME     Exposure time (optional)
                    e.g. 120.
	NCOMBINE    Number of images combined (optional)
	            e.g. 1
	QUALITY     Image quality (optional)
	            e.g. 0.
	MATCH       Match string (optional)
		    e.g. 'DA=4 FS=16'
        VALUE       Destination directory path (in IRAF network notation)
                    e.g. 'pipedevn!/pipelines/fpierfed/MarioCal/'
        DATATYPE    Type of data to ingest
                    e.g. file
        
        Exit codes:
        0          success
        1          failure: no data
        """
        global FREP_HAS_STRUCTURE
        
        # get the required keywords
        keywords = {
               'class': '<<REQUIRED>>', 
            'mjdstart': '<<REQUIRED>>', 
              'mjdend': '<<REQUIRED>>', 
            'detector': None, 
             'imageid': None, 
              'filter': None, 
             'exptime': None, 
            'ncombine': None,
             'quality': None,
               'match': None,
               'value': None}
        
        status = 0
        error  = None
        value  = None
        
        if self.VERBOSE_MODE:
            print 'RUNNING PUT CAL HANDLER'
            pass
        
        # datatype is required information
        try:
            datatype = self.params ['datatype']
        except:
            return composePacket ({'STATUS': '1 putcal missing keyword (datatype)'})
            #self.send_error (1, 'Missing keyword (datatype)')
            #return ('', '')
        
        for key in keywords:
            if keywords [key] == '<<REQUIRED>>':
                # This is a required keyword. If it is not
                # present, we should abort with an error.
                try:
                    keywords [key] = self.params [key]
                except:
                    return composePacket ({
                        'STATUS': '1 putcal missing keyword (%s)' % (key)})
                    #self.send_error (1, 'Missing keyword (%s)' % (key))
                    #return ('', '')
                pass
            else:
                # This is an optional keyword. The value in the
                # keywords dictionary is already the correct
                # default.
                try:
                    keywords [key] = self.params [key]
                except:
                    continue
                pass
            pass
        
        keywords ['datatype'] = datatype
        
        if keywords ['datatype'] == 'keyword':
            # no need to copy anything; just return its value.
            value = keywords ['value']
        else:
            # copy the file/directory to the file repository
            # (self.server.repository) source file path is
            # encoded in keywords['value']
            frep = self.server.get_repository ()
            
            # does the frep have structure?
            if FREP_HAS_STRUCTURE:
                # round fmjd to the nearest int
                fmjd = str (int (round (float (keywords ['mjdstart']))))
                frep = makeDirs ((frep, keywords ['class'], fmjd))
                if not frep:
                    status = 1
                    error  = '1 Error in creating the directory '
                    error += 'structure (%s/%s/%s).' % (frep, fclass, fmjd)
                    pass
                pass
            
            status = copyto (
                    src = keywords ['value'],
                    dst = frep,
                   type = datatype,
                verbose = self.VERBOSE_MODE)
            
            if status == 0:
                value = keywords ['value']
            else:
                error = '%s Error in copying the files (%s -> %s/).' % (
                    status, keywords ['value'], frep)
                pass
            
            # update keywords ['value'] to reflect the fact
            # the file will be in frep
            keywords ['value'] = os.path.basename (keywords ['value'])
            pass
        
        clobber = False
        if self.server.TEST_MODE:
            clobber = True
            pass
        
        sql = compose_sql_insert ('catalog', keywords, CLOBBER_MODEOK=clobber)
	if self.VERBOSE_MODE:
	    print_msg ('sql: (%s)' % (sql), False)
	    pass
        
        # ingest the file info in the database
        connection = self.server.get_connection ()
        if connection and not error:
            # Send off the query...
            # The database could be locked!
            querySent = False
            start     = time.time ()
            while not querySent and (time.time () - start) < TIMEOUT:
                try:
                    # get a cursor
                    dbc = connection.cursor ()
                    # send the query
                    dbc.execute (sql)
                    querySent = True
                except:
                    msg = 'putcal: the DB is busy. Retrying in %fs.\n'
                    warning (PREFIX, msg % RETRY)
                    time.sleep (RETRY)
                    pass
                pass
            
            if not querySent:
                error  = '1 Error in sending the query (%s)' % (sql)
                pass
            pass
        else:
            error  = '1 Error in connecting to the DB.'
            pass
        
        # compose the packet
        if not error:
            # close the transaction
            connection.commit ()
            pkt = composePacket ({'STATUS': '0 putcal', 'VALUE': value})
        else:
            pkt = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_setkeywordvalue (self):
        
        """
        Set a keyword value for indicated object in DM
        
        The input parameters (stored in self.params) are:
        
        STR_ID         The (Unique) ID of the object 
        
        CLASS          The type of data we want to alter.
                       e.g. 'ZeroImage'
        
        KEYWORD        The name of the keyword (field) we want to
                       set the value of. e.g. 'observemjd'
        
        VALUE          The value we want to set the keyword to
                       e.g. '53000.2'
        
        Returns:
        STATUS      'exit_code setkeywordvalue'
        
        Exit codes:
        0          success
        1          failure: no keyword value set
        """
        
        # set up
        value = None
        sql, pkt = '',''
        keywords = {
             'id_str': '<<REQUIRED>>',
              'class': '<<REQUIRED>>',
            'keyword': '<<REQUIRED>>',
              'value': '<<REQUIRED>>'}
        
        try:
            terms = {}
            # get keywords from passed parameters
            # and include a check for the required keywords
            keywords = self.getKeywords (keywords)
            
            # Sanity check1 : does the declared class exist?
            obj_class, viewName, tableName = self.find_class_info (
                keywords ['class'])
            
            if obj_class < 0:
                msg = 'class:%s is unknown/not allowed'
                raise UnknownDBObjectError, msg % keywords ['class']
            #else:
            #     keywords ['class'] = obj_class
            
            # Sanity check2 : does the object exist in our db?
            obj_id = self.get_object_id (keywords ['id_str'], tableName)
            if obj_id < 0:
                msg = 'id:%s is unknown. Cannot set object.'
                raise UnknownDBObjectError, msg % keywords ['id_str']
            #else:
            #     keywords ['id'] = obj_id
            
            # FIX: sloppy!
            terms ['id_str'] = keywords ['id_str'] # should use 'id'
            
            # create the sql insert statement
            sql = compose_sql_col_update (
                tableName, keywords ['keyword'], keywords ['value'], terms)
            
            if self.VERBOSE_MODE:
                print_msg (' SetKeyWordValue GOT SQL: (%s)' % sql, True)
                pass
            
            # ingest the file info in the database
            # and compose the packet as appropriate
            self.server.update_db (sql, MasterSync=True)
            
            # hmm. everything OKEY..make a packet
            pkt = composePacket ({'STATUS': '0 SetKeywordValue'})
            
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_sql_query (self):
        
        """
        Allow a remote SQL query of this Data manager.
        The input parameters (stored in self.params) are:
        
        SQL     The sql query to be executed.
	FORMAT	The output format for the rows (rows|keyword)
                The default is 'keyword'.
        THROWERR Specifies whether or not to throw an error
                 if the query returns no data (True|False)
                 The default is True.
        
        Returns:
        
        STATUS      'exit_code sql_query'
        FIELDS      The names of the fields by column 
        FIELD_TYPES The column types of the fields 
        FIELD_PRECISION The column precision of the fields 
        FIELD_NULLABLE The column nullable attribute of the fields 
        DATA        The values of the matching table rows returned
                    in a newline ('\n') delimited list.
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success
        1          failure: no data
        """
        
        # set up
        values = []
        sql, pkt = '',''
        keywords = {
                 'sql': '<<REQUIRED>>',
              'format': 'keyword',
            'throwerr': 'true'}
        
        # main body..catch all possible error conditions in try
        try:
            # get keywords from passed parameters
            # and include a check for the required keywords
            # FIX: Sloppy! All we are using this for is to
            # check that keyword is present!
            keywords = self.getKeywords (keywords)
            
            # query the database
            sql     = self.params ['sql']
            results = self.server.query_db (sql)
            result  = results ['data']
            
            if result:
                if isinstance (result, tuple) or isinstance (result, list):
                    # parse result list..tack in to our return list
                    for item in result:
                        if self.VERBOSE_MODE:
                            print_msg (' SQL FINDS ITEM:%s' % item, True)
                            pass
                        
                        values.append (str (item))
                        pass
                    pass
                else: # only one value
                    values.append (result [0])
                    pass
                pass
            else:
                if self.VERBOSE_MODE:
                    print_msg ('NO result!', True)
                    pass
                
		raise UnknownDBObjectError, 'NO result!'
            
            # hmm. everything OKEY..make a packet
            dict = {     'STATUS': '0 sql_query', 
                         'FIELDS': results ['fields'],
                    'FIELD_TYPES': results ['field_types'],
                 'FIELD_NULLABLE': results ['field_nullable'],
                'FIELD_PRECISION': results ['field_precision']}
            
            if string.lower (keywords ['format']) == 'keyword':
                dict ['DATA'] = values
                pkt = composePacket (dict)
            else:
                dict ['ROW'] = values
                pkt = composeMultilineListPacket (dict)
                pass
            pass
        except:
            # may need to have additional conditions specified here..
            error = '1 NO result!'
            if string.lower (keywords ['throwerr']) == 'true':
                error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
                pass
            pkt = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def mgr_sql_update (self):
        
        """
        Allow a remote SQL update of this Data manager.
        The input parameters (stored in self.params) are:
        
        SQL          The sql update to be executed.
        MASTERSYNC   Indicating whether or not to pass along this update
                     to other master mode DM
        
        Returns:
        STATUS      'exit_code sql_update'
        EOF
        
        Exit codes:
        0          success
        1          failure: no update
        """
        
        # set up
        values = []
        sql, pkt = '',''
        keywords = {
                   'sql': '<<REQUIRED>>',
            'mastersync': False}
        
        # main body..catch all possible error conditions in try
        try:
            # get keywords from passed parameters
            # and include a check for the required keywords
            # FIX: Sloppy! All we are using this for is to
            # check that keyword is present!
            keywords = self.getKeywords (keywords)
            
            # query the database
            sql = self.params ['sql']
            self.server.update_db (sql, keywords ['mastersync'])
            
            # hmm. everything OKEY..make a packet
            pkt = composePacket ({'STATUS': '0 UpdateSQL'})
            
        except:
            # may need to have additional conditions specified here..
            error = '1 %s:%s' % (sys.exc_type, sys.exc_value)
            pkt = composePacket ({'STATUS': error})
            pass
        
        return (sql, pkt)
    
    
    def getKeywords (self, keywords):
        
        """
        get keywords from passed parameters
        """
        
        for key in keywords:
            #print_msg ('check keyword: %s' % str (key), True)
            if keywords [key] == '<<REQUIRED>>':
                # This is a required keyword. If it is not
                # present, we should abort with an error.
                try:
                    #if self.VERBOSE:
                    #    msg = '  keyword: %s == %s' % (
                    #        str (key), self.params [key])
                    #    print_msg (msg, True)
                    #    pass
                    keywords [key] = self.params [key]
                except:
                    msg = 'Missing required keyword (%s)'
                    raise MissingAttributeError, msg % key
                pass
            else:
                # This is an optional keyword. The value in the
                # keywords dictionary is already the correct
                # default.
                try:
                    #if self.VERBOSE:
                    #    msg = '  keyword: %s == %s' % (
                    #        str (key), self.params [key])
                    #    print_msg (msg, True)
                    #    pass
                    keywords [key] = self.params [key]
                except:
                    #if self.VERBOSE:
                    #    msg = '  keyword: %s doesn't exist in passed params'
                    #    print_msg (msg % str (key), True)
                    #    pass
                    continue
                pass
            pass
        
        return keywords
    
    pass
# <-- end class DataManagerRequestHandler

class DimensionError (IndexError):
    'Used to indicate that a dict or sequence has the incorrect dimension'
    pass
# <-- end class DimensionError

class UnknownDBObjectError (MissingAttributeError):
    'Used to indicate that an unknown object has been requested from the DM'
    pass
# <-- end class UnknownDBObjectError

class UndefinedAttributeError (AttributeError):
    'Used to indicate that a keyword/parameter is not defined'
    pass
# <-- end class UndefinedAttributeError


#
# 'STATIC' methods
#

# FIX: 
HAS_DICT_SUBSTRUCT_REGEX  = re.compile ('^[\"|\']\{.*$')
HAS_TUPLE_SUBSTRUCT_REGEX = re.compile ('^[\"|\']\(.*$')
HAS_SEQ_SUBSTRUCT_REGEX   = re.compile ('^[\"|\']\[.*$')

CL_BEGIN_REGEX            = re.compile ('\s*begin\s*$')
CL_END_REGEX              = re.compile ('\s*end\s*$')

MAX_KEY_REGEX             = re.compile ('.*_max$')
MIN_KEY_REGEX             = re.compile ('.*_min$')
CORE_KEY_REGEX            = re.compile ('.*_core$')
NOT_UNIQUE_COL_REGEX      = re.compile ('^column .+ is not unique$')

CLASS_COL_REGEX           = re.compile ('^class$', re.IGNORECASE)
DATATYPE_COL_REGEX        = re.compile ('^datatype$', re.IGNORECASE)
MJD_COL_REGEX             = re.compile ('^mjd$', re.IGNORECASE)
VALUE_COL_REGEX           = re.compile ('^value$', re.IGNORECASE)


# FIX: should be in pipeutil
def __construct_current_pkt_string_item (itemList, currentItem):
    
    item = itemList [currentItem]
    
    # remove extraneous whitespace
    item = item.strip()
    
    # print 'CONSTUCT GETS ITEM:[%s]' % item
    
    if item == '':
        # do nothing...
        currentItem += 1
    else:
        # check for sub-structures
        if item [0] == '[': # sequence
            # find sequence substructure
            item, currentItem = __find_substring_struct (
                itemList, currentItem, '[', ']')
        elif item [0] == '(': # tuple
            # find tuple substructure
            item, currentItem = __find_substring_struct (
                itemList, currentItem, '(', ')')
        elif item [0] == '{': # dict/hash
            # find dict substructure
            item, currentItem = __find_substring_struct (
                itemList, currentItem, '{', '}')
        elif item [0] == '\'': # quoted item
            # find quoted item substructure
            item, currentItem = __find_substring_struct (
                itemList, currentItem, '\'', '\'', False)
            item = strip_first_last_char (item, '\'')
        elif item [0] == '"': # double-quoted item
            # find quoted item substructure
            item, currentItem = __find_substring_struct (
                itemList, currentItem, '"', '"', False)
            item = strip_first_last_char (item, '"')
        else:
            currentItem += 1
            pass
        pass
    
    return (item, currentItem)


def build_new_validating_rule_cl_script (code_contents,
                                         validateLoc,
                                         params,
                                         host,
                                         frep,
                                         codelocation):
    
    fileo = open (validateLoc, 'w')
    
    propList   = ''
    dTypeList  = ''
    nullOKList = ''
    counter    = 1
    
    for prop in params.keys():
        attribs = params[prop]
        propList = '%sparam%d=%s,' % (propList, counter, prop) 
        dTypeList = '%stype%d=%s,' % (dTypeList, counter, attribs ['datatype'])
        nullOKList = '%snullOk%d=%s ' % (
            nullOKList, counter, attribs ['nullallowed'])
        counter = counter + 1
        pass
    
    propList   = propList [:-1]
    dTypeList  = dTypeList [:-1]
    nullOKList = nullOKList [:-1]
    
    validate_block  = 'task $validate = "%s!%s/rules/validate_rule.cl"\n'
    validate_block += 'validate ("%s", "%s", "%s") | scan (s1)\n'
    validate_block += 'if (s1 == "0")\n   print "0"\nelse {\n'
    validate_block  = validate_block % (
        host, frep, propList, dTypeList, nullOKList)
    
    for line in string.split (code_contents, '\n'):
        if CL_BEGIN_REGEX.match (line):
            fileo.write ('string s1=""\nbegin\n%s' % (validate_block))
        elif CL_END_REGEX.match (line):
            fileo.write ('};\n%s' % line)
        else:
            fileo.write ('%s\n' % line)
            pass
        pass
    
    fileo.close ()
    
    return

# simple converstion: field (name) to data pairing in a dict structure
def convert_sql_result_to_simple_dict (result):
    
    dict = {}
    dataList = result ['data']
    
    counter = 0
    for field in result ['fields']:
        dict [field] = dataList [counter] 
        counter += 1
        pass
    
    return dict

# Examples 
#
# Dictionary: 
#
# "{'observeMJD': 7, 'ampSigma': 12, 'URL': 5, 'parentBiasId': 14,
# 'ampLevel': 11, 'producedBy': 4, 'id': 0, 'usableForCalibration': 13,
# 'id_str': 1, 'biasJump': 8, 'overscanLevel': 9, 'overscanSigma': 10,
# 'class':2, 'createMJD': 3, 'obsConfig': 6}"
#
# sequence of tples:
#
# '["(2, 'proc1', 1, 53327.469583300001, -1.0, '0.0.0.0', None, None, -1)",
# "(3, 'proc2', 1, 53327.469583300001,-1.0,'0.0.0.0', None, None, -1)"]'
#
# FIX: should be in pipeutil
def deserialize_pkt (str_value):
    
    result = None
    
    if str_value and str_value != '':
        
        #print 'DESERIALIZE PKT: %s ' % str_value
        
        # strip extraneous whitespace
        str_value = str_value.strip ()
        
        # strip outside quotes, if exist
        if str_value [0] in ['\'', '"']:
            str_value = strip_first_last_char (str_value, str_value [0])
            pass
        
        # check for type, and parse appropriately
        if str_value [0] == '[': # sequence 
            result = __deserialize_pkt_sequence (str_value)
        elif str_value [0] == '(': # tuple
            result = __deserialize_pkt_tuple (str_value) 
        elif str_value [0] == '{': # dict/hash
            result = __deserialize_pkt_dictionary (str_value)
        elif str_value != 'None':
            # special case: when string sez "None" it means None
            result = expand_serialized_tabs (str_value)
            pass
        pass
    
    # FIX
    #if result:
    #    print 'DESERIALIZE PKT RETURNS: %s' % result 
    #    pass
    
    return result


def __find_substring_struct (itemList,
                             currentItem,
                             start_struct_char,
                             end_struct_char,
                             check_quotes=True):
    
    """
    From a list of items try to find the highest level
    structure that matches the start/end characters
    """
    
    sub_string = ''
    
    start_item = currentItem
    
    is_inQuotes  = 0
    struct_level = 1
    while struct_level or is_inQuotes:
        item = itemList [currentItem].strip ()
        # FIX: print '  sub-item:[%d]:[%s]' % (currentItem, item)
        sub_string += '%s, ' % (item)
        
        second_char = 1
        last_char = len (item) - 1 
        next_last_char = last_char - 1
        
        if last_char < 0:
            last_char = 0
            pass
        
        if next_last_char < 0:
            second_char, next_last_char = 0, 0
            pass
        
        # FIX: print ' sec_char: %d last_char:%d next_last_char:%d' % (
        # second_char, last_char, next_last_char)
        
        # check for structure at outer level and if we
        # split a string with embedded comma !
        if start_item != currentItem and item[0] == start_struct_char:
            struct_level += 1
        elif (check_quotes and
              item [0]              == '\'' and
              item [last_char]      != '\'' and
              item [next_last_char] != '\''):
            is_inQuotes += 1
            #print ' IN QUOTE'
        elif item [last_char] == end_struct_char:
            struct_level -= 1
            if struct_level == 0:
                sub_string = sub_string [:-2] # peel off trailing comma
                pass
            pass
        elif (check_quotes and
              item [0]           != '\'' and
              item [second_char] != '\'' and
              item [last_char]   == '\''):
            is_inQuotes -= 1
            #print ' OUT QUOTE'
            pass
        
        currentItem += 1
        pass
    
    # FIX: print '  substring_struct: %s' % sub_string
    
    return (sub_string, currentItem)


# FIX: belongs in pipeutils
# FIX: not recursive
def __deserialize_pkt_dictionary (str_value):
    
    result = {}
    
    if str_value and str_value != '':
        
        # FIX: print 'DESERIALIZE PKT DICTIONARY : %s ' % str_value
        
        # remove extraneous whitespace
        str_value = str_value.strip ()
        
        # if empty bail out now
        if str_value == '{}':
            return result
        
        # strip outside brakets 
        if str_value [0] == '{':
            str_value = strip_first_last_char (str_value)
        else:
            msg  = 'deserialize pkt dictionary passed '
            msg += 'string which is not a dictionary'
            raise ValueError, msg
        
        # split on commas
        itemList = string.split (str_value, ',')
        
        for item in itemList:
            (key, value) = string.split (item, ':')
            result [key] = deserialize_pkt (value)
            pass
        pass
    
    return result


# FIX: belongs in pipeutils
# example:
# "(2, 'proc1', 1, 53327.469583300001, -1.0, '0.0.0.0', None, None, -1)", 
# "(3, 'proc2', 1, 53327.469583300001,-1.0,'0.0.0.0', None, None, -1)"
def __deserialize_pkt_sequence (str_value):
    
    result = []
    
    if str_value and str_value != '':
        
        # FIX: print 'DESERIALIZE PKT SEQUENCE: %s ' % str_value
        
        # remove extraneous whitespace
        str_value = str_value.strip ()
        
        # if empty bail out now
        if str_value == '[]':
            return result
        
        # strip outside parens
        if str_value [0] == '[':
            str_value = strip_first_last_char (str_value)
        else:
            msg  = 'deserialize pkt sequence passed '
            msg += 'string which is not a sequence'
            raise ValueError, msg
        
        # split on commas 
        itemList = string.split (str_value, ',')
        nrofItems = len (itemList)
        currentItem = 0
        while currentItem < nrofItems:
            
            item, currentItem = __construct_current_pkt_string_item (
                itemList, currentItem)
            
            # add item to sequence
            result.append (deserialize_pkt (item))
            pass
        pass
    
    return result

# examples:
# '[ "(2, 'proc1', 1, 53327.469583300001, -1.0, '0.0.0.0', None, None, -1)", 
#    "(3, 'proc2', 1, 53327.469583300001,-1.0,'0.0.0.0', None, None, -1)"
#  ]'
# ..and..
# "[2, 'proc1', 1, "[53327.469583300001, -1.0]", '0.0.0.0', None, None, -1]",
# "[3, 'proc2', 1, "[53327.469583300001, -1.0]", '0.0.0.0', None, None, -1]"
# FIX: TDB
# FIX: belongs in pipeutils
def __deserialize_pkt_tuple (str_value):
    
    result = [] # FIX returns a sequence!
    
    if str_value and str_value != '':
        # remove extraneous whitespace
        str_value = str_value.strip ()
        
        # FIX: print 'DESERIALIZE PKT TUPLE : %s ' % str_value
        
        # if empty bail out now
        if str_value == '()':
            return result
        
        # strip outside parens
        if str_value [0] == '(':
            str_value = strip_first_last_char (str_value)
        else:
            msg = 'deserialize pkt tuple passed string which is not a tuple'
            raise ValueError, msg
        
        # split on commas
        itemList = string.split (str_value, ',')
        nrofItems = len (itemList)
        currentItem = 0
        while currentItem < nrofItems:
            
            item, currentItem = __construct_current_pkt_string_item (
                itemList, currentItem)
            
            # add item to sequence
            result.append (deserialize_pkt (item))
            pass
        pass
    
    return result


def get_remote_calibration_file_list (addr,
                                      port,
                                      sinceMJD     = None,
                                      timeout      = TIMEOUT,
                                      halt_on_warn = False,
                                      verbose      = False):
    
    """
    Pull over master list, possibly supplying
    a limiting MJD limit on the list.
    """
    
    master_cal_files = None
    if verbose:
        print '  * Get remote calibration file list'
        pass
    
    sql  = 'select * from catalog where ( datatype == \'file\' '
    sql += 'or datatype == \'image\' or datatype == \'directory\' )'
    if sinceMJD:
        sql += ' and mjdstart > %f ' % sinceMJD
        pass
    
    sql += ';'
    
    if verbose:
        print ' SEND QUERY: %s' % sql
        pass
    
    pkt = composePacket ({'COMMAND': 'sql_query', 'SQL': sql})
    
    try:
        master_cal_retn = sendPacketTo (
            pkt, addr, int (port), timeout=timeout, will_respond=True)
        master_cal_result = deserialize_sql_result_pkt (master_cal_retn)
        master_cal_files  = find_cal_files_from_results (master_cal_result)
        
    except socket.error:
        warning (PREFIX, '%s:%s' % (sys.exc_type, sys.exc_value))
        if halt_on_warn:
            sys.exit (-1)
            pass
        pass
    except:
        if verbose:
            print_msg ('initTo PASSED EXCEPTION %s : %s' % (
                sys.exc_type, sys.exc_value), True)
            pass
        pass
    
    return master_cal_files


# FIX: just a little debugging routine
def __dump_table (connection, tableName, maxObj=1):
    
    """
    Debuging routine: remove when not needed
    """
    
    msg = 'dump up to %d items from table: %s'
    print_msg (msg % (maxObj, tableName), True)
    
    sql = 'SELECT * FROM %s LIMIT %d;' % (tableName, maxObj)
    #sql = 'select * from %s;' % tableName
    
    vals = query_db (connection, sql)
    
    print_msg ('-- %d values were returned' % len (vals), True)
    print_msg ('%s' % str (vals ['fields']), True)
    for val in vals ['data']:
        print_msg ('%s' % (val), True)
        pass
    
    return


def __merge_result_into_db_table(dbconnect,
                                 tableName,
                                 result,
                                 verbose         = False,
                                 clobber         = False,
                                 check_for_entry = True):
    
    """
    Merge data into the indicated table. Data
    are in the format of an SQL result.
    """
    
    # FIX
    print '__MERGE_RESULT_INTO_DB CALLED for table:%s' % tableName
    
    # sanity checking
    if not dbconnect:
        msg = 'Empty DB connection passed to merge_result'
        raise UndefinedAttributeError, msg
    
    if not tableName:
        raise UndefinedAttributeError, 'Empty tableName passed to merge_result'
    
    if not result:
        msg = 'Empty result passed to merge_result for table:%s' % tableName
        raise UndefinedAttributeError, msg
    
    # convert fields into 'ordered sequence' looking hash
    fields      = result ['fields']
    field_types = result ['field_types']
    
    # build sql statements list
    sql = ''
    skippedExistingData = 0
    totalData           = 0
    for datum in result ['data']:
        totalData += 1
        # optional safety check : does this data already exist?
        # Under testing situations, it almost certainly will, but its
        # debateable that it will under regular conditions
        if check_for_entry:
            sql = compose_sql_exists_query (
                tableName, fields, datum, verbose=verbose)
            result = query_db (dbconnect, sql, fetchone=True, verbose=False)
            if result ['data']:
                # already exists (give warning?)
                skippedExistingData += 1
                if verbose:
                    msg = 'Merge Skipping duplicate entry:%s'
                    print_msg (msg % str (datum), True)
                    pass
                continue
            pass
        
        sql = compose_sql_insert2 (
            tableName, fields, datum, CLOBBER_MODEOK=clobber)
        
        try:
            update_db (dbconnect, sql, verbose=verbose)
            if verbose:
                print_msg ('  updated entry in local db:%s' % str(datum), True)
                pass
            pass
        except sqlite.IntegrityError:
            # we may be colliding with existing data, if so, and
            # we are NOT in clobber mode, pass it, go to next entry
            if not clobber and NOT_UNIQUE_COL_REGEX.match(str(sys.exc_value)):
                if verbose:
                    msg  = '\n\n *** MERGE INFO : Entry already exists in '
                    msg += 'table, skipping to next \n\n'
                    print_msg (msg, True)
                    pass
                pass
            else:
                raise sys.exc_type, sys.exc_value
            pass
        pass
    
    if skippedExistingData > 0:
        msg  = 'There where %d (out of %d) rows of data that '
        msg += 'already exist in the local database'
        warning (PREFIX, msg % (skippedExistingData, totalData))
        pass
    
    return


def compose_sql_query (cols, tableName, limits=None, verbose=False):
    
    sql = 'SELECT %s FROM %s ' % (cols, tableName)
    
    # construct 'where' part of statement
    if limits and len (limits.keys ()) > 0:
        sql += ' WHERE '
        
        # order statement
        ordercol    = None
        ordercolval = None
        
        for key in limits.keys ():
            val = limits [key]
            
            #if verbose:
            #    msg = ' col_query gets key = %s, val = %s' % (key, val)
            #    print_msg (msg, True)
            #    pass
            
            # collect any ordering key..we can only have one
            if CORE_KEY_REGEX.match (key):
                if ordercol:
                    msg  = 'Warning: already have order col:%s '
                    msg += 'overriding with new one'
                    print_msg (msg % ordercol, True)
                    pass
                
                ordercol    = key [:-5]
                ordercolval = val
                continue
            
            # value is list/tuple, implying 'choice'
            if isinstance (val, tuple) or isinstance (val, list):
                for v in val:
                    sql += compose_sql_limit (key, v)
                    sql += ' or '
                    pass
                
                # trim off last ' or '
                sql = sql [:-4]
                
            else: # value is scalar
                sql += compose_sql_limit (key, val)
                pass
            
            sql += ' and '
            pass
        
        # trim off remaining 'and'
        sql = sql [:-5]
        pass
    
    # order statement?
    if ordercol and ordercolval:
        orderstm = ' order by abs(%s - %s)' % (ordercol, ordercolval)
        sql += orderstm
        print_msg (orderstm, True)
        pass
    
    # close off sql with semi-colon
    sql += ';'
    
    #if verbose:
    #    print_msg (' col_query_sql:[%s]' % (sql), True)
    #    pass
    
    return sql


def compose_sql_col_update (tableName, colName, value, dict):
    
    """
    Compose an SQL statement to update a column within a table.
    """
    
    # compose the SQL query so that we ingest the required
    # file information into the database. Everything happens
    # in an SQL transaction.
    sql = 'UPDATE %s SET %s = ' % (tableName, colName)
    
    if not value:
        sql += 'NULL'
    elif not isinstance (value, str):
        sql += str (val)
    else:
        sql += forceQuote (value, trim=False)
        pass
    
    sql += ' WHERE '
    
    keys = dict.keys ()
    for key in keys:
        sql += '%s = ' % (key)
        val = dict [key]
        if not isinstance (val, str):
            sql += '%s, ' % (str (val))
        elif not val:
            sql += 'NULL, '
        else:
            sql += forceQuote (val, trim=False) + ', '
            pass
        pass
    
    # remove the extra ', ' and close the statement
    sql = sql [:-2] + ';'
    
    return (sql)


def compose_sql_exists_query (tableName,
                              fields,
                              values,
                              search_col = None,
                              verbose    = False):
    
    """
    Create an SQL query to determine if the item in question exists.
    Preferentially, we check for the ID (primary key) in the table,
    otherwise, its just the first column value that is returned.
    """
    
    # sanity check
    nrofFields = len (fields)
    if nrofFields != len (values):
        msg  = 'compose_sql_exists can proceed: the length '
        msg += 'of fields/values are not the same.'
        raise DimensionError, msg
    
    # init limits for our search
    limits = {}
    for i in xrange (0, nrofFields):
        if values [i]:
            limits [fields [i]] = values [i]
            pass
        # Commmented out because of sqlite hack: can't handle
        # search for non-null items
        #else:
        #    limits [fields [i]] = 'NULL'
        pass
    
    # safety: if no search column (table has no index/key)
    # then return first col
    if not search_col:
        search_col = fields [0]
        pass
    
    sql = compose_sql_query (search_col, tableName, limits, verbose=verbose)
    
    return sql


def compose_sql_insert (tableName, dict, CLOBBER_MODEOK=False):
    
    """
    Convenience routine for better source code readibility.
    This version is dictionary-oriented (the dict contains
    field<=>value pairs)
    """
    
    # compose the SQL query so that we ingest the required
    # file information into the database. Everything happens
    # in an SQL transaction.
    sql = 'INSERT '
    
    if CLOBBER_MODEOK:
        sql += 'OR REPLACE '
        pass
    
    sql += 'INTO %s ( ' % (tableName)
    
    keys = dict.keys ()
    for key in keys:
        sql += key + ', '
        pass
    
    # remove the extra ', ' and close the
    # column specification list
    sql = sql [:-2] + ') VALUES ('
    
    # compose the rest of the SQL statement
    for key in keys:
        val = dict [key]
        if not val:
            sql += 'NULL, '
        elif not isinstance (val, str):
            sql += str (val) + ', '
        else:
            sql += forceQuote (val, trim=False) + ', '
            pass
        pass
    
    # remove the extra ', ' and close the statement
    sql = sql [:-2] + ')'
    
    return sql


def compose_sql_insert2 (tableName, fields, values, CLOBBER_MODEOK=False):
    
    """
    Convenience routine for better source code readibility.
    This one is array/sequence oriented.
    """
    
    # compose the SQL query so that we ingest the required
    # file information into the database. Everything happens
    # in an SQL transaction.
    sql = 'INSERT '
    
    if CLOBBER_MODEOK:
        sql += 'OR REPLACE '
        pass
    
    sql += 'INTO %s ( ' % (tableName)
    
    for field in fields:
        sql += field + ', '
        pass
    
    # remove the extra ', ' and close the
    # column specification list
    sql = sql [:-2] + ') VALUES ('
    
    # compose the rest of the SQL statement
    for val in values:
        if not val:
            sql += 'NULL, '
        elif not isinstance (val, str):
            sql += str (val) + ', '
        else:
            sql += forceQuote (val, trim=False) + ', '
            pass
        pass
    
    # remove the extra ', ' and close the statement
    sql = sql [:-2] + ')'
    
    return sql


def compose_sql_limit (key, val):
    
    """
    compose limit for sql string
    """
    
    sql = ''
    
    # min/max are exclusive
    if MAX_KEY_REGEX.match (key):
        sql +=  key [:-4] + ' < '
    elif MIN_KEY_REGEX.match (key):
        sql +=  key [:-4] + ' > '
    else:
        sql += key + ' = '
        pass
    
    if not isinstance (val, str):
        sql += str (val)
    elif not val:
        sql += 'NULL'
    else:
        sql += forceQuote (val, trim=False)
        pass
    
    return sql


def copyto (src, dst, type, verbose=False):
    
    """
    Handles the copying of files/directories etc. to
    and from the File Repository and the users's
    MarioCal directory.
    
    Input:
        src     input file name (UNIX or IRAF notation)
        dst     destination directory (UNIX or IRAF notation)
    
    Returns:
        !0      in case of error
        0       for success
    
    Discussion:
    Depending on the file type (file, image, directory...)
    we might use a different copying mechanism (cp vs rsync
    vs imcopy...).
    
    If fclass is specified (and fmjd=None), then a directory
    named <fclass> is created (if needed) in <dst> and the
    file <src> is copied to <dst>/<fclass>.
    
    If fmjd is specified (and fclass=None), then a directory
    named <fmjd> is created (if needed) in <dst> and the
    file <src> is copied to <dst>/<fmjd>.
    
    If both fmjd and fclass are specified, then a directory
    named <fclass> is created (if needed) in <dst> and a
    directory named <fmjd> is created (if needed) in
    <dst>/<fclass>. The file <src> is then copied into
    <dst>/<fclass>/<fmjd>.
    """
    
    t = time.time ()
    
    # copy_cmd = 'rsync -a --delete %s'         # DOES NOT WORK WITH POPEN
                                                # AND PYTHON 2.2.2
    # copy_cmd = 'scp -prqC %s'
    # copy_cmd = 'sh -c \"rsync -aq --delete %s\"'
    copy_cmd = 'csh -cf \"rsync -aq --delete --exclude=\'.*\' %s\"'
    # copy_cmd = 'ssh 127.0.0.1 \"rsync -aq --delete %s\"'
    # copy_cmd = 'rsh 127.0.0.1 \"rsync -aq --delete %s\"'
    
    # make sure that the directory structure is OK
    dst_dir = string.split (dst, '!')[-1]
    dst_dir = string.split (dst_dir, ':')[-1]
    path    = makeDirs (dst_dir, dry_run=True)
    if not path:
        # something went bad!
        warning (PREFIX, '%s cannot be created.\n' % (dst_dir))
        return 1
    
    # tnrn the src and dst paths from IRAF networking notation
    # to UNIX networking notation (if needed).
    unix_src = iraf2unix (src)
    unix_dst = iraf2unix (dst)
    if unix_dst [-1] != '/':
        unix_dst += '/' + os.path.basename (unix_src)
    else:
        unix_dst += os.path.basename (unix_src)
        pass
    
    # decide what to do based on the file type.
    if type == 'directory':
        # rsync the directory. Don't forget the trailing slash!
        copy_args = '%s/ %s/' % (unix_src, unix_dst)
    elif type in ['file', 'image']:
        # just copy the file over. We use rsync for efficiency
        copy_args = '%s %s' % (unix_src, unix_dst)
    #elif type == 'image':
    #    # just copy the image over.
    #    copy_args = '%s %s' % (unix_src, unix_dst)
    else:
        # unsupported data type!
        warning (PREFIX, 'unsupported data type (%s)' % (datatype))
        return 1
    
    # actually copy the thing!
    command = copy_cmd % (copy_args)
    if verbose:
        print command
        pass
    
    status = os.popen (command).close ()
    
    dt = time.time () - t
    if verbose:
        print_msg ('TIME\tCOPYTO\t%.02f' % (dt), False)
        pass
    
    # parse the status code
    if status == None:
        return 0
    else:
        return os.WEXITSTATUS (status)
    # return
    pass


# ugh. if python worked right, we wouldnt need this..
def expand_serialized_tabs (mystring):
    
    #tran_table = string.maketrans ('\t', ' ')
    #mystring   = mystring.translate (tran_table)
    
    # ugh. this is the only way I have found that works..
    mystring = mystring.replace ('\\t', '[TAB]')
    mystring = mystring.replace ('\[TAB]', '\t')
    
    return mystring


def deserialize_sql_result_pkt (string):
    
    """
    Used to deserialize pkt information from a SQL packet string example:
    STATUS = '0 sql_query'
    FIELDS = '{'observeMJD': 7, 'ampSigma': 12, 'URL': 5, 'parentBiasId': 14,
               'ampLevel': 11, 'producedBy': 4, 'id': 0,
               'usableForCalibration': 13, 'id_str': 1, 'biasJump': 8,
               'overscanLevel': 9, 'overscanSigma': 10, 'class':2,
               'createMJD': 3, 'obsConfig': 6}'
    VALUES = '['(2, 'proc1', 1, 53327.469583300001, -1.0, '0.0.0.0', None,
               None, -1)', '(3, 'proc2', 1, 53327.469583300001,-1.0, '0.0.0.0',
               None, None, -1)']'
    
    NOTE: This isn't the fastest implementation processing-wise, but thats
    probably not important as this method will only be called at start-up,
    by the 'init{To|From}' (or related) methods and only some recent part of
    the other SQL database is not going to be requested and returned.
    """
    
    result = {   'fields':[],
            'field_types':[],
        'field_precision':[],
         'field_nullable':[],
                   'data':[]}
    
    if string and string != '':
        dict = parsePacket (string , False)
        # FIX : remove
        # print 'PARSE PAK on [%s]' % string
        # print 'LEN: %d KEYS %s ' % (len (dict), str (dict.keys ()))
        for key in dict.keys ():
            if key == 'status':
                status, prog = strip_first_last_char (dict [key]).split (' ')
                # FIX:
                #msg = '\n---------\n GOT Status:%s Prog:%s\n'
                #print_msg (msg % (status, prog), True)
                if prog != 'sql_query':
                    msg = 'Can\'t parse SQL packets from method: %s'
                    raise ValueError, msg % prog
                #if status != '0':
                #    msg = 'Can\'t parse bad status SQL packets. Status:%s'
                #    raise ValueError, msg % status
                pass
            
            # deserialize sequence-like stuff
            elif key in result:
                # remove quotes from string value
                str_value    = dict [key].strip ()
                result [key] = deserialize_pkt (str_value)
            else:
                # deserialize sequence within sequence-like stuff
                msg = 'deserialize_sql_pkt passed a badly formatted packet'
                raise ValueError, msg
            pass
        
        # --> end loop
        pass
    
    return result


def get_remote_repository (addr,port):
    
    pkt = composePacket ({'COMMAND': 'get_repository'})
    response = sendPacketTo (
        pkt, addr, int (port), timeout=TIMEOUT, will_respond=True)
    
    result = parsePacket (response)
    
    return result ['value']


def find_cal_files_from_results (results,
                                 dir          = None,
                                 check_exist  = False,
                                 halt_on_warn = False):
    
    fields = results ['fields']
    data   = results ['data']
    
    # find column positions of value, datatype
    col, class_col, mjd_col, value_col, datatype_col = 0, -1, -1, -1, -1
    
    for field in fields:
        if CLASS_COL_REGEX.match (field):
            class_col = col
            pass
        if DATATYPE_COL_REGEX.match (field):
            datatype_col = col
        elif MJD_COL_REGEX.match (field):
            mjd_col = col
        elif VALUE_COL_REGEX.match (field):
            value_col = col
            pass
        col += 1
        pass
    
    # find all the "shippable" items.. e.g. 'file', 'directory' and 'image'
    # datatypes
    items = {}
    for row in data:
        datatype = row [datatype_col]
        #print 'ROW %s' % str (row)
        #print 'DATATYPE %s' % datatype
        if datatype in ['file', 'image', 'directory']:
            value         = row [value_col]
            iclass        = row [class_col]
            mjd           = row [mjd_col]
            items [value] = {'type': datatype, 'class': iclass, 'mjd': mjd}
            pass
        pass
    
    if check_exist and dir:
        # check that files exist locally
        for item in items:
            path = '%s/%s' % (dir, item)
            if not os.path.exists (path):
                items [item] = None
                msg = ' Database refers to MISSING calibration item %s'
                warning (PREFIX, msg % path)
                if halt_on_warn:
                    sys.exit (-1)
                    pass
                pass
            pass
        pass
    
    return items


def initFrom (addr         = None,
              port         = None,
              sinceMJD     = None,
              dbconnection = None,
              caldir       = None,
              verbose      = False,
              clobber      = False,
              halt_on_warn = False,
              timeout      = TIMEOUT):
    
    """
    Sync the database of the DM to another one, with
    an option to set how far back in time we wish to sync,
    and whether or not we wish to clobber current, local
    information which conflicts with that in the other DM.
    """
    
    if verbose:
        print_msg (' Init DM from DM at %s:%s' % (addr,port), True)
        pass
    
    # safety : all of these method params are critical
    if not (addr and port and caldir and dbconnection):
        raise MissingAttributeError, 'initFrom missing critical parameter'
    
    # safety: if no time limit is specified, lets make it ONE day ago
    if not sinceMJD:
        sinceMJD = MJD() - 1
        pass
    
    if verbose:
        msg = '   * retrieving data since date: MJD %f'
        print_msg (msg % (sinceMJD), True)
        pass
    
    # retrieve remote info
    # we use the Pipeline packet format to do so.
    
    # get calibration information
    sql = 'select * from catalog where MJD > %s;' % (str (sinceMJD))
    if verbose:
        print_msg ('  SEND QUERY: %s' % sql, True)
        pass
    
    pkt = composePacket ({'COMMAND': 'sql_query','SQL': sql})
    calibration_result = deserialize_sql_result_pkt (sendPacketTo (
        pkt, addr, int (port), timeout=timeout, will_respond=True))
    #FIX:
    #if verbose:
    #    print_msg (' GOT CAL RESULT:%s' % calibration_result, True)
    #    pass
    
    # get process information
    sql = 'select * from ProcessingProcesses where startMJD > %s;' % sinceMJD
    if verbose:
        print_msg ('  SEND QUERY: %s' % sql, True)
        pass
    
    pkt = composePacket ({'COMMAND': 'sql_query', 'SQL': sql})
    processes_result = deserialize_sql_result_pkt (sendPacketTo (
        pkt, addr, int (port), timeout=timeout, will_respond=True))
    #FIX:
    #if verbose:
    #    print_msg (' GOT PROC RESULT:%s' % processes_result, True)
    #    pass
    
    # get data-product information
    sql = 'select * from ProcessingData where createMJD > %s;' % sinceMJD
    if verbose:
        print_msg ('  SEND QUERY: %s' % sql, True)
        pass
    
    pkt = composePacket ({'COMMAND': 'sql_query','SQL': sql})
    data_result = deserialize_sql_result_pkt (sendPacketTo (
        pkt, addr, int (port), timeout=timeout, will_respond=True))
    # FIX:
    #if verbose:
    #    print_msg (' GOT DATA RESULT:%s' % data_result, True)
    #    pass
    
    # merge info into local DB
    try:
        # list of cal data products we *may* need to ship (copy)
        # in the case where we *don't* clobber, we only send what
        # is missing at the master site. In verbose mode, report any
        # collisions.
        remote_cal_files = get_remote_calibration_file_list (
            addr, port, sinceMJD=sinceMJD, halt_on_warn=halt_on_warn,
            timeout=TIMEOUT, verbose=verbose)
        
        if not clobber:
            local_cal_files = find_cal_files_from_results (
                calibration_result, dir=caldir,
                check_exist=True, halt_on_warn=halt_on_warn)
            # Compare local list to the master list. IF the LOCAL list
            # is missing that product, we mark it for transfer.
            if verbose:
                msg  = '  * Compare remote/local calibration '
                msg += 'file lists for duplicates'
                print_msg (msg, True)
                pass
            
            if local_cal_files:
                for local_cal_file in local_cal_files:
                    if remote_cal_files [local_cal_file]:
                        remote_cal_files [local_cal_file] = 0
                        print_msg (
                            ' IGNORE DUPLICATE file %s' % local_cal_file, True)
                        pass
                    pass
                pass
            pass
        
        # 2. Retrieve missing calibration products.
        if verbose:
            print_msg ('  * Retrieving missing calibration products', True)
            pass
        
        repository = get_remote_repository (addr, port)
        remote_loc = '%s:%s' % (addr, repository)
        move_files (remote_cal_files, remote_loc, True, caldir, True, verbose)
        
        # 3. Merge meta-data on data products/processes.
        print '  * Merging meta-data on data products into local database'
        __merge_result_into_db_table (
            dbconnection, 'catalog', calibration_result,
            clobber=clobber, verbose=verbose)
        __merge_result_into_db_table (
            dbconnection, 'ProcessingProcesses', processes_result,
            clobber=clobber, verbose=verbose)
        # FIX : remove dump table block
        if verbose: 
            print ' AFTER MERGE LOCAL Processing Table is now:'
            __dump_table (dbconnection, 'ProcessingProcesses', 20) 
            pass
        
        __merge_result_into_db_table (
            dbconnection, 'ProcessingData', data_result, clobber)
        # FIX : remove dump table block
        if verbose: 
            print ' AFTER MERGE LOCAL DataProducts Table is now:'
            __dump_table (dbconnection, 'ProcessingData', 20) 
            pass
        pass
    except:
        msg = 'FAILED MERGE - %s:%s'
        print_msg (msg % (sys.exc_type, sys.exc_value), True)
        if halt_on_warn:
            sys.exit (-1)
            pass
        pass
    
    if verbose:
        print_msg (' ...finished', True) # FIX
        pass
    
    return


def initTo (addr,
            port,
            dbconnection,
            caldir,
            sinceMJD     = None,
            clobber      = False,
            verbose      = False,
            dry_run      = False,
            halt_on_warn = False):
    
    """
    Init a remote data manager with information contained in the local 
    DM environment.
    """
    
    # 1. Check which files do/don't exist in the remote location.
    # In CLOBBER mode, we don't bother checking, but just send anyways.
    # The most effecient way to do this is to see what the oldest
    # calibration product is in our database, then pull over all
    # the entries in the remote db that have been entered since
    # that time.
    
    # 1a. Get local list of cal/pmass products.
    if verbose:
        print '  * get local data products'
        pass
    
    if sinceMJD:
        sql = 'select * from catalog where MJD > %s;' % (sinceMJD)
    else:
        sql = 'select * from catalog;'
        pass
    
    local_cal_results = query_db (dbconnection, sql, verbose=verbose)
    
    if sinceMJD:
        sql = 'select * from ProcessingProcesses where startMJD > %s;'
        sql = sql % sinceMJD
    else:
        sql = 'select * from ProcessingProcesses;'
        pass
    
    local_proc_results = query_db (dbconnection, sql, verbose=verbose)
    
    if sinceMJD:
        sql = 'select * from ProcessingData where createMJD > %s;'
        sql = sql % sinceMJD
    else:
        sql = 'select * from ProcessingData;'
        pass
    
    local_data_results = query_db (dbconnection, sql, verbose=verbose)
    
    # list of cal data products we *may* need to ship (copy)
    # in the case where we *don't* clobber, we only send what
    # is missing at the remote site. In verbose mode, report any
    # collisions.
    local_cal_files = find_cal_files_from_results (
        local_cal_results, dir=caldir,
        check_exist=True, halt_on_warn=halt_on_warn)
    
    if not clobber:
        # 1b. Pull over remote list, using the oldest local cal product
        #     as a limit on the list.
        if verbose:
            print '  * Pull remote list'
            pass
        
        sql  = 'select * from catalog where datatype == \'file\' or '
        sql += 'datatype == \'image\' or datatype == \'directory\';'
        if verbose:
            print ' SEND QUERY: %s' % sql
            pass
        
        pkt = composePacket ({'COMMAND': 'sql_query', 'SQL': sql})
        remote_cal_files = None
        try:
            remote_cal_retn = sendPacketTo (
                pkt, addr, int (port), timeout=TIMEOUT, will_respond=True)
            remote_cal_result = deserialize_sql_result_pkt (remote_cal_retn)
            remote_cal_files  = find_cal_files_from_results (remote_cal_result)
        except socket.error:
            warning (PREFIX, '%s:%s' % (sys.exc_type, sys.exc_value))
            if halt_on_warn:
                sys.exit (-1)
                pass
            pass
        except:
            if verbose:
                msg = 'initTo PASSED EXCEPTION %s : %s'
                print_msg (msg % (sys.exc_type, sys.exc_value), True)
                pass
            pass
        
        # 1c. Compare local list to the master list. IF master list
        #     is missing that product, we mark it for transfer.
        if verbose:
            msg  = '  * Compare remote/local calibration '
            msg += 'file lists for duplicates'
            print_msg (msg, True)
            pass
        
        if remote_cal_files:
            for remote_cal_file in remote_cal_files:
                try:
                    if local_cal_files [remote_cal_file]:
                        msg = ' IGNORE DUPLICATE master file %s'
                        print msg % remote_cal_file
                        local_cal_files [remote_cal_file] = 0
                        pass
                    pass
                except KeyError:
                    pass # no local version
                pass
            pass
        pass
    
    # 2. Send calibration products.
    if verbose:
        print_msg ('  * Sending missing calibration products', True)
        pass
    
    repository = get_remote_repository (addr, port)
    remote_loc = '%s:%s' % (addr, repository)
    move_files (local_cal_files, caldir, True, remote_loc, True, verbose)
    
    # 3. Update meta-data about calibration products (both CAL/PMASS)
    if verbose:
        print '  * Updating calibration/pmass meta-data'
        pass
    
    update_remote_table (
        addr, port, 'catalog', local_cal_results ['fields'],
        local_cal_results ['field_types'], local_cal_results ['data'],
        dry_run=dry_run, clobber=clobber, verbose=verbose,
        halt_on_warn=halt_on_warn)
    
    update_remote_table (
        addr, port, 'ProcessingProcesses', local_proc_results ['fields'],
        local_proc_results ['field_types'], local_proc_results ['data'],
        dry_run=dry_run, clobber=clobber, verbose=verbose,
        halt_on_warn=halt_on_warn)
    
    update_remote_table (
        addr, port, 'ProcessingData', local_data_results ['fields'],
        local_data_results ['field_types'], local_data_results ['data'],
        dry_run=dry_run, clobber=clobber, verbose=verbose,
        halt_on_warn=halt_on_warn)
    
    # finished!
    return


def move_files (files                = {},
                src_repository       = None,
                src_frep_has_struct  = True,
                dest_repository      = None,
                dest_frep_has_struct = True,
                verbose              = False):
    
    """
    Utility method to move files between repositories
    """
    
    # sanity check
    if not (src_repository and dest_repository):
        msg = 'move_files missing either/both src or dest repos names.'
        raise MissingAttributeError, msg
    
    for file in files:
        if files [file]:
            ldict  = files [file]
            dtype  = ldict ['type']
            fclass = ldict ['class']
            fmjd   = ldict ['mjd']
            
            # round fmjd to the nearest int
            fmjd = str (int (round (float (fmjd))))
            master_path = '/%s/%s' % (fclass, fmjd)
            
            src_loc = src_repository
            if src_frep_has_struct:
                src_loc += master_path
                pass
            
            src_loc += '/%s' % file
            
            dest_loc = dest_repository
            if dest_frep_has_struct:
                dest_loc += master_path
                pass
            
            dest_loc += '/'
            
            print 'COPY %s => %s' % (src_loc, dest_loc)
            copyto (src_loc, dest_loc, dtype, verbose)
            pass
        pass
    
    return


def open_connection (database, prefix=PREFIX, retry=RETRY, timeout=TIMEOUT):
    
    start      = time.time ()
    connection = None
    while not connection and (time.time () - start) < timeout:
        try:
            connection = sqlite.connect (database)
        except:
            msg = 'cannot open a connection to the DB. Retrying in %fs.\n'
            warning (prefix, msg % retry)
            time.sleep (retry)
            pass
        pass
    
    return connection


def parse_dm_list_str (str_list, ignore_addr='', ignore_port=''):
    
    dm_list = []
    
    for dmitem in str_list.split ():
        subitems = dmitem.split (':')
        if ignore_addr != subitems [1] or ignore_port != subitems [2]:
            # now connect to that server and find:
            # 1. its up and running
            #  and
            # 2. its in master mode
            pkt = composePacket ({'COMMAND': 'is_mastermode',})
            try:
                response = sendPacketTo (
                    pkt, subitems [1], int (subitems [2]), will_respond=True)
                result = parsePacket (response)
                # could connect, now check mode..
                if not result ['value'] or result ['value'] != 'True':
                    continue
                pass
            except:
                # can't connect...
                #msg = ' ** IGNORING : %s because %s'
                #print_msg (msg % (str(subitems), sys.exc_value), True)
                continue
            
            dm_list.append (subitems [1:])
            pass
        pass
    
    return dm_list


def print_msg (msg, debug=True):
    
    print (msg)
    sys.stdout.flush ()
    return

def query_db (connection, sql, fetchone=False, verbose=False):
    
    """
    send a query to the database
    """
    
    if verbose:
        type = 'multi'
        if fetchone:
            type = 'single'
            pass
        print_msg ('start %s query on sql: (%s)' % (type, sql), True)
        pass
    
    # prepare vars
    results = {  'fields':[],
            'field_types':[],
        'field_precision':[],
         'field_nullable':[],
                   'data':None}
    
    executeOK = False
    
    # get starttime
    start = time.time ()
    
    # Send off the query...
    # being wary that the database could be locked!
    while not executeOK and (time.time () - start) < TIMEOUT:
        try:
            # get a cursor
            dbc = connection.cursor ()
            # send the query
            dbc.execute (sql)
            executeOK = True
            if fetchone:
                results ['data'] = dbc.fetchone ()
            else:
                results ['data'] = dbc.fetchall ()
                pass
            
            # record our fields in the results container
            for field in dbc.description:
                #if verbose:
                #    print 'Result FIELD:%s is col %d' % (field [0], column)
                #    pass
                
                results ['fields'].append          (field [0])
                results ['field_types'].append     (field [1])
                results ['field_precision'].append (field [4])
                results ['field_nullable'].append  (field [6])
                pass
            pass
        except sqlite.OperationalError:
            # operational error..db may be busy. lets retry.
            msg =  '%s:%s. Retrying in %fs.\n'
            warning (PREFIX, msg % (sys.exc_type, sys.exc_value, RETRY))
            time.sleep (RETRY)
            
        except:
            # all other exceptions..cause us to 'bail'
            raise sys.exc_type, 'QueryDB error:[%s]' % str (sys.exc_value)
        pass
    
    if not executeOK:
        # we get here if we time out
        raise sqlite.OperationalError, 'Timed out statement (%s)' % (sql)
    
    return results


def start_data_manager_srv (server_address = ('', 9000),
                            HandlerClass   = DataManagerRequestHandler,
                            ServerClass    = DataManagerServer,
                            db             = None,
                            repository     = None,
                            master_mode    = True,
                            verbose_mode   = False,
                            clobber_mode   = False,
                            test_mode      = False,
                            pidFile        = None):
    
    """
    Test the DataManager request handler class.
    
    This runs a DataManager server on port 8000 (or the first command line
    argument).
    """
    
    # create the server
    try:
        data_manager_srv = ServerClass (
            server_address, HandlerClass, db, repository,
            MASTER_MODE=master_mode, VERBOSE_MODE=verbose_mode,
            CLOBBER_MODE=clobber_mode, TEST_MODE=test_mode)
    except socket.error:
        msg = 'unable to start DataManager: (port %d already in use??)'
        fatalError (PREFIX, msg % (server_address [1]))
    except:
        msg = 'unable to start DataManager: %s:%s'
        fatalError (PREFIX, msg % (sys.exc_type, sys.exc_value))
        pass
    
    # init basic data from the master server, if it exists

    """
    # FIX: remove
    try:
        if not master_mode and DMMASTERHOST and DMMASTERPORT:
            data_manager_srv.initFrom(
                DMMASTERHOST, DMMASTERPORT,
                toMJD (toUTC (START_TIME-86400)), True)
            pass
        pass
    except:
        msg = '%s:%s:Unable to sync slave DataManager'
        fatalError (PREFIX, msg % (sys.exc_type, sys.exc_value))
        pass
    """
    
    if pidFile:
        try:
            pid = file (pidFile, 'w')
            pid.writelines ('%d' % os.getpid())
            pid.close ()
        except:
            print 'Unable to write pid:', os.getpid()
            pass
        pass
    
    print_msg ('%s started on %s:%d at MJD %f' % (
        PREFIX, NODE, server_address [1], MJD ()))
    
    # handle requests, as desired
    try:
        while not data_manager_srv.DONE:
            try:
                data_manager_srv.handle_request ()
            except:
                print_msg ('%s terminated on %s:%s.' % (
                    PREFIX, sys.exc_type, sys.exc_value))
                data_manager_srv.DONE = True
                pass
            pass
        pass
    except:
        print_msg ('%s terminated on exception %s.' % (PREFIX, sys.exc_type))
        pass
    return


def strip_first_last_char (string, chr=None):
    
    last_pos = -1
    if chr:
        last_pos = string.rfind (chr)
        pass
    return string [1:last_pos]

def update_db (connection, sql, verbose=False, prefix=PREFIX):
    
    """
    send a request to update the database
    """
    
    # Send off the query...
    # The database could be locked!
    executeOK = False
    start     = time.time ()
    
    if verbose:
        print_msg ('UPDATE_DB SQL:[%s]' % sql, True)
        pass
    
    while not executeOK and (time.time () - start) < TIMEOUT:
        try:
            # get a cursor
            dbc = connection.cursor ()
            # send the query
            dbc.execute (sql)
            executeOK = True
            
        except sqlite.OperationalError:
            # operational error..db may be busy. lets retry.
            warning (prefix, '%s:%s. Retrying in %fs.\n' % (
                sys.exc_type,sys.exc_value,RETRY))
            time.sleep (RETRY)
        except:
            # all other exceptions..cause us to 'bail'
            raise sys.exc_type, sys.exc_value
        pass
    
    if not executeOK:
        # we get here if we time out
        raise sqlite.OperationalError, 'Timed out statement (%s)' % (sql)
    
    # if we got here, then we where successfull
    # and may make our commit now
    connection.commit ()
    
    return


def update_remote_table (addr,
                         port,
                         tableName,
                         in_fields,
                         in_field_types,
                         in_values,
                         check_for_entry = True,
                         dry_run         = False,
                         clobber         = False,
                         verbose         = False,
                         timeout         = TIMEOUT,
                         halt_on_warn    = False):
    
    skippedExistingData = 0
    totalData           = 0
    
    # FIX:
    expected_vals = len (in_values)
    for vals in in_values:
        totalData += 1
        if not clobber:
            if check_for_entry:
                sql = compose_sql_exists_query (
                    tableName, in_fields, vals, verbose=verbose)
                pkt = composePacket ({'COMMAND': 'sql_query', 'SQL': sql})
                result = deserialize_sql_result_pkt (sendPacketTo (
                    pkt, addr, int (port), timeout=timeout, will_respond=True))
                
                if result ['data']:
                    # already exists (give warning?)
                    skippedExistingData += 1
                    #if verbose:
                    #    msg = 'Skipping duplicate entry:%s' % str(vals)
                    #    print_msg (msg, True)
                    #    pass
                    
                    continue
                pass
            pass
        
        (fields, values) = (in_fields, vals)
        
        # FIX
        msg = 'UPDATE remote table %s %d/%d : %s'
        print msg % (tableName, totalData, expected_vals, str (vals))
        
        sql = compose_sql_insert2 (
            tableName, fields, values, CLOBBER_MODEOK=clobber)
        #if verbose:
        #    print 'SQL : %s' % sql
        #    pass
        
        if not dry_run:
            pkt = composePacket ({'COMMAND': 'sql_update', 'SQL': sql})
            response = sendPacketTo (
                pkt, addr, int (port), timeout=TIMEOUT, will_respond=True)
            result = parsePacket (response)
            status = result ['status'].split ()
            if int (status [0]):
                msg = 'Can\'t update remote table using sql:[%s]\n error:%s'
                warning (PREFIX, msg % (sql, str (response)))
                if halt_on_warn:
                    sys.exit (-1)
                    pass
                pass
            elif verbose:
                print_msg (' Updated values:%s' % str (vals), True)
                pass
            pass
        pass
    
    if skippedExistingData > 0:
        msg  = 'There where %d (out of %d) rows of data '
        msg += 'that already exist in the remote database'
        warning (PREFIX, msg % (skippedExistingData, totalData))
        pass
    
    return


# END - STATIC Methods
#

# In the MOSAIC/NEWFIRM pipelines the data manager database and file
# repository are defined by the combination of DMData, DMDB, and pipeapp.

try:
    db = os.environ ['DMDB']
except:
    fatalError (PREFIX, 'the $DMDB variable is not defined.')

# determine the master file repository
try:
    dmdata = os.environ ['DMData']
except:
    fatalError (PREFIX, 'the $DMData variable is not defined.')

dmdb   = os.path.join (dmdata, db)

frep = dmdata
DBTable = db.rsplit ('.', 1)[0]

# do we need to connect to a monitor?
# if so set the DMMONHOST and DMMONPORT 
# global variables
try:
    DMMONITOR = (os.environ ['DMMONHOST'], os.environ ['DMMONPORT'])
except:
    DMMONITOR = None
    pass

# NOT good, would be better to use a resource registry to find other
# master mode DM's.
if DMMASTERHOST and DMMASTERPORT:
    msg = '* Running DM in slave (telescope) mode. Master is at %s:%s'
    print_msg (msg % (DMMASTERHOST, DMMASTERPORT), True) 
else:
    print_msg ('* Running DM in master mode.', True) 
    pass

# determine the DIRECTORY server address/port
try:
    DIRHOST = os.environ ['NHPPS_NODE_DS']
    DIRPORT = int (os.environ ['NHPPS_PORT_DS'])
except:
    DIRHOST = None
    pass

# init our globals
TRANSACTION = 0
START_TIME  = time.time ()

def start (port        = 9000,
           pidFile     = None,
           VERBOSE     = False,
           MASTER_MODE = True,
           TEST_MODE   = False,
           pipeapp     = 'MOSAIC'):
    
    try:
        port = int (port)
    except:
        port = 9000
        pass
    
    if VERBOSE:
        warning (PREFIX, '** running in VERBOSE mode **')
        pass
    if TEST_MODE:
        msg = '** running in TEST mode (DB corruption may result) **'
        warning (PREFIX, msg)
        pass
    
    server_address = ('', port)

    dmdata = frep
    dmdb   = os.path.join (dmdata, db)
    if not os.path.exists (dmdb):
	dmdata = os.path.join (frep, pipeapp)
	dmdb   = os.path.join (dmdata, db)
    start_data_manager_srv (
        server_address=server_address, db=dmdb, repository=dmdata,
        test_mode=TEST_MODE, verbose_mode=VERBOSE, clobber_mode=TEST_MODE,
        master_mode=MASTER_MODE, pidFile=pidFile)
    
    # All done. Exit.
    sys.exit (0)
    return

