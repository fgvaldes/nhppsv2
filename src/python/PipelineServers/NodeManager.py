"""MGR server base class.

The Node Manager provides an interface to Pipeline(s) running on the
host machine. The idea behind it is that a user might interact with the Node
Manager (either directly or via a GUI) without having to know any of the
specific details of how the Pipeline System works.

The basic functionality of the Node Manager is to start/stop pipeline modules
and to start/stop/restart processing on any given dataset.

Additional functionality might include getting information on the load of the
host machine, on its resources (e.g. disk space), and basic information about
the state of the pipelines.

The Node Manager runs as a server accepting connection through a given port.
Connections are stateless. The Node Manager always return a value.
Communications follow the Pipeline Team RPC Protocol.

The Node Manager externally callable functions always return a status code of
0 for success or 1 (or other non-zero) for failure.
"""

# On some systems, the loop in runall which queries the Pyro Name Server
# for the pyro proxy for a pipeline causes the system to crawl.
# To remedy this problem, set SLOW_PYRO to True. This will cause the loop
# to sleep 1 second between failed queries to the Pyro Name Server.
SLOW_PYRO = True

__ACTIVE__ = 1
__HALTED__ = 0

from server import *

import os, re
import pipeadmin

# Global variables
PREFIX = 'NodeManager'      # Prefix for stderr messages

# CONFIG_FILE specifies a configuration file for the Node Manager.
CONFIG_FILE = 'piperc'

# If USE_MEMORY_FILE is set to a non-false value, we allow the Node Manager to
# provide get & set methods for variables in the Node Manager memory space
# which are mirrored in a disk file.
USE_MEMORY_FILE = False

# If USE_KEEP_ALIVE is set to a non-false value, we allow the Node Manager to
# start the pipeline module managers in a thread (no value-added features).
# This is a 'feature' which needs further review and possibly deprecation.
# NOTE: original comments suggest that this option was used to keep the modules
#       alive. This is not necessary though as the NHPPS keeps modules alive
#       until they are told to shutdown. Also, this 'option' does not keep
#       anything alive more than not using this option would.
USE_KEEP_ALIVE = False

EXECUTIONER = 'PipelineMgr'

# classes

class paInfo:
    
    """
    This manages the pipelines within a pipeline application.
    """
    
    def __init__ (self):
        self.pipelines = {}
        return
    
    def addPipeline (self, pipeline, num=None, xml=None, status=-1, proxy=None):
        if pipeline not in self.pipelines:
            self.pipelines.append (
                pipelineInfo (num, xml, status, proxy))
            return True
        return False
    
    def delPipeline (self, pipeline):
        if pipeline in self.pipelines:
            del self.pipelines [pipeline]
            if not self.pipelines:
                return True
            pass
        return False
    
    def isRunning (self, pipeline):
        return pipeline in self.pipelines
    
    def getStatus (self, pipeline):
        return self.pipelines [pipeline].getStatus ()
    
    def setStatus (self, pipeline, val):
        return self.pipelines [pipeline].setStatus (val)
    pass

class pipelineInfo:
    
    """
    This class represents the 'vital' information we wish to track
    on each of the pipelines running on this NODE.
    """
    
    def __init__ (self, num=None, xml=None, status=-1, proxy=None):
        self.nInstances = num
        self.xmlFile    = xml
        self.status     = status
        self.proxy      = proxy
        return
    
    def getStatus (self):
        """
        Updates the status of the pipeline, and returns the appropriate value
        """
        try:
            self.status = self.proxy.getStatus ()
        except:
            pass
        return self.status
    
    def setStatus (self, val):
        """
        Sets the status of the 'status' variable
        """
        self.status = val
        try:
            self.proxy.setStatus (val)
        except:
            pass
        return
    pass

class NodeInfo:
    def __init__ (self, port):
        self.port = port
        self.pipeApps = []
        return
    pass

class dsCache (object):
    """
    The dsCache acts as a cache of the Directory Server (in the event it goes
    down). Its tasks include handling all communication between the Node
    Manager and the Directory Server to include registration with the DM,
    pulling list of other nodes from the DM, etc...
    """
    
    def __init__ (self, nmPort):
        try:
            self.dsHost = os.environ ['NHPPS_NODE_DS']
            self.dsPort = int (os.environ ['NHPPS_PORT_DS'])
        except:
            self.dsHost = None
            self.dsPort = None
            pass
        
        self.connected       = False
        self.nodeData        = {}
        self.nodeData [NODE] = NodeInfo (nmPort)
        
        return
    
    def add (self, pipeApp):
        if pipeApp not in self.nodeData [NODE].pipeApps:
            self.nodeData [NODE].pipeApps.append (pipeApp)
            pass
        if self.dsHost and self.dsPort:
            sock = connect (self.dsHost, self.dsPort)
            if sock:
                status = execute (sock, {
                    'COMMAND': 'add_pipeapp',
                    'NODE'   : NODE,
                    'PORT'   : self.nodeData [NODE].port,
                    'PIPEAPP': pipeApp})
                pass
            pass
        return
    
    def remove (self, pipeApp):
        if pipeApp in self.nodeData [NODE].pipeApps:
            self.nodeData [NODE].pipeApps.remove (pipeApp)
            pass
        if self.dsHost and self.dsPort:
            sock = connect (self.dsHost, self.dsPort)
            if sock:
                status = execute (sock, {
                    'COMMAND': 'del_pipeapp',
                    'NODE'   : NODE,
                    'PIPEAPP': pipeApp})
                pass
            pass
        return
    
    def getNodeManagers (self, pipeApp=None):
        if pipeApp and pipeApp in self.nodeData [NODE].pipeApps:
            hpList = ['%s:%s' % (NODE, self.nodeData [NODE].port)]
        elif not pipeApp:
            hpList = ['%s:%s' % (NODE, self.nodeData [NODE].port)]
        else:
            hpList = []
            pass
        if self.dsHost and self.dsPort:
            sock = connect (self.dsHost, self.dsPort)
            if sock:
                packet = {'COMMAND': 'get_list'}
                if pipeApp:
                    packet ['pipeapp'] = pipeApp
                    pass
                hostPorts = execute (sock, packet)
                if 'list' in hostPorts:
                    hostPorts = hostPorts ['list'].split ()
                    for hp in hostPorts:
                        if hp not in hpList:
                            hpList.append (hp)
                            pass
                        pass
                    pass
                pass
            pass
        return hpList
    pass

class MGRServer (PipeSocketServer):
    
    def __init__ (self, server_address, RequestHandlerClass):
        
        # Shall we use the faster polling time for the Scheduler internal loop?
        try:
            self.fast = os.environ ['NHPPS_POLL_SPEEDUP'].lower ()
            if self.fast != 'yes':
                self.fast = 'no'
                pass
            pass
        except:
            self.fast = 'no'
            pass
        
        # we need $NHPPS_LOGS to be defined
        try:
            self.home_dir = os.environ ['NHPPS_LOGS']
        except:
            fatalError (PREFIX, 'NHPPS_LOGS it not defined.')
            pass
        
        # Figure out where the XML files are
        try:
            self.pipeSrc = os.environ ['NHPPS_PIPEAPPSRC']
        except:
            msg = 'the environment variable NHPPS_PIPEAPPSRC is not defined.'
            fatalError (PREFIX, msg)
            pass
        
        PipeSocketServer.__init__ (self, server_address, RequestHandlerClass)
        
        self.ds = dsCache (server_address [1])
        
        # Maintain a list of the pipelines which are running on the node.
        self.pipeData = {}
        
        # cf, if present, lists the pipeline that we want to start as soon as
        # the Node Manager starts. It also lists the number of instance per
        # pipeline that we want started. The syntax is a list of:
        # pipeapp:pipeline = nInst
        try:
            cf = os.path.join (
                os.environ ['NHPPS'], 'config', CONFIG_FILE)
        except:
            cf = None
            pass
        
        # check if the configuration file exists. If so, read it
        if cf and os.path.exists (cf):
            config = readConfFile (cf)
            for pipe in config.keys ():
                pipeapp, pipeline = pipe.split (':')
                try:
                    n = int (config [pipe][0])
                except:
                    continue
                if n > 0:
                    xmlFile = os.path.join (
                        self.xmlDir (pipeapp), '%s.xml' % (pipe))
                    pipeName = extractPipeName (xmlFile)
                    
                    if not pipeName:
                        continue
                    
                    if pipeapp in self.pipeData:
                        if pipeName in self.pipeData [pipeapp]:
                            continue
                        pass
                    else:
                        self.pipeData [pipeapp] = {}
                        self.ds.add (pipeapp)
                        pass
                    
                    #if pipeapp not in self.pipeData:
                    #    if pipeName in self.pipeData or not pipeName:
                    #        continue
                    #    pass
                    
                    msg = '%s Added %d instances of %s under pipeline application %s'
                    print (msg % (PREFIX, n, pipeName, pipeapp))
                    self.pipeData [pipeapp][pipeName] = pipelineInfo (
                        num=n, xml=xmlFile)
                    pass
                pass
            pass
        return
    
    def xmlDir (self, pipeapp):
        
        """
        Returns the path to the XML directory
        for a specfied pipeline application.
        """
        
	xmldir = os.path.join (self.pipeSrc, 'xml')
	if not os.path.exists (xmldir):
	    try:
		self.pipeSrc = os.environ ['NHPPS_PIPESRC']
	    except:
	        pass
	    xmldir = os.path.join (self.pipeSrc, 'xml')
	    if not os.path.exists (xmldir):
		xmldir = os.path.join (self.pipeSrc, pipeapp, 'xml')
        return xmldir
    
    def runall (self):
        
        """
        Launches a EXECUTIONER for all of the pipelines in pipeData which have
        not yet been 'started', i.e., the status is -1.
        """
        
	nm = os.environ ['NHPPS_PORT_NM']
        for pipeapp in self.pipeData:
            logDir = os.path.join (self.home_dir, pipeapp)
            if not os.path.exists (logDir):
                os.mkdir (logDir)
                pass
            for pipe in self.pipeData [pipeapp]:
                if self.pipeData [pipeapp][pipe].status == -1:
	            print "run %s %s" % (pipeapp, pipe)
                    print '%s --file=%s --ninst=%d --log=%s --fast=%s >& %s' % (
                        EXECUTIONER,
                        self.pipeData [pipeapp][pipe].xmlFile,
                        self.pipeData [pipeapp][pipe].nInstances,
                        os.path.join (logDir, '%s.log' % (pipe)),
                        self.fast,
                        os.path.join (logDir, '%s.stdout' % (pipe)))
                    os.system (
                        '%s --file=%s --ninst=%d --log=%s --fast=%s < /dev/null >& %s &' % (
                        EXECUTIONER,
                        self.pipeData [pipeapp][pipe].xmlFile,
                        self.pipeData [pipeapp][pipe].nInstances,
                        os.path.join (logDir, '%s.log' % (pipe)),
                        self.fast,
                        os.path.join (logDir, '%s.stdout' % (pipe))))
                    self.pipeData [pipeapp][pipe].setStatus (__ACTIVE__)
                    pass
                pass
            pass
        
        for pipeapp in self.pipeData:
            for pipe in self.pipeData [pipeapp]:
                while not self.pipeData [pipeapp][pipe].proxy:
                    proxies = getNSattrProxies (
                        group=NSGROUP, pipeApp=pipeapp, user=USER, host=NODE, pipe=pipe, nm=nm)
                    if SLOW_PYRO:
                        time.sleep (2)
                        pass
                    for proxy in proxies:
                        try:
                            x = proxy.id
                            self.pipeData [pipeapp][pipe].proxy = proxy
                            break
                        except:
                            continue
                        pass
                    pass
                pass
            pass
        return
    
    def updatePipeData (self, pipeApp):
        
        """
        Gets a list of the pipeline processes (actual pipelines) running on the
        node. Once this list is compiled, the contents are compared to pipeData
        those pipelines which are no longer active are removed from pipeData
        and therefore may be executed again...
        """

	p = re.compile ('python', re.IGNORECASE)
        psline = 'ps xww | grep -i python | grep %s' % (EXECUTIONER)
        lines = os.popen (psline).readlines ()
        
        runningPipelines = []
        for line in lines:
            data = line.split ()
            if len (data) > 5 and p.search (data[4]):
                xmlFile = line.split ('--file=')[1].split ()[0]
                pipe = os.path.basename (xmlFile).split ('.')[0]
                #pipeApp = os.path.dirname (xmlFile).split (os.path.sep)[-2]
                pipeInfo = (pipeApp, pipe)
                if pipeInfo not in runningPipelines:
                    runningPipelines.append (pipeInfo)
                    pass
                pass
            pass
        
        # Remove the `dead` pipelines from the pipeData list
        for pipeapp in self.pipeData.keys ():
            for pipe in self.pipeData [pipeapp].keys ():
                if (pipeapp, pipe) not in runningPipelines:
                    del self.pipeData [pipeapp][pipe]
                    pass
                pass
            if not self.pipeData [pipeapp]:
                del self.pipeData [pipeapp]
                self.ds.remove (pipeapp)
                pass
            pass
        return
    pass

class MGRRequestHandler (PipeRequestHandler):
    
    """
    MGR request handler base class.
    """
    
    #if USE_MEMORY_FILE:
    #    from MemoryFile import mgr_get, mgr_set
    #
    #if USE_KEEP_ALIVE:
    #    from KeepAlive import mgr_set_ptime
    
    #from pipeselect import mgr_pipeselect
    
    def mgr_start_pipe (self):
        
        """
        Starts [an] instance[s] of [a] pipeline[s]. The input parameters
        (stored in self.params) are:
        
        pipelines   a list of lowercase pipeline names. (Pipeline names are
                    actually the names of the XML files to use, stripped of the
                    .xml extension. The actual pipeline names are specified in
                    the XML file itself.)
        instances   number of instances of input pipelines we want to start.
                    This parameter is optional and its default value is 1.
        pipeapp     The pipeline application under which the pipeline is to
                    be run.
        """

        if not self.hasParam ('pipelines', True):
            return
        if not self.hasParam ('pipeapp', True):
            return
        
        # Determine the number of instances of each pipeline to start. Default
        # is 1.
        try:
            num = int (self.params ['instances'])
            if num <= 0:
                num = 1
                pass
            pass
        except:
            num = 1
            pass
        
        activated = ''
        ignored = ''
        pipeApp = self.params ['pipeapp']

        self.server.updatePipeData (pipeApp)
        
        for pipe in self.params ['pipelines']:
            # grep the name of the pipeline
            xmlFile = os.path.join (
                self.server.xmlDir (pipeApp), '%s.xml' % (pipe))
            pipeName = extractPipeName (xmlFile)
            
            if not pipeName:
                ignored += pipe + ' '
                continue
            
            if pipeApp in self.server.pipeData:
                if pipeName in self.server.pipeData [pipeApp]:
                    ignored += pipe + ' '
                    continue
                pass
            else:
                self.server.pipeData [pipeApp] = {}
                self.server.ds.add (pipeApp)
                pass
            
            activated += pipe + ' '
            self.server.pipeData [pipeApp][pipeName] = pipelineInfo (
                num=num, xml=xmlFile)
            pass
        # <-- end for loop
        
        # actually start the pipeline(s)
        self.server.runall ()
        if not activated:
            activated = 'N/A'
            pass
        if not ignored:
            ignored = 'N/A'
            pass
        
        out_msg = 'STATUS = "0 Pipeline(s) activated: %s; ignored: %s"\nEOF'
        out_msg = out_msg % (activated.strip (), ignored.strip ())
        
        # write the out_msg back to the client
        self.wfile.write (out_msg)
        
        return
    
    def mgr_stop_pipe (self):
        
        """
        It stops all the modules of a given pipeline (all instances).
        
        The input parameters (stored in self.params) are:
        
        pipelines   a list of pipeline names
        remove_logs either 1 or 0, controls whether we want to remove the
                    process logs or not (1 = remove log files, 0 = keep log
                    files).
        
        Returns:
        STATUS = 'exit_code message'
        EOF
        """
        
        if not self.hasParam ('pipelines'):
            return
        if not self.hasParam ('pipeapp'):
            return
        
        if 'allpa' in self.params ['pipeapp']:
            pipeApps = self.server.pipeData.keys ()
        else:
            pipeApps = [self.params ['pipeapp']]
            pass
        
        # do we want to remove the logs?
        if (not self.hasParam ('remove_logs', warn=False) or 
            self.params ['remove_logs'] == '0' or
            self.params ['remove_logs'].lower () == 'no'):
            remove_logs = False
        else:
            remove_logs = True
            pass

        ec = 0
        for pipeApp in pipeApps:
            if pipeApp in self.server.pipeData:
                pipes = self.params ['pipelines']
                if 'all' in pipes:
                    pipes = self.server.pipeData [pipeApp].keys ()
                    pass
                
                for pipe in pipes:
                    # In case of error, we get status != 0
                    try:
                        status = self._shutdownPipe (pipe, pipeApp)
                    except:
                        status = 1
                        pass
                    
                    # Update the exit code
                    ec += status
                    pass
                # <-- end of the for loop
                if remove_logs:
                    # Do we want to remove the logs as well?
                    logDir = os.path.join (self.home_dir, pipeApp)
                    logFiles = [lfile for lfile in os.listdir (logDir)
                                if lfile [-4:] == '.log']
                    for logFile in logFiles:
                        os.remove (os.path.join (logDir, logFile))
                        pass
                    pass
                pass
            else:
                print 'stop_pipe: PA (%s) not active on node' % (pipeApp)
                pass
            pass
        
        out_msg = 'STATUS = %d\nEOF' % (ec)
        # write the out_msg back to the client
        self.wfile.write (out_msg)
        return
    
    def mgr_halt_pipes (self):
        self.wfile.write (self._callOSF ('Halt'))
        return
    
    def mgr_step_pipe (self):
        self.wfile.write (self._callOSF ('Step'))
        return
    
    def mgr_resume_pipes (self):
        self.wfile.write (self._callOSF ('Resume'))
        return
    
    def mgr_get_load (self):
        
        """
        Returns the 1 minute, 5 minute and 15 minute averageload.
        
        The input parameters (stored in self.params) are:
        
        none
        
        Returns:
        STATUS = 'exit_code message'
        EOF
        In case of error;
        
        STATUS = 0
        1MINLOAD = yyy
        5MINLOAD = www
        15MINLOAD = zzz
        EOF
        Otherwise.
        """
        
        try:
            out_msg = 'STATUS=0\n1MINLOAD=%s\n5MINLOAD=%s\n15MINLOAD=%s\nEOF'
            out_msg = out_msg % os.getloadavg ()
        except:
            self.send_error (204, "os.getloadavg() failed!")
            pass
        
        self.wfile.write (out_msg)
        return
    
    def mgr_get_queue (self):
        
        """
        Returns the number of files to be processed by a given pipeline(s).
        
        The input parameters (stored in self.params) are:
        
        pipelines       a list of pipeline names.
        pipeapp         name of the pipeline application running pipelines
        
        Returns:
        STATUS = 'exit_code message'
        NFILES = '<pipeline name>:<number of files> \
                  [<pipeline name>:<number of files>] \
                  [<pipeline name>:<number of files>] [...] '
        EOF
        
        Description:
        Given a list of pipeline names (e.g. 'ngt mef cal'), we count the
        number of files in the pipeline's input directory. This gives an
        indication of the queue of the selected pipelines.
        
        The exit code is encoded in <number of files>:
        <number of files> =  -1: Pipeline is not running
        <number of files> =  -2: Error in getting the directory listing
        <number of files> =  -3: Trigger directory not defined
        <number of files> >=  0: success
        """
        
        if not self.hasParam ('pipelines'):
            return
        if not self.hasParam ('pipeapp'):
            return
        
        # init the output message
        out_msg = 'STATUS="0 Success"\nNFILES="'
        pipeApp = self.params ['pipeapp']
        
        # loop though the pipelines
        for pipe in self.params ['pipelines']:
            # do we the ddirectories?
            trigdir, rootdir, indir, datadir,obsdir,errdir = self._get_dir (pipeApp, pipe)
            if trigdir == -1:
                # get_dir returned an error.
                # return -3 files so that we do not select this node.
                out_msg += '%s:-3' % (pipe)
                continue
            
            # is the selected pipeline not currently active?
            if pipeApp in self.server.pipeData:
                if pipe in self.server.pipeData [pipeApp]:
                    if self.server.pipeData [pipeApp][pipe].getStatus () != __ACTIVE__:
                        out_msg += '%s:-1 ' % (pipe)
                        continue
                    pass
                pass
            
            # For now, we just return the total
            # number of entries in the directory list...
            try:
                nfiles = len (os.listdir (trigdir))
            except:
                # we got an error. Write -2 so that 
                # this machine is not selected.
                out_msg += '%s:-2' % (pipe)
                continue
            else:
                # no error! Write the number of files.
                out_msg += '%s:%d ' % (pipe, nfiles)
                pass
            pass
        
        # close the packet and send it out
        out_msg += '"\nEOF'
        self.wfile.write (out_msg)
        return
    
    def mgr_get_dir (self):
        
        """
        Returns the path of the data directory for the given pipeline.
        
        The input parameters (stored in self.params) are:
        
        pipelines       a list of (blank separated) pipeline
                        names.
        pipeapp         name of the pipeline application running pipelines
        check_space     OPTIONAL parameter. It can have only two values: 'yes'
                        or 'no'. If 'yes', the Node Manager will return the
                        available disk space (in Kb) for every data directory.
                        It defaults to 'no'.
        
        Returns:
        STATUS   = 'exit_code message'
        DATADIRS = '<pipeline name>:<path>[:<freespace>] \
                    [<pipeline name>:<path>[:<freespace>]] \
                    [<pipeline name>:<path>[:<freespace>]] [...] '
        EOF
        
        Description:
        Given a list of pipeline names (e.g. 'ngt mef cal'), we retrieve the
        (absolute) paths of the respective data directiries (from the
        environment). Optionally, the available disk space (in Kb) for each
        directory is computed and returned to the client.
        """
        
        if not self.hasParam ('pipelines'):
            return
        if not self.hasParam ('pipeapp'):
            return
        pipeApp = self.params ['pipeapp']
        
        # are we being asked to check for free space?
        if (not self.hasParam ('check_space', warn=False) or 
            self.params ['check_space'].lower () != 'yes'):
            self.params ['check_space'] = False
        else:
            self.params ['check_space'] = True
            pass
        
        # init the output message
        out_msg = 'STATUS = "0 Success"\nDATADIRS = "'
        
        # split the list and loop though its elements
        for pipe in self.params ['pipelines']:
            if self.params ['check_space']:
                trigdir, rootdir, indir, datadir, obsdir, errdir, space = self._get_dir (
                    pipeApp, pipe, check_space = True)
            else:
                trigdir, rootdir, indir, datadir, obsdir, errdir = self._get_dir (pipeApp, pipe)
                pass
            
            if trigdir == -1:
                # no input directory currently defined for pipe!
                # return -1 as the path to signal the error.
                if self.params ['check_space']:
                    out_msg += pipe + ':-1:-1:-1 '
                else:
                    out_msg += pipe + ':-1:-1 '
                    pass
                continue
            
            # write the absolute paths for this pipeline:
            # if the path has spaces, we need to enclose it in quotes:
            trigpath = quote (trigdir)
            rootpath = quote (rootdir)
            inpath = quote (indir)
            datapath = quote (datadir)
            obspath = quote (obsdir)
            errpath = quote (errdir)
            
	    out_msg += pipe
	    out_msg += ':' + trigpath
	    out_msg += ':' + rootpath
	    out_msg += ':' + inpath
	    out_msg += ':' + datapath
	    out_msg += ':' + obspath
	    out_msg += ':' + errpath
            if self.params ['check_space']:
                out_msg += ':' + str (space)
	    out_msg += ' '
        
        # close the packet and send it out
        out_msg += '"\nEOF'
        self.wfile.write (out_msg)
        return
    
    def mgr_get_node_list (self):
        
        """
        Returns the full list of known Node Managers running,
        optionally restricted to a specific PA.
        It gets this information from the Directory Server.
        """
        
        pipeApp = None
        if 'pipeapp' in self.params:
            pipeApp = self.params ['pipeapp']
            pass
        
        node_list = self._get_nodes (pipeApp)
        self.wfile.write (node_list)
        return
    
    def mgr_getOpenDatasetCount_pipes (self):
        self.wfile.write (self._callOSF ('Count'))
        return
    
    def mgr_shutdown (self):
        
        """
        Performs a clean shutdown of the Node Manager.
        
        Returns:
        STATUS = 'exit_code message'
        EOF
        """
        
        for pipeApp in self.server.pipeData.keys ():
            self.server.ds.remove (pipeApp)
            pass
        
        out_msg = 'STATUS = "0 Node Manager terminated."\nEOF'
        
        self.wfile.write (out_msg)
        self.server.done = True
        return
    
    def _get_dir (self, pipeApp, pipeline, check_space = False):
        
        """
        Returns the path of the input and root directories for the
	given pipeline.
        
        The input parameters are:
        
        pipeline    the name of a pipeline.
        
        Returns:
        -1:         Directory is not defined.
        <path>      Directory (absolute) path.
        
        Description:
        Given a pipeline name (e.g. ngt), we retrieve the paths of the
        corresponding input and root directories from the pipelines XML tree.
        """
        
        try:
            proxy   = self.server.pipeData [pipeApp][pipeline].proxy
            trigdir = proxy.pipeline.Directories.trig
            rootdir = proxy.pipeline.Directories.root
            indir   = proxy.pipeline.Directories.input
            datadir = proxy.pipeline.Directories.data
            obsdir  = proxy.pipeline.Directories.obs
            errdir  = proxy.pipeline.Directories.error
        except:
            if not check_space:
                return -1, -1
            else:
                return -1, -1, -1
            pass
        if not check_space:
            return trigdir, rootdir, indir, datadir, obsdir, errdir
        else:
            return trigdir, rootdir, indir, datadir, obsdir, errdir, get_freespace (rootdir, pipeApp)
        pass
    
    def _get_nodes (self, pipeApp=None):
        
        """
        Returns the full list of known Node Managers running as a given user.
        It gets this information from the Directory Server. Each list entry is
        in the form
        
        <host name>:<port>
        
        Entries are separated by a white space. If more than one entry is
        returned, the list will be enclosed in double quotes.
        
        Returns:
        STATUS   = 'exit_code[ message]'
        LIST     = '<host name>:<port> [...]'
        EOF
        
        In case of error, LIST is empty.
        In case the Directory Server is unknown or not responding, LIST returns
        the content of node_managers, only.
        """
        
        nmList = string.join (self.server.ds.getNodeManagers (pipeApp))
        return composePacket ({'status':'0 OK', 'list': nmList})
    
    def _callOSF (self, action):
        validActions = ['Halt', 'Resume', 'Count', 'Step']
        if action not in validActions:
            out_msg = 'STATUS="1 Failure"\nEOF'
            return out_msg
        
        if not self.hasParam ('pipelines'):
            return
        if not self.hasParam ('pipeapp'):
            return
        
        if action == 'Halt':
            origStatus = __ACTIVE__; newStatus  = __HALTED__; swapStatus = True
        elif action == 'Resume':
            origStatus = __HALTED__; newStatus  = __ACTIVE__; swapStatus = True
        elif action == 'Count':
            origStatus = __ACTIVE__; newStatus = ''; swapStatus = False
        elif action == 'Step':
            origStatus = __ACTIVE__; newStatus = __HALTED__; swapStatus = False
            pass
        
        pipeApp = self.params ['pipeapp']
        
        # If we didn't specify any pipelines in particular, halt all of them
        if not self.params ['pipelines'] and pipeApp in self.server.pipeData:
            self.params ['pipelines'] = self.server.pipeData [pipeApp].keys ()
            pass

        if pipeApp not in self.server.pipeData:
            self.params ['pipelines'] = []
            pass

        # Init the output message
        out_msg = ''
        
        Success = self.params ['pipelines'] != []
        # Loop through the pipelines halting them if possible
        command = 'self.server.pipeData [pipeApp][pipe].proxy.osf%s ()' % (
            action)
        while self.params ['pipelines'] != []:
            updated = False
            pipe = self.params ['pipelines'][0]
            if pipe in self.server.pipeData [pipeApp]:
                pipeStatus = self.server.pipeData [pipeApp][pipe].getStatus ()
                if pipeStatus == origStatus or action == 'Step':
                    # Call osfX on the pipeline
                    try:
                        result = eval (command)
                    except:
                        result = -9
                        Success = False
                        pass
                    if swapStatus:
                        self.server.pipeData [pipeApp][pipe].setStatus (
                            newStatus)
                        pass
                    out_msg += '%s:%d ' % (pipe, result)
                    updated = True
                elif pipeStatus == newStatus:
                    out_msg += '%s:-1 ' % (pipe)
                    updated = True
                else:
                    out_msg += '%s:-2 ' % (pipe)
                    updated = True
                    Success = False
                    pass
                if updated:
                    self.params ['pipelines'].remove (pipe)
                    pass
                pass
            else:
                # The pipeline is not running on this node...
                self.params ['pipelines'].remove (pipe)
                out_msg += '%s:-3 ' % (pipe)
                Success = False
                pass
            pass
        # Finish the output message
        if Success:
            out_msg = 'STATUS="0 Success"\nLIST="%s"\nEOF' % out_msg
        else:
            out_msg = 'STATUS="1 Failure"\nLIST="%s"\nEOF' % out_msg
            pass
        return out_msg
    
    def _shutdownPipe (self, pipeline, pipeApp):
        
        """
        Shutdown pipeline and remove the corresponding entry from pipeData.
        """
        
        # In case of error, we get an exit code != 0
        status = pipeadmin.execCmd ('stop', pipeline, pipeApp)
        
        # Remove pipe from pipeData
        if status == 0:
            if pipeApp in self.server.pipeData:
                if pipeline in self.server.pipeData [pipeApp]:
                    try:
                        del self.server.pipeData [pipeApp][pipeline]
                    except:
                        warning (
                            PREFIX, 'Cannot remove %s from pipeData.' % (
                            pipeline))
                        self.server.pipeData [pipeApp][pipeline].setStatus (-1)
                        pass
                    if not self.server.pipeData [pipeApp]:
                        self.server.ds.remove (pipeApp)
                        del self.server.pipeData [pipeApp]
                        pass
                    pass
                pass
            pass
        else:
            warning (PREFIX, 'Cannot stop the %s:%s pipeline.' % (
                pipeApp, pipeline))
            pass
        return status    
    pass

# utility routines
def extractPipeName (xmlFile):
    
    """
    Extract the name of the first pipeline from a given pipeline description
    XML file (xmlFile).
    """
    
    try:
        data = file (xmlFile).read ()
    except:
        warning (PREFIX, 'unable to read %s' % (xmlFile))
        return None
    
    # We simply look for the first occurrence of the <pipeline> tag. Then we
    # look for the first occurrence of the <name> tag. Quite simple minded, 
    # but should work.
    pattern = re.compile ('<Pipeline.+name="(.+?)"')
    match = pattern.search (data)
    if match and len (match.groups ()):
        return match.group (1)
    else:
        warning (PREFIX, 'RE match failed')
        return None
    return

def start (port=8000, pidFile=None, keep_alive=False, slow_pyro=False):
    
    """
    Starts up the Node Manager on the node.
    """

    global SLOW_PYRO
    
    try:
        port = int (port)
    except:
        port = int (8000)
        pass
    
    try:
        servernm = MGRServer (('', port), MGRRequestHandler)
    except:
        msg = 'failed starting the NodeManager. Is port %d already in use?'
        fatalError (PREFIX, msg % (port))
        pass

    if slow_pyro:
        SLOW_PYRO = True
        pass
    
    # Informational prints
    print ('%s NHPPS_PIPEAPPSRC: %s' % (PREFIX, servernm.pipeSrc))
    print ('%s NHPPS_POLL_SPEEDUP: %s' % (PREFIX, servernm.fast))
    print ('%s EXECUTIONER: %s' % (PREFIX, EXECUTIONER))
    
    if pidFile:
        try:
            pid = file (pidFile, 'w')
            pid.writelines ('%d' % os.getpid())
            pid.close ()
        except:
            print "Unable to write pid:", os.getpid()
            pass
        pass
    print ('%s started on %s:%d' % (PREFIX, NODE, port))
    
    #if USE_KEEP_ALIVE and keep_alive:
    #    from KeepAlive import KeepAlive
    #    print ('%s Keep Alive: ON' % (PREFIX))
    #    t = KeepAlive (target = servernm.runall)
    #    t.start ()
    #else:
    print ('%s Keep Alive: OFF' % (PREFIX))
    servernm.runall ()
    while not servernm.done:
        try:
            servernm.handle_request ()
        except:
            print ('%s terminated.' % (PREFIX))
            servernm.done = 1
            pass
        pass
    pass
