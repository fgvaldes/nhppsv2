# 
# Generic Server
# 
# Get application-specific functionality from the managers module.
# 

# Python Standard Library
import asyncore
import asynchat
import getopt
import socket
import sys

__version__ = "0.3"

# Constants
TERMINATOR = '\n'                      # The end-of-message string
NCONNECTIONS = 1000                    # The number of concurrent connections

PREFIX = 'GenericManager'

class TCPServer(asyncore.dispatcher):
    """
    AsyncWrapperServer class TCPServer implementation
    Inherits from asyncore.dispatcher
    
    Provide a wrapper to the high performance asynchronous socket server by 
    implementing the same interface as Python's SocketServer.TCPServer.
    
    At startup, bind to self.server_address and wait for connections. Once a 
    connection is established, create a self.RequestHandlerClass instance and
    pass the connection object (request) to it.
    """
    # Class variables (they set the socket options)
    address_family = socket.AF_INET
    socket_type = socket.SOCK_STREAM
    request_queue_size = NCONNECTIONS
    allow_reuse_address = True
    
    def __init__(self, server_address, RequestHandlerClass):
        """
        Standard constructor: setup instance variables and bind to the socket.
        """
        asyncore.dispatcher.__init__(self)
        
        self.server_address = server_address
        self.RequestHandlerClass = RequestHandlerClass
        
        self.handler = None
        
        self.create_socket(self.address_family, self.socket_type)
        self.server_bind()
        self.server_activate()
        return
    
    
    def server_bind(self):
        """
        Called by constructor to bind the socket.
        
        May be overridden.
        """
        if self.allow_reuse_address:
            self.set_reuse_addr()
        self.bind(self.server_address)
        return
    
    
    def server_activate(self):
        """
        Called by constructor to activate the server.
        
        May be overridden.
        """
        self.listen(self.request_queue_size)
        return
    
    
    def handle_accept(self):
        """
        Called on listening channels (passive openers) when a connection can 
        be established with a new remote endpoint that has issued a connect()
        call for the local endpoint (from Python's manual).
        
        This implementation delegates the connection handling to a newly 
        created self.RequestHandlerClass instance.
        """
        request, client_address = self.accept()
        self.handler = self.RequestHandlerClass(request, client_address, self)
        return

    def handle_request (self):
        """ Start handling requests from to the server """
        loop()
        return

    def serve_forever (self): loop() 

class StreamRequestHandler(asynchat.async_chat):
    """
    Performance.StreamRequestHandler class
    Inherits from asynchat.async_chat

    High performance stream request handler for AsyncWrapperServer.
    
    Handle one connection witha  remote client. Communication follows the
    NOAO Pipeline RPC protocol.
    """
    def __init__(self, request, client_address, server, terminator=TERMINATOR):
        """
        Standard constructor: setup instance variables.
        """
        asynchat.async_chat.__init__(self, request)
        
        self.request = request
        self.client_address = client_address
        self.server = server
        
        self.set_terminator(terminator)
        self.raw_requestlines = []
        return
    
    
    def collect_incoming_data(self, data):
        """
        Buffer the data coming from the socket
        """
        self.raw_requestlines.append(data)
        return
    
    
    def handle (self):
        """
        abstract method..should be overridden by child class. 
        """
        data = Null

        raise "handle method abstract", NotImplementedError

        return data


    def found_terminator(self):
        """
        Called when the "end-of-message" string (TERMINATOR) is found. It 
        replaces the handle() method in SocketServer.BaseRequestHandler class.
        """
        # run the handler
        self.handle()

        # Cleanup and close the connection.
        self.raw_requestlines = []
        self.close_when_done()

        return

    
    def handle_close(self):
        """
        Called when the channel is closed.
        """
        self.raw_requestlines = []
        asynchat.async_chat.handle_close(self)
        return
        

def loop(timeout=30.0, use_poll=0, map=None):

    asyncore.loop(timeout, use_poll, map)


def startServer(options):
    """
    Create a Server object using the information is the options hash table.
    Start the asyncore.loop once the Server object is up and running.
    """
    s = AsyncWrapperServer.TCPServer(server_address=('', options['port']), 
               RequestHandlerClass=AsyncWrapperServer.StreamRequestHandler) 
    
    try:
        loop()
    except KeyboardInterrupt:
        print('\n%s terminated.' % (PREFIX))
    return

