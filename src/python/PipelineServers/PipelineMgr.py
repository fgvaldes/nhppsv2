"""
Pipeline Manager

Pipeline Manager instances provide the glue between the various 
components of NHPPS (e.g Blackboard, Scheduler objects etc).
They also provide a means of communicating with the outside so that GUIs
and command line tools can control and/or monitor the pipeline.

Inherits from object, Pyro.core.ObjBase

Methods:
    Constructors/Destructors:
        __init__
        
    Internal Methods:
        run
        echo
        setDaemon
        shutDown
        
    Blackboard Communicaton:
        osfUpdate
        osfTest
        osfCleanup
"""

# NHPPS Imports
from pipeutil import *

from ModuleMgr import *
from Scheduler import *
from BlackBoard import *

from nhpps import *
from nhpps.dtd.dtd import *

import sys
import os
import cPickle

# Globals
PREFIX = 'PIPELINEMGR'                          # STDERR message prefix
USAGESTR = 'PipelineMgr --file=XML_file ' + \
           '[--ninst=N] [--log=logFile] [--fast=yes|no]'


class PipelineMgr (Pyro.core.ObjBase):
    def __init__ (self,
                  pipeline,
                  nInstances=1,
                  port=None,
                  ignore=None,
                  idString='',
                  log=None,
                  fast=False):
        """
        Constructor:
        Given a pipeline System object and a
        number of instances (n), it sets things up
        so that the modules are active and so on.
        """
        Pyro.core.ObjBase.__init__ (self)
        self.daemon = None
        
        self.pipeline   = pipeline
        self.nInstances = nInstances
        self.port       = port
        self.ignore     = ignore
        self.id         = idString
        self.log        = log
        self.fast       = fast
        self.pid        = '%05d' % (os.getpid ()) + '%03d'
        
        if not (self.pipeline and isinstance (self.pipeline, Pipeline)):
            fatalError (PREFIX, 
                        'invalid or null pipeline information in the ' +
                        'parsed XML data.')
            pass
        
        # Store the pipeline name
        self.pipe = self.pipeline.name
        
        # Setup the environment
	os.environ['NHPPS_SYS_NAME']  = self.pipeline.system
        os.environ['NHPPS_DIR_ROOT']  ='%s/'%(self.pipeline.Directories.root)
        os.environ['NHPPS_DIR_TRIG']  ='%s/'%(self.pipeline.Directories.trig)
        os.environ['NHPPS_DIR_INPUT'] ='%s/'%(self.pipeline.Directories.input)
        os.environ['NHPPS_DIR_OUTPUT'] ='%s/'%(self.pipeline.Directories.output)
        os.environ['NHPPS_DIR_DATA']  ='%s/'%(self.pipeline.Directories.data)
        os.environ['NHPPS_DIR_ERROR'] ='%s/'%(self.pipeline.Directories.error)
        os.environ['NHPPS_DIR_OBS']   ='%s/'%(self.pipeline.Directories.obs)
        os.environ['NHPPS_SUBPIPE_NAME'] = self.pipe#line.name
        os.environ['NHPPS_SUBPIPE_ID'] = self.id

	# Create directories if needed.  Ignore errors.  The error dirctory
	# is not used and the absence or presence of the obs directory 
	# can control whether a disk blackboard is kept.
	if not os.path.exists (self.pipeline.Directories.root):
	    try:
		os.mkdir (self.pipeline.Directories.root)
	    except:
	        pass
	if not os.path.exists (self.pipeline.Directories.trig):
	    try:
		os.mkdir (self.pipeline.Directories.trig)
	    except:
	        pass
	if not os.path.exists (self.pipeline.Directories.input):
	    try:
		os.mkdir (self.pipeline.Directories.input)
	    except:
	        pass
	if not os.path.exists (self.pipeline.Directories.output):
	    try:
		os.mkdir (self.pipeline.Directories.output)
	    except:
	        pass
	if not os.path.exists (self.pipeline.Directories.data):
	    try:
		os.mkdir (self.pipeline.Directories.data)
	    except:
	        pass
        
        # Setup the extended environment...
        pipe = self.pipeline.system.upper ()
	if 'MarioCal' in os.environ:
            os.environ ['MarioCal'] = os.path.join (
                os.environ ['MarioCal'], pipe)
            pass
        
        if '%s_DM_NODE' % (pipe) in os.environ:
            os.environ ['NHPPS_NODE_DM'] = os.environ [
                '%s_DM_NODE' % (pipe)]
            pass
        
        if '%s_DM_PORT' % (pipe) in os.environ:
            os.environ ['NHPPS_PORT_DM'] = os.environ [
                '%s_DM_PORT' % (pipe)]
            pass
                
        self.log.flush ()
        
        # Clean out 'foreign' Pipeline Applications from the PATH
        self.cleanPath ()
        
        # What is our polling time (i.e. sleep time)?
        self.poll = self.pipeline.poll
        
        # Create the BlackBoard for the current pipeline.
        self.bb = BlackBoard (self.pipeline)
        
        # For each module, create a Module Manager. The Module Manager
        # makes sure that the corresponding module gets activated at the 
        # right time (or when the appropriate conditions are met).
        self.moduleManagers = []

        subpid = 0
        for modIndex in xrange (len (self.pipeline.Modules)):
            module  = self.pipeline.Modules [modIndex]
            numInst = self.nInstances
            if (module.maxActive and module.maxActive < numInst):
                numInst = module.maxActive
                pass
            
            for i in xrange (numInst):
                self.moduleManagers.append (ModuleMgr (
                    pipeline = self.pipeline,
                      parent = self,
                    modIndex = modIndex,
                         pid = self.pid % (subpid),
                      instID = i))
                subpid += 1
                pass
            pass
        
        # Create a scheduler for the Module Managers.
        self.scheduler  = Scheduler (
                threads = self.moduleManagers, 
            pollingTime = self.poll,
                 daemon = self.daemon,
               pipeName = self.pipeline.name,
                    log = self.log,
                   fast = fast)
        return
    
    def cleanPath (self):
        
        """
        Removes directories in the $PATH which
        belong to another pipeline application.
        """
        
        pathdirs = os.environ ['PATH'].split (os.path.pathsep)
        pipesrc  = os.environ ['NHPPS_PIPESRC']
        shrdbin  = os.path.join (pipesrc, 'BIN')
        pipeApp  = os.path.join (pipesrc, self.pipeline.system.upper ())
        newpath  = ""
        
        for item in pathdirs:
            if pipesrc in item and not (shrdbin in item or pipeApp in item):
                # Do not include in the new path...
                continue
            else:
                # include the item...
                newpath += '%s%s' % (item, os.path.pathsep)
                pass
            pass
        os.environ ['PATH'] = newpath [:-1]
        return
    
    def setInstances (self, num):
        
        """
        Sets the nInstances value to num IF
        num >= self.nInstances... i.e. (add only)
        """
        
        if num > self.nInstances:
            newInstances = num - self.nInstances
            newModuleManagers = []
            for modIndex in xrange (len (self.pipeline.Modules)):
                module  = self.pipeline.Modules [modIndex]
                addInst = newInstances
                if (module.maxActive > 0):
                    if module.maxActive <= num:
                        addInst = module.maxActive - self.nInstances
                        if (addInst < 0):
                            addInst = 0
                            pass
                        pass
                    pass
                if (addInst > 0):
                    for i in xrange (addInst):
                        newModuleManagers.append (ModuleMgr (
                            pipeline=self.pipeline, parent=self, modIndex=modIndex))
                        pass
                    pass
                pass
            self.moduleManagers += newModuleManagers
            self.scheduler.addInstances (newModuleManagers)
            self.nInstances = num
            pass
        
        return
    
    def getModuleManagers(self):
        return self.moduleManagers
    
    def moduleInfo(self):
        modules = []
        
        for m in self.getModuleManagers():
            namedescr = (m.getModuleName(), m.getModuleDescription())
            if namedescr not in modules:
                modules.append(namedescr)
                pass
            pass
        return modules
    
    def run (self):
        """
        Activates the whole machinery created in the Constructor.
        """
        # Activate the scheduler.
        self.scheduler.run ()
        return
    
    def echo (self, msg):
        return ('%s: %s' % (PREFIX, msg))
    
    def setDaemon (self, d):
        self.daemon = d
        self.scheduler.daemon = d
        return
    
    def setUri (self, uri):
        os.environ ['NHPPS_PIPE_URI'] = uri
        return
    
    def shutDown (self):
        # stop the Scheduler
        if self.log:
            msg = '%s %s: stopping the Scheduler...'
            self.log.write (msg % (PREFIX, self.pipe))
            pass
        self.scheduler.join (timeout=0)
        if self.log:
            msg = '%s done.\n%s %s: cleaning up the blackboards...\n'
            self.log.write (msg % (PREFIX, PREFIX, self.pipe))
            pass
        self.bb.osfCleanUp ()
        self.bb.pstatCleanUp ()
        if self.log:
            self.log.write ('%s done.\n' % (PREFIX))
            pass
        
        for m in self.moduleManagers:
            m.shutDown ()
            pass
        
        if self.log:
            self.log.write ('%s %s: stoppped.\n' % (PREFIX, self.pipe))
            pass
        
        return
    
    # Pass-through methods
    def getStatus (self):
        return self.bb.getStatus ()
    
    def setStatus (self, val):
        #self.scheduler.status = val
        #return
        return self.bb.setStatus (val)
    
    def echo (self, text='Hello Tim'):
        return text
    
    def osfCount (self):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return self.bb.osfCount ()
    
    def osfUpdate (self, dataset, module, stage, flag):
        """
        Simply invoke the corresponding BlackBoard method.
        
        This is just a pass-through method and its reson d'etre is simply
        to provide an interface to the BlackBoard object self.bb.
        """
        return (self.bb.osfUpdate (dataset, module, stage, flag))
    
    def osfWait (self, dataset, module, flag):
        """
        Simply invoke the corresponding BlackBoard method.
        
        This is just a pass-through method and its reson d'etre is simply
        to provide an interface to the BlackBoard object self.bb.
        """
        return (self.bb.osfWait (dataset, module, flag))
    
    def osfBB (self,
               dataset,
               module,
               stage,
               flag,
               openDatasetsOnly=False, 
               closedDatasetsOnly=False,
	       format='OSFFMT1'):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return (self.bb.osfSelect (dataset=dataset,
                               module=module,
                               stage=stage,
                               flag=flag,
                               openDatasetsOnly=openDatasetsOnly,
                               closedDatasetsOnly=closedDatasetsOnly,
			       format=format))
    
    def osfTest (self,
                 dataset,
                 module,
                 stage,
                 flag,
                 openDatasetsOnly=False, 
                 closedDatasetsOnly=False):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return (self.bb.osfSelect (dataset=dataset,
                                   module=module,
                                   stage=stage,
                                   flag=flag,
                                   openDatasetsOnly=openDatasetsOnly,
                                   closedDatasetsOnly=closedDatasetsOnly,
				   format='OSFFMT'))
    
    def osfCleanup (self, dataset=None):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return (self.bb.osfCleanUp (dataset))
    
    def osfClose (self, dataset):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return (self.bb.osfClose (dataset))
    
    def osfConditRemove (self, dataset):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return (self.bb.osfConditRemove (dataset))
    
    def osfFinal (self, dataset):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return (self.bb.osfFinalClose (dataset))
    
    def osfOpen (self, dataset):
        """
        Just invoke the corresponding BlackBoard method.
        """
        return (self.bb.osfOpen (dataset))
    
    def osfHalt (self):
        """
        Cause the pipeline to stop processing after the current stage
        """
        return self.scheduler.halt ()
    
    def osfResume (self):
        """
        Resume the pipeline after a halt
        """
        return self.scheduler.resume ()
    
    def osfStep (self):
        """
        Resume the pipeline after a halt
        """
        return self.scheduler.step ()
    
    def handleAdminRequest (self, command, argv=[]):
        """
        Handle a request from a control GUI or script.
        
        This is the entry point for control requests: depending on the 
        particular command (and on its argument array argv) a different
        method will be invoked.
        """
        # TODO: Add more methods!
        command = command.lower ()
        out = None
        if (command == 'info'):
            # Return a hash table of the form {methodName: argv, info}
            out = {'info': ([], 'Introspection API implementation.'),
                   'stop': ([], 'Shut down the Pipeline Manager.')}
        elif (command == 'stop'):
            out = ''
            self.shutDown ()
        else:
            # Return a hash table of the form {methodName: argv, info}
            out = {'info': ([], 'Introspection API implementation.'),
                   'stop': ([], 'Shut down the Pipeline Manager.')}
            pass
        return (out)
    
    def setID (self, idString):
        """
        Setter method for self.id
        """
        self.id = idString
        os.environ['NHPPS_SUBPIPE_ID'] = self.id
        return
    pass

    
def main (xmlFile, numInstances, logFile, fast):
    global PREFIX

    path = os.path.dirname (xmlFile)
    pipe = os.path.basename (xmlFile).split ('.')[0]

    pipeline = loadNHPPSxml (pipe, path=path)
    if not pipeline:
        fatalError (PREFIX, 'Unable to load the pipeline XML file')
        pass

    # Output the compiled XML to the standard output and the exit info to a pickle file.
    print pipeline

    # Pickle the exit codes for quick access.
    if 'NHPPS_LOGS' in os.environ and 'NHPPS_SYS_NAME' in os.environ:
        path = os.path.join (os.environ['NHPPS_LOGS'],
	    os.environ['NHPPS_SYS_NAME'], pipeline.name+'.exit')
	try:
	    f = open (path, 'w')
	except:
	    pass
	else:
	    cPickle.dump (pipeline.ExitCodes, f)
	    f.close()
    
    # Choose a unique ID for the PipelineMgr instance (pipeApp.pipe!host!user)
    nm = os.environ ['NHPPS_PORT_NM']
    name = '%s^%s^%s^%s^%s^%s' % (NSGROUP,
                               pipeline.system,
                               pipeline.name,
                               NODE,
                               USER,
			       nm)
    id = '%s!%f' % (name, time.time ())
    
    # Update PREFIX
    PREFIX += ' (%s)' % (name)
    
    # Handle the log file opening and basic setup
    log = None
    if logFile:
        try:
            log = file (logFile, 'w', 0)
        except:
            warning (PREFIX, 'unable to open log file (%s).' % (logFile))
            pass
        pass
    
    # Create a PipelineMgr object
    pipeMgr = PipelineMgr (pipeline=pipeline,
                           nInstances=numInstances,
                           idString=id,
                           log=log,
                           fast=fast)
    
    # Register with the Name Server and activate the daemon

    Pyro.core.initServer (0)
    try:
        ns = findNameServer (timeout=2.)
    except:
        pipeMgr.shutDown ()
        fatalError (PREFIX,
                    'the Name Server cannot be either started or discovered.')
        pass

    # Create the NSGROUP group
    try:
        ns.createGroup (NSGROUP)
    except:
        pass
    
    daemon = Pyro.core.Daemon ()
    daemon.useNameServer (ns)
    
    try:
        uri = daemon.connectPersistent (pipeMgr, name)
        if log:
            log.write ('%s: registered Pyro daemon as %s\n' % (PREFIX, name))
            pass
        pipeMgr.setDaemon (daemon)
        pipeMgr.setUri (str (uri))
    except:
        pipeMgr.shutDown ()
        fatalError (PREFIX, 'Unable to register Pyro daemon as %s' % (name))
        pass
    
    # Activate the whole machinery
    try:
        pipeMgr.run ()
    except KeyboardInterrupt:
        if log:
            log.write ('Shutting down...\n')
            pass
        pipeMgr.shutDown ()
        daemon.disconnect (pipeMgr)
        if log:
            log.write ('Done.\n')
            log.close ()
            pass
        pass
    # Exit
    return (0)


def checkConfiguration ():
    """
    Check the user installation setup for macroscopic problems (mainly
    related to the Name Server). Abort with a fatal error message if any 
    major issue arises.
    """
    # Make sure that the Name Server executable is there.
    ns = which (NS_EXE)
    if not ns:
        fatalError (PREFIX, 'Pyro installation seems to be broken: ' +
                    'no %s script can be found.' % (NS_EXE))
        pass
    # TODO: Add some more sanity checks.
    return
