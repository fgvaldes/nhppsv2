from server import *
from submit import *
import sys, os
import time, datetime

from psautil import getGMDT, INGEST_TIME
if 'NHPPS_PSQDB' in os.environ:
    from psqdb_sqlite import psqdb_query, psqdb_update
else:
    from psqdb_mysql import psqdb_query, psqdb_update

# Global variables
PREFIX    = 'PipeScheduler'      # Prefix for stderr messages
if 'MarioData' in os.environ:
    PSQ_INPUT = os.path.join (os.environ ['MarioData'], '%s', '%s_%s', 'input')
else:
    PSQ_INPUT = os.path.join (os.environ ['NHPPS_DATAAPP'], '%s_%s')
EXECUTE   = True
VERBOSE   = True

class SchedulerServer (PipeSocketServer):
    
    def __init__ (self, server_address, RequestHandlerClass, autoSubmit):
        
        PipeSocketServer.__init__ (self, server_address, RequestHandlerClass)
        self.realTimeQueue   = {}
        self.wakeHandlerDate = None
        self.autoSubmit      = autoSubmit
        self.wakeTimer       = flexibleTimer ()
        self.autoSubmit      = autoSubmit
        self.wakeTimer.setAction (self.wakeHandler)
        if self.autoSubmit:
            self.wakeTimer.set (15) # Call submit after the server is started.
            pass
        return
    
    def setNextEndDate (self):
        
        """
        Determines the next applicable end date and sets a timer to wake
        up on that date at a pre-defined time in order to insert the
        dataset(s) which should be archived that day into the rtQueue.
        """
        
        queueData = "SELECT psqname, data FROM PSQ WHERE state='enabled'"
        endDate   = "SELECT end FROM %s, %s WHERE dataset=name AND "
        endDate  += "status IN ('pending', 'resubmit') AND "
        endDate  += "end >'%s' ORDER BY end LIMIT 1"
        
        enabledQueues = psqdb_query (queueData)
        wakeDate = None
        hours, minutes = INGEST_TIME.split (':', 1)
        hours    = int (hours)
        minutes  = int (minutes)
        currDate = datetime.datetime.utcnow ()
        utcDate  = datetime.datetime (
            currDate.year, currDate.month, currDate.day)
        utcDate -= datetime.timedelta (hours=hours, minutes=minutes)
        utcDate  = utcDate.strftime ('%Y%m%d')
        
        if VERBOSE:
            print 'Looking for datasets with end dates greater than', utcDate
            pass
        
        for (psqname, data) in enabledQueues:
            if VERBOSE:
                print 'Checking queue', psqname, data
                pass
            end = psqdb_query (endDate % (psqname, data, utcDate))
            if end:
                if VERBOSE:
                    print '\tNext end date:', end[0]
                    pass
                if not wakeDate or wakeDate > end [0]:
                    if VERBOSE:
                        print '\t\tselected as new next end date...'
                        pass
                    wakeDate = end [0]
                    pass
                pass
            else:
                if VERBOSE:
                    print '\tNo future end dates found'
                    pass
                pass
            pass
        
        if wakeDate:
            wakeDate = str (wakeDate)
            self.wakeHandlerDate = wakeDate
            # We have found a UTC date on which we need to wake the system.
            # Set the timer...
            futureDate = datetime.datetime (
                int (wakeDate [0:4]), #Year
                int (wakeDate [4:6]), #Month
                int (wakeDate [6:]))  #Day
            
            futureDate += datetime.timedelta (hours=hours, minutes=minutes)
            currDate = datetime.datetime.utcnow ()
            if VERBOSE:
                print 'Scheduled to wake up and submit real-time datasets',
                print 'with end date of', wakeDate, 'on',
                print futureDate.strftime ('%Y%m%d %H:%M'), 'UTC'
                print 'Current time:', currDate.strftime('%Y%m%d %H:%M'), 'UTC'
                pass
            delta      = futureDate - currDate
            seconds    = (delta.days * (3600*24)) + delta.seconds
            self.wakeTimer.set (seconds)
            pass
        else:
            if VERBOSE:
                print 'No datasets found with end dates in the future...'
                pass
            # There are no (active) datasets queued past todays date
            self.wakeTimer.cancel ()
            pass
        return
    
    def wakeHandler (self):
        
        """
        Acts as the handler for the onWake timer.
        
        Queues the PSQ database for the dataset(s) which have
        listed today as their end date.
        These dataset(s) are inserted into the realTimeQueue, which is
        given the highest level of priority over other datasets
        in terms of which are submitted to their pipelines.
        When completed, a call to submit is made in order to ensure
        that if a pipeline is inactive and there are now pending
        datasets, they are feed to the pipeline.
        """
        
        if self.wakeHandlerDate:
            queueData  = "SELECT psqname,data,application,pipeline FROM PSQ "
            queueData += "WHERE state='enabled'"
            info  = "SELECT dataset, priority, status FROM %s, %s "
            info += "WHERE end='%s' AND dataset=name AND "
            info += "status IN ('pending', 'resubmit', 'queued') ORDER BY priority"
            
            enabledQueues = psqdb_query (queueData)
            pipeEntries = {}
            for (psqname, data, application, pipeline) in enabledQueues:
                app_pipe = '%s:%s' % (application, pipeline)
                if app_pipe not in pipeEntries:
                    pipeEntries [app_pipe] = {}
                    pass
		print 'wakeHandler: ' + info % (psqname, data, self.wakeHandlerDate)
                datasets = psqdb_query (info % (psqname, data, self.wakeHandlerDate))
                for (dataset, priority, status) in datasets:
                    if priority not in pipeEntries [app_pipe]:
                        pipeEntries [app_pipe][priority] = [
                            (psqname, data, dataset, status)]
                    else:
                        pipeEntries [app_pipe][priority].append (
                            (psqname, data, dataset, status))
                        pass
                    psqdb_update ("UPDATE %s SET status='queued' WHERE dataset='%s'" % (psqname, dataset))
                    pass
                pass
            for app_pipe in pipeEntries:
                if app_pipe not in self.realTimeQueue:
                    self.realTimeQueue [app_pipe] = []
                    pass
                priorities = pipeEntries [app_pipe].keys ()
                priorities.sort ()
                for priority in priorities:
                    for (psqname, data, dataset, status) in pipeEntries [app_pipe][priority]:
                        self.realTimeQueue [app_pipe].append (
                            (psqname, data, dataset, status))
                        pass
                    pass
                pass
            pass
        
        self.submit ()
        return
    
    def submit (self, queue='enabled', ds=None, submit=1, state='',
                limit=None, order=None, result=None, verbose=VERBOSE, execute=EXECUTE):
        
        """
        Submit a dataset from the queue...
        """
        
        self._submit (queue, ds, submit, state, limit, order, result, verbose, execute)
        if self.autoSubmit:
	    self.setNextEndDate ()
        sys.stdout.flush ()
        
        return
    
    def _submit (self, queue, ds, submit, state, limit, order, result, verbose, execute):
        
        # Log task (use UTC rather than local time)
        print 'PSA SUBMIT:', datetime.datetime.utcnow ().strftime (
            '%a %H:%M:%S %d-%b-%Y')
        
        # Find queues and pipelines.
        queues = getQUEUES (queue, state)
        if verbose:
            print 'Selected Queue(s), Application(s), Pipeline(s):'
            for (psqname, queue, data, application, pipeline, query) in queues:
                print '\t%s, %s, %s' % (queue, application, pipeline)
                pass
            pass
        
        # Store the selected queues according to
        # the pipeline that they trigger...
        pipeData = {}
        while queues != []:
            psqname, queue, data, application, pipeline, query = queues.pop ()
            
            if application not in pipeData:
                pipeData [application] = {}
                pass
            
            if pipeline not in pipeData [application]:
                pipeData [application] [pipeline] = pipeInfo (
                    pipeline, application, execute)
                pass
            
	    # Hard-code one pipeline instance if we are
	    # just writing the enids out to a file.
	    if result:
		pipeData [application] [pipeline].nPipes = 1
		pass
	    
            # Pass if no pipeline instances
            if pipeData [application] [pipeline].nPipes == 0:
                continue
            
            pipeData [application] [pipeline].addQueue (
                psqname, queue, data, query, ds, execute=execute)
            pass
        
        # At this point, we now know which queues are enabled, have pending
        # datasets and which of those datasets go to which pipeline. Also, we
        # know how many datasets are already 'submitted' to each pipeline and
        # the number of available pipeline instances to handle new datasets.
        
        pipeApps = pipeData.keys ()
        for pipeapp in pipeApps:
            
            DATE_PRIORITY = NEWEST
            
            if limit == None:
                limitAPP = getPSQ_LIMIT (pipeapp)
            else:
                limitAPP = limit
                pass
            if order == None:
                orderAPP = getPSQ_ORDER (pipeapp)
            else:
                orderAPP = order
                pass
            if orderAPP:
                if 'desc' in orderAPP:
                    DATE_PRIORITY = NEWEST
                else:
                    DATE_PRIORITY = OLDEST
                    pass
                pass
            redoAPP = getPSQ_REDO (pipeapp)
            
            # Work in the input directory of the target pipeline application.
	    psqdir = PSQ_INPUT % (pipeapp, 'PSQ')
	    if not os.path.exists (psqdir):
	        os.makedirs (psqdir)
            os.chdir (psqdir)
            
            # Make if easier to address the data tree...
            pipeAppData = pipeData.pop (pipeapp)
            
            pipelines = pipeAppData.keys ()
            for pipeline in pipelines:
                
                # Make it easier to address the data tree...
                pipe = pipeAppData.pop (pipeline)
                
                if pipe.nPipes == 0:
                    continue
                
                # Calculate the maximum number of datasets to submit this time 'round.
                pipe.computeMaxSubmit (submit)
                
                print 'Processing pipeline: %s:%s' % (pipeapp, pipeline)
                print '\tCurrently submitted %d/%d' % (pipe.nSubmitted, pipe.nPipes)
                
                # Set pipeline directories.
                if execute and (not result or result == 'stage'):
                    pipeDirs = pipeselect (pipeapp, pipeline, pipe.nSubmit)
		    pipeDirs = pipeDirs.pop(0).split()
		    trigDir = pipeDirs[0]
		    inDir = pipeDirs[2]
                    psqDirs = pipeselect (pipeapp, "psq", pipe.nSubmit)
		    psqDirs = psqDirs.pop(0).split()
		    psqTrigDir = psqDirs[0]
		    psqInDir = psqDirs[2]
                    pass
                
                # Filter through the various queues to determine which datasets
                # will be submitted this time 'round.
                app_pipe = '%s:%s' % (pipeapp, pipeline)
                if app_pipe in self.realTimeQueue:
                    pipe.selectDatasets (
                        ds, DATE_PRIORITY, self.realTimeQueue [app_pipe], verbose=verbose)
                else:
                    pipe.selectDatasets (
                        ds, DATE_PRIORITY, verbose=verbose)
                    pass
                
                if verbose:
                    print '\tSubmitting %d entries:' % (len (pipe.entries))
                    if showpending:
                        for (psqname, queue, data, dsname, query, status) in pipe.entries:
                            print '\t\t%s %s' % (queue, dsname)
                            pass
                        pass
                    pass
                
                queueSettings = {}
                # Go through the list of datasets 'entries' to submit...
                for (psqname,queue,data,dsname,query,status) in pipe.entries:
                    
                    # Read queue and qname configuration file for psq_redo and
                    # psq_limit. This was not read when the CL started up because
                    # this pipeline may not have a dataset associated with it.
                    redoNOW, limitNOW = readCL (
                        redoAPP, limitAPP, queue, queueSettings, dsname)
                    
                    ## If the dataset status is 'resubmit' then redoNOW = 'yes'
                    ## regardless of any other configuration settings...
                    #if status == 'resubmit':
                    #    redoNOW = 'yes'
                    #    pass
                    
		    if result and result == 'stage':
			dataset = '%s_%s_%s' % (psqname, dsname, 'stage')
		    else:
			timestamp = int (time.time ())
			timestamp = hex (timestamp / 10) [2:]
			dataset = '%s_%s_%s' % (psqname, dsname, timestamp)
                    if verbose:
                        print '\tQueue %s, dataset %s:\n\t\t%s'%(queue, dsname, query)
                        pass
                    
                    # Query the IQS for ENIDs.
                    enids = getENIDS (query, queue, dsname, limitNOW)
                    
                    # Check for data.
                    nfiles = len (enids)
                    if nfiles == 0:
                        print '\t\tWARNING: dataset %s has no files' % (dsname)
                        if execute:
                            psqdb_update (UPDATE_NODATA % (psqname, dsname))
                            pass
                        continue
                    
                    # Set data list.
                    datalist = '%s.%s' % (dataset, pipeline)
                    file (datalist, 'w').writelines (enids)
                    
                    # Eliminate processed enids if desired.
                    if redoNOW != 'yes':
                        
                        if verbose:
                            print '\t\tChecking for previous processing.'
                            pass
                        psqredo (psqname, dsname, datalist, redoNOW)
                        
                        enids = file (datalist, 'r').readlines ()
                        enids = [enid.strip () for enid in enids if enid.strip ()]
                        idx = nfiles
                        nfiles = len (enids)
                        idx -= nfiles
                        if verbose and idx > 0:
                            if nfiles == 0:
                                print '\t\t\tAll files previously processed.'
                            else:
                                print '\t\t\tExcluded %d previously processed files.' % (idx)
                                pass
                            pass
                        pass
                    
                    if nfiles == 0:
                        continue
                    
                    if verbose:
                        print '\t\tSubmitting dataset %s (%d files)' % (dataset,nfiles)
                        if listdata:
                            for enid in enids:
                                print '\t\t\t%s' % (enid.strip ())
                                pass
                            pass
                        pass
                    
		    if result and result == 'stage':
                        node, tPath = trigDir.strip ().split ('!', 1)
                        node, iPath = inDir.strip ().split ('!', 1)
                        
                        os.system (MK_ENIDS   % (datalist, node, iPath))
                        os.system (MK_TRIGGER % (node, tPath, datalist))
                        
                        # Update the queue...
                        psqdb_update (UPDATE_STAGE % (psqname, dsname))

			# Setup return.
                        node, ptPath = psqTrigDir.strip ().split ('!', 1)
                        node, piPath = psqInDir.strip ().split ('!', 1)
			os.system (MK_RETURN  % (node, ptPath, piPath, iPath, datalist))
                        pass

		    elif result:
			# Write the results file to the specified host:path
			node, rPath = result.strip ().split ('!', 1)
			os.system (MK_ENIDS % (datalist, node, rPath))
			pass

                    elif execute:
                        node, tPath = trigDir.strip ().split ('!', 1)
                        node, iPath = inDir.strip ().split ('!', 1)
                        
                        os.system (MK_ENIDS   % (datalist, node, iPath))
                        os.system (MK_TRIGGER % (node, tPath, datalist))
                        
                        # Update the queue...
                        psqdb_update (UPDATE_SUBMIT % (psqname, getGMDT (), dsname))

			# Setup return.
                        node, ptPath = psqTrigDir.strip ().split ('!', 1)
                        node, piPath = psqInDir.strip ().split ('!', 1)
			os.system (MK_RETURN  % (node, ptPath, piPath, iPath, datalist))
                        pass
                    
                    # Remove the local copy of the ENID list
                    os.remove (datalist)
                    pass
                pass
            pass
        return
    pass

class SchedulerRequestHandler (PipeRequestHandler):
    
    """
    Scheduler request handler base class.
    """
    
    from complete import _complete
    
    def mgr_finish (self):
        
        """
        Updates the status of a dataset in queue in the PSQ database
        after the pipeline has competed processing the dataset.
        Optionally (with autoSubmit = True) queues a new dataset into
        the pipeline system.
        
        The input parameters are:
        
        dataset
        pipeline
        
        Returns:
        STATUS   = 'exit_code[message]'
        EOF
        
        Exit codes:
        0          Success
        1          Error
        """
        
        if not self.hasParam ('dataset', True):
            return
        if not self.hasParam ('pipeline', True):
            return
        if not self.hasParam ('pipeapp', True):
            return
        
        status = self._complete (VERBOSE, EXECUTE)
        
        if self.server.autoSubmit:
            self.params ['queue']   = 'enabled'
            self.params ['dataset'] = None
            self.params ['submit']  = '0'
            self.params ['state']   = ''
            #if 'submit' not in self.params or self.params ['submit'] == '0':
            #    self.params ['submit'] = '1'
            #    pass
            status = self._submit ()
            pass
        
        # compose the packet        
        pkt = composePacket ({'status': status})
        self.wfile.write (pkt)
        return
    
    def mgr_submit (self):
        
        """
        Queues a dataset to the pipeline system if all required
        conditions are meet. These conditions include any
        constraints like not submitting if within X hours of
        a dataset submission.
        
        The input parameters are:
        
        None
        
        Returns:
        STATUS   = 'exit_code[message]'
        LIST     = '<host name>:<port> [...]'
        EOF
        
        In case of error, LIST is empty.
        
        Exit codes:
        0          success.
        2          failure: no data for given user.
        1          failure: no data at all.
        """
        
        list = ''
        status = '1 No data available.'
        
        list = self._submit ()
        
        # compose the packet
        pkt = composePacket ({'status': status, 'list': list})
        self.wfile.write (pkt)
        return
    
    def _submit (self):
        
        queue  = 'enabled'
        ds     = None #'pending'
        submit = 0
        state  = ''
        limit  = None
        order  = None
	result = None
        
        if 'queue'     in self.params:
            queue      = self.params ['queue']
            pass
        if 'dataset'   in self.params:
            ds         = self.params ['dataset']
            pass
        if 'submit'    in self.params:
            submit     = int (self.params ['submit'])
            pass
        if 'limit'     in self.params:
            limit      = int (self.params ['limit'])
            pass
        if 'order'     in self.params:
            order      = self.params ['order']
            pass
        if 'state'     in self.params:
            state      = self.params ['state']
            pass
	if 'result'    in self.params:
	    result     = self.params ['result']
	    pass
        
        return self.server.submit (queue, ds, submit, state, limit, order, result)
    pass

def start (port=8000, pidFile=None, autoSubmit=True):
    
    """
    Starts up the Node Manager on the node.
    """
    try:
        port = int (port)
    except:
        port = int (8000)
        pass
    
    try:
        serverps = SchedulerServer (
            ('', port),SchedulerRequestHandler,autoSubmit)
    except:
        msg  = 'failed starting the Pipeline Scheduler. '
        msg += 'Is port %d already in use?'
        fatalError (PREFIX, msg % (port))
        pass
    
    if pidFile:
        import os
        try:
            pid = file (pidFile, 'w')
            pid.writelines ('%d' % os.getpid())
            pid.close ()
        except:
            print "Unable to write pid:", os.getpid()
            pass
        pass
    
    print ('%s started on %s:%d' % (PREFIX, NODE, port))
    
    while not serverps.done:
        try:
            serverps.handle_request ()
        except:
            print ('%s terminated.' % (PREFIX))
            serverps.done = 1
            pass
        pass
    pass
