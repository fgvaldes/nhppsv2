#IQS = "NSA"
IQS = "PLANB"
#IQS = DCI_DB

import os
import datetime
import string
import re
from pipeselect import main as plSelect

if 'NHPPS_PSQDB' in os.environ:
    from psqdb_sqlite import psqdb_query
else:
    from psqdb_mysql import psqdb_query

try:
    iqs = os.environ['IQS']
except:
    iqs = IQS

if (iqs == "NSA"):
    import httplib
    import urllib
    import socket
    
    def nsa_query (sql, host='vo-test.tuc.noao.edu', port=7004, timeout=None):
        
        socket.setdefaulttimeout (timeout)
        
        try:
            httpConn = httplib.HTTPConnection (host, port)
            params = urllib.urlencode ({'method': 'execute', 'sqlQuery': sql})
            httpConn.request ("POST", "/", params)
            httpResponse = httpConn.getresponse()
            data = httpResponse.read ()
        except httplib.HTTPException, e:
            raise e
        except socket.timeout, e:
            raise e
        except socket.error, e:
            raise e
        else:
            return data.splitlines ()
    pass
elif (iqs in ["PLANB", "DECam"]):
    import pgdb
    pass

# Offset in hours and minutes from the beginning of UTC date X, where X
# is the end date of a dataset before the PSA will wake up to submit that
# dataset (we need the data in the archive and the DCI updated before
# submission).

try:
    INGEST_TIME = os.environ['INGEST_TIME']
except:
    INGEST_TIME = '40:00'

# Define the SQL queries which are used...
# For now we translate the PSQ entries rather than changing the
# database.

SQL_ENIDS  = None

if (iqs in ["PLANB", "NSA"]):
    SQL_ENIDS  = "SELECT reference FROM voi.siap "
    SQL_ENIDS += "WHERE proctype='Raw' AND %s "
    SQL_ENIDS += "AND reference not like 'static_%%' "
    SQL_ENIDS += "AND obstype not in ('focus','test') "
    SQL_ENIDS += "AND dtacqnam not like '%%test.fits%%' "
    SQL_ENIDS += "AND object not like '%%test%%' "
    SQL_ENIDS += "AND object not like '%%pointing%%' "
    SQL_ENIDS += "AND filter not like '%%tres%%' "
    SQL_ENIDS += 'ORDER BY SUBSTR( CAST(start_date AS TEXT), 1, 10)%s'
elif (iqs in ["DECam"]):
    SQL_ENIDS  = "SELECT '/net/archive/mtn/'||to_char(start_date,'YYYYMMDD')||'/ct4m/'||prop_id||'/'||reference FROM voi.siap "
    SQL_ENIDS += "WHERE proctype='Raw' AND %s "
    SQL_ENIDS += "AND filter SIMILAR TO '[giruzY] %%'"
    SQL_ENIDS += "AND reference not like 'static_%%' "
    SQL_ENIDS += "AND obstype not in ('focus','test') "
    SQL_ENIDS += "AND dtacqnam not like '%%test.fits%%' "
    SQL_ENIDS += "AND object not like '%%test%%' "
    SQL_ENIDS += "AND object not like '%%pointing%%' "
    SQL_ENIDS += 'ORDER BY SUBSTR( CAST(start_date AS TEXT), 1, 10)%s'
else:
    SQL_ENIDS  = "SELECT pathname FROM dtkpct WHERE %s AND pathname LIKE "
    SQL_ENIDS += "'%%z' ORDER BY SUBSTR(pathname,1,8)%s"
    pass

# Define the functions which connect to the database(s) and
# execute SQL queries on those database(s).

def get_enids (sql, limit):
    
    """
    """

    if limit:
	LIMIT = ' LIMIT %d' % limit
    else:
        LIMIT = ''

    if (iqs == "NSA"):
        try:
	    data = nsa_query (SQL_ENIDS % (sql, LIMIT))
        except Exception, e:
            print "ERROR:", e
            data = []
        return data
    else:
        # Connect to the database...
        db = None
        if (iqs in ["PLANB", "DECam"]):
            try:
                database = os.environ['PGDATABASE']
            except:
                database = 'metadata'
            
            try:
                node = os.environ['PGHOST']
            except:
		node = 'archdbn2.tuc.noao.edu'
	    
            try:
                port = os.environ['PGPORT']
            except:
	        port = '5432'
            
	    host = '%s:%s' % (node,port)
            
            try:
                user = os.environ['PGUSER']
            except:
                user = 'pipeline'
            
            try:
                password = os.environ['PGPASSWORD']
            except:
                password = 'Mosaic_DHS'
            
            db = pgdb.connect (
                database = database,
                host     = host,
                user     = user,
                password = password)

	    # Convert sql from viewspace to voi.
	    sql = re.compile('dtinstru').sub('instrument', sql)
	    sql = re.compile('dtcaldat').sub('start_date', sql)
            pass
        else:
            db = MySQLdb.connect (
                db     = 'dci',
                #host   = 'stb.tuc.noao.edu',
                host   = 'dpopsn.tuc.noao.edu',
                user   = 'dcidata',
                passwd = 'Mosaic_DHS')

	    # Convert psq sql to dci sql.
	    sql = re.compile("proctype='Raw' and ").sub('', sql)
	    sql = re.compile('dtinstru').sub('instrument', sql)
	    sql = re.compile('dtcaldat').sub('date', sql)
	    sql = re.compile('obstype').sub('imagety', sql)
	    sql = re.compile('[-%]').sub('', sql)
            pass
        
	print SQL_ENIDS % (sql, LIMIT)
        c = db.cursor ()
        c.execute (SQL_ENIDS % (sql, LIMIT))
        enids = c.fetchall ()
        db.close ()
        return [enid [0] for enid in enids]
    

def pipeselect (pipeApp, pipeline, num=0):
    
    """
    """

    lines = plSelect (pipeApp, pipeline, num, 0, 1000000)
    if not lines:
        lines = []
    return lines

def psqredo (arg1, arg2, arg3, arg4):
    
    """
    """
    
    os.system ('psqredo %s %s %s %s' % (arg1, arg2, arg3, arg4))
    return

def getQUEUES (queue, state):
    
    """
    Creates an SQL query for the PSQ database which is will
    extract the names of the QUEUES which are desired.
    """
    
    sql  = "SELECT psqname, queue, data, application, pipeline, query "
    sql += "FROM PSQ WHERE "
    if queue in ['enabled', 'disabled']:
        sql += "state='%s'" % (queue)
    elif state != "":
        sql += "state='%s' AND queue='%s'" % (state, queue)
    else:
        sql += "queue='%s'" % (queue)
        pass

    return psqdb_query (sql)

def getPendingCount (queue, ds, data):

    """
    Creates and executes and SQL query to the PSQ database to
    determine the number of pending entries in a specified queue.
    """

    sql =  "SELECT COUNT(*) FROM %s, %s WHERE dataset=name AND" % (queue, data)
    sql += " end<=%s AND " % (getGMDT (dateOnly=True))
    
    if ds in [None, 'pending']:
        # No dataset or status specified, or status of pending (which includes
        # resubmit) specified, look for pending and resubmit status
        sql += "status IN ('pending', 'resubmit')"
    elif ds in ['error', 'submitted', 'resubmit']:
        # Specified a particular dataset status...
        sql += "status='%s'" % (ds)
    else:
        # Specified a particular dataset
        sql += "dataset='%s'" % (ds)
        pass
    #
    #if ds in ['pending', 'error', 'submitted']:
    #    sql += "status='%s'" % (ds)
    #    #sql = "SELECT COUNT(*) FROM %s WHERE status='%s'" % (queue, ds)
    #else:
    #    sql += "dataset='%s'" % (ds)
    #    #sql = "SELECT COUNT(*) FROM %s WHERE dataset='%s'" % (queue, ds)
    #    pass
    return int (psqdb_query (sql)[0])

def getSubmittedCount (queue, ds):
    
    """
    Creates and executed an SQL query to the PSQ database which
    determines the number of submitted datasets on a queue.
    """

    nsubmitted = 0
    if ds in [None, 'pending', 'resubmit']:
        sql = "SELECT COUNT(*) FROM %s WHERE status='submitted'" % (queue)
	nsubmitted = int (psqdb_query (sql)[0])
        pass
    return nsubmitted

def getCLvalue (infile, value):
    retVal = None
    if not infile:
        infile = os.environ ['NHPPS_SYS_NAME']
        pass
    paths = []
    if 'NHPPS_PIPESRC' in os.environ:
        paths.append (os.path.join (os.environ ['NHPPS_PIPESRC'],
	    infile, 'config'))
    if 'PipeConfig' in os.environ:
        paths.append (os.environ ['PipeConfig'])
    for path in paths:
        filename = os.path.join (path, '%s.cl' % (infile))
        if os.path.exists (filename):
            inlines = file (filename, 'r').readlines ()
            inlines = [line for line in inlines if value in line]
            if inlines:
                retVal = inlines [-1].split ('=')[1].split ('{')[0]
                retVal = retVal.replace ("'",'')
                retVal = retVal.replace ('"','')
                pass
            pass
        pass
    return retVal

def getPSQ_LIMIT (infile=None):
    psq_limit = getCLvalue (infile, 'psq_limit')
    if psq_limit:
        psq_limit = int (psq_limit)
    return psq_limit

def getPSQ_ORDER (infile=None):
    psq_order = getCLvalue (infile, 'psq_order')
    return psq_order

def getPSQ_REDO (infile=None):
    psq_redo = getCLvalue (infile, 'psq_redo')
    return psq_redo

def getDATASETS (queue, data, ds, newestFirst, num, date):
    
    """
    """

    if newestFirst:
        order = "desc"
    else:
        order = "asc"
        pass
    
    # Select entries to submit.
    sql =  "SELECT dataset, priority, end, subquery, status FROM %s, %s "
    if ds in [None, 'pending']:
        sql += "WHERE status IN ('%s', 'resubmit', 'queued') "
        ds = "pending"
    elif ds in ['resubmit', 'error', 'submitted']:
        sql += "WHERE status='%s' "
    else:
        sql += "WHERE dataset='%s' "
        pass
    #if ds in ['pending', 'error', 'submitted']:
    #    sql += "WHERE status='%s' "
    #else:
    #    sql += "WHERE dataset='%s' "
    #    pass
    sql += "AND end<=%s " % (date)
    sql += "AND dataset=name ORDER BY priority, end " + order + " LIMIT %d"
    print sql % (queue, data, ds, num)
    
    return psqdb_query (sql % (queue, data, ds, num))

def getGMDT (dateOnly=False):
    
    """
    Returns the Greenwich Meridian date and time in (almost) the IRAF format.
    """
    
    if not dateOnly:
        #retString = datetime.datetime.utcnow ().strftime ('%Y-%m-%dT%H:%M:%S')
        retString = datetime.datetime.utcnow ().strftime ('%Y-%m-%dT%H:%M')
    else:
        retString = datetime.datetime.utcnow ().strftime ('%Y%m%d')
    return retString


class flexibleTimer (object):
    
    def __init__ (self):
        
        self.timer = None
        self.action = None
        return
    
    def set (self, seconds):
        """
        Creates a timer thread to wait for an event.
        """
        
        import threading
        
        if self.timer:
            self.timer.cancel ()
            pass
        if self.action:
            self.timer = threading.Timer (seconds, self.action)
            self.timer.start ()
        return

    def setAction (self, action):
        self.action = action
        return
    
    def cancel (self):
        """
        Cancels the timer...
        """
        if self.timer:
            self.timer.cancel ()
            del self.timer
            self.timer = None
            pass
        return
    pass
