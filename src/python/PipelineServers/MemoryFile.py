# memory_storage, if present, lists the variable names (and their
# values) that we want the Node Manager to know about. This provides a
# simple way for a pipeline module to dynamically change the value of
# any arbitrary variable. Pipeline modules can add variables as well.
# 
# The important concept is that we want the 'variables' hash table
# (defined below) and this file to be in sync at all times. We,
# therefore, implement a simple transaction model: the set accessor
# method (defined below) return success if and only if both the hash
# table and the file have been updated successfully. For this reason,
# the memory_storage is overwritten every time a set routine is
# executed.

MEMORY_FILE = 'mgrsrvrc'

import os
try:
    # Define a location to store the temporary files.
    tempdir = os.path.join (os.environ ['NHPPS'], 'config')
except:
    pass
else:
    import tempfile, string
    from pipeutil import readConfFile, quote
    tempfile.tempdir = tempdir
    # variables is a dictionary (hash table) which is mapped to the
    # memory_storage file. It contains variables (name and values) that
    # we want to store on a node-wide basis.
    variables = {}
    memory_storage = os.path.join (tempfile.tempdir, MEMORY_FILE)
    
    # check if the memory_storage file exists, if so read it remember
    # that variables [key] is always an array; we need to flatten it.
    if os.path.exists (memory_storage):
        variables = readConfFile (memory_storage)
        for key in variables.keys ():
            flattened_value = string.join (
                [str (x) for x in variables [key]])
            variables [key] = flattened_value.strip ()
            
    def mgr_get (self):
        
        """
        Returns the value of a given variable or list of variables.
        
        The input parameters (stored in self.params) are:
        
        variables    a list of (space separated) variable names
        
        Returns:
        STATUS      = 'exit_code[ message]'
        <variable>  = <value>    for each variable name
        EOF
        
        In case of error, <value> is NULL.
        
        Exit codes (STATUS):
        0          success.
        1          failure.
        2          warning: some variables are not defined 
                   (hence the NULL value).
        """
        
        if not self.hasParam ('variables'):
            return
        
        values_text = ''
        status = 0
        
        # loop through the variables
        for var_name in self.params ['variables'].split ():
            # get the variable value. If the variable
            # is not defined, return the string NULL.
            try:
                var_value = variables [var_name]
            except:
                var_value = 'NULL'
                status = 1
                
            # update the values_text string
            values_text += '%s = ' % str (var_name)
            values_text += quote (str (var_value)) + '\n'
            
        # compose the output packet
        if status != 0:
            # ummm... some vars were not found.
            if values_text.count ('=') == values_text.count ('NULL'):
                # ummm... no vars were found!
                status = 1
            else:
                status = 2
                
        out_msg = 'STATUS = %d\n%sEOF' % (status, values_text)
        # write the out_msg back to the client
        self.wfile.write (out_msg)
        return
    
    
    def mgr_set (self):
        
        """
        Sets the value of a given variable or list of variables.
        
        The input parameters (stored in self.params) are:
        
        <variable>    variable name (e.g. foo)
        
        Returns:
        STATUS   = 'exit_code[ message]'
        EOF
        
        Exit codes (STATUS):
        0          success.
        >0         failure.
        """
        
        if len (self.params.keys ()) < 1:
            self.send_error (
                203, "Partial information: no variable defined.")
            return
        
        # TRANSACTION begins here
        
        # create a temporary file name
        temp_name = tempfile.mktemp ()
        
        # loop through the variables, skipping 'command'
        for var_name in self.params.keys ():
            # get the variable value. If the variable
            # is not defined, return the string NULL.
            if var_name.lower () == 'command':
                continue
            variables [var_name] = self.params [var_name]
            
        # now dump the variables hash table to the temporary
        # memory_storage
        try:
            f = open (temp_name, 'w')
        except:
            msg = "Cannot open the temporary memory_storage "
            msg += "(%s) for writing." % (temp_name)
            self.send_error (500, msg)
            return
        
        # overwite the content
        for key in variables.keys ():
            f.write (str (key) + ' = ' + str (variables[key]) + '\n')
        f.close ()
        
        # clobber memory_storage with the temp file
        try:
            if (os.path.exists (memory_storage)):
                os.rename (memory_storage, memory_storage + '.BAK')
            os.rename (temp_name, memory_storage)
        except:
            msg = "Cannot update the memory_storage "
            msg += "(with the contetnt of %s)." % (temp_name)
            self.send_error (500, msg)
            return
        
        # TRANSACTION ends here
        
        # compose the output packet
        out_msg = 'STATUS = 0\nEOF'
        
        # write the out_msg back to the client
        self.wfile.write (out_msg)
        return
