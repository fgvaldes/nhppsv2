# Polling time in seconds
POLLING_TIME = 5
SLEEP_TIME = 1

import threading
import time

class KeepAlive (threading.Thread):
    done = 0
    tot = 0
    
    def run (self):
        t0 = time.time ()
        while not self.done:
            dt = time.time () - t0
            if dt >= POLLING_TIME:
                t1 = time.time ()
                threading.Thread.run (self)
                self.tot += time.time () - t1
                t0 = time.time ()
            time.sleep (SLEEP_TIME)
        return
    
    def join (self, timeout=None):
        self.done = 1
        threading.Thread.join (self, timeout)
        return

def mgr_set_ptime (self):
    
    """
    Sets the POLLING _TIME internal variable.
    
    The input parameters are:
    
    ptime       polling time (in seconds).
    
    
    Returns:
    STATUS = 'exit_code message'
    EOF
    
    exit_code is:
    0:          Success.
    >0:         Error.
    """
    
    try:        
        POLLING_TIME = int (self.params ['ptime'].split ()[0])
    except:
        err = "Partial information: "
        err += "'ptime' parameter is missing/incorrect."
        self.send_error (203, err)
    else:
        out_msg = 'STATUS = "0 Polling time set to %d seconds."\nEOF'
        self.wfile.write (out_msg % (POLLING_TIME))
    return
