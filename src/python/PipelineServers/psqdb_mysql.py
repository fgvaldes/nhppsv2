# Version for MySQL.

import MySQLdb

def psadb_query (sql):
    
    """
    Connects to the MySQL database named for the user and executes the
    SQL statement contained in sql.  Returns a list which contains the
    entries for each row retrieved from the database. If more than one
    value is contained in a row, the return list will be of tuples,
    otherwise it will be a list of single values, one per row.
    """
    
    db = MySQLdb.connect (
        db     = os.environ ['USER'],
        passwd = 'Mosaic_DHS')
    
    c = db.cursor ()
    c.execute (sql)
    db.close ()
    
    try:
        columns = len (c.description)
    except:
        columns = 0
        pass
    
    if columns > 1:
        # Multiple items per row...
        return [row for row in c]
    elif columns == 1:
        # Only one item per row...
        return [row [0] for row in c]
    return c.rowcount

def psadb_update (sql):
    
    """
    Connects to the MySQL database named for the user and executes the
    SQL statement contained in sql.  Returns a list which contains the
    entries for each row retrieved from the database. If more than one
    value is contained in a row, the return list will be of tuples,
    otherwise it will be a list of single values, one per row.
    """
    
    db = MySQLdb.connect (
        db     = os.environ ['USER'],
        passwd = 'Mosaic_DHS')
    
    c = db.cursor ()
    c.execute (sql)
    db.close ()
