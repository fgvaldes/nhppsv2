# psq_complete (dataset, pipe, pipeApp)
#
# Updates the PSQ Database with the status of a 'completed' dataset.
#

import os
from psautil import getGMDT
if 'NHPPS_PSQDB' in os.environ:
    from psqdb_sqlite import psqdb_update
else:
    from psqdb_mysql import psqdb_update

_SUBMITTED = "UPDATE %s SET status='%s', completed='%s' WHERE dataset='%s' AND status='submitted';\n"
_LOCKED    = "UPDATE %s SET status='%s' WHERE dataset='%s' AND status='lock';\n"
_COMPLETED = _SUBMITTED % ('%s', 'completed', '%s', '%s')
_NODATA    = _SUBMITTED % ('%s', 'nodata',    '%s', '%s')
_ERROR     = _SUBMITTED % ('%s', 'error',     '%s', '%s')
_UNLOCK    = _LOCKED % ('%s', 'pending',   '%s')
if 'MarioData' in os.environ:
    _PSQ_DATA  = os.path.join (os.environ ['MarioData'], '%s', '%s_PSQ')
else:
    _PSQ_DATA  = os.path.join (os.environ ['NHPPS_DATAAPP'], '%s_PSQ')

def _complete (self, verbose, execute):
    
    """
    """
    
    dataset  = self.params ['dataset']
    pipe     = self.params ['pipeline']
    pipeApp  = self.params ['pipeapp'].upper ()
    psq_data = _PSQ_DATA % (pipeApp)
    
    # Work in data directory.
    os.chdir (psq_data)
    
    status = '1 Invalid dataset'
    if dataset != 'start':
        fName = '%s.%s' % (dataset, pipe)
        #node, dataset = dataset.split ('_', 1)
        queue, data, procid = dataset.split ('_', 2)
        
        if queue:
            if self.server.autoSubmit:
                self.params ['queue'] = 'enabled'
                ## Store the Queue name so that the auto submission
                ## functionality has access to it.
                #self.params ['queue'] = queue
                pass
            
            if os.path.exists (fName):
                lines = len (file (fName, 'r').readlines ())
                if verbose:
		    if procid == 'stage':
                        print 'Update dataset %s as staged' % (dataset)
                    elif lines > 0:
                        print 'Update dataset %s as completed' % (dataset)
                    else:
                        print 'Update dataset %s as nodata' % (dataset)
                        pass
                    pass
                if execute:
		    if procid == 'stage':
                        os.remove (fName)
                        psqdb_update (_UNLOCK % (queue, data))
                        status = '0 staged'
                    elif lines > 0:
                        nfName = os.path.join (psq_data, 'output', '%s.list' % (dataset))
                        os.rename (fName, nfName)
                        psqdb_update (_COMPLETED % (queue, getGMDT (), data))
                        status = '0 completed'
                    else:
                        os.remove (fName)
                        psqdb_update (_NODATA % (queue, getGMDT (), data))
                        status = '0 nodata'
                        pass
                    pass
                pass
            else:
                if verbose:
		    if procid == 'stage':
			print 'Update dataset %s as staged' % (dataset)
		    else:
			print 'Update dataset %s as error' % (dataset)
                    pass
                if execute:
		    if procid == 'stage':
			psqdb_update (_UNLOCK % (queue, data))
			status = '0 staged'
		    else:
			psqdb_update (_ERROR % (queue, getGMDT (), data))
			status = '1 error'
                    pass
                pass
            pass
        pass
    return status
