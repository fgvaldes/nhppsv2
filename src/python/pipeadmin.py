from pipeutil import *

# Constants
PREFIX = 'pipeadmin'
USAGE = "[stop|info] -a pipeApp -p pipeline -h host"

def execCmd (cmd, pipe, pipeApp, host=None, user=None):
    
    if not host:
        host = NODE
        pass
    if not user:
        user = USER
        pass
    nm = os.environ ['NHPPS_PORT_NM']
    
    # Init the Pyro client and contact the NameServer
    try:
        Pyro.core.initClient ()
        ns = findNameServer (timeout=2)
    except:
        print 'ERROR: The Name Server cannot be either started or discovered.'
        return 1
        pass
    
    # Build the process unique name using our convention:
    # NSGROUP^pipeApp^pipe^host^user^nm
    processID = '%s^%s^%s^%s^%s^%s' % (NSGROUP, pipeApp, pipe, host, user, nm)
    
    # Resolve processID using the NameServer and connect to the proxy
    obj = None
    try:
        uri = ns.resolve (processID)
        obj = Pyro.core.getProxyForURI (uri)
    except:
        print 'ERROR: Unable to resolve reference to %s' % (processID)
        return 1
        pass
    
    # Use the remote object (via its proxy obj)
    err = 1
    if obj:
        try:
            print (obj.handleAdminRequest (command=cmd, argv=[]))
            ns.unregister (processID)
            err = 0
        except:
            print 'ERROR: Failure during execution of command on remote object'
            pass
        pass
    else:
        print 'ERROR: Unable to contact the remote xpoll at %s' % (processID)
        pass
    return err
