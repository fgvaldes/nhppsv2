import array
from pipeutil import *
from ModuleMgr import *

# Stage and Flag defaults
STAGEFMT = "%02d"
IDLEFLAG = '_'
FLAGDELIM = ':'

# OSF/PSTAT file naming formats:
OSFFMT   = "%d:%-24s.%-74s:%3s:000:%-8s"
OSFFMT1  = "%s:%s:%s:%s"
MODFMT   = "%s:%s:%s:%s:%d:%s:%s:%s:%s%s"
PSTATFMT = "%08s-%-9s-%-15s.%d-%-9s-%-20s-%-4s"

# OSF/PSTAT masks for update
OSFMASK   = "%d:*.%-74s:%3s:*:*"
PSTATMASK = "%s-%-9s-*.%d-%-9s-%-20s-*"

# Module log filename format
LOGFMT = "%s-%d-%s-%s.plog"

# Do we need to dump the content of the OSF BB?
FILE_BB = True
FILE_PSTAT = False
FILE_PM = True

# STDERR message prefix
PREFIX = 'BlackBoard'


class BlackBoardEntry:
    
    """
    Defines a generic BlackBoard entry. Structure:
    time   --> The time when the dataset was added to the OSFBlackBoard.
    status --> A string which maintains the status for this dataset
    code   --> A integer arry
    count  --> A integer arry
    pipe   --> A string which contains the name of the pipeline
    node   --> A string which contains the name of the local NODE
    """
    
    def __init__ (self, inPipe, inNode):
        
        """
        Create and initialize a new BlackBoardEntry
        """
        
        self.time = time.time ()
        self.status = ''
	self.code = array.array ('i')
	self.count = array.array ('i')
        self.pipe = inPipe
        self.node = inNode
        
        return
    
    pass


class OSFBlackBoardEntry (BlackBoardEntry):
    
    """
    Defines an OSF BlackBoard entry. Structure:
    From BlackBoardEntry:
        time   --> The time when the dataset was added to the OSFBlackBoard.
        status --> A string which maintains the status (flags) for this dataset
        pipe   --> A string which contains the name of the pipeline
        node   --> A string which contains the name of the local NODE
    OSFBlackBoardEntry:
        firstFlag   --> A string which contains the status flag for stage 1
                        This is used to allow us to provide feedback during
                        halting operations, without loosing the actual status
                        flag.
        isClosed    --> A boolean value which tells us if the dataset is closed
                        The Closed flag tells us whether the entry is currently
                        active or not. Every entry is Open (Closed = False) by
                        default. The only way to close an entry is by using
                        osfClose(). Once closed, an entry may only be opened by
                        using osfOpen(). A closed entry will be considered not
                        part of the OSFBlackBoard, as far as selectOSFDataset()
                        etc. are concerned. Note that osfUpdate() will work on
                        closed entries; but it will not be able to open them.
    """
    
    def __init__ (self, inPipe, inNode, inStages, inFlag):
        
        """
        Create and initialize a new OSFBlackBoardEntry
        """
        
        BlackBoardEntry.__init__ (self, inPipe, inNode)
        
        self.firstFlag          = inFlag
        self.isClosed           = False
        self.status             = inFlag * inStages
        self.code               = [-1] * inStages
        self.count              = [-1] * inStages

        return
    
    
    def __setitem__ (self, key, flag):
        
        """
        Sets the status of 'key' to 'flag' in the blackboard (status, code).
        Used as an 'override' to the [] operator. We may now simply
        'index' into the OSFBlackBoardEntry object to set the status
        of the 'key' stage (stages range from 1-nStages)
        e.x. entry = OSFBlackBoardEntry ()
        ...
        entry[1] = 'c'# Set the first status flag to 'c'
        """

        if isinstance (key, str):
            try:
                key = int (key)
            except:
                pass
            pass

	# Separate the flag into status and code.
	fields = flag.split (FLAGDELIM)
	status = fields[0]
	if (len(fields) < 2):
	    codeStr = ""
	else:
	    codeStr = fields[1]
	try:
	    code = int (codeStr)
	except:
	    codeStr = ""

	index = key - 1
	for c in status:
	    if index in range (len (self.status)):
		if c != '.':
		    self.status = '%s%s%s' % (
			self.status[:index], c, self.status[index+1:])
		if (codeStr == ""):
		    pass
		elif (codeStr[0] == '+' or codeStr[0] == '-'):
		    self.code[index] += code
		else:
		    self.code[index] = code

		pass
	    else:
		warning (PREFIX, 'Unable to set stage %s; max stage is %d' %
		    (key, len (self.status)))
		pass
	    pass

	    index += 1
	    pass

	return

    
    def __getitem__ (self, key):
        
        """
        Gets the status flag for 'key' from the status string.
        used as an 'override' to the [] operator. We may now simply
        'index' into the OSFBlackBoardEntry object to get the status
        of the 'index' stage (stages range from 1-nStages)
        e.x. entry = OSFBlackBoardEntry ()
        ...
        stageStatus = entry[1] # Get the status for the first stage
        """
        
        if isinstance (key, str):
            try:
                key = int (key)
            except:
                pass
            pass
        
        if isinstance (key, int) and key-1 in range (len (self.status)):
            return self.status[key-1]
        else:
            warning (PREFIX, 'Unable to get stage %s; max stage is %d' % (
                key, len (self.status)))
            pass
        
        return None
    
    
    def getFileName (self, dataset, format='OSFFMT'):
        
        """
        Given a dataset name formats and returns the
        corresponding OPUS-esque file name.
        """
        
        # compose the file name
	if format == 'OSFFMT':
	    fileName = OSFFMT % (
		int (self.time), self.status, dataset, self.pipe, self.node)
	else:
	    fileName = OSFFMT1 % (self.node, self.pipe, dataset, self.status)
	    for i in range (len(self.code)):
	        c = self.code[i]
	        n = self.count[i]
		if c == -1:
		    fileName += ':_'
		else:
		    fileName += ':' + str (c)
		if n >= 0:
		    fileName += ',' + str (n)
        
        # substitute spaces with '_'
        return fileName.replace (' ', '_')
    


class PSTATBlackBoardEntry (BlackBoardEntry):
    
    """
    Defines an OSF BlackBoard entry. Structure:
    From BlackBoardEntry:
        time   --> The time when the dataset was added to the OSFBlackBoard.
        status --> A string which maintains the status for this dataset
        pipe   --> A string which contains the name of the pipeline
        node   --> A string which contains the name of the local NODE
    PSTATBlackBoardEntry:
        process  --> Name of the pipeline module which this entry is for.
        isLocked --> A boolean value which tells routines which operate on the
                     entry to wait until everybody else is done using them.
        #command --> A string which is not used
    """
    
    def __init__ (self, inPipe, inNode, inProcess, inStatus):
        
        """
        Create and initialize a new OSFBlackBoardEntry
        """
        
        BlackBoardEntry.__init__ (self, inPipe, inNode)
        
        self.process  = inProcess
        self.isLocked = False
        self.status   = inStatus
        
        return
    
    
    def getFileName (self, pid):
        
        """
        Given a pid formats and returns the
        corresponding OPUS-esque file name.
        """
        
        # compose the file name
        fileName = PSTATFMT % (
            pid, self.process, self.status,
            int (self.time), self.pipe, self.node, '')
        
        # substitute spaces with '_'
        return fileName.replace (' ', '_')
    
    pass


class BlackBoard:
    
    def __init__ (self, pipeline):
        
        """
        Sets up the internal (in memory) blackboard and
        the necessary information needed to keep a copy
        of the blackboard on disk (a la OPUS).
        
        We are maintaining two blackboards:
        1. OSFBlackBoard:
            This is a dictionary with the keys being datasets
            and the value being an OSFBlackBoardEntry.
            The OSFBlackBoardEntry is used to store information
            unique (or not) to the dataset, such as it's status
            in the pipeline.
            
        2. PSTATBlackBoard:
            This is a dictionary with the keys being the PID of the
            ??? and the value being a PSTATBlackBoardEntry.
            The PSTATBlackBoardEntry is used to store information,
            such as the state of the module (processing, idle, etc).
        
        The optional 'monitor' parameter is a 2 element array of the 
        form (host, port). It gives the address of the optional BB
        monitor (aka PIPEMON). The monitor application follows the
        Pipeline RPC protocol (key = value pairs).
        PIPEMON uses the switchboard.
        
        The BlackBoard sends to the monitor (if present) updates on the
        status of the OSF blackboard.
        """
        
        global FILE_BB
        global FILE_PSTAT
        
        self.pipeline         = pipeline
        self.pipe             = pipeline.name
        self.obsDir           = pipeline.Directories.obs
        self.status           = 1
        self.lockOSF          = False        
        self.nstages          = len (pipeline.Modules)
        self.moduleStage      = {}
	self.exitCode         = {}
	self.exitFlag         = {}
        self.OSFBlackBoard    = {}
        self.PSTATBlackBoard  = {}
        # Keep track of the closed and open OSF datasets
        self.osfCloseDatasets = []
        self.osfOpenDatasets  = []
        
        # Build a dictionary to map module names to stages (starting 
        # from 1 and excluding inactive modules).
        stage = 1
        for module in pipeline.Modules:
            if module.isActive:
                self.moduleStage [str (module.name)] = stage
                stage += 1
                pass
            pass

	# Build a dictionary of exit codes and flags.
	for ExitCode in pipeline.ExitCodes:
	    self.exitCode [ExitCode.code] = ExitCode
	    self.exitCode [ExitCode.id] = ExitCode
	    if ExitCode.flag:
	        for c in ExitCode.flag:
		    self.exitFlag [c] = ExitCode

        # Ensure that the log directory is available...
        try:
            self.homeDir = os.path.join (
                os.environ ['NHPPS_LOGS'], pipeline.system)
        except:
            self.homeDir = os.path.join (
                os.environ ['HOME'],
                '.nhpps', pipeline.system)
            msg =  '$NHPPS_LOGS is not defined; reverting to '
            msg += '$HOME/.nhpps/$NHPPS_SYS_NAME'
            warning (PREFIX, msg)
            pass
        
        if not os.path.exists (self.homeDir):
            try:
                os.mkdir (self.homeDir)
            except:
                msg =  '%s cannot be created; running '
                msg += 'without log files.'
                warning (PREFIX, msg % (self.homeDir))
                self.homeDir = ''
		FILE_PSTAT = False
                pass
            pass

	# Ensure that the obsDir is available...
	if FILE_BB and not os.path.exists (self.obsDir):
	    #msg = 'File blackboard directory not found (%s); '
	    #msg += 'running without file blackboard entries'
	    #warning (PREFIX, msg % (self.obsDir))
	    FILE_BB = False
	    pass
        
        # Monitor Information
        try:
            pm = string.split (os.environ ['NHPPS_PORT_PM'], ':')
            self.monitorHost = pm[2]
            self.monitorPort = int (pm[1])
        except:
            self.monitorHost = None
            self.monitorPort = None
	    FILE_PM = False
            pass

        return
    
    
    def setStatus (self, val=-1):
        
        self.status = val
        
        return
    
    
    def getStatus (self):
        
        return self.status
    
    
    def osfCount (self):
        
        """
        Returns the number of open datasets in the current blackboard
        """
        
        return len (self.osfOpenDatasets)
    
    
    def osfUpdate (self, dataset, module, stage, flag, Halt=False):
        
        """
        We update the given dataset entry so that module is set to flag.
        If the blackboard entry for dataset does not exist, we create it.
        """

        if dataset not in self.OSFBlackBoard:
            return self.osfCreate (dataset, module, stage, flag)
        
        entry = self.OSFBlackBoard[dataset]
    
        # Try and recover the stage number from the module name
        if module:
            try:
                stage = self.moduleStage [str (module)]
            except:
                stage = 1
                msg = 'Module name is invalid (osfUpdate %s %s %%s)\n'
                warning (PREFIX, msg % (module, dataset, flag))
                pass
            pass
        
        if not stage:
            stage = 1
            pass

	# separate flag into status and code.
	fields = flag.split(FLAGDELIM)
	status = fields[0]
	code = ""
	if (len(fields) > 1):
	    # Convert the status to an integer.
	    if fields[1].isdigit():
	        code = FLAGDELIM + fields[1]
	    elif fields[1] in self.exitCode:
		code = FLAGDELIM + str (self.exitCode[fields[1]].code)
        
        # flag can have more than one char. In that case
        # it is to be considered an array of chars
        for ch in status:
            # update the status hash table
            try:
                if stage > 1:
                    entry [stage] = ch + code
                elif stage == 1:
                    # Do not store the 'status' of the update in 'long-term'
                    # storage is it is from an osf_update which sets the
                    # status of the 'halt' flag...
                    if not Halt:
                        entry.firstFlag = ch + code
                        pass
                    
                    # Prevent the first stage from being updated while the
                    # pipeline is being halted
                    if Halt or not self.lockOSF:
                        entry [stage] = ch + code
                        pass
                    pass
                else:
                    raise Error
                pass
            except:
                warning (PREFIX, 'invalid stage (%s)' % (stage))
                return
            
            # increment the stage index
            stage += 1
            if stage > self.nstages:
                break
            pass
        
        # Write the changes to the monitor?
	if FILE_PM:
	    self.osfMonitorWrite (dataset, entry)
        
        # Do we need to update the file system copy?
        if FILE_BB:
            err = self.osfUpdateFile (dataset, entry)
            if err:
                msg = 'could not update the OSF file for %s.'
                warning (PREFIX, msg % (dataset))
                pass
            pass
        
        return
        
    
    def osfWait (self, dataset, module, flag):
        
        """
	The count for the module is set or decremented.
	If a count is specified the count and flag are set.
	If a count is not specified the count is decremented
	and the flag set if the count is zero.
	When the count is zero set the specified flag in the blackboard
        If the blackboard entry for dataset does not exist or
	the stage is not determined do nothing.
        """

	# Get the entry.
        if dataset not in self.OSFBlackBoard:
	    return
        entry = self.OSFBlackBoard[dataset]
    
        # Determine the index for the module.
	try:
	    stage = self.moduleStage [str (module)]
	except:
	    msg = 'Module name is invalid (osfWait %s %s %%s)\n'
	    warning (PREFIX, msg % (module, dataset, flag))
	    return

	# Separate the flag into status and count.
	fields = flag.split(FLAGDELIM)
	flag = fields[0]
	count = None
	if len(fields) > 1:
	    count = int (fields[1])

	# If a status:count is specified set it.
	# Otherwise decrement the count and set the status when zero.

	if count:
	    entry.count[stage-1] = count
	    entry[stage] = flag
	else:
	    if entry.count [stage-1] > 0:
		entry.count [stage-1] -= 1
	    if entry.count [stage-1] == 0:
		entry [stage] = flag
        
        # Write the changes to the monitor?
	if FILE_PM:
	    self.osfMonitorWrite (dataset, entry)
        
        # Do we need to update the file system copy?
        if FILE_BB:
            err = self.osfUpdateFile (dataset, entry)
            if err:
                msg = 'could not update the OSF file for %s.'
                warning (PREFIX, msg % (dataset))
                pass
            pass
        
        return
    
    def osfUpdateFile (self, dataset, osfEntry):
        
        """
        This is part of the OPUS compatibility layer: it simply
        writes the OSF blackboard entry (osfEntry) to disk (in 
        the OBS directory of self.pipleine).
        
        We try and keep the same OSF file format OPUS uses, with 
        only a couple of differences. 
        1. Times are not in HEX, but in seconds.
        2. PIDs are bogus, for the time being.
        
        It uses the global variable OSFFMT to create the 
        appropriate file names.
        
        As the name implies, this routine just updates an
        existing OSF file.
        """
        
        # compose the file name
        fileName = self.osfFileName (dataset, osfEntry, short=True)
        
        mask = OSFMASK % (int (osfEntry.time), dataset, osfEntry.pipe)
        
        # substitute spaces with '_'
        mask = mask.replace (' ', '_')
        
        err = updateFile (dir=self.obsDir, old=mask, new=fileName)
        
        return err
    
    
    def osfCreate (self, dataset, module, stage, flag=None):
        
        if dataset in self.OSFBlackBoard:
            # update the old entry
            return self.osfUpdate (dataset, module, stage, flag)
        
        # create a new entry.
        self.OSFBlackBoard [dataset] = OSFBlackBoardEntry (
            self.pipe, NODE, self.nstages, IDLEFLAG)
        
        if flag and (module or stage):
            # If needed, update the stage
            # Try and recover the stage number from the module name
            if module:
                try:
                    stage = self.moduleStage [str (module)]
                except:
                    stage = 1
                    msg = 'Module name is invalid (osfCreate %s %s %s)\n'
                    warning (PREFIX, msg % (module, dataset, flag))
                    pass
                pass
            
            self.OSFBlackBoard [dataset][stage] = flag
            if stage == 1:
                self.OSFBlackBoard [dataset].firstFlag = flag
                pass
            pass
        
        # Add the dataset to self.osfOpenDatasets and, if necessary, remove it
        # from self.CloseDatasets
        self.osfOpenDatasets.append (dataset)
        if dataset in self.osfCloseDatasets:
            self.osfCloseDatasets.remove (dataset)
            pass
        
        # Do we need to create a OSF file?
        if FILE_BB:
            err = self.osfCreateFile (dataset, self.OSFBlackBoard[dataset])
            if err:
                msg = 'could not create the OSF file for %s.'
                warning (PREFIX, msg % (dataset))
                pass
            pass
        
        return
    
    
    def osfCreateFile (self, dataset, osfEntry):
        
        """
        This is part of the OPUS compatibility layer: it simply
        writes the OSF blackboard entry (osfEntry) to disk (in 
        the OBS directory of self.pipleine).
        
        We try and keep the same OSF file format OPUS uses, with 
        only a couple of differences. 
        1. Times are not in HEX, but in seconds.
        2. PIDs are bogus, for the time being.
        
        It uses the global variable OSFFMT to create the 
        appropriate file names.
        
        As the name implies, this routine just creates a
        new OSF file.
        """
        
        fileName = self.osfFileName (dataset, osfEntry)
        
        try:
            # touch the file
            f = file (fileName, 'w')
            f.close ()
        except:
            return 1
        
        return None
    
    
    def osfSelect (self,
                   dataset=None,
                   module=None,
                   stage=None,
                   flag=None,
                   openDatasetsOnly=True,
                   closedDatasetsOnly=False,
                   format='OSFFMT'):
        
        """
        Performs a SQL-like SELECT on the OSF blackboard.
        Constraints are given by 'dataset', 'stage' and 
        'flag'. A null value in any of these parameters
        is equivalent to no constraint (on that column).
        
        Returns an array of results formatted according
        to the selected format. In case no matching record
	is found, an empty array is returned.
        """
        
	# Try and recover the stage index from module name
	# or module name from stage index.
	if module and not stage:
	    try:
		stage = self.moduleStage [str (module)]
	    except:
		msg = 'Module name is invalid (osfSelect %s %s %s)\n'
		warning (PREFIX, msg % (module, dataset, flag))
		return []
	    pass
	elif not stage:
	    stage = 0
	    # This will become -1, we'll check for it later...
	    # if no stage is defined, search the entire status for flag
	    pass
	
	# The status string is 0 based, while the 'stage' we have in our
	# code are 1 based. Therefore, subtract 1 from stage so that we can
	# do a string comparison.
	stage = stage - 1
	pass
        
        # Prepare an array of temporary results
        results = []
        
        # Loop through the OSF BB entries and throw away non matching 
        # elements. Return what is left.
        for ds in self.OSFBlackBoard:
            # 0. skip closed entries
            if openDatasetsOnly and self.OSFBlackBoard [ds].isClosed:
                continue
            if closedDatasetsOnly and not self.OSFBlackBoard [ds].isClosed:
                continue
            
            # 1. select on dataset
            if dataset and ds != dataset:
                continue
            
            bb = self.OSFBlackBoard [ds]
            
            # 2. select on stage/flag values
            if flag and stage >= 0 and bb.status [stage:stage+len(flag)]!=flag:
                continue
            elif flag and stage < 0 and bb.status.find (flag) < 0:
                continue
            
            # 3. If we are here, append the OSF BB entry to results
            if format in ['OSFFMT', 'OSFFMT1']:
                results.append (self.osfFileName (ds, bb, short=True,
		    format=format))
	    elif format in ['MODFMT','MODFMT1']:
	        #results.append (self.exitCode)
		for i in range(self.nstages):
		    if stage >= 0 and i != stage:
		        continue
		    n = bb.code[i]
		    f = bb.status[i]
		    if format == 'MODFMT1' and n < 0:
		        continue
		    if flag and f != flag:
		        continue

		    mod = [k for k, v in self.moduleStage.iteritems() if v == i+1][0]

		    c = '_'
		    id = '_'
		    severity = '_'
		    desc = ''
		    if n >= 0:
		        c = str (n)
			id = 'UNDEFINED'
			if n in self.exitCode:
			    id = self.exitCode [n].id
			    if self.exitCode[n].severity:
				severity = self.exitCode [n].severity
			    if self.exitCode[n].desc:
				desc = ':' + self.exitCode [n].desc
			elif f in self.exitFlag:
			    id = self.exitFlag [f].id
			    if self.exitFlag[f].severity:
				severity = self.exitFlag [f].severity
			    if self.exitFlag[f].desc:
				desc = ':' + self.exitFlag [f].desc
			elif bb.status[i] in 'pcwd':
			    severity = 'OK'
			elif bb.status[i] in 'n':
			    severity = 'WARN'
			elif bb.status[i] in 'e':
			    severity = 'ERROR'
			elif bb.status[i] in 'f':
			    severity = 'FATAL'
			elif bb.status[i] in 'htx':
			    severity = 'HALT'
		    if bb.count[i] > 0:
			c = '%s,%s' % (c, str(bb.count[i]))
		    results.append (MODFMT %
		        (bb.node, bb.pipe, ds, mod, i+1, f, c, id, severity, desc))
            else:
                results.append (ds)
                pass
            pass
        
        return results
    
    
    def osfCleanUp (self, dataset=None):
        
        """
        Wipes the OSF BlackBoard clean. It *should* never 
        be used in operations. In practice it could happen 
        that the operator wants to re-process a given set
        of images. In that case, he/she would do a 
        'cleanall' from the command line, followed by a call
        to osfCleanUp in order to start from scratch.
        
        Input: None
        Return: None
        """
        
        err = 0
        # do we need to remove the OSF files as well?
        if FILE_BB:
            err = self.osfCleanUpFile (dataset)
            if err and not dataset:
                msg = 'could not cleanup the OSF files for pipeline %s'
                warning (PREFIX, msg % (self.pipe))
            elif err and dataset:
                msg =  'could not cleanup the OSF files for pipeline %s '
                msg += '(dataset %s).'
                warning (PREFIX, msg % (self.pipe, dataset))
                pass
            pass
        
        if not dataset:
            self.OSFBlackBoard = {}
        else:
            try:
                del self.OSFBlackBoard[dataset]
            except:
                pass
            pass
        
        return err
    
    
    def osfCleanUpFile (self, dataset=None):
        
        """
        Complement to osfCleanUp, this routine simply removes
        all the files in the pipeline OBS directory. It is 
        only used in OPUS compatible mode.
        
        Input: None
        Return: 0 for success
        n for error (in this case, n is the number of files
        that we failed to delete).
        """
        
        failed = 0
        if not dataset:
            datasets = self.OSFBlackBoard.keys ()
        elif dataset in self.OSFBlackBoard:
            datasets = [dataset]
        else:
            return 0
        
        for dataset in datasets:
            osfEntry = self.OSFBlackBoard [dataset]
            fileName = self.osfFileName (dataset, osfEntry)
            try:
                # remove the file
                os.remove (fileName)
            except:
                failed += 1
                pass
            pass
        
        return failed
    
    
    def osfMonitorWrite (self, dataset, osfEntry):
        
        """
        Given a dataset and the corresponding OSF BB entry
        it writes the relevant information to PIPEMON or
        equivalent. If either self.monitorHost or
        self.monitorPort are None, it immediately returns.
        """
        
        if not (self.monitorHost and self.monitorPort):
            # warning (PREFIX, 'Warning: no monitor information.')
            return
        
        fileName = self.osfFileName (dataset, osfEntry, short=True)
        
        # connect to the Monitor
        sock = connect (self.monitorHost, self.monitorPort)
        if not sock:
            # the Monitor is not responding
            # warning (PREFIX, 'Warning: monitor not responding.\n')
            return
        
        # We prefix the fileName with NODE
        fileName = NODE + ' ' + fileName + '\n'
        sent = sock.send (fileName)    
        disconnect (sock)
        
        return
    
    
    def osfFileName (self, dataset, osfEntry, short=False, format='OSFFMT'):
        
        """
        Given a dataset name and the corresponding OSF
        blackboard entry, it formats and returns the
        corresponding OPUS-esque file name.
        """
        
        fileName = osfEntry.getFileName (dataset, format)
        if not short:
            fileName = os.path.join (self.obsDir, fileName)
            pass
        
        return fileName
    
    
    def osfClose (self, dataset):
        
        if dataset in self.OSFBlackBoard:
            if not self.OSFBlackBoard [dataset].isClosed:
                self.OSFBlackBoard [dataset].isClosed = True
                
                # Remove the dataset from self.osfOpenDatasets and add it to
                # self.osfCloseDatasets
                self.osfCloseDatasets.append (dataset)
                self.osfOpenDatasets.remove (dataset)
                pass
            pass
        
        return

    def osfConditRemove (self, dataset):

        if dataset in self.OSFBlackBoard:
            if not self.OSFBlackBoard [dataset].isClosed:
                self.OSFBlackBoard [dataset].isClosed = True

                # Remove the dataset from self.osfOpenDatasets and add it to
                # self.osfCloseDatasets
                self.osfCloseDatasets.append (dataset)
                self.osfOpenDatasets.remove (dataset)
                pass
            m = re.match( "^c[cns_]*d", self.OSFBlackBoard [dataset].status )
            if m:
                self.osfCleanUp(dataset)
                pass
            pass
        return

    def osfFinalClose (self, dataset):

        if dataset in self.OSFBlackBoard:
            if not self.OSFBlackBoard [dataset].isClosed:
                self.OSFBlackBoard [dataset].isClosed = True

                # Remove the dataset from self.osfOpenDatasets and add it to
                # self.osfCloseDatasets
                self.osfCloseDatasets.append (dataset)
                self.osfOpenDatasets.remove (dataset)
                pass
            self.osfCleanUp(dataset)
            pass

        return

    def osfOpen (self, dataset):
        
        if dataset in self.OSFBlackBoard:
            if self.OSFBlackBoard [dataset].isClosed:
                self.OSFBlackBoard [dataset].isClosed = False
                
                # Add the dataset to self.osfOpenDatasets and remove it from
                # self.osfCloseDatasets
                self.osfOpenDatasets.append (dataset)
                self.osfCloseDatasets.remove (dataset)
                pass
            pass
        
        return
    
    
    def pstatUpdate (self, process, pid, status='idle', instID=0):
        
        """
        We update the PSTAT entry for the input process
        to reflect its new status.
        """
        
        if pid in self.PSTATBlackBoard:
            entry = self.PSTATBlackBoard [pid]
        else:
            self.pstatCreate (process, pid, status, instID)
            return
        
        # update status
        entry.status = status
        
        # Do we need to update the file system copy?
        if FILE_PSTAT:
            err = self.pstatUpdateFile (pid, entry)
            if err:
                msg = 'could not update the PSTAT file for %s (%d)'
                warning (PREFIX, msg % (process, pid))
                pass
            pass
        
        return
    
    
    def pstatUpdateFile (self, pid, pstatEntry):
        
        """
        OPUS Compatibility Layer: updates a PSTAT entry in 
        self.homeDir ($OPUS_HOME_DIR) for the given pid.
        """
        
        # compose the file name
        fileName = self.pstatFileName (pid, pstatEntry)
        
        time = int (pstatEntry.time)
        mask = PSTATMASK % (
            pid, pstatEntry.process, time, pstatEntry.pipe, pstatEntry.node)
        
        # substitute spaces with '_'
        mask = mask.replace (' ', '_')
        
        err = updateFile (dir=self.homeDir, old=mask, new=fileName)
        
        return err
    
    
    def pstatCreate (self, process, pid, status='idle', instID=0):
        
        """
        We simply create a PSTAT blackboard entry for 
        the given pipeline module (process), status and 
        PID.
        """
        
        if pid in self.PSTATBlackBoard:
            # update the old entry
            self.pstatUpdate (process, pid, status)
            return
        
        # create a new entry
        self.PSTATBlackBoard [pid] = PSTATBlackBoardEntry (
            self.pipe, NODE, process, status)
        
        # build the name of the log file
        logName = LOGFMT % (
            process, self.PSTATBlackBoard [pid].time,
            self.PSTATBlackBoard [pid].node, instID)#pid)
        
        # Do we need to create a PSTAT file?
        if FILE_PSTAT and self.homeDir:
            err = self.pstatCreateFile (pid, self.PSTATBlackBoard [pid])
            if err:
                msg = 'could not create the PSTAT file for %s (%d).'
                warning (PREFIX, msg % (process, pid))
                pass
            pass
        
        return os.path.join (self.homeDir, logName)
    
    
    def pstatCreateFile (self, pid, pstatEntry):
        
        """
        OPUS Compatibility Layer: creates a PSTAT entry in 
        self.homeDir ($OPUS_HOME_DIR) for the given pid.
        """
        
        fileName = self.pstatFileName (pid, pstatEntry)
        
        try:
            # touch the file
            f = file (fileName, 'w')
            f.close ()
        except:
            return 1
        
        return None
    
    
    def pstatCleanUp (self):
        
        """
        Wipes the PSTAT BlackBoard clean. It should get
        called every time a pipeline is stopped by the
        operator (or any control GUI).
        
        Input: None
        Return: None
        """
        
        # do we need to remove the OSF files as well?
        if FILE_PSTAT:
            err = self.pstatCleanUpFile ()
            if err:
                msg = 'could not cleanup the PSTAT files for pipeline %s.'
                warning (PREFIX, msg % (self.pipe))
                pass
            pass
        
        # cleanup the in-memory blackboard
        self.PSTATBlackBoard = {}
        
        return
    
    
    def pstatCleanUpFile (self):
        
        """
        Complement to pstatCleanUp, this routine simply removes
        all the files in the pipeline HOME directory. It is 
        only used in OPUS compatible mode.
        
        Input: None
        Return: 0 for success
        n for error (in this case, n is the number of files
        that we failed to delete).
        """
        
        failed = 0
        for pid in self.PSTATBlackBoard:
            fileName = self.pstatFileName (pid, self.PSTATBlackBoard [pid])
            try:
                # remove the file
                os.remove (fileName)
            except:
                failed += 1
                pass
            pass
        
        return failed
    
    
    def pstatFileName (self, pid, pstatEntry, short=False):
        
        """
        Given a pid and the corresponding PSTAT
        blackboard entry, it formats and returns the
        corresponding OPUS-esque file name.
        """
        
        fileName = pstatEntry.getFileName (pid)
        if not short:
            fileName = os.path.join (self.homeDir, fileName)
            pass
        
        return fileName
    
    
    def selectOSFDatasets (self, where, logicalOp='AND'):
        
        """
        Perform a SQL like select on the OSF BB, given an array of 
        (moduleName, operator, flag) (where) and a overall logical operator 
        (logicalOp).
        
        This method returns a list of datasets satisfying the query.
        
        Returns:
        datasets     if any
        None        if none
        """
        
        # Substitute stage numbers to module names
        requirements = []
        for (module, op, flag) in where:
            try:
                stage = self.moduleStage [str (module)]
            except:
                warning (PREFIX, 'Module name is invalid: "%s"' % (module))
                continue
            
            if op in ['==','!='] and isinstance(flag, str) and len(flag) == 1:
                requirements.append ([stage, op, flag])
            else:
                msg = 'Unsurpported operator: "%s" or bad flag: "%s"'
                warning (PREFIX, msg % (op, flag))
                pass
            pass
        
        if requirements:
            pendingDataSets = []
            logicalOp = logicalOp.strip ().upper ()
            for ds in self.OSFBlackBoard:
                # 0. Skip closed entries
                if self.OSFBlackBoard [ds].isClosed:
                    continue
                
                testResults = []
                for (stage, op, flag) in requirements:
                    testResults.append (eval ("'%s' %s '%s'" % (
                        self.OSFBlackBoard [ds][stage], op, flag)))
                    pass
                
                if logicalOp == 'AND' and False not in testResults:
                    pendingDataSets.append ((self.OSFBlackBoard [ds].time, ds))
                elif logicalOp == 'OR' and True in testResults:
                    pendingDataSets.append ((self.OSFBlackBoard [ds].time, ds))
                    pass
                pass
            
            if pendingDataSets:
                pendingDataSets.sort ()
                return pendingDataSets
            pass
        
        return None
    
    
    def getOSFBBEntry (self, dataset):
        
        return self.OSFBlackBoard [dataset]
    
    
    def getOSFBBFlag (self, dataset, moduleName):
        
        blackboard = self.OSFBlackBoard [dataset]
	stage = self.moduleStage [str(moduleName)]
        
	return blackboard [stage]
    
    
    def getOSFBBCount (self, dataset, moduleName):
        
        blackboard = self.OSFBlackBoard [dataset]
	stage = self.moduleStage [str(moduleName)]

	try:
	    c = blackboard.count [stage-1]
	except:
	    c = 0
	    print 'getOSFBBCount: ERROR - %s %s %d' % (dataset, moduleName, stage)
        
	#return blackboard.count [stage-1]
	return c
    
    
    def getOpenOSFDatasets (self):
        
        return self.osfOpenDatasets
    
    pass
