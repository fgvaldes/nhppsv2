#!/bin/env python

"""
author: f. pierfederici (fpierfed@noao.edu)

Usage:
pipeselect.py pipelines nfiles bias
  pipelines:  a list of space separated pipeline names.
              if more that one pipeline name is specified
              the whole list mist be enclosed in quotes.
  nfiles:     number of files to distribute.
  bias:       bias factor (not used).

Description:
pipeselect communicates with the Node Managers (NM) of the
available nodes. It queries the NMs for the list of files
in 'pipelines' FIRST pipeline name, on all the nodes.
If any one of pipelines is not running on a node, that 
node is silently skipped.

The Node Managers are then queried for the path to the
data directory of the FIRST of 'pipelines' entries. At
this point, pipeselect builds a list of nfiles paths
the nfiles could be distributed to. The paths follow
the IRAF networking syntax.

It is important to understand that ONLY the FIRST entry
in the pipelines input variable is used to actually
distribute data. The other pipeline names are used as
additional contraint in the node selection process.

In other words, pipeselect answers the question:
'I want to distribute nfiles files to a given pipeline (A)
running on different nodes. I want to make sure that the 
files end up on nodes where these other pipelines are
running. I am aware of the fact that the first pipeline (A)
will actually receive the files in its data directory, but
I want to make sure that the other pipelines are running on 
the nodes my files will be copied onto. Where shall I copy 
the files?'


We now handle negative bias values: a negative bias is
equivalent of forcing the query of the localhost alone.
"""


import string, os, sys, glob
from pipeutil import *
import socket
from math import ceil
from random import shuffle

# Constants
PREFIX = 'pipeselect'


def get_node_port ():

    """
    Returns a list of node:port couples of
    machines running the Node Manager and ports 
    the Node Managers listen at (one port per
    machine).
    
    This routine gets this information from the local
    Node Manager, listening at port $NHPPS_PORT_NM, which in
    turn, gets the information from the Directory Server
    
    Input
    NONE
    
    Output
    {<host>:<port>, [...]}  dictionary of host:port couples
    {error:error_mesage}       in case of error
    """

    try:
        output = getNodeManagers ()
    except:
        output = {'error':'The local Node Manager is down. (%s:%s).' % (NODE, NHPPS_PORT_NM)}
        pass
    
    return output


def readConfig (pipeApp=None):
    
    """
    Let's see if we have a config file. It should be:
    $NHPPS_PIPEAPPSRC/config/pipeselect.config or
    $NHPPS_PS_CONFIG or
    $NHPPS/config/pipeselect.config
    If no file is found, no special behaviour is assumed.
    """
    
    config = None
    if not pipeApp and 'NHPPS_SYS_NAME' in os.environ:
        pipeApp = os.environ ['NHPPS_SYS_NAME']
        pass
    if 'NHPPS_PIPEAPPSRC' in os.environ:
        cfName = os.path.join (
            os.environ ['NHPPS_PIPEAPPSRC'], 'config', 'pipeselect.config')
    elif 'NHPPS_PS_CONFIG' in os.environ:
        cfName = os.environ ['NHPPS_PS_CONFIG']
    elif 'NHPPS' in os.environ:
        cfName = os.path.join (
            os.environ ['NHPPS'], 'config', 'pipeselect.config')
    else:
        cfName = None
        pass
    try:
        config = readConfFile (cfName)
    except:
        config = None
    return config


def readExclusion (pipeApp=None):
    
    """
    Let's see if we have exclusion files pipeselect.exclude*. Look in:
	$HOME/pipeselect.exclude*
	$PipeConfig/pipeselect.exclude*
	$NHPPS_PIPEAPPSRC/config/pipeselect.exclude*
	$NHPPS/config/pipeselect.exclude*
	glob ($NHPPS_PS_EXCLUDE)
    If no file is found, no special behaviour is assumed.
    """
    
    files = glob.glob (os.path.join (os.environ ['HOME'],
	'pipeselect.exclude*'))
    if 'PipeConfig' in os.environ:
        files += glob.glob (os.path.join (os.environ ['PipeConfig'],
	    'pipeselect.exclude*'))
    if 'NHPPS_PIPEAPPSRC' in os.environ:
        files += glob.glob (os.path.join (os.environ ['NHPPS_PIPEAPPSRC'],
	    'config/pipeselect.exclude*'))
    if 'NHPPS' in os.environ:
        files += glob.glob (os.path.join (os.environ ['NHPPS'],
	    'config/pipeselect.exclude*'))
    if 'NHPPS_PS_EXCLUDE' in os.environ:
        files += glob.glob (os.environ ['NHPPS_PS_EXCLUDE'])

    exclude = None
    for cfName in files:
	try:
	    exclude1 = readConfFile (cfName)
	except:
	    pass
	else:
	    if not exclude:
		exclude = {}
	    for key in exclude1:
		if key in exclude:
		    exclude[key][0] += ' ' + exclude1[key][0]
		else:
		    exclude[key] = exclude1[key]
    return exclude


def main (pipeApp, pipelines, nfiles, bias, space_per_node, delim1='!', delim2=' '):
    
    if not pipeApp and 'NHPPS_SYS_NAME' in os.environ:
        pipeApp = os.environ ['NHPPS_SYS_NAME']
        pass
    
    # Check for, and read a configuration file
    config = readConfig (pipeApp)
    
    # Check for and read an 'Exclusion' list file
    exclude = readExclusion (pipeApp)
    
    """
    Let's now check if we have any requirements on our main pipeline.
    Requirements, if any, are 
    all: we can select every node in the network
    self: only select the localhost
    other: select any node in the network BUT the localhost
    """
    
    mainPipe = pipelines.split ()[0]
    requirement = None
    if config and mainPipe in config:
        requirement = config[mainPipe][0].lower ()
        pass
    
    excludedNodes = None
    if exclude:
        if 'all' in exclude:
            excludedNodes = exclude['all'][0]
            pass
        if mainPipe in exclude:
            excludedNodes += ' ' + exclude[mainPipe][0]
            pass
        pass
    
    """
    Figure out the node:port list
    """
    
    node_port = get_node_port ()
    if 'error' in node_port:
        warning (PREFIX, node_port ['error'])
        return
    
    if requirement == 'self' or bias < 0:
        nodes = [NODE]
        space_per_node = 0
    else:
        nodes = node_port.keys ()
        # If necessary, remove the localhost.
        if requirement == 'other' and NODE in nodes:
            nodes.pop (nodes.index (NODE))
            pass
        pass
    
    # If necessary, exclude nodes
    if excludedNodes:
        excludedNodes = excludedNodes.lower ()
        excludedNodes = excludedNodes.split ()
        i = 0
        while i < len (nodes):
            try:
                index = excludedNodes.index (nodes[i])
            except:
                index = -1
            if index >= 0:
                nodes.pop (i)
            else:
                i += 1
                pass
            pass
        pass
    
    # connect to the nodes and retrieve their load info
    nodeData = {} # openDS: [(node,dataDir)]
    numNodes = 0  # number of nodes lsited in nodeData
    
    getQueue = {  'COMMAND': 'getOpenDatasetCount_pipes',
                'PIPELINES': pipelines,
                  'pipeapp': pipeApp
               }
    
    for node in nodes:
        if node not in node_port:
            continue
        
        # get the load info
        sock = connect (node, node_port[node])
        if not sock:
            # the node is down
            continue
        
        dq = execute (sock, getQueue)
        
        disconnect (sock)
        if dq == -1:
            # error in client-server communication
            continue
        
        # in case of success, we have a 'list' key
        # the corresponding value has the form
        # <pipeline>:<number of files> [<pipeline>:<number of files>]
        # Split the space separated list...
        try:
            pipe_queues = dq['list'].split ()
        except:
            continue
        
        # make sure that all the pipelines are
        # running on the node (queue != -1)
        skip_it = False
        for pq in pipe_queues:
            pipe, queue = pq.split(':')
            
            try:
                queue = float (queue)
            except:
                # queue is not a number: syntax error
                # skip the node.
                skip_it = True
                break
            if queue < 0:
                # either the pipeline is not running
                # or something wrong happened on that
                # node. Skip it.
                skip_it = True
                break
            pass
        
        if skip_it:
            # skip the node
            continue
        
        # the first pipeline rules:
        try:
            pipe, q = pipe_queues[0].split (':')
        except:
            continue
        pipe = pipe.strip ()
        q = float (q)
        
        # get the pipeline data - the first pipeline rules.
        sock = connect (node, node_port[node])
        if not sock:
            # the node is down
            continue
        
        # query the Node Manager for the directory
        # path and free space, given the input
        # pipeline(s)
        d = {}
        d ['command']   = 'get_dir'
        d ['pipelines'] = pipe
        d ['pipeapp']   = pipeApp
        
        if space_per_node:
            d ['check_space'] = 'yes'
        else:
            d ['check_space'] = 'no'
            pass
        
        ddir = execute (sock, d)
        disconnect (sock)
        
        if ddir == -1:
            # error in client-server communication
            continue
        
        fields = ddir['datadirs'].split (':')
        
        try:
            trigdir = fields[1].strip ()
	    rootdir = fields[2].strip ()
	    indir   = fields[3].strip ()
	    datadir = fields[4].strip ()
	    obsdir  = fields[5].strip ()
	    errdir  = fields[6].strip ()
            if space_per_node:
                space = long (fields[7])
                pass
            
            if trigdir == '-1' or rootdir == '-1' or (space_per_node and space == -1):
                # Something bad happened, skip the node but output an error message
                raise Error
            pass
        
        except:
            #sys.stderr.write ('Warning: get_dir failed for ' + pipe + '\n')
            #sys.stderr.write ('         response: ' + ddir['datadirs'] + '\n')
            continue
        
        # saving the data
        if q not in nodeData:
            nodeData[q] = []
            pass
        
        if space_per_node:
            nodeData[q].append ( (node,trigdir,rootdir,indir,datadir,obsdir,errdir,space) )
        else:
            nodeData[q].append ( (node,trigdir,rootdir,indir,datadir,obsdir,errdir) )
            pass
        
        numNodes += 1
        
        # print (node + ': ' + str (int (space / space_per_node)))
        pass
    
    pipes = pipelines.split ()
    # do we not have any data?
    if not nodeData:
        if len (pipes) > 1:
            pipes = string.join (pipes, (' and '))
            hostText = 'hosts are'
        else:
            pipes = pipes[0]
            hostText = 'host is'
            pass
        
        if space_per_node:
            spaceReq = ' with enough available disk space'
        else:
            spaceReq = ''
            pass
        
        #fatalError (PREFIX, 'No %s running %s%s at the moment.' % (hostText,pipes,spaceReq))
        warning (PREFIX, 'No %s running %s:%s%s at the moment.' % (hostText,pipeApp,pipes,spaceReq))
	#exit (1)
        return # 1
    
    # Check for any nodes which do not have enough space and remove them
    if space_per_node:
        # Get a list of keys into the nodeData dictionary
        nodeKeys = nodeData.keys ()
        keyIndex = 0
        while keyIndex < len (nodeKeys):
            key = nodeKeys[keyIndex]
            nodeIndex = 0
            while key in nodeData and nodeIndex < len (nodeData[key]):
                if nodeData[key][nodeIndex][-1] < space_per_node:
                    # The node does not have sufficient disk space
                    data = nodeData[key][nodeIndex]
                    sys.stderr.write ('Only %dK free in %s%s%s while we require %dK\n' % (data[2], delim1, data[0], data[1], space_per_node))
                    sys.stderr.write ('\tRemoving %s from list of nodes.\n' % (data[0]))
                    del nodeData[key][nodeIndex]
                    numNodes -= 1
                    if not nodeData[key]:
                        del nodeData[key]
                        pass
                    pass
                else:
                    # The node is fine, strip the disk space from the data
                    nodeData[key][nodeIndex] = nodeData[key][nodeIndex][:7]
                    nodeIndex += 1
                    pass
                pass
            keyIndex += 1
            pass
        
        # Do we not have enough space?
        if not len (nodeData):
            #fatalError(PREFIX, 'Not enough available space in the network.')
            warning (PREFIX, 'Not enough available space in the network.')
            return # 1
        pass
    
    # Ensure that we do not have any 'errant' keys...
    nodeKeys = nodeData.keys ()
    
    # If all we want is a list of the nodes which COULD run the pipeline...
    if nfiles == 0:
        retList = []
        nodeKeys.sort()
        for val in nodeKeys:
            shuffle (nodeData[val])
            for (node,trigdir,rootdir,indir,datadir,obsdir,errdir) in nodeData[val]:
                retList.append ('%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/' %
		    (node, delim1, trigdir,
		    delim2, node, delim1, rootdir,
		    delim2, node, delim1, indir,
		    delim2, node, delim1, datadir,
		    delim2, node, delim1, obsdir,
		    delim2, node, delim1, errdir))
                pass
            pass
        return retList
    
    # Determine the number of currently open datasets on the 'primary' pipeline
    openDS = 0
    for val in nodeKeys:
        openDS += (val * len (nodeData[val]))
        pass
    
    # Sort the node data by the number of currently open datasets
    # from highest to lowest, to reduce processing in the loop
    nodeKeys.sort()
    nodeKeys.reverse()
    
    newOpenDS = float(openDS + nfiles)
    # Determine if any node is already processing at least 'load' datasets
    working = True
    while working:
        # Calculate the desired target load:
        if nodeData:
            load = ceil (newOpenDS / numNodes)
        else:
            load = 0
            pass
        
        updatedNodeList = False
        for val in nodeKeys:
            if val >= load:
                newOpenDS -= (val * len (nodeData[val]))
                numNodes -= len (nodeData[val])
                nodeKeys.remove (val)
                del nodeData[val]
                updatedNodeList = True
                break
            pass
        
        if not updatedNodeList:
            working = False
            pass
        pass
    
    # Reverse the order of nodeKeys, so that they are sorted by lowest
    # to highest number of open datasets. Also, shuffle the order of the
    # nodes around in a random fashion.
    nodeKeys.reverse()
    for val in nodeKeys:
        shuffle (nodeData[val])
        #print val, nodeData[val]
        pass
    
    # Compute the number of files per node:
    hosts = []
    numFiles = nfiles
    for val in nodeKeys:
        for (node,trigdir,rootdir,indir,datadir,obsdir,errdir) in nodeData[val]:
            if numFiles:
                n = float (load - val)
                if n > numFiles:
                    n = float (numFiles)
                    pass
                numFiles -= n
                hosts.append ([n, node, trigdir, rootdir, indir, datadir, obsdir, errdir])
            else:
                break
            pass
        pass
    
    # Print IRAF format directories for the location
    # in which to place trigger files
    retList = []
    n = 0
    for (ds,node,trigdir,rootdir,indir,datadir,obsdir,errdir) in hosts:
        for i in xrange (int (ds)):
            retList.append ('%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/' %
	        (node, delim1, trigdir,
		delim2, node, delim1, rootdir,
		delim2, node, delim1, indir,
		delim2, node, delim1, datadir,
		delim2, node, delim1, obsdir,
		delim2, node, delim1, errdir))
            n += 1
            pass
        pass
    
    # did we forget any file?
    delta = int (nfiles) - n
    for i in xrange (delta):
        retList.append ('%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/%s%s%s%s/' %
	    (hosts[-1][1], delim1, hosts[-1][2],
	    delim2, hosts[-1][1], delim1, hosts[-1][3],
	    delim2, hosts[-1][1], delim1, hosts[-1][4],
	    delim2, hosts[-1][1], delim1, hosts[-1][5],
	    delim2, hosts[-1][1], delim1, hosts[-1][6],
	    delim2, hosts[-1][1], delim1, hosts[-1][7]))
        pass
    return retList
