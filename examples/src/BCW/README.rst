===
BCW
===
------------------------------------
Basic Calibration Workflow Test Case
------------------------------------

:Manual Section: NHPPS
:Manual Group:   Demonstration Pipeline Applications
:Date:           June 24, 2010

Description
===========

The **Basic Calibration Workflow (BCW)** was defined at a Science Data
Processing workshop held at the Space Telescope Science Institute on
January 25-26, 2010.  The BCW is not a real pipeline application but a
demonstration workflow which astronomers will recognize as a practical
use case and which pipeline workflow developers can implement within
different frameworks.  To this end the workflow steps or stages have
names, descriptions, and outputs that make sense in a real BCW but the
actual core processing modules are dummy placeholders and the data files
are not bulk image data.

The basic calibration workflow consists of the following logical steps.  The
implementation or simulation of the step in the demonstration is also
described.

1.  Get a raw (uncalibrated) mosaic camera exposure to process. The
    exposure data is a multi-extension format (MEF) FITS file having a
    data-less primary (global) header and extensions for each CCD detector
    making up the mosaic camera.

    Demonstration:
	Getting a raw mosaic camera exposures consists of creating a simple
	text file which looks like a header listing of an MEF FITS file.
	There are no actual pixels.  The example has one global header
	and three extension headers.

2.  Split up the MEF file into separate CCD image FITS files for
    parallel calibration.

    Demonstration:
	Splitting up the MEF file consists of separating the text file
	into smaller text files for each FITS header and data unit (HDU).
	This produces four pseudo-FITS files, the global header and the
	CCD extensions.

3.  Send the CCD images to multiple instances of a CCD calibration
    sub-pipeline.  These may be parallel processes on the same node or a set
    of available processing nodes.

    Demonstration:
	The CCD FITS files are sent to instances of a CCD sub-pipeline
	using NHPPS method of discovering available nodes and pipelines.

4.  The CCD sub-pipeline applies a flux calibration, typically
    overscan, bias, dark, and dome flat calibrations.  The calibration
    or reference files are assumed to be available but the BCW doesn't
    reflect how this is done for the sake of simplicity.

    Demonstration:
	The flux calibration consists of adding a keyword to the
	pseudo-FITS file and sleeping for 5 seconds.

5.  The CCD sub-pipeline applies a astrometric calibration.  Typically
    this consists of matching stars in the field to astrometric reference
    stars in a refrence catalog.  A world coordinate system (WCS) is derived
    by fitting a functional transformation between reference celestial
    coordinates and pixel coordinates as produced by a source detection
    step.  The WCS is associated with the image data through header keywords.

    Demonstration:
	The asatrometric calibration consists of adding a keyword to
	the pseudo-FITS file and sleeping for 5 seconds.

6.  The calibrated CCD images are collected together from the various
    calibration sub-pipeline instances.

    Demonstration:
	The collecting of the parallel results is another NHPPS function.

7.  The final data products are produced.  In this BCW this consist of creating
    a new MEF file which is the calibrated version of the mosaic exposure.

    Demonstration:
	Making the final data product consists of putting the individual
	pseudo-FITS text files back into a single psuedo-FITS MEF file.

8.  The calibrated mosaic exposure is disposed of in some way.

    Demonstration:
	There is no step of disposing of the final data product.
	But in the demonstration control program **runbcw** the raw and
	calibrated files are paged as header listings.

The demonstration command: runbcw
=================================

A script command, **runbcw**, is provided to describe what is happening,
pace the steps, run a simple monitor program while the processing is
going on, and show the final results.

Once you have used this program you can examine the script and understand the
NHPPS commands being used.   All the stages that make up the processing are
also simple scripts which can be examined.
