<html><head><title>Basic Calibration Workflow (BCW)</title></head><body>

<center>
<h2>Basic Calibration Workflow (BCW)</h2>
<h4> F. Valdes, August 2010</h4>
</center>

<h3> 1. Description </h3>

<p> The <i>Basic Calibration Workflow (BCW)</i> was defined at a <b>Science
Data Processing</b> workshop held at the Space Telescope Science Institute
on January 25-26, 2010.  The BCW is not a real pipeline application but
a demonstration workflow which astronomers will recognize as a practical
use case and which pipeline workflow developers can implement within
different frameworks.  To this end the workflow steps or stages have
names, descriptions, and outputs that make sense in a real BCW but the
actual core processing modules may be dummy placeholders and the data
files may not be actual bulk image data.

<p> Figure 1 graphically demonstrates the basic sequence of actions that
make up the BCW.  In the figure time goes down.  The columns represent
different actors and the arrows are potential transitions between
actors. In this case, the user is the actor sending data or triggers for
the processing.  The other two columns represent computational nodes
or pipelines.  Note that the two computational columns need not be on
separate nodes nor a single node.  A high performance implementation
might parallelize the "ccd" column with multiple nodes carrying out the
same steps with instances of the same pipeline.  This parallelization is
represented by the dashed arrow.

<h4> Figure 1: Generic BCW Sequence Diagram </h4>

<div class=wsd wsd_style="modern-blue"><pre>
participant user
participant mos
participant ccd
note over mos
process mef
mosaic
end note
user->mos:
activate mos
note left of mos
1. get mosaic
exposure
2. split mosaic
into CCD pieces
3. submit CCD pieces
for calibration
end note
note over ccd
process
ccds
end note
mos-->ccd:
deactivate mos
activate ccd
note left of ccd
4. flux calibrate
5.astrometric calibrate
end note
ccd-->mos:
deactivate ccd
activate mos
note left of mos
6. collect CCDs
7. make MEF
8. etc.
end note
mos->user:
deactivate mos
</pre></div><script type="text/javascript" src="http://www.websequencediagrams.com/service.js"></script> 


<ol>
<li> Get a raw (uncalibrated) mosaic camera exposure to process. The
exposure data is a multi-extension format (MEF) FITS file having a
dataless primary (global) header and extensions for each CCD detector
making up the mosaic camera.  Getting calibration data is implicit
in this workflow.  The NOAO demonstration implementations this means
creating a pseudo MEF file.

<li> Split up the MEF file into separate CCD FITS image file for
sequential or parallel calibration.

<li> Send the CCD images to multiple instances of a CCD calibration
sub-pipeline.  These may be parallel processes on the same node or a set
of available processing nodes.

<li> The CCD actor (pipeline) applies a flux calibration,
typically overscan, bias, dark, and dome flat calibrations.  The
calibration or reference files are assumed to be available but the BCW
doesn't reflect how this is done for the sake of simplicity.

<li> The CCD actor (pipeline) applies a astrometric calibration.  Typically
this consists of matching stars in the field to astrometric reference
stars in a refrence catalog.  A world coordinate system (WCS) is derived
by fitting a functional transformation between reference celestial
coordinates and pixel coordinates as produced by a source detection
step.  The WCS is associated with the image data through header keywords.

<li> The calibrated CCD images are collected together from the various
CCD calibration pipelines.

<li> The final data products are produced.  In this NOAO BCW demonstrations
this consist of creating a new MEF file which is the calibrated version
of the mosaic exposure.

<li> The calibrated mosaic exposure is disposed of in some way.  This
might include user interactions or archive interactions.

</ol>

<p> In the next two sections we describe working implementations of the BCW.
These implementations are related as explained in those sections.

<h3> 2. NOAO High Performance Pipeline Implementation </h3>

<p> The NHPPS framework, as used at NOAO, consists of long
lasting server processes: the key ones being a <i>node managers
(NM)</i> for each node and <i>pipeline managers (PM)</i> for
each pipeline on each node.  For more on the architecture see <a
href="http://chive.tuc.noao.edu/noaodpp/Pipeline/PL001.pdf">Valdes,
et. al., 2006, <i>The NOAO High-Performance Pipeline System</i></a>.
So as part of the BCW demonstration, the framework is first started and
then the example dataset is triggered.

<p> Data processing consists of submitting pieces of data, called
<i>datasets</i> to the pipelines which then sequence them through the
various steps.  Multiple datasets can be going through the same pipeline
in parallel.  In this demonstration the initial dataset is a single mosaic
MEF and the sequence diagram represents processing on this single dataset.
For the ccd pipeline there are actually multiple datasets processing on
the same node or distributed nodes.

<h4> Figure 2: NHPPS BCW Sequence Diagram </h4>

<div class=wsd wsd_style="modern-blue"><pre>
participant nhpps
participant mos
participant ccd
note over mos
process mef
mosaic
end note
nhpps->mos:
activate mos
mos->mos: mosgetdata
mos->mos: mossplit
mos->mos: mos2ccd
note over ccd
process
ccds
end note
mos-->ccd:
activate ccd
ccd->ccd: ccdsetup
ccd->ccd: ccdfluxcal
ccd->ccd: ccdwcscal
ccd-->mos:
deactivate ccd
mos->mos: mosmef
mos->nhpps:
deactivate mos
</pre></div><script type="text/javascript" src="http://www.websequencediagrams.com/service.js"></script>

<h4> 2.1 The NHPPS BCW Demonstration Modules </h4>

<p> The steps in the BCW have been implemented in the NHPPS BCW
Demonstration as CSH scripts.  These modules correspond directly with what
is shown in figure 2.  As noted previously, the modules are not necessarily
meant to process astronomical data but to act as placeholders
or simulations of the processing.

<DL>
<DT> mosgetdata
<DD> Getting a raw mosaic camera exposures consists of creating a simple text
file which looks like a header listing of an MEF FITS file.  There are
no actual pixels.  The example has one global header and three extension
headers.
<DT> mossplit
<DD> Splitting up the MEF file consists of separating the text file
into smaller text files for each FITS header and data unit (HDU).
This produces four pseudo-FITS image files, the global header and the
CCD extensions. As with the MEF file, the image files are a simple text
file which looks like a header listing.
<DT> mos2ccd
<DD> Preparing the data for transfer to a CCD pipeline consists
of making a file with the URI's to the CCD files to be processed.  The
execution framework typically will make use of this file.
<DT> ccdsetup
<DD> Setting up for processing a single CCD image consists
of moving the file, whose URI is received from the list prepared by
mos2ccd, to a working area for this specific image.
<DT> ccdfluxcal
<DD> Flux calibration consists of adding a keyword to the pseudo-FITS
image file and sleeping for a number of seconds to simulate computing.
<DT> ccdwcscal
<DD> The astrometric calibration consists of adding a keyword to the
pseudo-FITS file and sleeping for a number of seconds to simulate
computing.
<DT> mosmef
<DD> Making the final data product consists of putting the individual
pseudo-FITS text files back into a single psuedo-FITS MEF file.
A listing of this final data product will then show keywords indicating
each CCD was flux and astrometrically calibrated.
</DL>






<h3> 3. Open Grid Computing Environment (OGCE) Hybrid Adaptation </h3>

<p> This section describes a BCW implementation that is a hybrid using
both the <a href="http://www.collab-ogce.org/ogce/index.php/Main_Page">
Open Grid Computing Environment (OGCE)<a> and NHPPS execution frameworks.
In this case the stages and modules executing those stages are the same
as the native NHPPS implementations described in section 2.  A goal of
the hybrid approach is to allow developing pipeline applications that
run under both pipeline frameworks with trivial porting effort.

<p> In the hybrid approach (to be described in Valdes, 2011, ADASS
Proceedings) the NHPPS pipelines making up the pipeline application
are wrapped into web services, constructed into workflows, and managed
and executed by OGCE.  The primary purpose of OGCE is to enable this
execution to take place on the Grid.

<p> The role of NHPPS in this approach is to execute the pipeline stages
within a web service.  The key differences in this case as compared to what
is described in section 2 are that the NHPPS components run internally
and independently within the services, any transfer of control between
pipelines managed by NHPPS must be done by OGCE, and only a single dataset
is processed by a particular service instance.

<p> An OGCE workflow consists of services which must be representable as
a directed acyclic graph (DAG); specifically with no connections back to a
previous service.  For NHPPS pipelines that have only an initial starting
point and a final endpoint conversion to an OGCE service is simple.
But pipelines which logically call and wait for returns from other
pipelines within their workflow (see the mos pipeline in figure 2) must be
restructured.  The standard wrapper application, <tt>nhppsapp</tt>, that
converts NHPPS pipelines into independent applications for wrapping into
OGCE services automatically breaks up these pipelines into separate pieces.

<p> Figure 3 illustrates all these points.  The sequence shows starting
and stopping internal instances of NHPPS and the single NHPPS mos pipeline
from section 2 broken up into two pieces (mos1 and mos2) such that the
flow is always from left to right.

<h4> Figure 3: OGCE/NHPPS BCW Hybrid Sequence Diagram </h4>

<div class=wsd wsd_style="modern-blue"><pre>
participant ogce
participant mos1
participant ccd
participant mos2
note over mos1
process mef
mosaic
end note
ogce->mos1:
activate mos1
mos1->mos1: start NHPPS
mos1->mos1: mosgetdata
mos1->mos1: mossplit
mos1->mos1: mos2ccd
mos1->mos1: stop NHPPS
mos1->mos1: OGCE output
note over ccd
process
ccds
end note
mos1-->ccd:
deactivate mos1
activate ccd
ccd->ccd: start NHPPS
ccd->ccd: ccdsetup
ccd->ccd: ccdfluxcal
ccd->ccd: ccdwcscal
ccd->ccd: stop NHPPS
ccd->ccd: OGCE output
note over mos2
finish mef
processing
end note
ccd-->mos2:
deactivate ccd
activate mos2
mos2->mos2: start NHPPS
mos2->mos2: mosmef
mos2->mos2: stop NHPPS
mos2->mos2: OGCE output
mos2->ogce:
deactivate mos2
</pre></div><script type="text/javascript" src="http://www.websequencediagrams.com/service.js"></script>

<p> The "OGCE output" step of the diagram represent the part of the
standard wrapper that collects all the information from the NHPPS pipeline
and converts it into OGCE service outputs.  Some of these outputs are
what are then connect to inputs in other services in a workflow.

<p> There are actually two implementations of the workflow shown in
figure 3.  One is the actual OGCE implementation with web services
and OGCE managed workflow.  As a simplication of this is a CSH script
implementation where the web service wrappers are replaced by direct calls
to the applications (the standard wrapping of a pipelines with internal
NHPPS instances), and the service input/output parameters and connections
between the services handled by the script.


<h5> Acknowledgements </h5>

<p> The sequence diagrams are generated by <a href="http://www.websequencediagrams.com">WebSequenceDiagrams</a>.

</body></html>
