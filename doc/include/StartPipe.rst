This is the standard *StartPipe* stage for a dataset which:

    * responds to the trigger file
    * creates the working directory
    * creates initial parameter directory
    * creates a blackboard entry
    * checks the input exposure list for duplicate values

There is logic to allow re-entry to this stage.

Input
=====

* trigger file <dataset>.exptrig
* exposure list <dataset>.exp

Output
======

* creates working directories
* blackboard entry
* log file
* standard output

Dependencies
============

* checks the blackboard with ``osf_test``
* sets uparm directory 

Exit Actions
============

This stage has two normal exit actions.

:0:  Error
:1:  Success
