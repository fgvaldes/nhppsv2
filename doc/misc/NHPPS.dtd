<!--
The System element is the root of Instrument level pipeline definitions to
include 'global' Pre & Post processing actions as well as constraints on
computing resources.
-->
<!ELEMENT System
    (
        Description,
        Directories?,
        MaxExec?,
        MinDisk?,
        PreProcAction?,
        PostProcAction*
    )
>

<!ATTLIST System
    maxActive CDATA '0'
>

<!--
The Pipeline element is the root of a pipeline for an instrument. Values which
are not defined at this level are taken from the System element. Each 'pipe'
within the instruments pipeline has an xml file which declares a single
Pipeline element.
-->
<!ELEMENT Pipeline
    (
        Description,
        Directories?,
        MaxExec?,
        MinDisk?,
        PreProcAction?,
        PostProcAction*,
        Module+
    )
>

<!ATTLIST Pipeline
    system    CDATA #REQUIRED
    name      CDATA #REQUIRED
    poll      CDATA #REQUIRED
    maxActive CDATA '0'
>

<!--
The Description tag is used to provide a region in which the purpose of the
System, Pipeline, or Module may be described. This is a required value, and
should be filled in with meaningful information.
-->
<!ELEMENT Description
    (
        #PCDATA
    )
>

<!--
The Directories tag is used to specify the locations of the directories the
pipeline should use to store its data, read input, etc...
The trigger directory is actually set by the environment variable
NHPPS_TRIG if present otherwise the default below is used.  The environment
variable is directly substituted and may contain a string like
'${Pipeline.Directories.root}/input'.
-->
<!ELEMENT Directories
    EMPTY
>

<!ATTLIST Directories
    root  CDATA 'ENV(NHPPS_DATA)/CAPS(${Pipeline.system}/${Pipeline.system}_${Pipeline.name})'
    trig  CDATA '${Pipeline.Directories.root}/input'
    input CDATA '${Pipeline.Directories.root}/input'
    data  CDATA '${Pipeline.Directories.root}/data'
    obs   CDATA '${Pipeline.Directories.root}/obs'
    error CDATA '${Pipeline.Directories.root}/err'
>

<!--
The MaxExec tag provides a means to constrain the amount of time that a Modules
ProcAction is allowed before being killed as a hung process.
-->
<!ELEMENT MaxExec
    EMPTY
>

<!ATTLIST MaxExec
    time   CDATA #REQUIRED
    status CDATA '0'
>

<!--
The MinDisk tag provides a mechanism by which a Modules ProcAction may be
bypassed if the Node on which it is running does not have sufficient disk space
available.
-->
<!ELEMENT MinDisk
    EMPTY
>

<!ATTLIST MinDisk
    space  CDATA #REQUIRED
    status CDATA '0'
>

<!--
A PreProcAction describes the actions that are to occur when a Module is
executed, in order to prepare for the Modules ProcAction. There may only be one
PreProcAction per Module, however a PreProcAction may have several commands.
-->
<!ELEMENT PreProcAction
    (
        Foreign       |
        PlugIn        |
        OSFUpdate     |
        OSFClose      |
        OSFFinal      |
        OSFOpen       |
        RenameTrigger |
        UpdateTrigger |
        RemoveTrigger
    )+
>

<!--
The PostProcAction describes the actions a Module should take after the
ProcAction, based upon the value of the ExitCode from the ProcAction. There may
be several PostProcActions in a Module, to specify different actions for
different types of results from the ProcAction.
-->
<!ELEMENT PostProcAction
    (
        ExitCode+,
        (
            Foreign       	| 
            PlugIn        	|
            OSFUpdate     	|
            OSFClose      	|
            OSFFinal      	|
            OSFConditRemove	|
            OSFOpen       	|
            RenameTrigger 	|
            UpdateTrigger 	|
            RemoveTrigger
        )+
    )
>

<!ATTLIST PostProcAction
    isFailure CDATA 'False'
    isGlobal CDATA 'False'
>

<!--
The Module element defines the actions a Module should execute, as well as
overrides for default values (or values defined globally for the Pipeline or
System).
-->
<!ELEMENT Module
    (
        Description,
        Trigger+,
        MaxExec?,
        MinDisk?,
        PreProcAction?,
        ProcAction,
        PostProcAction*,
        Directories?
    )
>

<!ATTLIST Module
    name      CDATA #REQUIRED
    maxActive CDATA '0'
    isActive  CDATA 'True'
>

<!--
The Foreign tag provides a pipeline architect the ability to launch programs as
a valid action within the pipeline. This allows the pipeline to call any
program on the system allowing flexability in the design/coding of the
scientific modules which compose the core code of the pipeline.
-->
<!ELEMENT Foreign
    EMPTY
>

<!ATTLIST Foreign
    argv CDATA #REQUIRED
>

<!--
The StartPipe tag provides a pipeline architect the ability to
start a pipeline dataset.  Currently this is a synoym for Foriegn and
the tag is for external parsers to recognized this type of action.
-->
<!ELEMENT StartPipe
    EMPTY
>

<!ATTLIST StartPipe
    argv CDATA #REQUIRED
>

<!--
The CallPipe tag provides a pipeline architect the ability to
call other pipelines.  Currently this is a synoym for Foriegn and
the tag is for external parsers to recognized this type of action.
-->
<!ELEMENT CallPipe
    EMPTY
>

<!ATTLIST CallPipe
    argv CDATA #REQUIRED
>

<!--
The WaitPipe tag provides a pipeline architect the ability to
wait for other pipelines.  Currently this is a synoym for Foriegn and
the tag is for external parsers to recognized this type of action.
-->
<!ELEMENT WaitPipe
    EMPTY
>

<!ATTLIST WaitPipe
    argv CDATA #REQUIRED
>

<!--
The DonePipe tag provides a pipeline architect the ability to
complete a pipeline dataset.  Currently this is a synoym for Foriegn and
the tag is for external parsers to recognized this type of action.
-->
<!ELEMENT DonePipe
    EMPTY
>

<!ATTLIST DonePipe
    argv CDATA #REQUIRED
>

<!--
-->
<!ELEMENT PlugIn
    EMPTY
>

<!ATTLIST PlugIn
    argv CDATA #REQUIRED
>

<!--
The OSFUpdate tag allows the pipeline system to issue an OSFUpdate call on the
current dataset.
-->
<!ELEMENT OSFUpdate
    EMPTY
>

<!ATTLIST OSFUpdate
    argv CDATA #REQUIRED
>

<!--
Closes an OSF BlackBoard entry.
-->
<!ELEMENT OSFClose
    EMPTY
>

<!--
Removes an OSF BlackBoard entry.
-->
<!ELEMENT OSFFinal
    EMPTY
>

<!--
Conditionally removes an OSF BlackBoard entry.
-->
<!ELEMENT OSFConditRemove
    EMPTY
>

<!--
Opens an OSF BlackBoard Entry.
-->
<!ELEMENT OSFOpen
    EMPTY
>

<!--
-->
<!ELEMENT RenameTrigger
    EMPTY
>

<!--
-->
<!ELEMENT UpdateTrigger
    EMPTY
>

<!ATTLIST RenameTrigger
    argv CDATA #REQUIRED
>

<!ELEMENT RemoveTrigger
    EMPTY
>

<!ATTLIST RemoveTrigger
    argv CDATA #REQUIRED
>

<!--
Provides an ExitCode which a PostProcAction listens for, if it is received, the
PostProcAction is performed.
-->
<!ELEMENT ExitCode
    EMPTY
>

<!ATTLIST ExitCode
    val CDATA #REQUIRED
>

<!--
-->
<!ELEMENT ProcAction
    (
        Foreign       |
        PlugIn        |
	StartPipe     |
	CallPipe      |
	WaitPipe      |
	DonePipe      |
        OSFUpdate     |
        OSFClose      |
	OSFFinal      |
        OSFOpen       |
        RenameTrigger |
        UpdateTrigger |
        RemoveTrigger
    )
>

<!--
-->
<!ELEMENT Trigger
    (
        FileRequirement |
        OSFRequirement  |
        TimeRequirement
    )+
>

<!ATTLIST Trigger
    conditional (AND|OR) 'AND'
>

<!--
-->
<!ELEMENT FileRequirement
    EMPTY
>

<!ATTLIST FileRequirement
    directory CDATA '${Pipeline.Directories.input}'
    fnPattern CDATA #REQUIRED
>

<!--
-->
<!ELEMENT OSFRequirement
    EMPTY
>

<!ATTLIST OSFRequirement
    argv CDATA #REQUIRED
>

<!--
-->
<!ELEMENT TimeRequirement
    EMPTY
>

<!ATTLIST TimeRequirement
    start    CDATA #IMPLIED
    end      CDATA #IMPLIED
    interval CDATA #REQUIRED
>
