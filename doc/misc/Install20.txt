Installation and Configuration Guide
NOAO High-Performance Pipeline System (2010)
with Basic Calibration Workflow (BCW) Pipeline Application

Francisco Valdes <fvaldes@noao.edu>


* Introduction

The NOAO High-Performance Pipeline System (NHPPS) is a lightweight
framework for event-based based pipelines. It is written in Python and,
as such, supports a wide variety of Operating Systems (e.g. Mac OS X,
Linux, Solaris etc.).  It requires Python 2.3.4 or later.  The other
class of source is csh scripts.  Some of the networking and configuration
assumes the pipeline accounts use csh/tcsh as the default login shell.
The distribution includes all the extra modules and packages you need and
there is no need to compile anything.  It comes with an example pipeline,
the Basic Calibration Workflow (BCW), and a script, runbcw, that
demonstrates it.

The installation directions and tips are limited to what is needed
to run the BCW demonstration.


* Obtaining NHPPS

NHPPS is available by anonymous ftp at

    ftp://iraf.noao.edu/misc/nhpps20/


* Installing and configuring NHPPS and the BCW Pipeline Application

Extract the contents of the compressed tar file in some directory.
Set the path to the directory in the environment variable NHPPS.
Source the file $NHPPS/config/BCW.csh.  This sets the a few
environment variables for the BCW demonstration pipeline application
and then sources $NHPPS/config/Setup.py which is the basic
setup for any NHPPS pipeline application.  In more advanced
usage the environment variables in this setup script can be set before
sourcing the file(s) to something other than the defaults.  Of course,
you could also modify the files or use a customized copy.

Clearly these steps can be defined in a shell file which is sourced
at login.  For the recommended csh shell this would be .cshrc and the
steps would be

    setenv NHPPS <some path>
    source $NHPPS/config/BCW.csh

The defaults for the pipeline application log and data directories are
for the supplied BCW which are within the NHPPS tree.  In production
these would be outside the tree on some large data partition.

* Network Environment

NHPPS is intended to be used in a networked environment with a
cluster of processing nodes, though a single node is a possible
case.  To use multiple nodes they must be configured so they
can communicate via sockets and password-less ssh.  Note that one
should be sure that each node can also ssh to itself.  Also the
nodes must be addressable by their "short" names, the ones without
periods.

For use with multiple nodes one may either have separate installations
on each node, or more simply, use an NFS server for the source.

For working directories NHPPS may be used either with separate local data
on each node's file system or with a shared field system.  For the BCW
demonstration, if the NHPPS installation is on an NFS file system then
the default data and log directories contained within the installation
will automatically provide a shared file system processing model.

An aspect of the networked environment is use of Pyro (a python remote
object system) for communication between the NHPPS python processes.
Pyro is included in the distribution but one should check if the name
server can be started and reached.  The script $NHPPS/config/configure.py
should be run to check the NHPPS installation.  If the network does not
support UDP broadcasting you will need to set PYRO_NS_HOSTNAME to point
to the machine running the (Pyro) Name Server.

To use multiple nodes one needs to define the environment variable
PROCNODES to be a comma separate list of nodes.  The nodes must satisfy
the above requiremets: password-less ssh, ability to communications with
a PYRO name server, an installation of NHPPS with .cshrc which includes
setting the desired NHPPS environment (e.g. setting the NHPPS path and
sourcing the BCW.csh file).

* Demonstration Example BCW Pipeline

To run the BCW pipeline execute the command runbcw which should be in
your path after sourcing BCW.csh.  This must be run in a terminal
window which
supports the "clear" command.
See the file $NHPPS/examples/src/BCW/README.rst for information about
the BCW demonstration.

* Support and Caveats

For support questions, please contact Frank Valdes at <valdes@noao.edu>

NHPPS is a fairly well developed system but, as with most non-commercial
products, the documentation is not complete.  However, there is still
a considerable amount of documentation distributed with the system.
We will be glad to help within our limited resources.  Those serious
about using NHPPS in a production system should plan to visit the NOAO
Pipeline Team in Tucson to learn about real production pipelines and
many of the details of the system and applications.
