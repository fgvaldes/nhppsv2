#!/bin/env pipecl
#
# DONE -- Finish up dataset processing.
#
# The end result of the pipeline is a list of all the products.
# It is up to later software to decide when to fetch and clean up the results.
# If the pipeline was triggered with a return file the list is returned in
# the file specified in the return file otherwise a file is produced in
# the output directory.

string  option = ""		# null, "empty", or pipeline to call
string	tpipe, idir, tdir

# Tasks and packages.
task $pipeselect = "$!pipeselect -ti $1 $2 1 0"
task $utouch = "$!touch $(*)"

# Get arguments.
i = fscan (cl.args, option)

if (option == "empty")
    tpipe = ""
else
    tpipe = option

# Create or extend dataset list.

# Remove temporary files.
delete ("*.tmp,*tmp.fits,*tmp.pl")

# Hide excluded files.  Note that files are still given in the list file.
if (access(".exclude.list")) {
    list = ".exclude.list"
    while (fscan(list,s1)!=EOF) {
	if (access(s1)) {
	    fspathnames (s1) | scan (s2)
	    print (s2//"-IGNORE", >> dataset.dlist)
	    rename (s1, "."//s1)
	}
	;
    }
}
;

# Create or add files in directory which are not hidden.
fspathnames ("*", sort-, >> dataset.dlist)

# Restore excluded files.
if (access(".exclude.list")) {
    list = ".exclude.list"
    while (fscan(list,s1)!=EOF) {
	if (access("."//s1))
	    rename ("."//s1, s1)
	;
    }
}
;

# Log the final data list.
concat (dataset.dlist) | tee (dataset.lfile, out="text", append+)

# Trigger a pipeline to handle the dataset list otherwise return to
# to the  parent pipeline.  When calling a target pipeline, the return
# file is also copied so it can return to the parent pipeline.

tdir = ""; idir = ""
if (tpipe != "") {
    printf("Calling %s ...\n", tpipe)
    pipeselect (nhppsargs.application, tpipe) | scan (tdir, idir)
    if (tdir != "")
	fspathnames (tdir) | scan (tdir)
    ;
    if (idir != "")
	fspathnames (idir) | scan (idir)
    ;
}
;
if (tpipe != "" && (tdir == "" || idir == "")) {
    printf ("ERROR: pipeline is not running (%s)\n", tpipe)
    plexit ERROR
}
;
if (tdir != "" && idir != "") {
    printf ("%s-%s/\n", dataset.name, tpipe) | scan (s1)
    if (access (idir//s1) == YES)
        idir += s1
    ;
    printf ("%s-%s.%s\n", dataset.name, tpipe, tpipe) | scan (s1)
    printf ("copy %s -> %s\n", dataset.dlist, idir//s1)
    copy (dataset.dlist, idir//s1)
    if (access (dataset.rfile)) {
	printf (".%s-%s.%srtn\n", dataset.name, tpipe, tpipe) | scan(s2)
	printf ("copy %s -> %s\n", dataset.rfile, idir//s2)
	printf ("%s %s\n", dataset.name, nhppsargs.pipeline) |
	    joinlines (dataset.rfile, "STDIN", maxchars=1024, > idir//s2)
	delete (dataset.rfile)
    }
    ;
    #touch (tdir//s1//"trig")
    utouch (tdir//s1//"trig")
} else {
    if (tpipe != "")
	printf ("WARN: pipeline is not running (%s)\n", tpipe)
    ;
    if (access (dataset.rfile)) {
	s2 = dataset.name
	s3 = nhppsargs.pipeline
	head (dataset.rfile, nlines=1) | scan (tdir, idir, s2, s3)
        if (tdir != "" && idir != "") {
	    fspathnames (tdir) | scan (tdir)
	    fspathnames (idir) | scan (idir)
	    printf ("%s.%s\n", s2, s3) | scan (s1)
	    if (option == "empty")
		#touch (idir//s1)
		utouch (idir//s1)
	    else
		copy (dataset.dlist, idir//s1)
	    #touch (tdir//s1//"rtntrig")
	    utouch (tdir//s1//"rtntrig")
	}
	;
    }
    ;
}

# Clean up dataset files in the input directory.
delete (nhppsargs.indir//dataset.name//".*", verify-)

plexit COMPLETED
