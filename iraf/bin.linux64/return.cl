#!/bin/env pipecl
#
## =========
## return.cl
## =========
## ------------------------
## respond to return events
## ------------------------
## 
## :Manual Group: NHPPS/iraf/bin
## :Manual Section: 1
## 
## Usage
## =====
## 
## ``return.cl``
## 
## Exit Status Values
## ==================
## 
## COMPLETED
##     Return event successfully handled.
## NODATA
##    No data returned with the event.
## FATAL
##    Undefined fatal error occured.
## 
## 
## Description
## ===========
## 
## Return events result in the folowing possible actions:
## 
##     1.  move event data to the dataset working directory
##     2.  append event data to the dataset list file
##     3.  append event log, if present, to dataset and process logs
##     4.  triggering new datasets from staged files
## 
## A return event consists of a trigger file in the trigger directory, an event
## data file in the input directory, and an optional event log file.  The files
## have a name consisting of the called dataset name, the first part of the
## extension (usually the called pipeline name though multiple return points
## use different identifiers), and the last part of the extension being ``rtn``
## for the trigger file, ``log`` for event log information, and nothing else
## for the event data file.
## 
## The module copies the event data file from the input directory to the
## dataset working directory and appends the contents to the dataset list file
## (``dataset.dlist``).  If there is no event data file the status value will
## be ``NODATA`` and the PDL can take appropriate action.  The contents of the
## optional event log file are appended to the dataset logfile
## (``dataset.lfile``) and written to the standard output (i.e. the module
## process log if run by a pipeline manager) after which it is deleted.
## 
## Once the event files are handled the module looks for staging files produced
## by ``call.cl``.  It looks for the directory ``nhppsargs.calldir`` and a file
## with the event data file name with an appended ``.node``.  If present it
## extracts the node name from the ``.node`` file and then deletes all files
## with the same event dataset basename, since the return indicates successful
## completion of the dataset.  Then the module looks for a file with the
## extension ``.tbd``.  If none are found it cleans up any files associated
## with the type of return event.
## 
## If a new dataset is found it renames the ``.tbd`` extension to ``.proc``.  It
## checks the the set of available pipelines for the same node used by the
## returned dataset.  If found it triggers the new dataset to that node using
## the data file previously prepared by ``call.cl`` and creates a ``.node``
## file for that dataset.  If the node is no longer available it restore the
## file to TBD status.
## 
## A check is made for new or dead nodes.  New nodes are those that were not
## present at the time ``call.cl`` or ``return.cl`` were last executed.
## Currently only one dataset is submitted to any new nodes found.  Dead nodes
## are thost that were present the last time ``call.cl`` or ``return.cl`` were
## executed but are no longer found by ``pipeselect``.  Datasets submitted to
## those nodes, as found in ``.node`` files, are returned to TBD status for
## submission by a subsequent return event.
## 
## Note that the return status must be handled by the PDL to normally
## decrement the blackboard field for the number of datasets originally
## defined by ``call.cl``.  When the value drops to zero all
## returns are assumed to have been handled.  Also note that the PDL
## must define only a single ``return.cl`` instance to run at a time to
## avoid concurrence problems.
## 
## See Also
## ========
## 
## call.cl

string	tfile, tdataset, tpipe, pipes, node, idir, tdir, exit
struct	new, *fd

# Tasks and packages.
task $pipeselect = "$!pipeselect -ti"
task $diff = "$!diff $(1) $(2)"

# Set names.
s1 = envget ("EVENT_FULL_NAME")
tfile = substr (s1, strldx("/",s1)+1, strstr("rtn",s1)-1)
tdataset = substr (tfile, 1, strldx(".",tfile)-1)
tpipe = substr (tfile, strldx(".",tfile)+1, 999)

# Handle the returned file(s).
if (access (nhppsargs.indir//tfile)) {
    move (nhppsargs.indir//tfile, ".")
    sort (tfile) | uniq (> tfile)
    concatenate (tfile, dataset.dlist, append+)
    exit = "COMPLETED"
} else if (access(tfile))
    exit = "COMPLETED"
else
    exit = "NODATA"

if (access (nhppsargs.indir//tfile//"log")) {
    type (nhppsargs.indir//tfile//"log") | tee (dataset.lfile)
    delete (nhppsargs.indir//tfile//"log")
}
;

# Trigger new datasets.

lpar nhppsargs
if (access(nhppsargs.calldir) && access(nhppsargs.calldir//tfile//".node")) {
    cd (nhppsargs.calldir)

    pipes = dataset.name // "-pipes." // tpipe 

    # Check for new datasets.
    dir (dataset.name//"*."//tpipe//".tbd") | scan (new)
    if (new != "no files found") {
        new = substr (new, 1, strstr(".tbd",new)-1)
	rename (new//".tbd", new//".proc")

	head (tfile//".node") | scan (node)
	pipeselect (nhppsargs.application, tpipe) | sort ( > pipes//".tmp")
	#copy (pipes, pipes//".tmp")

	tdir = ""; idir = ""
	match (node, pipes//".tmp") | scan (tdir, idir)
	if (tdir != "" && access (pipes//"exclude")) {
	    s1 = ""; match (node, pipes//"exclude") | scan (s1)
	}
	;
	if (tdir != s1) {
	    printf ("Submitting %s -> %s\n", new, node)
	    print (node, > new//".node")
	    sendtrig (new, nhppsargs.trigdir, nhppsargs.indir, idir//new,
		idir//new//"com", idir//"."//new//"rtn", tdir//new//"trig")
	} else
	    rename (new//".proc", new//".tbd")

	# Check for new or dead pipelines.
	diff (pipes, pipes//".tmp", > pipes//"diff")
	fd = pipes//"diff"
	for (j=0; fscan(fd,s1,tdir,idir)!=EOF; j+=1) {
	    if (nscan() < 2)
	        next
	    ;

	    # Check for excluded node.
	    if (access (pipes//".exclude")) {
		s2 = ""; match (node, pipes//".exclude") | scan (s2)
		if (s2 != "")
		    next
	    }
	    ;
	    
	    # New nodes available.
	    if (s1 == ">") {
	        node = substr (tdir, 1, stridx("!",tdir)-1)
		if (defvar("ntrig") == YES) {
		    for (i=1; i<=ntrig; i+=1) {
			dir (dataset.name//"*."//tpipe//".tbd") | scan (new)
			if (new == "no files found")
			    break
			;
			new = substr (new, 1, strstr(".tbd",new)-1)
			rename (new//".tbd", new//".proc")
			printf ("Adding %s -> %s\n", new, node)
			print (node, > new//".node")
			sendtrig (new, nhppsargs.trigdir, nhppsargs.indir,
			    idir//new, idir//new//"com", idir//"."//new//"rtn",
			    tdir//new//"trig")
		    }
		}
		;

	    # Node is not available.  Need to add a check if node is not
	    # in pipeselect for some reason other than pipeline or
	    # host being down.
	    } else if (s1 == "<") {

		printf ("Pipeline %s is no longer available on %s\n",
		    tpipe, node)
		grep ("-l", node, dataset.name//"*."//tpipe//".node",
		    > tfile//".tmp")
		list = tfile//".tmp"
		while (fscan (list, s1) != EOF) {
		    new = substr (s1, 1, strldx(".",s1)-1)
		    delete (new//".node")
		    rename (new//".proc", new//".tbd")
		}
		list = ""; delete (tfile//".tmp")
	    }
	    ;
	}
	fd = ""; delete (pipes//"diff")

	# Replace new list of pipelines.
	if (j > 0)
	    rename (pipes//".tmp", pipes)
	;
    } else
        delete (pipes//"*")

    # Clean up.
    if (exit != "COMPLETED") {
        rename (tfile//".proc", tfile//".procerr")
        rename (tfile//".node", tfile//".nodeerr")
    } else {
	if (access("DONE") == NO)
	    mkdir ("DONE")
	;
	move (tfile, "DONE/")
	delete (tfile//"*")
    }
    ;

    cd (nhppsargs.datadir)
} else if (access(nhppsargs.calldir) && access(nhppsargs.calldir//tfile//".proc")) {
    cd (nhppsargs.calldir)

    # Clean up.
    if (exit != "COMPLETED") {
        rename (tfile//".proc", tfile//".procerr")
    } else {
	if (access("DONE") == NO)
	    mkdir ("DONE")
	;
	move (tfile, "DONE/")
	delete (tfile//"*")
    }
    ;

    cd (nhppsargs.datadir)
}
;

# Finish up.
if (exit == "ENODATA")
    sendmsg ("ERROR", "No data returned", "", "RETURN")
else if (exit == "NODATA")
    sendmsg ("WARNING", "No data returned", "", "RETURN")
else if (exit == "WAITING")
    sendmsg ("WARNING", "Resubmitting", "", "RETURN")
;

plexit (exit)
