# LOGIN.CL -- Pipeline login file.

# Common variables. 
string	telescope = ""	{prompt="Telescope name or reference"}
string	instrument = "" {prompt="Instrument name or reference"}
string	cm = ""		{prompt="Calibration manager connection"}
string	dm = ""		{prompt="Data manager connection"}

# Read the command line variables.
nhpps
unlearn nhppsargs dataset
nhppsargs

# Make more unique pipe names.  We still use /tmp for efficiency.
reset pipes = ("/tmp/" // nhppsargs.dataset)

# Load standard packages.
images
utilities
proto
#servers
clpackage

# Load more package for non-pipeline invocations.
if (strstr ("tv", envget("iraf")) > 0) {
    printf ("plot\nnfextern\nmsctools\nkeep\n") | cl
}
;

# Cache common parameters and override defaults.
cache dir delete tee hedit nhedit imexpr
cache thedit tinfo tstat keypar
dir.max = 0
delete.verify = no
tee.append = yes
hedit.verify = no
hedit.show = no
hedit.update = yes 
nhedit.add = yes
nhedit.verify = no
nhedit.show = no
nhedit.update = yes 
thedit.show = no
imexpr.verbose = no
imexpr.outtype = "real"
keypar.silent = yes

# Do application, module, and dataset dependent setups.
if (nhppsargs.application != "None") {

    # Define application packages if available.
    s1 = strlwr(nhppsargs.application) // "pkg"
    s2 = "NHPPS_PIPEAPPSRC$/" // s1 // "/"
    if (access(s2))
	printf ("reset %s = %s; task %s.pkg = %s%s.cl; keep\n",
	    s1, s2, s1, s2, s1, s1) | cl
    ;

    if (access("NHPPS_PIPEAPPSRC$/plapp/")) {
	set plapp = "NHPPS_PIPEAPPSRC$/plapp/"
	task plapp = "NHPPS_PIPEAPPSRC$/plapp/plapp.cl"
	plapp
    }
    ;
        
    # Set application configuration parameters and override
    # with user application and dataset specific values.

    plconfig

    # Do standard module/dataset initialization and logging.

    if (nhppsargs.module != "None" && nhppsargs.dataset != "None")
	dataset
    ;
}
;

keep
