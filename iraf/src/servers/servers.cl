#{ SERVERS -- Package of IRAF network servers.

cl < "servers$lib/zzsetenv.def"
package	servers, bin = srvbin$

# Source directories.
set	srvutil	= "servers$srvutil/"
set	dm	= "servers$dm/"

# Tasks
task	cpmsgs,
	sndserver,
	switchbd	= "srvutil$x_srvutil.e"
task	rmcal,
	findid,
	findids,
	findrule,
	getcal,
	getkeyval,
	getkeyvals,
	newdataproduct,
	newprocess,
	putcal,
	setkeyval	 = "dm$x_dm.e"

# Cache the *cal tasks so they can write status without conflicts.
cache rmcal
cache getcal
cache putcal
unlearn getcal

clbye()
