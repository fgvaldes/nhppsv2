# MSG -- This is an interface that allows both files and network connections
# to be used transparently.


define	MSG_LEN		154			# Length of structure
define	MSG_SZFNAME	99			# Length of filenames

define	MSG_FD		Memi[$1]		# File descriptor
define	MSG_MODE	Memi[$1+1]		# Mode
define	MSG_TYPE	Memi[$1+2]		# Type
define	MSG_SERVER	Memi[$1+3]		# Server?
define	MSG_FNAME	Memc[P2C($1+4)]		# Connection name
define	MSG_SCKT	Memc[P2C($1+54)]	# Socket name

define	MSG_FILE	1
define	MSG_SOCK	2

# MSG_OPEN -- Open a descriptor for messages.

pointer procedure msg_open (name, mode, type)

char	name[ARB]		#I Message I/O name
int	mode			#I Mode (READ_ONLY|WRITE_ONLY)
int	type			#I Type (TEXT_FILE|BINARY_FILE)
pointer	msg			#O Message descriptor

int	fd, server
pointer	sp, fname, sckt

int	open(), ndopen(), stridxs(), strncmp(), strmatch()
errchk	open, ndopen

begin
	call smark (sp)
	call salloc (fname, SZ_FNAME, TY_CHAR)
	call salloc (sckt, SZ_FNAME, TY_CHAR)

	call strcpy (name, Memc[fname], SZ_FNAME)

	# The default is regular FIO.
	server = NO
	Memc[sckt] = EOS

	switch (mode) {
	case READ_ONLY, READ_WRITE, NEW_FILE:
	    # Network connections have a colon in their name.
	    if (stridxs (":", Memc[fname]) > 0) {
	        # Add localhost if not specified.
		if (strncmp (Memc[fname], "inet:", 5) == 0 &&
		    stridxs (":", Memc[fname+5]) == 0)
		    call strcat (":localhost", Memc[fname], SZ_FNAME)

		# Add flags (if needed) to allow multiple connections,
		# and nonblocking reads.
		if (strmatch (":text", Memc[fname]) == 0 &&
		    strmatch (":binary", Memc[fname]) == 0) {
		    if (type == TEXT_FILE)
			call strcat (":text", Memc[fname], SZ_FNAME)
		    else
			call strcat (":binary", Memc[fname], SZ_FNAME)
		}
		if (strmatch (":nonblock", Memc[fname]) == 0)
		    call strcat (":nonblock", Memc[fname], SZ_FNAME)
		if (strmatch (":nodelay", Memc[fname]) == 0)
		    call strcat (":nodelay", Memc[fname], SZ_FNAME)

		# Open the connection.
		fd = ndopen (Memc[fname], mode)

		# Setup server.
		if (mode == NEW_FILE) {
		    server = YES
		    call sprintf (Memc[sckt], SZ_FNAME, "sock:%d")
			call pargi (fd)
		}

	    # Open a simple file.  If NEW_FILE treat as input connection.
	    } else {
		if (mode == NEW_FILE)
		    fd = open (Memc[fname], READ_ONLY, type)
		else
		    fd = open (Memc[fname], mode, type)
	    }

	case WRITE_ONLY, APPEND:
	    # Network connections have a colon in their name.
	    if (stridxs (":", Memc[fname]) > 0) {
	        # Add localhost if not specified.
		if (strncmp (Memc[fname], "inet:", 5) == 0 &&
		    stridxs (":", Memc[fname+5]) == 0)
		    call strcat (":localhost", Memc[fname], SZ_FNAME)

		# Add flags (if needed).
		if (strmatch (":text", Memc[fname]) == 0 &&
		    strmatch (":binary", Memc[fname]) == 0) {
		    if (type == TEXT_FILE)
			call strcat (":text", Memc[fname], SZ_FNAME)
		    else
			call strcat (":binary", Memc[fname], SZ_FNAME)
		}

		# Open the connection.
		fd = ndopen (Memc[fname], mode)

	    # Open a simple file.
	    } else
		fd = open (Memc[fname], mode, type)

	default:
	    call sfree (sp)
	    call error (1, "Message mode not supported")
	}

	# Create the message descriptor.
	call malloc (msg, MSG_LEN, TY_STRUCT)
	MSG_SERVER(msg) = server
	MSG_MODE(msg) = mode
	MSG_TYPE(msg) = type
	MSG_FD(msg) = fd
	call strcpy (name, MSG_FNAME(msg), MSG_SZFNAME)
	call strcpy (Memc[sckt], MSG_SCKT(msg), MSG_SZFNAME)

	call sfree (sp)
	return (msg)
end


# MSG_FD -- Return a file descriptor.
# These file descriptors are closed by the application.
#
# For simple files/clients the file descriptor is returned on the first call
# and NULL thereafter.  For servers a new connection is attempted
# and returns either the new file descriptor or NULL for no new connection.

pointer	procedure msg_fd (msg)

pointer	msg			#I MSG descriptor
pointer	fd			#O File descriptor

int	ndopen()
errchk	ndopen

begin
	if (msg == NULL)
	    call error (1, "Message stream not open")

	if (MSG_SERVER(msg) == YES) {
	    iferr (fd = ndopen (MSG_SCKT(msg), NEW_FILE))
		fd = NULL
	} else {
	    fd = MSG_FD(msg)
	    MSG_FD(msg) = NULL
	}

	return (fd)
end


# MSG_CLOSE -- Close the message descriptor.

procedure msg_close (msg)

pointer	msg			#I MSG descriptor

begin
	if (msg == NULL)
	    return

	if (MSG_FD(msg) != NULL)
	    call close (MSG_FD(msg))

	call mfree (msg, TY_STRUCT)
end
