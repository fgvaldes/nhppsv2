include	<ctype.h>


# SRV_GKWS -- Get keyword protocol data.
#
# This is a generic routine intended to be used by different servers based
# on the keyword protocol.  This routine searches for new connections on
# the stream provided by the msg pointer.  It reads data from each stream and
# parses each line and enters the keyword data in a simply symbol table.
# When an EOF is encountered in a stream it returns the index in the array
# of file descriptors and symbol tables of the completed symbol table.
# The calling routine should then act on the information in the symbol
# table and the next time this routine is called return the index to be
# reset.
#
# The symbol tables have two standard entries, STPMARK containing a symbol
# table mark to use to free the previously defined information, and
# TRANSACTION which has an incrementing with each new connection.  Except
# STPMARK the symbol data consists of just a symbol table offset for the
# string value.  The offset is used to dereference a string in the symbol
# table string buffer using stprefsbuf.  The keyword symbols are either
# expected by the calling routine or can be discovered using sthead/stnext
# and stname.

procedure srv_gkws (msg, fd, stp, nfd, nlines, transaction, index, log)

pointer	msg			#I MSG interface structure
int	fd[nfd]			#I Working array of file descriptors
int	stp[nfd]		#I Working array of keyword symbol tables
int	nfd			#I Size of working arrays
int	nlines			#I Lines to read per connection / per call
int	transaction		#U Transaction counter
int	index			#U Index of completed symbol table
int	log			#I File descriptor to log actions

int	i, j, k, n
pointer	sp, line, key
pointer	sym

bool	streq()
int	msg_fd(), getline(), nscan(), stpstr(), strlen()
pointer	stopen(), stenter(), stfind()


begin
	call smark (sp)
	call salloc (line, SZ_LINE, TY_CHAR)
	call salloc (key, SZ_LINE, TY_CHAR)

	# Initialize last completed transaction.
	if (index > 0) {
	    sym = stfind (stp[index], "STPMARK")
	    call stfree (stp[index], Memi[sym])
	    call stmark (stp[index], Memi[sym])
	} else {
	    i = 1
	    j = 1
	}
	index = 0

	# Check for new input connections.
	do k = 1, nfd {
	    if (fd[k] == NULL)
		break
	    if (stp[k] == NULL) {
		stp[k] = stopen ("keywords", 100, 100*SZ_FNAME, 100*SZ_FNAME)
		transaction = transaction + 1
		sym = stenter (stp[k], "TRANSACTION", 1)
		call sprintf (Memc[line], SZ_LINE, "%d")
		    call pargi (transaction)
		Memi[sym] = stpstr (stp[k], Memc[line], 0)
		sym = stenter (stp[k], "STPMARK", 1)
		call stmark (stp[k], Memi[sym])
	    }
	}
	if (k <= nfd) {
	    fd[k] = msg_fd (msg)
	    if (fd[k] != NULL) {
		stp[k] = stopen ("keywords", 100, 100*SZ_FNAME, 100*SZ_FNAME)
		transaction = transaction + 1
		sym = stenter (stp[k], "TRANSACTION", 1)
		call sprintf (Memc[line], SZ_LINE, "%d")
		    call pargi (transaction)
		Memi[sym] = stpstr (stp[k], Memc[line], 0)
		sym = stenter (stp[k], "STPMARK", 1)
		call stmark (stp[k], Memi[sym])
	    }
	}

	# Read from input and store in a symbol table.
	for (; i<=nfd; i=i+1) {
	    if (fd[i] == NULL)
		break
	    for (; j<=nlines; j=j+1) {
		iferr (n = getline (fd[i], Memc[line]))
		    break
		if (n == EOF) {
		    call close (fd[i])
		    call stclose (stp[i])
		    do k = i, nfd-1 {
			fd[k] = fd[k+1]
			stp[k] = stp[k+1]
			if (fd[k] == NULL)
			    break
		    }
		    fd[k] = NULL
		    stp[k] = NULL
		    i = i - 1
		    break
		}

		k = strlen (Memc[line])
		if (k > 1 && streq (Memc[line+k-2], "\r\n")) {
		    Memc[line+k-2] = '\n'
		    Memc[line+k-1] = EOS
		}

		if (log != NULL)
		    call putline (log, Memc[line])

		# Check for end of input block.
		if (streq (Memc[line], "EOF\n")) {
		    index = i
		    break
		}

		# Find '=' and replace with space.
		# Check for comments, blank lines, or invalid formats.
		for (k=line; Memc[k]!=EOS&&IS_WHITE(Memc[k]); k=k+1) 
		    ;
		if (Memc[k] == EOS || Memc[k] == '#')
		    next
		for (; Memc[k]!=EOS&&!IS_WHITE(Memc[k]); k=k+1)
		    if (Memc[k] == '=')
			break
		for (; Memc[k]!=EOS&&IS_WHITE(Memc[k]); k=k+1) 
		    ;
		if (Memc[k] == EOS || Memc[k] != '=')
		    next
		Memc[k] = ' '

		# Extract the keyword and value.
		call sscan (Memc[line])
		call gargwrd (Memc[key], SZ_LINE)
		call gargwrd (Memc[line], SZ_LINE)
		if (nscan() != 2)
		    next

		# Store in symbol table.
		call strupr (Memc[key])
		sym = stfind (stp[i], Memc[key])
		if (sym == NULL)
		    sym = stenter (stp[i], Memc[key], 1)
		Memi[sym] = stpstr (stp[i], Memc[line], 0)

		if (log != NULL) {
		    call fprintf (log, "  add: %s = '%s'\n")
		        call pargstr (Memc[key])
			call pargstr (Memc[line])
		}
	    }

	    if (index > 0)
	        break
	    j = 1
	}

	call sfree (sp)
end
