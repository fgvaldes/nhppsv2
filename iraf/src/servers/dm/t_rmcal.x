# T_RMCAL -- Delete calibrations in a data manager.

procedure t_rmcal ()

pointer	dm				# Data manager connection
int	timeout				# Time out (sec)
pointer	class				# Data class
pointer	value				# Value pattern
pointer	detector			# Detector
pointer	imageid				# Image ID
pointer	filter				# Filter
real	exptime				# Exposure time
real	dexptime			# Exposure time window
real	quality				# Quality
pointer	match				# Match pattern
double	mjd				# MJD start of calibration
pointer	datatype			# Data type

int	err
pointer	sp, line, errstr, tmp, msg, frdm, todm

int	clgeti(), reopen(), msg_fd(), errget(), nowhite()
real	clgetr()
pointer	msg_open()
errchk	msg_open, msg_fd, reopen, rmcal

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE, TY_CHAR)
	call salloc (class, SZ_LINE, TY_CHAR)
	call salloc (value, SZ_LINE, TY_CHAR)
	call salloc (detector, SZ_LINE, TY_CHAR)
	call salloc (imageid, SZ_LINE, TY_CHAR)
	call salloc (filter, SZ_LINE, TY_CHAR)
	call salloc (match, SZ_LINE, TY_CHAR)
	call salloc (datatype, SZ_LINE, TY_CHAR)
	call salloc (errstr, SZ_LINE, TY_CHAR)

	# Get  parameters.
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	timeout = clgeti ("timeout")
	call clgstr ("class", Memc[class], SZ_LINE)
	call clgstr ("value", Memc[value], SZ_LINE)
	call clgstr ("detector", Memc[detector], SZ_LINE)
	call clgstr ("imageid", Memc[imageid], SZ_LINE)
	call clgstr ("filter", Memc[filter], SZ_LINE)
	exptime = clgetr ("exptime")
	dexptime = clgetr ("dexptime")
	quality = clgetr ("quality")
	call clgstr ("match", Memc[match], SZ_LINE)
	call clgstr ("datatype", Memc[datatype], SZ_LINE)

	# Allow for null string with whitespace.
	if (nowhite (Memc[class], Memc[line], SZ_LINE) == 0)
	    Memc[class] = EOS
	if (nowhite (Memc[value], Memc[line], SZ_LINE) == 0)
	    Memc[value] = EOS
	if (nowhite (Memc[detector], Memc[line], SZ_LINE) == 0)
	    Memc[detector] = EOS
	if (nowhite (Memc[imageid], Memc[line], SZ_LINE) == 0)
	    Memc[imageid] = EOS
	if (nowhite (Memc[filter], Memc[line], SZ_LINE) == 0)
	    Memc[filter] = EOS
	if (nowhite (Memc[match], Memc[line], SZ_LINE) == 0)
	    Memc[match] = EOS

	iferr {
	    msg = NULL; frdm = NULL; todm = NULL

	    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); frdm = tmp
	    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

	    call rmcal (todm, frdm, timeout, Memc[class], Memc[value],
	        Memc[detector], Memc[imageid], Memc[filter],
		exptime, dexptime, quality, Memc[match], mjd, Memc[datatype])
	    
	    call clputi ("statcode", 0)
	    call clpstr ("statstr", "RMCAL")
	    call printf ("0 RMCAL\n")
	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    call clputi ("statcode", err)
	    call clpstr ("statstr", Memc[line])
	    call printf ("%d %s\n")
	        call pargi (err)
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)

	call sfree (sp)
end


# RMCAL -- Delete calibration entries.
# Errors include missing keywords, timeout, and disconnect.

procedure rmcal (todm, frdm, timeout, class, value, detector, imageid, filter,
	exptime, dexptime, quality, match, mjd, datatype)

int	todm			#I FD to send to data manager 
int	frdm			#I FD from to data manager 
long	timeout			#I Timeout (sec)
char	class[ARB]		#I Data class
char	value[ARB]		#I Value pattern
char	detector[ARB]		#I Detector
char	imageid[ARB]		#I Image ID
char	filter[ARB]		#I Filter
real	exptime			#I Exposure time
real	dexptime		#I Exposure time window
real	quality			#I Quality
char	match[ARB]		#I Match pattern
double	mjd			#I MJD
char	datatype[ARB]		#I MJD

int	err, n
long	t
pointer	sp,line

bool	streq()
int	getlline(), strncmp(), errget()
long	clktime()
errchk	getlline

begin
	call smark (sp)
	call salloc (line, SZ_LINE, TY_CHAR)

	# Send request.
	call fprintf (todm, "COMMAND = rmcal\n")
	if (value[1] != EOS) {
	    call fprintf (todm, "VALUE = '%s'\n")
		call pargstr (value)
	}
	if (class[1] != EOS) {
	    call fprintf (todm, "CLASS = '%s'\n")
		call pargstr (class)
	}
	if (detector[1] != EOS) {
	    call fprintf (todm, "DETECTOR = '%s'\n")
		call pargstr (detector)
	}
	if (imageid[1] != EOS) {
	    call fprintf (todm, "IMAGEID = '%s'\n")
		call pargstr (imageid)
	}
	if (filter[1] != EOS) {
	    call fprintf (todm, "FILTER = '%s'\n")
		call pargstr (filter)
	}
	if (!IS_INDEFR(exptime)) {
	    call fprintf (todm, "EXPTIME = '%g'\n")
		call pargr (exptime)
	    if (!IS_INDEFR(dexptime)) {
		call fprintf (todm, "DEXPTIME = '%g'\n")
		    call pargr (dexptime)
	    }
	}
	if (!IS_INDEFR(quality)) {
	    call fprintf (todm, "QUALITY = '%g'\n")
		call pargr (quality)
	}
	if (match[1] != EOS) {
	    call fprintf (todm, "MATCH = '%s'\n")
		call pargstr (match)
	}
	if (datatype[1] != EOS) {
	    call fprintf (todm, "DATATYPE = '%s'\n")
		call pargstr (datatype)
	}

	call fprintf (todm, "EOF\n")
	call flush (todm)

	# Read from data manager.  Check for timeout or disconnect.
	t = clktime (0)
	repeat {

            # Poll for a response.
            iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
                err = errget (Memc[line], SZ_LINE)

                # ONLY want to trap socket read errors here (e.g. nothing yet)
                # for all other errors we pass them along
                if (err != 746)
                    call error (err, Memc[line])

                # check if we exceeded our timeout, if so, throw an error
                if ((clktime(0) - t) > timeout)
                    call error (499, "Data manager response timed out !!")

                # pause
                call zwmsec (50)
                next
            }

	    if (n == EOF)
		call error (498, "Data manager disconnected")
	    if (streq (Memc[line], "EOF\n"))
	       break
	    if (strncmp (Memc[line], "STATUS", 6) == 0) {
	        call sscan (Memc[line])
		call gargwrd (Memc[line], SZ_LINE)
		call gargwrd (Memc[line], SZ_LINE)
		call gargwrd (Memc[line], SZ_LINE)
		call sscan (Memc[line])
		call gargi (err)
		call gargstr (Memc[line], SZ_LINE)
		if (err > 0)
		    call error (err, Memc[line])
		break
	    }
	}

	call sfree (sp)
end
