
# T_NEWPROCINFO -- create new data entry in PMAS

procedure t_newproc ()

pointer	line                            # error msg
pointer	dm				# Data manager connection
int	timeout				# Time out (sec)
pointer	class 				# Observation type
pointer	procid				# Proc ID
pointer	parentid                        # parent proc ID
pointer	logURL                          # Process Log location
pointer	uparmURL                        # Process uparm directory location
pointer	nodeIP                          # IP address where process was run

int	err
pointer	sp, tmp, msg, frdm, todm

int	clpopnu(), clgeti(), reopen(), msg_fd(), errget()
pointer	msg_open()
errchk	msg_open, msg_fd, reopen, newprocess

begin
	call smark (sp)
	call salloc (line, SZ_LINE*5, TY_CHAR)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (class, SZ_LINE, TY_CHAR)
	call salloc (procid, SZ_LINE, TY_CHAR)
	call salloc (parentid, SZ_LINE, TY_CHAR)
	call salloc (logURL, SZ_LINE, TY_CHAR)
	call salloc (uparmURL, SZ_LINE, TY_CHAR)
	call salloc (nodeIP, SZ_LINE, TY_CHAR)

	# Get  parameters.
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	call clgstr ("class", Memc[class], SZ_LINE)
	call clgstr ("procid", Memc[procid], SZ_LINE)
	#call clgstr ("parentid", Memc[parentid], SZ_LINE)
	call clgstr ("logURL", Memc[logURL], SZ_LINE)
	call clgstr ("uparmURL", Memc[uparmURL], SZ_LINE)
	call clgstr ("nodeIP", Memc[nodeIP], SZ_LINE)
	timeout = clgeti ("timeout")

        iferr {
	    msg = NULL; frdm = NULL; todm = NULL

	    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); frdm = tmp
	    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

	    if (Memc[class]=='!'||Memc[procid]=='!')
		call error (1, "Incomplete params for newprocess")

	    call newprocess ( todm, frdm, timeout, Memc[class], 
                              Memc[procid], Memc[parentid], Memc[logURL], Memc[uparmURL], Memc[nodeIP] )
	    
	    call printf ("0 NEWPROCINFO\n")

	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    call printf ("ERROR:%d dm_port:%s class:%s procid:%s\n msg:%s\n")
	        call pargi (err)
		call pargstr (Memc[dm])
		call pargstr (Memc[class])
		call pargstr (Memc[procid])
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)

	call sfree (sp)
end


# NEWPROCESS -- create new process object entry in PMAS
# Errors include missing keywords, timeout, disconnect, and existing (meta-data) found.
# The classes to get are those in the list excluding those not required
# by the observation type.

procedure newprocess (todm, frdm, timeout, class, procid, parentid, logURL, uparmURL, nodeIP)

int	todm			#I FD to send to data manager 
int	frdm			#I FD from to data manager 
long	timeout			#I Timeout (sec)
char	class[ARB]		#I Class to create
char	procid[ARB]		#I id of process which 'owns' this data
char	parentid[ARB]		#I id of parent process to this one 
char	logURL[ARB]		#I url to log for this process
char	uparmURL[ARB]		#I url to uparm directory for this process
char	nodeIP[ARB]		#I node ip address for this process

int	err, n, ip
long	t
pointer	sp, line, key, val

bool	streq()
int	clgfil(), strdic(), getlline(), nscan(), strlen(), ctowrd(), ctoi(), errget()
long	clktime()
errchk	imgstr, getlline

begin
	call smark (sp)
        call salloc (line, SZ_LINE*5, TY_CHAR)
        call salloc (key, SZ_FNAME, TY_CHAR)
        call salloc (val, SZ_LINE, TY_CHAR)

	# Send keywords.
#	call printf ("SND:\n COMMAND = newprocess\n")
	call fprintf (todm, "COMMAND = newprocess\n")

#	call printf (" CLASS = '%s'\n")
#	    call pargstr (class)
	call fprintf (todm, "CLASS = '%s'\n")
	    call pargstr (class)

#	call printf (" PROCID = '%s'\n")
#	   call pargstr (procid)
	call fprintf (todm, "PROCID = '%s'\n")
	   call pargstr (procid)

        #call printf (" PARENTPROCID = '%s'\n")
        #   call pargstr (parentid)
        #call fprintf (todm, "PARENTPROCID = '%s'\n")
        #   call pargstr (parentid)

#        call printf (" LOGURL = '%s'\n")
#           call pargstr (logURL)
        call fprintf (todm, "LOGURL = '%s'\n")
           call pargstr (logURL)

#        call printf (" UPARMURL = '%s'\n")
#           call pargstr (uparmURL)
        call fprintf (todm, "UPARMURL = '%s'\n")
           call pargstr (uparmURL)

#        call printf (" NODEIP = '%s'\n")
#           call pargstr (nodeIP)
        call fprintf (todm, "NODEIP = '%s'\n")
           call pargstr (nodeIP)


	# Send the end.
	call printf ("EOF\n")
	call fprintf (todm, "EOF\n")
	call flush (todm)

	# Read from data manager.  Check for timeout or disconnect.

	t = clktime (0)
	repeat {
                # Poll for a response.
                iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
                    err = errget (Memc[line], SZ_LINE)

                    # ONLY want to trap socket read errors here (e.g. nothing yet)
                    # for all other errors we pass them along
                    if (err != 746)
                        call error (err, Memc[line])

                    # check if we exceeded our timeout, if so, throw an error
                    if ((clktime(0) - t) > timeout)
                        call error (499, "Data manager response timed out !!")

                    # pause
                    call zwmsec (50)
                    next
                }


		if (n == EOF)
		    call error (498, "Data manager disconnected")
		if (streq (Memc[line], "EOF\n"))
		   break

		ip = 1
		if (ctowrd (Memc[line], ip, Memc[key], SZ_FNAME) == 0)
		    next
		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next
 
		if (streq (Memc[key], "STATUS")) {
		    ip = ip + 2
		    if (ctoi (Memc[line], ip, err) == 0)
		        next
		    if (err > 0) {
			call error (err, Memc[line+ip])
		    }
		    next
		}

		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next

                # debug message
 		#call printf ("NewProc RETURNS: %s = '%s'\n")
		#    call pargstr (Memc[key])
		#    call pargstr (Memc[val])

	}

	call sfree (sp)
end
