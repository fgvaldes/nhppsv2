# DM.H -- Prototype Data Manager

# Master data structure.

define	DM_MAXIN	100		# Maximum number of input connections
define	DM_SZSTR	199		# Size of string
define	DM_LEN		(107+3*DM_MAXIN) # Length of structure

define	DM_CAT		Memi[$1]	# Catalog pointer (ACECAT)
define	DM_MSGIN	Memi[$1+1]	# Input connection (MSG)
define	DM_MSGCTRL	Memi[$1+2]	# Control connection (MSG)
define	DM_MSGMON	Memi[$1+3]	# Monitor connection (MSG)
define	DM_MON		Memi[$1+4]	# Monitor connection (FIO)
define	DM_CTRL		Memi[$1+5]	# Control connection (FIO)
define	DM_STR		Memc[P2C($1+6)]	# String (199)
define	DM_IN		Memi[$1+$2+106]	# Array of input connections (FIO)
define	DM_STP		Memi[$1+$2+205]	# Array of input symbol tables (SYMTAB)
define	DM_STPMARK	Memi[$1+$2+305]	# Array of input symbol markers (SYMTAB)


# Input commands/functions
define	DMCMDS		"|getcal|putcal|listcal|"
define	GETCAL		1		# Get calibration
define	PUTCAL		2		# Put calibration
define	LISTCAL		3		# List calibrations

# Control commands/functions


# Errors

define	DM_NOKEY	1
define	DM_BADKEY	2
define	DM_NOCAL	3
define	DM_NOCOPY	4
define	DM_NOINFO	5


# Catalog name.

define	DM_CATALOG	"catalog.dat"
