
# T_NEWDATAPROD -- create new data entry in PMAS

procedure t_newdataprod ()

pointer	class 				# Observation type
pointer	dataid				# Data Product ID
pointer	observedMJD                     # MJD date when the base data
pointer	URL                             # URL to data product
int	nelements			# Number of elements
real	size				# Size of data (Mb)
int	timeout				# Time out (sec)

# Environment
pointer	dm				# Data manager
pointer	procid				# Proc ID

int	err
pointer	sp, line, tmp, msg, frdm, todm

int	envfind(), clpopnu(), clgeti(), reopen(), msg_fd(), errget() 
real	clgetr()
pointer	msg_open()
errchk	msg_open, msg_fd, reopen, newdataproduct

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (procid, SZ_LINE, TY_CHAR)
	call salloc (class, SZ_LINE, TY_CHAR)
	call salloc (line, SZ_LINE*5, TY_CHAR)
	call salloc (dataid, SZ_LINE, TY_CHAR)
	call salloc (observedMJD, SZ_LINE, TY_CHAR)
	call salloc (URL, SZ_LINE, TY_CHAR)

	# Get environment information.
	if (envfind ("NHPPS_PORT_DM", Memc[dm], SZ_FNAME) != ERR) {
	    if (Memc[dm] == EOS)
		call strcpy ("STDOUT", Memc[dm], SZ_FNAME)
	    else {
		Memc[line] = ':'
		if (envfind ("NHPPS_NODE_DM", Memc[line+1], SZ_LINE) != ERR)
		    call strcat (Memc[line], Memc[dm], SZ_FNAME)
	    }
	} else
	    call strcpy ("STDOUT", Memc[dm], SZ_FNAME)
	if (envfind ("OSF_DATASET", Memc[procid], SZ_LINE) != ERR) {
	    if (Memc[procid] == EOS)
		call strcpy ("Test", Memc[procid], SZ_FNAME)
	    else {
		Memc[line] = '_'
		if (envfind ("NHPPS_MODULE_NAME", Memc[line+1], SZ_LINE) != ERR)
		    call strcat (Memc[line], Memc[procid], SZ_LINE)
	    }
	} else
	    call strcpy ("Test", Memc[procid], SZ_FNAME)

	# Get  parameters.
	call clgstr ("class", Memc[class], SZ_LINE)
	call clgstr ("dataid", Memc[dataid], SZ_LINE)
	call clgstr ("observedMJD", Memc[observedMJD], SZ_LINE)
	call clgstr ("URL", Memc[URL], SZ_LINE)
	nelements = clgeti ("nelements")
	size = clgetr ("size")
	timeout = clgeti ("timeout")

        iferr {
	    msg = NULL; frdm = NULL; todm = NULL

	    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); frdm = tmp
	    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

	    if (Memc[class]==''||Memc[dataid]==''||Memc[procid]=='')
		call error (1, "Incomplete params for newdataproduct")

	    call newdataprod (todm, frdm, timeout, Memc[class], Memc[dataid], 
                                 Memc[procid], Memc[observedMJD], Memc[URL],
				 nelements, size)
	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    call printf ("%d %s\n")
	        call pargi (err)
		call pargstr (Memc[line])
	} else
	    call printf ("0 NEWDATAPROD\n")

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)

	call sfree (sp)
end


# NEWDATAPROD -- create new data product object entry in PMAS
# Errors include missing keywords, timeout, disconnect, and existing (meta-data) found.
# The classes to get are those in the list excluding those not required
# by the observation type.

procedure newdataprod (todm, frdm, timeout, class, dataid, procid, obsMJD, URL,
	nelements, size)

int	todm			#I FD to send to data manager 
int	frdm			#I FD from to data manager 
long	timeout			#I Timeout (sec)
char	class[ARB]		#I Class to create
char	dataid[ARB]		#I id of the new data product
char	procid[ARB]		#I id of process which 'owns' this data
char	obsMJD[ARB]		#I the date when base data where acquired
char	URL[ARB]		#I the location of the data
int	nelements		#I the number of elements
real	size			#I size of data (Mb)

int	err, n, ip
long	t
pointer	sp, line, key, val

bool	streq()
int	clgfil(), strdic(), getlline(), nscan(), strlen(), ctowrd(), ctoi(), errget() 
long	clktime()
errchk	imgstr, getlline

begin
	call smark (sp)
        call salloc (line, SZ_LINE*5, TY_CHAR)
        call salloc (key, SZ_FNAME, TY_CHAR)
        call salloc (val, SZ_LINE, TY_CHAR)

	# Send keywords.
	call fprintf (todm, "COMMAND = newdataproduct\n")
        call fprintf (todm, "DATAID = '%s'\n")
           call pargstr (dataid)
	call fprintf (todm, "PRODUCEDBY = '%s'\n")
	   call pargstr (procid)
	if (obsMJD[1] != EOS) {
	    call fprintf (todm, "OBSERVEMJD = '%s'\n")
	       call pargstr (obsMJD)
	}
	if (class[1] != EOS) {
	    call fprintf (todm, "CLASS = '%s'\n")
		call pargstr (class)
	}
	if (URL[1] != EOS) {
	    call fprintf (todm, "URL= '%s'\n")
	       call pargstr (URL)
	}
	if (!IS_INDEFI(nelements)) {
	    call fprintf (todm, "NELEMENTS = '%d'\n")
	       call pargi (nelements)
	}
	if (!IS_INDEFR(size)) {
	    call fprintf (todm, "SIZE = '%.3f'\n")
	       call pargr (size)
	}

	# Send the end.
	#call printf ("EOF\n")
	call fprintf (todm, "EOF\n")
	call flush (todm)

	# Read from data manager.  Check for timeout or disconnect.

	t = clktime (0)
	repeat {
                # Poll for a response.
                iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
                    err = errget (Memc[line], SZ_LINE)

                    # ONLY want to trap socket read errors here (e.g. nothing yet)
                    # for all other errors we pass them along
                    if (err != 746)
                        call error (err, Memc[line])

                    # check if we exceeded our timeout, if so, throw an error
                    if ((clktime(0) - t) > timeout)
                        call error (499, "Data manager response timed out !!")

                    # pause
                    call zwmsec (50)
                    next
                }

		if (n == EOF)
		    call error (498, "Data manager disconnected")
		if (streq (Memc[line], "EOF\n"))
		   break

		ip = 1
		if (ctowrd (Memc[line], ip, Memc[key], SZ_FNAME) == 0)
		    next
		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next
 
		if (streq (Memc[key], "STATUS")) {
		    ip = ip + 2
		    if (ctoi (Memc[line], ip, err) == 0)
		        next
		    if (err > 0) {
			call error (err, Memc[line+ip])
		    }
		    next
		}

		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next

                # debug message
 		#call printf ("NewDataProd RETURNS: %s = '%s'\n")
		#    call pargstr (Memc[key])
		#    call pargstr (Memc[val])

	}

	call sfree (sp)
end
