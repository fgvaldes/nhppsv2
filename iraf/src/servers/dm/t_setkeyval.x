
# T_SETKEYVAL -- set keyword value for given id, class of object

procedure t_setkeyval ()

pointer	dm				# Data manager connection
pointer	line                            # error msg
int	timeout				# Time out (sec)
pointer	id				# the object id we want to interrogate
pointer	class 				# class of object id pertains to
pointer	keyname                         # name of keyword
pointer	kvalue                          # value to set keyword to

int	err
pointer	sp, tmp, msg, frdm, todm

int	clpopnu(), clgeti(), reopen(), msg_fd(), errget()
pointer	msg_open()
errchk	msg_open, msg_fd, reopen, setkeyval1

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE*5, TY_CHAR)
	call salloc (class, SZ_LINE, TY_CHAR)
	call salloc (id, SZ_LINE, TY_CHAR)
	call salloc (keyname, SZ_LINE, TY_CHAR)
	call salloc (kvalue, SZ_LINE, TY_CHAR)

	# Get  parameters.
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	call clgstr ("class", Memc[class], SZ_LINE)
	call clgstr ("id", Memc[id], SZ_LINE)
	call clgstr ("keyword", Memc[keyname], SZ_LINE)
	call clgstr ("value", Memc[kvalue], SZ_LINE)
	timeout = clgeti ("timeout")

        iferr {
	    msg = NULL; frdm = NULL; todm = NULL

	    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); frdm = tmp
	    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

	    if (Memc[class] =='!'||Memc[id] =='!'||Memc[keyname] =='!'||Memc[kvalue] =='!')
		call error (1, "Incomplete params for setkeyval")

	    call setkeyval1 (todm, frdm, timeout, Memc[class], Memc[id], Memc[keyname], Memc[kvalue])
	    
	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    call printf ("ERROR:%d dm_port:%s class:%s id:%s keyword:%s val:%s\n msg:%s\n")
	        call pargi (err)
		call pargstr (Memc[dm])
		call pargstr (Memc[class])
		call pargstr (Memc[id])
		call pargstr (Memc[keyname])
		call pargstr (Memc[kvalue])
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)

	call sfree (sp)
end


# SETKEYVAL -- set specified keyword value for given id of object
# Errors include missing keywords, timeout, disconnect, and existing (meta-data) found.

procedure setkeyval1 (todm, frdm, timeout, class, id, keyname, kvalue)

int	todm			#I FD to send to data manager 
int	frdm			#I FD from to data manager 
long	timeout			#I Timeout (sec)
char	class[ARB]		#I object Class
char	id[ARB]			#I object id
char	keyname[ARB]		#I object keyword name
char	kvalue[ARB]		#I object keyword value

int	err, n, ip
long	t
pointer	sp, line, key, val

bool	streq()
int	clgfil(), strdic(), getlline(), nscan(), strlen(), ctowrd(), ctoi()
int     errget()
long	clktime()
errchk	imgstr, getlline

begin
	call smark (sp)
        call salloc (line, SZ_LINE*5, TY_CHAR)
        call salloc (key, SZ_FNAME, TY_CHAR)
        call salloc (val, SZ_LINE, TY_CHAR)

	# Send keywords.
	call fprintf (todm, "COMMAND = setkeywordvalue\n")
	call fprintf (todm, "CLASS = '%s'\n")
	    call pargstr (class)
        call fprintf (todm, "ID_STR = '%s'\n")
            call pargstr (id)
        call fprintf (todm, "KEYWORD = '%s'\n")
            call pargstr (keyname)
        call fprintf (todm, "VALUE = '%s'\n")
            call pargstr (kvalue)

	# Send the end.
	call fprintf (todm, "EOF\n")
	call flush (todm)

	# Read from data manager.  Check for timeout or disconnect.
	t = clktime (0)
	repeat {
		# Poll for a response.
		iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
	            err = errget (Memc[line], SZ_LINE)

                    # ONLY want to trap socket read errors here (e.g. nothing yet)
                    # for all other errors we pass them along
                    if (err != 746) 
			call error (err, Memc[line])

                    # check if we exceeded our timeout, if so, throw an error 
		    if ((clktime(0) - t) > timeout)
			call error (499, "Data manager response timed out !!")

                    # pause 
                    call zwmsec (50)
		    next
		} 

		if (n == EOF)
		    call error (498, "Data manager disconnected")
		if (streq (Memc[line], "EOF\n"))
		   break

		ip = 1
		if (ctowrd (Memc[line], ip, Memc[key], SZ_FNAME) == 0)
		    next
		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next
 
		if (streq (Memc[key], "STATUS")) {
		    ip = ip + 2
		    if (ctoi (Memc[line], ip, err) == 0)
		        next
		    if (err > 0) {
			call error (err, Memc[line+ip])
		    }
		    next
		}

		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next

	}

	call sfree (sp)
end
