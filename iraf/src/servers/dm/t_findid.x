
# T_FINDIDS -- get keyword values for search params

procedure t_findid ()

pointer	dm				# Data manager connection
pointer	line                            # error msg
pointer	search                          # name of search parameter file
int	timeout				# Time out (sec)
pointer	class 				# class of object we are searching for

int	err
pointer	sp, tmp, msg, frdm, todm

int	clpopnu(), clgeti(), reopen(), msg_fd(), errget()
pointer	msg_open()
errchk	msg_open, msg_fd, reopen, getkeyvals1

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE*5, TY_CHAR)
	call salloc (class, SZ_LINE, TY_CHAR)
	call salloc (search, SZ_LINE, TY_CHAR)

	# Get  parameters.
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	call clgstr ("class", Memc[class], SZ_LINE)     # class we are searching 
        call clgstr ("search", Memc[search], SZ_FNAME)  # Get the filename that has search params
	timeout = clgeti ("timeout")

        iferr {
	    msg = NULL; frdm = NULL; todm = NULL

	    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); frdm = tmp
	    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

	    if (Memc[class] =='!'||Memc[search] =='!')
		call error (1, "Incomplete params for findid")

	    call getkeyvals1 (todm, frdm, timeout, Memc[class], Memc[search], "id_str", 0)
	    
	    call printf ("0 FINDID\n")

	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    call printf ("ERROR:%d dm_port:%s class:%s keyword:id_str\n msg:%s\n")
	        call pargi (err)
		call pargstr (Memc[dm])
		call pargstr (Memc[class])
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)

	call sfree (sp)
end

