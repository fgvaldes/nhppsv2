
# T_GETKEYVALS -- get keyword values for search params

procedure t_getkeyvals ()

pointer	dm				# Data manager connection
pointer	line                            # error msg
pointer	search                          # name of search parameter file
int	timeout				# Time out (sec)
int	multiple                        # Return multiple results or not
pointer	class 				# class of object we are searching for
pointer	keyname                         # name of keyword

int	err
pointer	sp, tmp, msg, frdm, todm

int	clpopnu(), clgeti(), reopen(), msg_fd(), errget()
pointer	msg_open()
errchk	msg_open, msg_fd, reopen, getkeyvals1

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE*5, TY_CHAR)
	call salloc (class, SZ_LINE, TY_CHAR)
	call salloc (search, SZ_LINE, TY_CHAR)
	call salloc (keyname, SZ_LINE, TY_CHAR)

	# Get  parameters.
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	call clgstr ("class", Memc[class], SZ_LINE)     # class we are searching 
        call clgstr ("search", Memc[search], SZ_FNAME)        # Get the filename that has search params
	call clgstr ("keyword", Memc[keyname], SZ_LINE) # name of keyword to get values of
	timeout = clgeti ("timeout")


        multiple = 1
        iferr {
	    msg = NULL; frdm = NULL; todm = NULL

	    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); frdm = tmp
	    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

	    if (Memc[class] =='!'||Memc[search] =='!'||Memc[keyname] =='!')
		call error (1, "Incomplete params for getkeyvals")

	    call getkeyvals1 (todm, frdm, timeout, Memc[class], Memc[search], Memc[keyname], multiple)
	    
	    call printf ("0 GETKEYVALS\n")
	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    call printf ("ERROR:%d dm_port:%s class:%s keyword:%s\n msg:%s\n")
	        call pargi (err)
		call pargstr (Memc[dm])
		call pargstr (Memc[class])
		call pargstr (Memc[keyname])
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)

	call sfree (sp)
end


# GETKEYVALS -- get specified keyword values for given search params.
# Errors include missing keywords, timeout, disconnect, and existing (meta-data) found.

procedure getkeyvals1 (todm, frdm, timeout, class, search_param_file, keyname, multiple)

int     todm                    #I FD to send to data manager
int     frdm                    #I FD from to data manager
int     multiple                #I have multiple results or not
long    timeout                 #I Timeout (sec)
char    class[ARB]              #I object Class
char    search_param_file[ARB]  #I file with search parameters
char    keyname[ARB]            #I object keyword name

int     err, n, ip
long    t
int fd
pointer sp, line, key, val
pointer test, pnam, pval, pmin, pmax

bool    streq()
int     clgfil(), strdic(), getlline(), nscan(), strlen(), ctowrd(), ctoi(), open(), fscan(), errget() 
long    clktime()
errchk  imgstr, getlline

begin
        call smark (sp)
        call salloc (line, SZ_LINE*5, TY_CHAR)
        call salloc (key, SZ_FNAME, TY_CHAR)
        call salloc (val, SZ_LINE, TY_CHAR)
        call salloc (test, SZ_FNAME, TY_CHAR)
        call salloc (pnam, SZ_LINE*5, TY_CHAR)
        call salloc (pval, SZ_LINE*5, TY_CHAR)
        call salloc (pmin, SZ_LINE*5, TY_CHAR)
        call salloc (pmax, SZ_LINE*5, TY_CHAR)

        # Send info for search
        call printf ("SND:\n COMMAND = getkeywordvalues\n")
        call fprintf (todm, "COMMAND = getkeywordvalues\n")

        call printf (" CLASS = '%s'\n")
            call pargstr (class)
        call fprintf (todm, "CLASS = '%s'\n")
            call pargstr (class)

        call printf (" KEYWORD = '%s'\n")
            call pargstr (keyname)
        call fprintf (todm, "KEYWORD = '%s'\n")
            call pargstr (keyname)

        if (multiple > 0) {
           call printf (" MULTI = '1'\n")
           call fprintf (todm, "MULTI = '1'\n")
        } else {
           call printf (" MULTI = '0'\n")
           call fprintf (todm, "MULTI = '0'\n")
        } 

	# parse the search param file, and send params
        # in format that the DM will understand
	fd = open (search_param_file, READ_ONLY, TEXT_FILE)
	while (fscan (fd) != EOF) {
                # get param name
		call gargwrd (test, SZ_FNAME)
                # capture pnam for later use
                call strcpy (test, Memc[pnam], SZ_LINE*4)

                # get param value
		call gargwrd (test, SZ_FNAME)
                # capture pval for later use
                call strcpy (test, Memc[pval], SZ_LINE*4)

                # get param min value
		call gargwrd (test, SZ_FNAME)

                # don't go further if only 2 args - its
                # an exact match request 
                if (nscan() < 3) { 
                   call printf (" %s = '%s'\n")
                      call pargstr (Memc[pnam])
                      call pargstr (Memc[pval])
                   # send DM info on param/value
                   call fprintf (todm, "%s = '%s'\n")
                      call pargstr (Memc[pnam])
                      call pargstr (Memc[pval])
                   next
                }

                # capture pmin for later use
                call strcpy (test, Memc[pmin], SZ_LINE*4)

                # get param max value
		call gargwrd (test, SZ_FNAME)

                # don't go further if only 3 args
                # a min/max range request 
                if (nscan() < 4) { 
                   call printf (" %s_min = '%s'\n")
                      call pargstr (Memc[pnam])
                      call pargstr (Memc[pval])
                   call printf (" %s_max = '%s'\n")
                      call pargstr (Memc[pnam])
                      call pargstr (Memc[pmin])
                   # send DM info on param/value
                   call fprintf (todm, "%s_min = '%s'\n")
                      call pargstr (Memc[pnam])
                      call pargstr (Memc[pval])
                   call fprintf (todm, "%s_max = '%s'\n")
                      call pargstr (Memc[pnam])
                      call pargstr (Memc[pmin])
                   next
               }

               # save max
               call strcpy (test, Memc[pmax], SZ_LINE*4)

               # IF we get this far, then its a 'full' statement where
               # we want to match the 'best' value, ie within a range 
               # near a centroid.

               # contact DM about core value
               call printf (" %s_core = '%s'\n")
                    call pargstr (Memc[pnam])
                    call pargstr (Memc[pval])
               call fprintf (todm, "%s_core = '%s'\n")
                    call pargstr (Memc[pnam])
                    call pargstr (Memc[pval])

               # contact DM about min/max vals
                call printf (" %s_min = '%s'\n")
                    call pargstr (Memc[pnam])
                    call pargstr (Memc[pmin])
               call fprintf (todm, "%s_min = '%s'\n")
                    call pargstr (Memc[pnam])
                    call pargstr (Memc[pmin])
               call printf (" %s_max = '%s'\n")
                    call pargstr (Memc[pnam])
                    call pargstr (Memc[pmax])
               call fprintf (todm, "%s_max = '%s'\n")
                    call pargstr (Memc[pnam])
                    call pargstr (Memc[pmax])

	}
	call close (fd)

        # Send the end.
        call printf ("EOF\n")
        call fprintf (todm, "EOF\n")
        call flush (todm)


        # Read from data manager.  Check for timeout or disconnect.
        t = clktime (0)
        repeat {
                # Poll for a response.
                iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
                    err = errget (Memc[line], SZ_LINE)

                    # ONLY want to trap socket read errors here (e.g. nothing yet)
                    # for all other errors we pass them along
                    if (err != 746)
                        call error (err, Memc[line])

                    # check if we exceeded our timeout, if so, throw an error
                    if ((clktime(0) - t) > timeout)
                        call error (499, "Data manager response timed out !!")

                    # pause
                    call zwmsec (50)
                    next
                }

                if (n == EOF)
                    call error (498, "Data manager disconnected")
                if (streq (Memc[line], "EOF\n"))
                   break


                ip = 1
                if (ctowrd (Memc[line], ip, Memc[key], SZ_FNAME) == 0)
                    next
                if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
                    next

                if (streq (Memc[key], "STATUS")) {
                    ip = ip + 2
                    if (ctoi (Memc[line], ip, err) == 0)
                        next
                    if (err > 0) {
                        call error (err, Memc[line+ip])
                    }
                    next
                }

                if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
                    next

                # debug message
                call printf ("%s = %s\n")
                    call pargstr (Memc[key])
                    call pargstr (Memc[val])

        }

        call sfree (sp)
end

