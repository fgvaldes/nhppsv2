
# T_GETKEYVAL -- get the code for a given rule id, and (optionally) time

procedure t_findrule ()

pointer	dm				# Data manager connection
pointer	line                            # error msg
int	validate 			# Validate (0= no)
int	timeout				# Time out (sec)
pointer	id				# the object id we want to interrogate

int	err
pointer	sp, tmp, msg, frdm, todm

int	clpopnu(), clgeti(), reopen(), msg_fd(), errget()
pointer	msg_open()
errchk	msg_open, msg_fd, reopen, findrule

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE*5, TY_CHAR)
	call salloc (id, SZ_LINE, TY_CHAR)
	#call salloc (keyname, SZ_LINE, TY_CHAR)

	# Get  parameters.
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	call clgstr ("id", Memc[id], SZ_LINE)
	validate = clgeti ("validate")
	timeout = clgeti ("timeout")

        iferr {
	    msg = NULL; frdm = NULL; todm = NULL

	    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); frdm = tmp
	    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

	    if (Memc[id] =='!')
		call error (1, "Missing rule id param for findrule")

	    call findrule (todm, frdm, validate, timeout, Memc[id])
	    
	    call printf ("0 GETRULECODE\n")

	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    call printf ("ERROR:%d dm_port:%s id:%s msg:%s\n")
	        call pargi (err)
		call pargstr (Memc[dm])
		call pargstr (Memc[id])
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)

	call sfree (sp)
end


# GETRULECODE -- get specified rule code for given id of a rule
# (and optionally an MJD date). Errors include missing id, timeout, 
# disconnect, and no existing rule with matching meta-data found.

procedure findrule (todm, frdm, validate, timeout, id)

int	todm			#I FD to send to data manager 
int	frdm			#I FD from to data manager 
int     validate                #I whether script should be validated
long	timeout			#I Timeout (sec)
char	id[ARB]			#I object id

int	err, n, ip
long	t
pointer	sp, line, key, val

bool	streq()
int	clgfil(), strdic(), getlline(), nscan(), strlen(), ctowrd(), ctoi(), errget() 
long	clktime()
errchk	imgstr, getlline

begin
	call smark (sp)
        call salloc (line, SZ_LINE*5, TY_CHAR)
        call salloc (key, SZ_FNAME, TY_CHAR)
        call salloc (val, SZ_LINE, TY_CHAR)

	# Send info to DM.
	call fprintf (todm, "COMMAND = findrule\n")

        call fprintf (todm, "ID_STR = '%s'\n")
            call pargstr (id)

        call fprintf (todm, "VALIDATE = '%d'\n")
            call pargi (validate)

	# Send the end.
	call fprintf (todm, "EOF\n")
	call flush (todm)

	# Read from data manager.  Check for timeout or disconnect.

	t = clktime (0)
	repeat {
                # Poll for a response.
                iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
                    err = errget (Memc[line], SZ_LINE)

                    # ONLY want to trap socket read errors here (e.g. nothing yet)
                    # for all other errors we pass them along
                    if (err != 746)
                        call error (err, Memc[line])

                    # check if we exceeded our timeout, if so, throw an error
                    if ((clktime(0) - t) > timeout)
                        call error (499, "Data manager response timed out !!")

                    # pause
                    call zwmsec (50)
                    next
                }

		if (n == EOF)
		    call error (498, "Data manager disconnected")
		if (streq (Memc[line], "EOF\n"))
		   break

		ip = 1
		if (ctowrd (Memc[line], ip, Memc[key], SZ_FNAME) == 0)
		    next
		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next
 
		if (streq (Memc[key], "STATUS")) {
		    ip = ip + 2
		    if (ctoi (Memc[line], ip, err) == 0)
		        next
		    if (err > 0) {
			call error (err, Memc[line+ip])
		    }
		    next
		}

		if (ctowrd (Memc[line], ip, Memc[val], SZ_LINE) == 0)
		    next

 		call printf ("%s\n")
		    call pargstr (Memc[val])

                # debug message
 		#call printf ("%s = '%s'\n")
		#    call pargstr (Memc[key])
		#    call pargstr (Memc[val])

	}

	call sfree (sp)
end
