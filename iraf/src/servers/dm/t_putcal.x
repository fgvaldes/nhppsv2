# T_PUTCAL -- Put calibrations in a data manager.


define	DTYPES		"|image|file|directory|keyword|"
define	DTYPE_IMAGE	1
define	DTYPE_FILE	2
define	DTYPE_DIR	3
define	DTYPE_KEY	4

procedure t_putcal ()

int	files				# List of calibration files
pointer	dm				# Data manager connection
int	timeout				# Time out (sec)
pointer	class				# Data class
pointer	mjdstart			# MJD start of calibration
pointer	mjdend				# MJD end of calibration
pointer	detector			# Detector
pointer	imageid				# Image ID
pointer	filter				# Filter
pointer	exptime				# Exposure time
pointer	quality				# Quality
pointer	match				# Match
int	dtype				# Data type

int	ip, err
pointer	sp, line, errstr, tmp, msg, frdm, todm, im

int	clgeti(), clgwrd(), imtopenp(), imtgetim(), imtlen()
int	reopen(), msg_fd(), errget(), access(), nowhite()
pointer	msg_open(), immap()
errchk	imtopenp, msg_open, msg_fd, reopen, putcal, immap

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE, TY_CHAR)
	call salloc (class, SZ_LINE, TY_CHAR)
	call salloc (mjdstart, SZ_LINE, TY_CHAR)
	call salloc (mjdend, SZ_LINE, TY_CHAR)
	call salloc (detector, SZ_LINE, TY_CHAR)
	call salloc (imageid, SZ_LINE, TY_CHAR)
	call salloc (filter, SZ_LINE, TY_CHAR)
	call salloc (exptime, SZ_LINE, TY_CHAR)
	call salloc (quality, SZ_LINE, TY_CHAR)
	call salloc (match, SZ_LINE, TY_CHAR)
	call salloc (errstr, SZ_LINE, TY_CHAR)

	# Get  parameters.
	files = imtopenp ("files")
	dtype = clgwrd ("datatype", Memc[line], SZ_LINE, DTYPES)
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	timeout = clgeti ("timeout")
	call clgstr ("class", Memc[class], SZ_LINE)
	call clgstr ("mjdstart", Memc[mjdstart], SZ_LINE)
	call clgstr ("mjdend", Memc[mjdend], SZ_LINE)
	call clgstr ("detector", Memc[detector], SZ_LINE)
	call clgstr ("imageid", Memc[imageid], SZ_LINE)
	call clgstr ("filter", Memc[filter], SZ_LINE)
	call clgstr ("exptime", Memc[exptime], SZ_LINE)
	call clgstr ("quality", Memc[quality], SZ_LINE)
	call clgstr ("match", Memc[match], SZ_LINE)

	if (nowhite (Memc[match], Memc[line], SZ_LINE) == 0)
	    Memc[match] = EOS

	iferr {
	    im = NULL; msg = NULL; frdm = NULL; todm = NULL

	    if (imtlen (files) == 0) {
		tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
		tmp = msg_fd (msg); frdm = tmp
		tmp = reopen (frdm, WRITE_ONLY); todm = tmp

		call clgstr ("value", Memc[line], SZ_LINE)
		call putcal (dtype, Memc[line], im, todm, frdm, timeout,
		    Memc[class], Memc[mjdstart], Memc[mjdend], Memc[detector],
		    Memc[imageid], Memc[filter], Memc[exptime], Memc[quality],
		    Memc[match])
	    } else {
		while (imtgetim (files, Memc[line], SZ_LINE) != EOF) {
		    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
		    tmp = msg_fd (msg); frdm = tmp
		    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

		    switch (dtype) {
		    case DTYPE_IMAGE, DTYPE_KEY:
			tmp = immap (Memc[line], READ_ONLY, 0); im = tmp
			call putcal (dtype, Memc[line], im, todm, frdm, timeout,
			    Memc[class], Memc[mjdstart], Memc[mjdend],
			    Memc[detector], Memc[imageid], Memc[filter],
			    Memc[exptime], Memc[quality], Memc[match])
		    case DTYPE_FILE:
			if (access (Memc[line], 0, 0) == NO) {
			    call sprintf (Memc[errstr], SZ_LINE,
				"File access error (%s)")
				call pargstr (Memc[line])
			    call error (499, Memc[errstr])
			}
			call putcal (dtype, Memc[line], NULL, todm, frdm,
			    timeout, Memc[class], Memc[mjdstart], Memc[mjdend],
			    Memc[detector], Memc[imageid], Memc[filter],
			    Memc[exptime], Memc[quality], Memc[match])
		    case DTYPE_DIR:
			if (access (Memc[line], 0, 0) == NO) {
			    call sprintf (Memc[errstr], SZ_LINE,
				"Directory access error (%s)")
				call pargstr (Memc[line])
			    call error (499, Memc[errstr])
			}
			call putcal (dtype, Memc[line], NULL, todm, frdm,
			    timeout, Memc[class], Memc[mjdstart], Memc[mjdend],
			    Memc[detector], Memc[imageid], Memc[filter],
			    Memc[exptime], Memc[quality], Memc[match])
		    }

		    if (im != NULL)
		        call imunmap (im)
		    call close (frdm)
		    call close (todm)
		    call msg_close (msg)
		}
	    }
	    
	    call clputi ("statcode", 0)
	    call clpstr ("statstr", "PUTCAL")
	    call printf ("0 PUTCAL\n")
	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    if (im != NULL)
	        call imunmap (im)
	    for (ip=line; Memc[ip]!=EOS; ip=ip+1) {
	        if (Memc[ip] == '"')
		    Memc[ip] = '\''
	    }
	    call clputi ("statcode", err)
	    call clpstr ("statstr", Memc[line])
	    call printf ("%d %s\n")
	        call pargi (err)
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)
	call imtclose (files)

	call sfree (sp)
end


# PUTCAL -- Put calibration image or file.
# Errors include missing keywords, timeout, and disconnect.

procedure putcal (dtype, fname, im, todm, frdm, timeout, class,
	mjdstart, mjdend, detector, imageid, filter, exptime, quality, match)

int	dtype			#I Datatype
char	fname[ARB]		#I Calibration filename
pointer	im			#I IMIO pointer (for images)
int	todm			#I FD to send to data manager 
int	frdm			#I FD from to data manager 
long	timeout			#I Timeout (sec)
char	class[ARB]		#I Data class
char	mjdstart[ARB]		#I MJD start
char	mjdend[ARB]		#I MJD end
char	detector[ARB]		#I Detector
char	imageid[ARB]		#I Image ID
char	filter[ARB]		#I Filter
char	exptime[ARB]		#I Exposure time
char	quality[ARB]		#I Quality
char	match[ARB]		#I Match

int	i, err, n, ncombine
real	q
long	t
pointer	list
pointer	sp, key, val, line, fpath

bool	streq()
int	imgeti(), getlline(), strncmp(), errget(), ctor(), imgnfn()
real	imgetr()
long	clktime()
pointer	imofnlu()
errchk	imgstr, getlline, imofnlu

begin
	call smark (sp)
	call salloc (key, SZ_FNAME, TY_CHAR)
	call salloc (val, SZ_LINE, TY_CHAR)
	call salloc (line, SZ_LINE, TY_CHAR)
	call salloc (fpath, SZ_PATHNAME, TY_CHAR)

	# Send request.
	call fprintf (todm, "COMMAND = putcal\n")
	if ((dtype==DTYPE_IMAGE||dtype==DTYPE_KEY) && class[1] == '!') {
	    call imgstr (im, class[2], Memc[line], SZ_LINE)
	    if (streq (Memc[line], "dome flat"))
	        call strcpy ("flat", Memc[line], SZ_LINE)
	} else
	    call strcpy (class, Memc[line], SZ_LINE)
	call fprintf (todm, "CLASS = '%s'\n")
	    call pargstr (Memc[line])

	if ((dtype==DTYPE_IMAGE||dtype==DTYPE_KEY) && mjdstart[1] == '!')
	    call imgstr (im, mjdstart[2], Memc[line], SZ_LINE)
	else
	    call strcpy (mjdstart, Memc[line], SZ_LINE)
	call fprintf (todm, "MJDSTART = '%s'\n")
	    call pargstr (Memc[line])

	if ((dtype==DTYPE_IMAGE||dtype==DTYPE_KEY) && mjdend[1] == '!')
	    call imgstr (im, mjdend[2], Memc[line], SZ_LINE)
	else
	    call strcpy (mjdend, Memc[line], SZ_LINE)
	call fprintf (todm, "MJDEND = '%s'\n")
	    call pargstr (Memc[line])

	if (detector[1] != EOS) {
	    if ((dtype==DTYPE_IMAGE||dtype==DTYPE_KEY) && detector[1] == '!') {
		Memc[line] = EOS
		list = imofnlu (im, detector[2])
		while (imgnfn (list, Memc[key], SZ_FNAME) != EOF) {
		    iferr (call imgstr (im, Memc[key], Memc[val],
			SZ_LINE)) {
		    #    if (Memc[line] != EOS)
		    #	call strcat (" ", Memc[line], SZ_LINE)
		    #    call strcat ("INDEF", Memc[line], SZ_LINE)
			;
		    } else {
			if (!(streq (Memc[key], "DTTELESC") &&
			      (streq (Memc[val], "kp4m") ||
			       streq (Memc[val], "ct4m")))) {
			    if (Memc[line] != EOS)
				call strcat (" ", Memc[line], SZ_LINE)
			    call strcat (Memc[val], Memc[line], SZ_LINE)
			}
		    }
		}
		call imcfnl (list)
	    } else
		call strcpy (detector, Memc[line], SZ_LINE)
	    if (Memc[line] != EOS) {
		call fprintf (todm, "DETECTOR = '%s'\n")
		    call pargstr (Memc[line])
	    }
	}

	if ((dtype==DTYPE_IMAGE||dtype==DTYPE_KEY) && imageid[1] == '!')
	    call imgstr (im, imageid[2], Memc[line], SZ_LINE)
	else
	    call strcpy (imageid, Memc[line], SZ_LINE)
	if (Memc[line] != EOS) {
	    call fprintf (todm, "IMAGEID = '%s'\n")
		call pargstr (Memc[line])
	}

	if ((dtype==DTYPE_IMAGE||dtype==DTYPE_KEY) && filter[1] == '!') {
	    iferr (call imgstr (im, filter[2], Memc[line], SZ_LINE))
	        Memc[line] = EOS
	} else
	    call strcpy (filter, Memc[line], SZ_LINE)
	if (Memc[line] != EOS) {
	    call fprintf (todm, "FILTER = '%s'\n")
		call pargstr (Memc[line])
	}

	if (dtype==DTYPE_IMAGE) {
	    if (exptime[1] == '!' && im != NULL) {
		iferr (q = imgetr (im, exptime[2]))
		    q = INDEFR
	    } else {
	        i = 1
		if (ctor (exptime, i, q) == 0)
		    q = INDEFR
	    }
	    if (!IS_INDEFR(q)) {
		call fprintf (todm, "EXPTIME = '%g'\n")
		    call pargr (q)
	    }
	}

	if (dtype==DTYPE_IMAGE && im != NULL) {
	    iferr (ncombine = imgeti (im, "ncombine"))
	        ncombine = 1
	} else
	    ncombine = INDEFI
	if (!IS_INDEFI(ncombine)) {
	    call fprintf (todm, "NCOMBINE = %d\n")
		call pargi (ncombine)
	}

	if (dtype==DTYPE_IMAGE) {
	    if (quality[1] == '!' && im != NULL) {
		iferr (q = imgetr (im, quality[2]))
		    q = 0.
	    } else {
		i = 1
		if (ctor (quality, i, q) == 0)
		    q = 0.
	    }
	    call fprintf (todm, "QUALITY = %g\n")
		call pargr (q)
	}

	if (match[1] == '!' && im != NULL) {
	    Memc[line] = EOS
	    list = imofnlu (im, match[2])
	    while (imgnfn (list, Memc[key], SZ_FNAME) != EOF) {
		iferr (call imgstr (im, Memc[key], Memc[val], SZ_LINE)) {
		    if (Memc[line] != EOS)
			call strcat (" ", Memc[line], SZ_LINE)
		    call strcat ("INDEF", Memc[line], SZ_LINE)
		} else {
		    if (!(streq (Memc[key], "CCDSUM") &&
			streq (Memc[val], "1 1"))) {
			if (Memc[line] != EOS)
			    call strcat (" ", Memc[line], SZ_LINE)
			call strcat (Memc[val], Memc[line], SZ_LINE)
		    }
		}
	    }
	    call imcfnl (list)
	} else
	    call strcpy (match, Memc[line], SZ_LINE)
	if (Memc[line] != EOS) {
	    call fprintf (todm, "MATCH = '%s'\n")
		call pargstr (Memc[line])
	}

	# Put file name.
	switch (dtype) {
	case DTYPE_IMAGE:
	    call fpathname (fname, Memc[fpath], SZ_PATHNAME)
	    call fprintf (todm, "DATATYPE = 'image'\n")
	case DTYPE_FILE:
	    call fpathname (fname, Memc[fpath], SZ_PATHNAME)
	    call fprintf (todm, "DATATYPE = 'file'\n")
	case DTYPE_DIR:
	    call fpathname (fname, Memc[fpath], SZ_PATHNAME)
	    call fprintf (todm, "DATATYPE = 'directory'\n")
	case DTYPE_KEY:
	    call fprintf (todm, "DATATYPE = 'keyword'\n")
	    if (im == NULL)
	        call strcpy (fname, Memc[fpath], SZ_PATHNAME)
	    else iferr (call imgstr (im, class, Memc[fpath], SZ_PATHNAME))
	        Memc[fpath] = EOS
	}
	call fprintf (todm, "VALUE = '%s'\n")
	    call pargstr (Memc[fpath])
	call fprintf (todm, "EOF\n")
	call flush (todm)

	# Read from data manager.  Check for timeout or disconnect.
	t = clktime (0)
	repeat {

            # Poll for a response.
            iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
                err = errget (Memc[line], SZ_LINE)

                # ONLY want to trap socket read errors here (e.g. nothing yet)
                # for all other errors we pass them along
                if (err != 746)
                    call error (err, Memc[line])

                # check if we exceeded our timeout, if so, throw an error
                if ((clktime(0) - t) > timeout)
                    call error (499, "Data manager response timed out !!")

                # pause
                call zwmsec (50)
                next
            }

	    if (n == EOF)
		call error (498, "Data manager disconnected")
	    if (streq (Memc[line], "EOF\n"))
	       break
	    if (strncmp (Memc[line], "STATUS", 6) == 0) {
	        call sscan (Memc[line])
		call gargwrd (Memc[fpath], SZ_PATHNAME)
		call gargwrd (Memc[fpath], SZ_PATHNAME)
		call gargwrd (Memc[fpath], SZ_PATHNAME)
		call sscan (Memc[fpath])
		call gargi (err)
		call gargstr (Memc[line], SZ_LINE)
		if (err > 0)
		    call error (err, Memc[line])
		break
	    }
	}

	call sfree (sp)
end
