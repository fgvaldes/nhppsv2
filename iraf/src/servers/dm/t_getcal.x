include	<finfo.h>
include	<lexnum.h>
include	"dm.h"

# Return information from data manager.
define	DMKEYS		"|STATUS|SIZE|MTIME|VALUE|PATH|TYPE|"
define	DMSTATUS	1
define	DMSIZE		2
define	DMMTIME		3
define	DMVALUE		4
define	DMPATH		5
define	DMTYPE		6

define	SZ_DMSTRING	199
define	LEN_DM		205
define	DM_STATUS	Memi[$1]
define	DM_TYPE		Memi[$1+1]
define	DM_SIZE		Memi[$1+2]
define	DM_MTIME	Memi[$1+3]
define	DM_VALUE	Memc[P2C($1+4)]
define	DM_PATH		Memc[P2C($1+104)]

define	DMTYPES		"|file|image|directory|keyword|"
define	DMFILE		1
define	DMIMAGE		2
define	DMDIR		3
define	DMKW		4

# Classes
define	CLASSES		"|xtalkfile|bpm|zero|dark|dflat|sflat|_flat|_flatasc|\
			 |_flatdesc|fringe|wcsdb|uparm|ampnames|sft|pupil|"
define	CXTALK		1
define	CBPM		2
define	CZERO		3
define	CDARK		4
define	CFLAT		5
define	CSFLAT		6
define	C_FLAT		7
define	C_FLATASC	8
define	C_FLATDESC	10
define	CFRINGE		11
define	CWCSDB		12
define	CUPARM		13
define	CAMPNM		14
define	CSFT		15
define	CPUPIL		16

# Observation types
define	OBSTYPES	"|zero|dark|dome flat|sky flat|object|"
define	OZERO		1
define	ODARK		2
define	OFLAT		3
define	OSFLAT		4
define	OOBJECT		5

# Number of retrys.
define	NTRY		100

# Number of times to check lock file.
define	NLOCK		300

# T_GETCAL -- Get calibrations for images from a data manager.

procedure t_getcal ()

int	images				# List of images
int	classes				# List of classes
pointer	dm				# Data manager connection
pointer	caldir				# Calibration directory
int	timeout				# Time out (sec)
pointer	obstype				# Observation type
pointer	detector			# Detector
pointer	imageid				# Image ID
pointer	filter				# Filter
pointer	exptime				# Exposure time
real	dexptime			# Exposure time window
pointer	mjd				# MJD of calibration
real	dmjd				# MJD window
real	quality				# Desired minimum quality
pointer	match				# Desired match string

int	i, nims, stat, err
#long	t1[2]
pointer	sp, line, tmp, msg, frdm, todm, im

int	clpopnu(), clgeti(), imtopenp(), imtgetim(), imtlen()
int	reopen(), msg_fd(), errget(), nowhite()
real	clgetr()
pointer	msg_open(), immap()
errchk	immap, imtopenp, msg_open, msg_fd, reopen, getcal, close

begin
	call smark (sp)
	call salloc (dm, SZ_FNAME, TY_CHAR)
	call salloc (caldir, SZ_PATHNAME, TY_CHAR)
	call salloc (line, SZ_LINE, TY_CHAR)
	call salloc (obstype, SZ_LINE, TY_CHAR)
	call salloc (detector, SZ_LINE, TY_CHAR)
	call salloc (imageid, SZ_LINE, TY_CHAR)
	call salloc (filter, SZ_LINE, TY_CHAR)
	call salloc (exptime, SZ_LINE, TY_CHAR)
	call salloc (mjd, SZ_LINE, TY_CHAR)
	call salloc (match, SZ_LINE, TY_CHAR)

	#call sys_mtime (t1)

	# Get  parameters.
	images = imtopenp ("images")
	classes = clpopnu ("classes")
	call clgstr ("dm", Memc[dm], SZ_FNAME)
	call clgstr ("caldir", Memc[caldir], SZ_LINE)
	timeout = clgeti ("timeout")
	call clgstr ("obstype", Memc[obstype], SZ_LINE)
	call clgstr ("detector", Memc[detector], SZ_LINE)
	call clgstr ("imageid", Memc[imageid], SZ_LINE)
	call clgstr ("filter", Memc[filter], SZ_LINE)
	call clgstr ("exptime", Memc[exptime], SZ_LINE)
	dexptime = clgetr ("dexptime")
	call clgstr ("mjd", Memc[mjd], SZ_LINE)
	dmjd = clgetr ("dmjd")
	quality = clgetr ("quality")
	call clgstr ("match", Memc[match], SZ_LINE)

	if (nowhite (Memc[match], Memc[line], SZ_LINE) == 0)
	    Memc[match] = EOS

	iferr {
	    im = NULL; msg = NULL; frdm = NULL; todm = NULL

	    nims = imtlen (images)
	    if (nims == 0) {
	       if (Memc[obstype]=='!'||Memc[detector]=='!'||
		    Memc[imageid]=='!'||Memc[filter]=='!'||
		    Memc[exptime]=='!'||Memc[mjd]=='!')
		    call error (1, "No image for keyword reference")
		stat = OK
	    } else
	        stat = imtgetim (images, Memc[line], SZ_LINE)
		
	    while (stat != EOF) {
		if (nims != 0) {
		    tmp = immap (Memc[line], READ_WRITE, 0); im = tmp
		}

		do i = 1, NTRY {
		    # Open connection to data manager for 2-way communication.
		    tmp = msg_open (Memc[dm], READ_WRITE, TEXT_FILE); msg = tmp
		    tmp = msg_fd (msg); frdm = tmp
		    tmp = reopen (frdm, WRITE_ONLY); todm = tmp

		    # Get calibration data.
		    err = 0
		    iferr (call getcal (im, Memc[line], Memc[caldir], todm,
			frdm, timeout, classes, Memc[obstype],
			Memc[detector], Memc[imageid], Memc[filter],
			Memc[exptime], dexptime, Memc[mjd], dmjd,
			quality, Memc[match]))
			err = errget (Memc[line], SZ_LINE)

		    # Close connection.
		    call close (frdm)
		    call close (todm)
		    call msg_close (msg)

		    # Check for errors.
		    if (err == 0)
		        break
		    else if (err == 498 || err == 499) {
			call eprintf ("WARNING: %s ... retry\n")
			    call pargstr (Memc[line])
		    } else
			call error (err, Memc[line])
		}
		    
		# Close image (if necessary) and get next image.
		if (im != NULL)
		    call imunmap (im)

		if (err != 0)
		    call error (err, Memc[line])

	        stat = imtgetim (images, Memc[line], SZ_LINE)
	    }

	    # Signal success.
	    call clputi ("statcode", 0)
	    call clpstr ("statstr", "GETCAL")

	} then {
	    # Send the error to the standard output.
	    err = errget (Memc[line], SZ_LINE)
	    if (im != NULL)
	        call imunmap (im)
	    call clputi ("statcode", err)
	    call clpstr ("statstr", Memc[line])
	    call clpstr ("value", "")
	    call printf ("%d %s\n")
		call pargi (err)
		call pargstr (Memc[line])
	}

	# Close descriptors if needed.
	if (frdm != NULL)
	    call close (frdm)
	if (todm != NULL)
	    call close (todm)
	if (msg != NULL)
	    call msg_close (msg)
	call clpcls (classes)
	call imtclose (images)

	#call sys_ptime (STDERR, "t_getcal", t1)

	call sfree (sp)
end


# GETCAL -- Get calibration keywords for image.
# Errors include missing keywords, timeout, disconnect, and no calib found.
# The classes to get are those in the list excluding those not required
# by the observation type.

procedure getcal (im, fname, caldir, todm, frdm, timeout, classes, obstype,
	detector, imageid, filter, exptime, dexptime, mjd, dmjd,
	 quality, match)

pointer	im			#I IMIO pointer
char	fname[ARB]		#I Image filename
char	caldir[ARB]		#I Calibration directory
int	todm			#I FD to send to data manager 
int	frdm			#I FD from to data manager 
long	timeout			#I Timeout (sec)
int	classes			#I Classes requested
char	obstype[ARB]		#I Observation type
char	detector[ARB]		#I Detector
char	imageid[ARB]		#I Image ID
char	filter[ARB]		#I Filter
char	exptime[ARB]		#I Exposure time
real	dexptime		#I Exposure time window
char	mjd[ARB]		#I MJD
real	dmjd			#I MJD window
real	quality			#I Desired minimum quality
char	match[ARB]		#I Match

bool	cp, del, isnull
int	i, fd, iobstype, iclass, err, n, ip, lextype
long	t, ta, tb, fstat[LEN_FINFO]
#long	t1[2], t2[2]
pointer	sp, patbuf, class, line, path, key, val, dm, lock, cmd, list

bool	streq()
int	clgfil(), strdic(), getlline(), ctowrd(), ctoi(), patmake(), patmatch()
int	errget(), strlen(), stridxs(), strldx(), nscan(), lexnum()
int	imaccf(), imofnlu(), imgnfn()
int	access(), open(), finfo(), futime()
long	clktime()
errchk	imgstr, getlline, delete, clcmdw, open, fcopy, delete, fmkdir, imofnlu

begin
	call smark (sp)
	call salloc (patbuf, SZ_FNAME, TY_CHAR)
	call salloc (class, SZ_FNAME, TY_CHAR)
	call salloc (line, 5*SZ_LINE, TY_CHAR)
	call salloc (cmd, SZ_LINE, TY_CHAR)
	call salloc (path, SZ_LINE, TY_CHAR)
	call salloc (key, SZ_FNAME, TY_CHAR)
	call salloc (val, SZ_LINE, TY_CHAR)
	call salloc (dm, LEN_DM, TY_STRUCT)
	call salloc (lock, SZ_FNAME, TY_CHAR)

	#call sys_mtime (t1)

	ip = patmake ("{[ck#]}[ #-]*[0-9][0-9][0-9][0-9]",
	    Memc[patbuf], SZ_FNAME)

	# Get input observation type.
	if (obstype[1] == '!')
	    call imgstr (im, obstype[2], Memc[line], SZ_LINE)
	else
	    call strcpy (obstype, Memc[line], SZ_LINE)
	iobstype = strdic (Memc[line], Memc[line], SZ_LINE, OBSTYPES)
	if (iobstype==OSFLAT)
	    iobstype = OFLAT

	# Get each requested class.
	call clprew (classes)
	while (clgfil (classes, Memc[class], SZ_FNAME) != EOF) {
	    iclass = strdic (Memc[class], Memc[class], SZ_FNAME, CLASSES)
	    switch (iclass) {
	    case CSFLAT, C_FLAT:
	        iclass = CFLAT
	    case C_FLATASC:
	        iclass = CFLAT
		call strcpy ("_flat asc", Memc[class], SZ_FNAME)
	    case C_FLATDESC:
	        iclass = CFLAT
		call strcpy ("_flat desc", Memc[class], SZ_FNAME)
	    }

	    if (iclass==CSFLAT||iclass==C_FLAT)
	        iclass = CFLAT

	    # Skip classes which are not needed by the observation type.
	    switch (iobstype) {
	    case OZERO, ODARK:
	        if (iclass==CZERO||iclass==CDARK||iclass==CFLAT||
		    iclass==CFRINGE||iclass==CPUPIL||iclass==CSFT||
		    iclass==CWCSDB)
		    next
	    case OFLAT:
	        if (iclass==CFLAT||iclass==CFRINGE||
		    iclass==CPUPIL||iclass==CSFT||iclass==CWCSDB)
		    next
	    }
	    
	    # Send common keywords.
	    #call sys_mtime (t2)
	    call fprintf (todm, "COMMAND = getcal\n")
	    call fprintf (todm, "CLASS = '%s'\n")
		call pargstr (Memc[class])

	    if (mjd[1] == '!')
		call imgstr (im, mjd[2], Memc[line], SZ_LINE)
	    else
		call strcpy (mjd, Memc[line], SZ_LINE)
	    call fprintf (todm, "MJD = '%s'\n")
		call pargstr (Memc[line])

	    if (!IS_INDEFR(dmjd)) {
		call fprintf (todm, "DMJD = %g\n")
		    call pargr (dmjd)
	    }

	    if (detector[1] != EOS) {
		isnull = false
                if (detector[1] == '!' && im != NULL) {
		    Memc[line] = EOS
		    list = imofnlu (im, detector[2])
		    while (imgnfn (list, Memc[key], SZ_FNAME) != EOF) {
			iferr (call imgstr (im, Memc[key], Memc[val],
			    SZ_LINE)) {
			#    if (Memc[line] != EOS)
			#	call strcat (" ", Memc[line], SZ_LINE)
			#    call strcat ("INDEF", Memc[line], SZ_LINE)
			    ;
			} else {
			    if (streq (Memc[key], "DTTELESC"))
			        isnull = true
			    if (!(streq (Memc[key], "DTTELESC") &&
			          (streq (Memc[val], "kp4m") ||
				   streq (Memc[val], "ct4m")))) {
				if (Memc[line] != EOS)
				    call strcat (" ", Memc[line], SZ_LINE)
				call strcat (Memc[val], Memc[line], SZ_LINE)
			    }
			}
		    }
		    call imcfnl (list)
                } else
                    call strcpy (detector, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
		    call fprintf (todm, "DETECTOR = 'like %s'\n")
			call pargstr (Memc[line])
		} else if (isnull)
		    call fprintf (todm, "DETECTOR 'isnull'\n")
	    }

	    # Send class specific keywords.
	    switch (iclass) {
	    case CAMPNM, CXTALK, CUPARM:
	        ;
            case CBPM:
                if (imageid[1] == '!')
                    call imgstr (im, imageid[2], Memc[line], SZ_LINE)
                else
                    call strcpy (imageid, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
                    call fprintf (todm, "IMAGEID = '%s'\n")
			call pargstr (Memc[line])
                }
                if (filter[1] == '!') {
                    iferr (call imgstr (im, filter[2], Memc[line], SZ_LINE))
			Memc[line] = EOS
                } else
                    call strcpy (filter, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
		    # Special code for using a serial number.
		    ip = patmatch (Memc[line], Memc[patbuf])
		    if (ip > 0) {
		        call strcpy (Memc[line+ip-6], Memc[val], 5)
		        call sprintf (Memc[line], SZ_LINE, "%%%s%%")
			    call pargstr (Memc[val])
		    }
                    call fprintf (todm, "FILTER = '%s'\n")
                    call pargstr (Memc[line])
                }
            case CZERO:
                if (imageid[1] == '!')
                    call imgstr (im, imageid[2], Memc[line], SZ_LINE)
                else
                    call strcpy (imageid, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
                    call fprintf (todm, "IMAGEID = '%s'\n")
			call pargstr (Memc[line])
                }

		if (!IS_INDEFR(quality)) {
		    call fprintf (todm, "QUALITY = %.3g\n")
			call pargr (quality)
		}
            case CDARK:
                if (imageid[1] == '!')
                    call imgstr (im, imageid[2], Memc[line], SZ_LINE)
                else
                    call strcpy (imageid, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
                    call fprintf (todm, "IMAGEID = '%s'\n")
			call pargstr (Memc[line])
                }

                if (exptime[1] == '!') {
                    iferr (call imgstr (im, exptime[2], Memc[line], SZ_LINE))
			Memc[line] = EOS
                } else
                    call strcpy (exptime, Memc[line], SZ_LINE)
		if (Memc[line] != EOS) {
		    call fprintf (todm, "EXPTIME = '%s'\n")
			call pargstr (Memc[line])
		    if (!IS_INDEFR(dexptime)) {
			call fprintf (todm, "DEXPTIME = %g\n")
			    call pargr (dexptime)
		    }
		}

		if (!IS_INDEFR(quality)) {
		    call fprintf (todm, "QUALITY = %.3g\n")
			call pargr (quality)
		}
            case CFLAT:
                if (imageid[1] == '!')
                    call imgstr (im, imageid[2], Memc[line], SZ_LINE)
                else
                    call strcpy (imageid, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
                    call fprintf (todm, "IMAGEID = '%s'\n")
			call pargstr (Memc[line])
                }
        
                if (filter[1] == '!') {
                    iferr (call imgstr (im, filter[2], Memc[line], SZ_LINE))
			Memc[line] = EOS
                } else
                    call strcpy (filter, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
		    # Special code for using a serial number.
		    ip = patmatch (Memc[line], Memc[patbuf])
		    if (ip > 0) {
		        call strcpy (Memc[line+ip-6], Memc[val], 5)
		        call sprintf (Memc[line], SZ_LINE, "%%%s%%")
			    call pargstr (Memc[val])
		    }
                    call fprintf (todm, "FILTER = '%s'\n")
                    call pargstr (Memc[line])
                }

                if (exptime[1] == '!') {
                    iferr (call imgstr (im, exptime[2], Memc[line], SZ_LINE))
			Memc[line] = EOS
                } else
                    call strcpy (exptime, Memc[line], SZ_LINE)
		if (Memc[line] != EOS) {
		    call fprintf (todm, "EXPTIME = '%s'\n")
			call pargstr (Memc[line])
		    if (!IS_INDEFR(dexptime)) {
			call fprintf (todm, "DEXPTIME = %g\n")
			    call pargr (dexptime)
		    }
		}

		if (!IS_INDEFR(quality)) {
		    call fprintf (todm, "QUALITY = %.3g\n")
			call pargr (quality)
		}
            case CSFT,CFRINGE,CPUPIL:
                if (imageid[1] == '!')
                    call imgstr (im, imageid[2], Memc[line], SZ_LINE)
                else
                    call strcpy (imageid, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
                    call fprintf (todm, "IMAGEID = '%s'\n")
			call pargstr (Memc[line])
                }
        
                if (filter[1] == '!') {
                    iferr (call imgstr (im, filter[2], Memc[line], SZ_LINE))
			Memc[line] = EOS
                } else
                    call strcpy (filter, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
		    # Special code for using a serial number.
		    ip = patmatch (Memc[line], Memc[patbuf])
		    if (ip > 0) {
		        call strcpy (Memc[line+ip-6], Memc[val], 5)
		        call sprintf (Memc[line], SZ_LINE, "%%%s%%")
			    call pargstr (Memc[val])
		    }
                    call fprintf (todm, "FILTER = '%s'\n")
                    call pargstr (Memc[line])
                }

		if (!IS_INDEFR(quality)) {
		    call fprintf (todm, "QUALITY = %.3g\n")
			call pargr (quality)
		}
            case CWCSDB:
                if (filter[1] == '!') {
                    iferr (call imgstr (im, filter[2], Memc[line], SZ_LINE))
			Memc[line] = EOS
                } else
                    call strcpy (filter, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
		    # Special code for using a serial number.
		    ip = patmatch (Memc[line], Memc[patbuf])
		    if (ip > 0) {
		        call strcpy (Memc[line+ip-6], Memc[val], 5)
		        call sprintf (Memc[line], SZ_LINE, "%%%s%%")
			    call pargstr (Memc[val])
		    }
                    call fprintf (todm, "FILTER = '%s'\n")
			call pargstr (Memc[line])
                }
            default:
                if (imageid[1] == '!')
                    call imgstr (im, imageid[2], Memc[line], SZ_LINE)
                else
                    call strcpy (imageid, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
                    call fprintf (todm, "IMAGEID = '%s'\n")
			call pargstr (Memc[line])
                }
        
                if (filter[1] == '!') {
                    iferr (call imgstr (im, filter[2], Memc[line], SZ_LINE))
			Memc[line] = EOS
                } else
                    call strcpy (filter, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
		    # Special code for using a serial number.
		    ip = patmatch (Memc[line], Memc[patbuf])
		    if (ip > 0) {
		        call strcpy (Memc[line+ip-6], Memc[val], 5)
		        call sprintf (Memc[line], SZ_LINE, "%%%s%%")
			    call pargstr (Memc[val])
		    }
                    call fprintf (todm, "FILTER = '%s'\n")
			call pargstr (Memc[line])
                }

		if (!IS_INDEFR(quality)) {
		    call fprintf (todm, "QUALITY = %.3g\n")
			call pargr (quality)
		}
	    }

	    # Generic match string.
	    if (match[1] != EOS) {
		isnull = false
                if (match[1] == '!' && im != NULL) {
		    Memc[line] = EOS
		    list = imofnlu (im, match[2])
		    while (imgnfn (list, Memc[key], SZ_FNAME) != EOF) {
			iferr (call imgstr (im, Memc[key], Memc[val],
			    SZ_LINE)) {
			    if (Memc[line] != EOS)
				call strcat (" ", Memc[line], SZ_LINE)
			    call strcat ("INDEF", Memc[line], SZ_LINE)
			} else {
			    if (streq (Memc[key], "CCDSUM"))
			        isnull = true
			    if (!(streq (Memc[key], "CCDSUM") &&
			        streq (Memc[val], "1 1"))) {
				if (Memc[line] != EOS)
				    call strcat (" ", Memc[line], SZ_LINE)
				call strcat (Memc[val], Memc[line], SZ_LINE)
			    }
			}
		    }
		    call imcfnl (list)
                } else
                    call strcpy (match, Memc[line], SZ_LINE)
                if (Memc[line] != EOS) {
		    call fprintf (todm, "MATCH = 'like %s'\n")
			call pargstr (Memc[line])
		} else if (isnull)
		    call fprintf (todm, "MATCH = 'isnull'\n")
	    }

	    # Send the value keyword.  This should not be needed but is
	    # currently needed by the DM??
	    call fprintf (todm, "VALUE = dummy\n")

	    # Send the end.
	    call fprintf (todm, "EOF\n")
	    call flush (todm)

	    #call sprintf (Memc[key], SZ_FNAME, "send %s")
	    #call pargstr (Memc[class])
	    #call sys_ptime (STDERR, Memc[key], t2)
	    #call sys_mtime (t2)

	    # Read from data manager.  Check for timeout or disconnect.
	    DM_STATUS(dm) = 0
	    DM_TYPE(dm) = 0
	    t = clktime (0)
	    ta = t
	    repeat {
                # Poll for a response.
                iferr (n = getlline (frdm, Memc[line], 4*SZ_LINE)) {
                    err = errget (Memc[line], SZ_LINE)

                    # ONLY want to trap socket read errors here
		    # (e.g. nothing yet)
                    # for all other errors we pass them along
                    if (err != 746)
                        call error (err, Memc[line])

                    # check if we exceeded our timeout, if so, throw an error
		    tb = clktime (0)
                    if ((tb - t) > timeout)
                        call error (499, "Timed out")

		    if ((tb - ta) > 60) {
			call eprintf ("Waiting...%d\n")
			call pargl (timeout - tb + t)
			ta = tb
		    }

                    # pause
                    call zwmsec (100)
                    next
                }

		if (n == EOF)
		    call error (498, "Disconnected")
		if (streq (Memc[line], "EOF\n"))
		   break

		ip = stridxs ("=", Memc[line])
		if (ip > 0)
		    Memc[line+ip-1] = ' '

		call sscan (Memc[line])
		call gargwrd (Memc[key], SZ_FNAME)
		call gargwrd (Memc[line], SZ_LINE)
		call sscan (Memc[line])

		switch (strdic (Memc[key], Memc[key], SZ_FNAME, DMKEYS)) {
		case DMSTATUS:
		    call gargi (DM_STATUS(dm))
		    if (nscan() == 0)
		        next
		    call gargstr (Memc[line], SZ_LINE)
		    call strcpy (Memc[line+1], Memc[line], SZ_LINE)
		    if (DM_STATUS(dm) > 0) {
			if (DM_STATUS(dm) != DM_NOCAL)
			    call error (DM_STATUS(dm), Memc[line])
			if (iclass==CZERO || iclass==CDARK || iclass==CFLAT)
			    call error (DM_STATUS(dm), Memc[line])
		    }
		case DMSIZE:
		    call gargi (DM_SIZE(dm))
		case DMMTIME:
		    call gargi (DM_MTIME(dm))
		    DM_MTIME(dm) = DM_MTIME(dm) - 315558000
		case DMVALUE:
		    call gargstr (DM_VALUE(dm), SZ_DMSTRING)
		case DMPATH:
		    call gargwrd (DM_PATH(dm), SZ_DMSTRING)
		case DMTYPE:
		    call gargwrd (Memc[key], SZ_FNAME)
		    DM_TYPE(dm) = strdic (Memc[key], Memc[key], SZ_FNAME,
		        DMTYPES)
		}
	    }

	    #call sprintf (Memc[key], SZ_FNAME, "response %s")
	    #call pargstr (Memc[class])
	    #call sys_ptime (STDERR, Memc[key], t2)
	    #call sys_mtime (t2)

	    # Check whether to update files or directories.
	    switch (DM_TYPE(dm)) {
	    case DMFILE, DMIMAGE, DMDIR:
		# Replace full path by original caldir.
		call sprintf (Memc[line], SZ_LINE, "%s%s")
		    call pargstr (caldir)
		    call pargstr (DM_VALUE(dm))

		# Use lock file to avoid conflicts.  Note that this is
		# for a specific file rather than a blanket lock on
		# the caldir.  So another process can be adding a
		# different file at the same time.

		call sprintf (Memc[lock], SZ_LINE, "%s.lock")
		    call pargstr (Memc[line])

		do i = 1, NLOCK {
		    cp = false; del = false
		    if (finfo (Memc[line], fstat) == ERR) {
			if (access (caldir, 0, 0) == NO)
			    call fmkdir (caldir)
			cp = true
		    } else {
			if (FI_SIZE(fstat) != DM_SIZE(dm) ||
			    FI_MTIME(fstat) != DM_MTIME(dm)) {
			    switch (FI_TYPE(fstat)) {
			    case FI_REGULAR, FI_EXEC:
				del = true
			    }
			    cp = true
			}
		    }

		    if (!cp)
		        break

		    if (access (Memc[lock], 0, 0) == NO) {
			fd = open (Memc[lock], NEW_FILE, TEXT_FILE)
			call close (fd)
			break
		    }

		    call tsleep (2)
		}
		if (i > NLOCK)
		    call error (3, "Lock file not going away")
	    default:
		cp = false
		call strcpy (DM_VALUE(dm), Memc[line], SZ_LINE)
	    }

	    # Double check.  In particular, the earlier file check may
	    # have occurred while it was being created by another
	    # process.

	    if (cp) {
		cp = false; del = false
		if (finfo (Memc[line], fstat) == ERR)
		    cp = true
		else {
		    if (FI_SIZE(fstat) != DM_SIZE(dm) ||
			FI_MTIME(fstat) != DM_MTIME(dm)) {
			switch (FI_TYPE(fstat)) {
			case FI_REGULAR, FI_EXEC:
			    del = true
			}
			cp = true
		    }
		}
		if (!cp)
		    call delete (Memc[lock])
	    }

	    # Copy files if necessary.
	    if (cp) {
		# Clean cache.  Note currently this is hardwired but later
		# we might add arguments.  We use a script so that we can
		# control things without recompiling.
		call fpathname (caldir, Memc[path], SZ_PATHNAME)
		ip = stridxs ("!", Memc[path])
		if (ip > 0)
		    call strcpy (Memc[path+ip], Memc[path], SZ_PATHNAME)
		call sprintf (Memc[cmd], SZ_LINE, "!!cleanmariocal %s")
		    call pargstr (Memc[path])
		call clcmdw (Memc[cmd])

		call sprintf (Memc[val], SZ_LINE, "%s/%s")
		    call pargstr (DM_PATH(dm))
		    call pargstr (DM_VALUE(dm))
		switch (DM_TYPE(dm)) {
		case DMFILE, DMIMAGE:
		    if (del)
			call delete (Memc[line])
		    call fcopy (Memc[val], Memc[line])
		    #if (futime (Memc[line], DM_MTIME(dm), DM_MTIME(dm)) == ERR)
		    if (futime (Memc[line], NULL, DM_MTIME(dm)) == ERR)
		        ;
		case DMDIR:
		    #call fpathname (caldir, Memc[path], SZ_PATHNAME)
		    #ip = stridxs ("!", Memc[path])
		    #if (ip > 0)
			#call strcpy (Memc[path+ip], Memc[path], SZ_PATHNAME)
		    ip = stridxs ("!", Memc[val])
		    if (ip > 0)
		        Memc[val+ip-1] = ':'
		    call sprintf (Memc[line], SZ_LINE, "!!rsync -aq %s %s")
		        call pargstr (Memc[val])
			call pargstr (Memc[path])
		    call clcmdw (Memc[line])
		    call sprintf (Memc[line], SZ_LINE, "%s%s")
			call pargstr (caldir)
			call pargstr (DM_VALUE(dm))
		}

		# Remove lock file.
		call tsleep (2)
		call delete (Memc[lock])

		#call sprintf (Memc[key], SZ_FNAME, "copy %s")
		#call pargstr (Memc[class])
		#call sys_ptime (STDERR, Memc[key], t2)
	    }

	    # Record result in header or print.
	    call clpstr ("value", Memc[line])
	    if (im != NULL) {
	        if (strlen (Memc[line]) >= 68) {
		    if (DM_TYPE(dm) == DMIMAGE) {
		        ip = strldx (".", Memc[line])
		        if (ip > 0) {
			    if (streq (Memc[line+ip-1], ".fits") ||
			        streq (Memc[line+ip-1], ".fit") ||
			        streq (Memc[line+ip-1], ".imh") ||
			        streq (Memc[line+ip-1], ".pl"))
			    Memc[line+ip-1] = EOS
			}
		    }
		    if (strlen (Memc[line]) >= 68) {
			call eprintf ("WARNING GETCAL: Line is too long (%s)\n")
			    call pargstr (Memc[line])
			#call eprintf ("ERROR GETCAL: Line is too long (%s)\n")
			#    call pargstr (Memc[line])
		        #call error (2, "String too long for header keyword")
		    }
		}
		if (iclass==CFLAT)
		    call imastr (im, "flat", Memc[line])
		else {
		    if (imaccf (im, Memc[class]) == NO) {
			ip = 1; lextype = lexnum (Memc[line], ip, n)
			if (n != strlen (Memc[line]))
			    lextype = LEX_NONNUM
			switch (lextype) {
			case LEX_REAL:
			    call imaddf (im, Memc[class], "r")
			case LEX_DECIMAL:
			    call imaddf (im, Memc[class], "i")
			default:
			    call imaddf (im, Memc[class], "c")
			}
		    }
		    call impstr (im, Memc[class], Memc[line])
		}

		call strupr (Memc[class])
		call printf ("0 %s = '%s'\n")
		    call pargstr (Memc[class])
		    call pargstr (Memc[line])
	    } else {
		call strupr (Memc[class])
		call printf ("%s = '%s'\n")
		    call pargstr (Memc[class])
		    call pargstr (Memc[line])
	    }
	}

	#call sys_ptime (STDERR, "getcal", t1)

	call sfree (sp)
end
