include	<fset.h>


# T_SNDSERVER -- Send the input to a server and write response to the output.
# The handshake signal to switch between sending and receiving is "EOF\n".
# When an actual EOF from the input is received the response will be
# written and the task will exit.  A timeout or disconnect by the server
# will also be detected and cause the task to exit.  Note that the time
# out is approximate because of polling overhead.

procedure t_sndserver ()

pointer	inlist				# List of input files
pointer	server				# Server name
pointer	output				# Output file
int	timeout				# Time out (sec)

int	stat, n, err
long	t
pointer	tmp, in, msg, srvin, srvout, out
pointer	sp, input, line

bool	streq()
int	clpopnu(), clgfil()
int	clgeti(), open(), reopen(), msg_fd(), getline(), errget()
long	clktime()
pointer	msg_open()
errchk	open, msg_open, msg_fd, reopen, getline, putline

define	err_	10

begin
	call smark (sp)
	call salloc (input, SZ_FNAME, TY_CHAR)
	call salloc (server, SZ_FNAME, TY_CHAR)
	call salloc (output, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE, TY_CHAR)

	# Get  parameters.
	inlist = clpopnu ("input")
	call clgstr ("server", Memc[server], SZ_FNAME)
	call clgstr ("output", Memc[output], SZ_FNAME)
	timeout = clgeti ("timeout")

	# Open output first to allow writing errors to output.
	out = open (Memc[output], APPEND, TEXT_FILE)
	call fseti (out, F_FLUSHNL, YES)

	iferr {
	    in = NULL; srvin = NULL; srvout = NULL; msg = NULL

	    # Open server connections.
	    tmp = msg_open (Memc[server], READ_WRITE, TEXT_FILE); msg = tmp
	    tmp = msg_fd (msg); srvin = tmp
	    tmp = reopen (srvin, WRITE_ONLY); srvout = tmp
	    #call fseti (srvout, F_FLUSHNL, YES)

	    while (clgfil (inlist, Memc[input], SZ_FNAME) != EOF) {
		# Open input.
		tmp = open (Memc[input], READ_ONLY, TEXT_FILE); in = tmp

		repeat {
		    # Read from input and write to server until EOF.
		    repeat {
			stat = getline (in, Memc[line])
			if (stat == EOF || streq (Memc[line], "EOF\n"))
			    break
			call putline (srvout, Memc[line])
		    }
		    call putline (srvout, "EOF\n")
		    call flush (srvout)

		    # If a positive timeout is specified then read from
		    # server and write to STDOUT until EOF.  Check for
		    # timeout or disconnect.
		    if (timeout <= 0)
		        next
		    t = clktime (0)
		    repeat {
			# Poll for a response.
			iferr (n = getline (srvin, Memc[line])) {
			    if (clktime(t) > timeout) {
				call sprintf (Memc[line], SZ_LINE,
				    "Server response timed out (%s)")
				    call pargstr (Memc[server])
				call error (499, Memc[line])
			    }
			    call tsleep (2)
			    next
			} else
			    t = clktime (0)
			if (n == EOF) {
			    call sprintf (Memc[line], SZ_LINE,
				"Server disconnected (%s)")
				call pargstr (Memc[server])
			    call error (498, Memc[line])
			    break
			}
			if (streq (Memc[line], "EOF\n"))
			   break
			call putline (out, Memc[line])
		    }
		} until (stat == EOF)
	    }
	} then {
	    # Upon an error send the error to the output rather than aborting.
	    err = errget (Memc[line], SZ_LINE)
	    call fprintf (out, "STATUS = '%d %s'\n")
	        call pargi (err)
		call pargstr (Memc[line])
        }

	# Close descriptors if needed.
	if (in != NULL)
	    call close (in)
	if (srvin != NULL)
	    call close (srvin)
	if (srvout != NULL)
	    call close (srvout)
	if (msg != NULL)
	    call msg_close (msg)
	call close (out)
	call clpcls (inlist)

	call sfree (sp)
end
