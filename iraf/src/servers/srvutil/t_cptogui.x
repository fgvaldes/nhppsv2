include	"srvutil.h"


.help cptogui Sep02 "Simple GUI Server"
.ih
DESCRIPTION

This server does very little except parse and copy keyword strings to
an OBM GUI.  Strings which begin with a # or are not in <keyword>=<value>
format are ignored.  The strings are parsed into a keyword and a value.
The equal sign is simply the delimiter and is not passed to the GUI.
The value is read as a single word so string values with blanks must
be quoted.  The quotes are stripped when the value is sent to the GUI.

The keyword is used as the GUI message parameter and the value is the
message.  In this mode the GUI would have callbacks for every keyword
of interest.  In addition, if a GUI parameter name is specified then a
message using that parameter is sent and the message is the whitespace
concatenation of the keyword and value.  In this mode the GUI can
have a generic callback and deal with the keyword itself.

The only command from the GUI that this server currently responds to is
the 'q' key to shutdown.  Any other cursor command  causes a read from
the input.  A GUI should have a timer callback to periodically cause the
server to read more input.  Note that a read consists of checking for
new connections, reading from all the open connections, and reading up
to a specified number of text lines from each connection.

This server is intended to read from a socket.  A socket will have a
"port" name with a ':'.  When reading from a socket the server will accept
multiple connections.  It checks for new connections with every read.
It will also close connections when they disappear.

For debugging or maybe some other use, the port may be a text file.
For file input there is can only be one file and when EOF is reached the
file is closed but the server continues running until it is told to quit.
If no "gui" file is specified then the server will simply print the
received lines and command input will be read from the "command" parameter
instead of the "cursor" parameter.
.endhelp



# T_CPTOGUI -- Simple GUI server that copies <keyword>=<value> lines to a GUI
#              from a socket connection or a file.

procedure t_cptogui ()

pointer	input			# Input port or file
pointer	gui			# GUI filename
pointer	param			# GUI message parameter
int	nlines			# Number of lines to read per loop
pointer	graphics		# Graphics device

int	i, j, k, n
int	in, fd[MAXIN], wcs, key
real	wx, wy
pointer	msgin, gp
pointer	sp, cur, line, str, cmd

int	clgeti(), clgcur()
int	nowhite(), stridxs(), ctowrd()
int	msg_fd(), getline()
pointer	msg_open(), gopenui()
errchk	msg_open, msg_fd, gopenui, open

begin
	call smark (sp)
	call salloc (input, SZ_FNAME, TY_CHAR)
	call salloc (gui, SZ_FNAME, TY_CHAR)
	call salloc (param, SZ_FNAME, TY_CHAR)
	call salloc (graphics, SZ_FNAME, TY_CHAR)
	call salloc (cur, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE, TY_CHAR)
	call salloc (str, SZ_LINE, TY_CHAR)
	call salloc (cmd, SZ_LINE, TY_CHAR)

	# Parameters.
	call clgstr ("input", Memc[input], SZ_FNAME)
	call clgstr ("gui", Memc[gui], SZ_FNAME)
	call clgstr ("param", Memc[param], SZ_FNAME)
	i = nowhite (Memc[param], Memc[param], SZ_FNAME)
	nlines = clgeti ("nlines")
	call clgstr ("graphics", Memc[graphics], SZ_FNAME)

	# Open input.
	msgin = msg_open (Memc[input], READ_ONLY, TEXT_FILE)
	call aclri (fd, MAXIN)

	# Open GUI.
	if (nowhite (Memc[gui], Memc[gui], SZ_FNAME) > 0) {
	    gp = gopenui (Memc[graphics], NEW_FILE, Memc[gui], STDGRAPH)
	    call gflush (gp)
	    call strcpy ("cursor", Memc[cur], SZ_FNAME)
	} else {
	    gp = NULL
	    call strcpy ("command", Memc[cur], SZ_FNAME)
	}

	# Cursor loop with GUI.
	while (clgcur (Memc[cur], wx, wy, wcs, key, Memc[cmd], SZ_LINE)!=EOF) {
	    switch (key) {
	    case 'q':
	        break
	    }

	    # Check for new connections.
	    do i = 1, MAXIN
		if (fd[i] == NULL)
		    break
	    if (i <= MAXIN)
	        fd[i] = msg_fd (msgin)

	    # Read pending data.
	    for (i=1; i<=MAXIN; i=i+1) {
		in = fd[i]
	        if (in == NULL)
		    break
		do j = 1, nlines {
		    iferr (n = getline (in, Memc[line]))
		        break
		    if (n == EOF) {
			call close (fd[i])
			do k = i, MAXIN-1 {
			    fd[k] = fd[k+1]
			    if (fd[k] == NULL)
			        break
			}
			fd[k] = NULL
			i = i - 1
			break
		    }

		    # Check for keyword format.
		    k = stridxs ("=", Memc[line])
		    if (k == 0)
			next
		    Memc[line+k-1] = EOS

		    # Keywords have no spaces.
		    k = 1
		    if (ctowrd (Memc[line], k, Memc[cmd], SZ_LINE) == 0)
		        next
		    if (Memc[cmd] == '#')
			next
		    if (ctowrd (Memc[line], k, Memc[str], SZ_LINE) != 0)
			next
		    k = k + 1
		    if (ctowrd (Memc[line], k, Memc[str], SZ_LINE) == 0)
		        next

		    # Send commands to GUI.
		    if (gp != NULL) {
			call gmsg (gp, Memc[cmd], Memc[str])
			if (Memc[param] != EOS) {
			    call sprintf (Memc[line], SZ_LINE, "%s %s")
			        call pargstr (Memc[cmd])
				call pargstr (Memc[str])
			    call gmsg (gp, Memc[param], Memc[line])
			}
		    } else {
		        call printf ("%s = '%s'\n")
			    call pargstr (Memc[cmd])
			    call pargstr (Memc[str])
			call flush (STDOUT)
		    }
		}
	    }
	}

	# Close file descriptors.
	do i = MAXIN, 1, -1 {
	    if (fd[i] != NULL)
		call close (fd[i])
	}
	call msg_close (msgin)

	# Close GUI.
	if (gp != NULL)
	    call gclose (gp)

	call sfree (sp)
end
