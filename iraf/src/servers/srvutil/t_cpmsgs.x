include	<error.h>
include	"srvutil.h"



# T_CPMSGS -- Copy messages.
# The message input and/or output can be files or sockets.
# The message output can be a GUI.

procedure t_cpmsgs ()

int	in				# Input
int	out				# Output
int	sblist				# List of switchboard connections
pointer	prefix				# Prefix string
int	nlines				# Number of lines to read per loop
int	sleep				# Seconds to sleep between loops
bool	wait 				# Wait for new connections?	

bool	gui, server
int	i, j, k, l, n, fd[MAXIN], ack
int	wcs, key
real	wx, wy
pointer	sp, fname, line, inline, msgin, msgout

bool	clgetb(), streq()
int	clpopnu(), clplen(), clgfil(), clgeti(), clgcur()
int	reopen(), strmatch(), msg_fd(), strlen(), getline(), stridxs()
pointer	msg_open(), gopenui()
errchk	msg_fd, gmsg, putline, flush

define	done_	10

begin
	call smark (sp)
	call salloc (fname, SZ_FNAME, TY_CHAR)
	call salloc (line, 2*SZ_LINE, TY_CHAR)

	call aclri (fd, MAXIN)

	# If the input is a network name then this task is a server if
	# no switchboard list is specified or a client of the switchboard
	# task if specified.  If the input is a regular file it simply
	# writes to the output.

	sblist = clpopnu ("switchbd")
	if (clplen (sblist) == 0) {
	    # Determine if the input is a file or network name.
	    call clgstr ("input", Memc[fname], SZ_FNAME)
	    if (stridxs (":",  Memc[fname]) > 0)
		server = true
	    else
	        server = false
	    iferr (msgin = msg_open (Memc[fname], NEW_FILE, TEXT_FILE)) {
		if (clgetb ("reporterr"))
		    call erract (EA_ERROR)
		return
	    }
	} else {
	    # Connect to server and request data streams.
	    server = false
	    call clgstr ("input", Memc[fname], SZ_FNAME)
	    iferr (msgin = msg_open (Memc[fname], READ_WRITE, TEXT_FILE)) {
		if (clgetb ("reporterr"))
		    call erract (EA_ERROR)
		return
	    }
	    fd[1] = msg_fd (msgin)
	    out = reopen (fd[1], WRITE_ONLY)
	    inline = line + SZ_LINE
	    while (clgfil (sblist, Memc[inline], SZ_LINE) != EOF) {
	        call fprintf (out, "%s %s\n")
		    call pargstr (Memc[inline])
		    call pargstr (Memc[fname])
	    }
	    call close (out)
	}

	# Open output connection.  Exit on error with or without error status.
	call clgstr ("output", Memc[fname], SZ_FNAME)
	if (strmatch (Memc[fname], "\.gui$") > 0) {
	    gui = true
	    msgout = NULL
	    iferr (out=gopenui ("stdgraph", NEW_FILE, Memc[fname], STDGRAPH)) {
	        call msg_close (msgin)
		if (clgetb ("reporterr"))
		    call erract (EA_ERROR)
		return
	    }
	    call gmsg (out, "init", "")
	} else {
	    gui = false
	    iferr (msgout = msg_open (Memc[fname], WRITE_ONLY, TEXT_FILE)) {
		call msg_close (msgin)
		if (clgetb ("reporterr"))
		    call erract (EA_ERROR)
		return
	    }
	    out = msg_fd (msgout)
	    sleep = clgeti ("sleep")
	}

	# Get other parameters.
	call clgstr ("prefix", Memc[line], SZ_LINE)
	nlines = clgeti ("nlines")
	wait = clgetb ("wait")

	prefix = line
	inline = line + strlen (Memc[prefix])

	# Read data.
	repeat {

	    # Check for new connections.
	    do i = 1, MAXIN
		if (fd[i] == NULL)
		    break
	    if (i <= MAXIN)
		fd[i] = msg_fd (msgin)

	    # Read from input and write to output.
	    l = 0
	    for (i=1; i<=MAXIN; i=i+1) {
		in = fd[i]
		if (in == NULL)
		    break
		do j = 1, nlines {
		    iferr (n = getline (in, Memc[inline]))
			break
		    if (n == EOF) {
			call close (fd[i])
			do k = i, MAXIN-1 {
			    fd[k] = fd[k+1]
			    if (fd[k] == NULL)
				break
			}
			fd[k] = NULL
			i = i - 1
			if (i == 0 && !wait)
			    goto done_
			break
		    }

		    iferr {
		    	# Check for client acknowledgement request.
			if (server && streq (Memc[inline], "EOF\n")) {
			    iferr {
			        ack = reopen (fd[i], WRITE_ONLY)
				call putline (ack, "STATUS = '0 CPMSGS'\n")
				call putline (ack, "EOF\n")
				call close (ack)
			    } then
			        ;
			} else if (gui)
			    call gmsg (out, "text", Memc[line])
			else {
			    call putline (out, Memc[line])
			    call flush (out)
			    l = l + 1
			}
		    } then {
			if (clgetb ("reporterr"))
			    call erract (EA_ERROR)
			goto done_
		    }
		}
	    }

	    if (gui) {
		if (clgcur ("gcur",wx,wy,wcs,key,Memc[inline],SZ_LINE) == EOF)
		    break
		switch (key) {
		case 'q':
		    break
		}
	    } else if (l == 0)
		call tsleep (sleep)
	}

done_
	# Close descriptors.
	do i = MAXIN, 1, -1 {
	    if (fd[i] != NULL)
	        call close (fd[i])
	}
	if (gui) {
	    call gmsg (out, "quit", "")
	    call gclose (out)
	}
	if (msgout != NULL)
	    call msg_close (msgout)
	call msg_close (msgin)

	call sfree (sp)
end
