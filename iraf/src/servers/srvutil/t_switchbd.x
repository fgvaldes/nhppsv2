include	<error.h>
include	<fset.h>
include	"srvutil.h"

# The following structure is associated with each uniqe connection name.
# The connection name and pointer to this structure is stored in a symbol table.

define	SB_LEN		6		# Length of symbol structure
define	SB_MSG		Memi[$1]	# MSG associated with symbol name
define	SB_MODE		Memi[$1+1]	# Connection type
define	SB_FDI		Memi[$1+2]	# Pointer to input file descriptors
define	SB_NFDI		Memi[$1+3]	# Number of input file descriptors
define	SB_FDO		Memi[$1+4]	# Pointer to output symbol structures
define	SB_NFDO		Memi[$1+5]	# Number of output file descriptors
define	SB_OID		Memi[$1+5]	# Output ID for servers


# T_SWITCHBD -- Connect input connections to output connections.
# Connections are made through FIO which supports network devices (eg
# sockets) as well as files.  The connections requests are defined by input
# and output "filenames".  Often the request connections are transient.
# This may consist of reading an input configuration file which is then
# closed or a network connection is made by some separate client to the
# receiver that knows about the switchboard server.  the registration is
# requested, and the connection is closed.  This allows receiving and sending
# programs that can connect directly to each other to work unchanged with
# the swithhboard server.
# 
# Alternatively, some receiving clients than understand the switchboard
# server can use the request channel to connect to the switchboard,
# request the input streams they want and receive the data on the same
# request channel.  This has the advantage that each client doesn't need
# to have a separate address.
# 
# The request data consists of lines of one or two columns.  The first
# column is an input connection and the second is an output to which the
# input is to be copied.  The input connections may be specified before they
# are requested, say in an input configuration file, by just an input name.
# 
# If the output name corresponds to the request connection then the
# switchboard server will copy the requested inputs on the same connection.
# This is used when the requesting program is a client and knows about
# making connections requests to the switchboard.
# 
# Note that a receiver client may disappear and then reconnect, though
# this requires a new connection request to be made.  Input clients on
# initially specified ports may come and go without further requests. 

procedure t_switchbd ()

int	conlist				# List of connection request streams
pointer	log				# Log file
int	nlines				# Number of lines to read per loop
int	sleep				# Seconds to sleep between loops
bool	reporterr			# Report errors?
bool	verbose				# Verbose?

int	i, j, k, l, m, n
int	nconlist, fd
int	connect[MAXIN]
pointer	stp, sym, sbcon, sbin, sbout, fdi, fdo, fdl
pointer	sp, fname, line, input, output

bool	clgetb(), streq()
int	clgeti(), clpopnu(), clplen(), clgfil()
int	msg_fd(), getline(), nscan(), open(), reopen(), nowhite()
pointer	msg_open(), sb_alloc()
pointer	stopen(), stfind(), sthead(), stnext(), stname()
errchk	msg_open, msg_fd, stopen, putline, flush, open

define	done_	10

begin
	call smark (sp)
	call salloc (fname, SZ_FNAME, TY_CHAR)
	call salloc (line, 2*SZ_LINE, TY_CHAR)
	call salloc (input, SZ_FNAME, TY_CHAR)
	call salloc (output, SZ_FNAME, TY_CHAR)
	call salloc (log, SZ_FNAME, TY_CHAR)

	# Get flag parameters.
	reporterr = clgetb ("reporterr")
	verbose = clgetb ("verbose")
	if (verbose)
	    call fseti (STDOUT, F_FLUSHNL, YES)

	# Open a symbol table.
	stp = stopen ("", 20, 20, 20*SZ_LINE)

	# Get list of connection request streams.
	conlist = clpopnu ("connect")
	nconlist = clplen (conlist)
	if (nconlist == 0)
	    goto done_
	if (nconlist > MAXIN) {
	    if (reporterr)
		call error (1, "Too many input connection request streams")
	    nconlist = 0
	    goto done_
	}

	# Open connection request streams.
	# Quit on error with or without error status.

	nconlist = 0
	while (clgfil (conlist, Memc[fname], SZ_FNAME) != EOF) {
	    call sb_stripnode (Memc[fname])
	    sym = stfind (stp, Memc[fname])
	    if (sym != NULL)
	        next

	    iferr (fd = msg_open (Memc[fname], NEW_FILE, TEXT_FILE)) {
		if (reporterr)
		    call erract (EA_ERROR)
		goto done_
	    }

	    if (verbose) {
		call printf ("Open connection request stream (%s)\n")
		    call pargstr (Memc[fname])
	    }

	    sbcon = sb_alloc (stp, Memc[fname], READ_WRITE)
	    SB_MSG(sbcon) = fd
	    SB_MODE(sbcon) = READ_WRITE

	    nconlist = nconlist + 1
	    connect[nconlist] = sbcon
	}

	# Get other parameters.
	nlines = clgeti ("nlines")
	sleep = clgeti ("sleep")
	call clgstr ("log", Memc[log], SZ_FNAME)

	fdl = NULL
	if (nowhite (Memc[log], Memc[log], SZ_FNAME) > 0) {
	    if (verbose) {
	        call printf ("Open log file (%s)\n")
		    call pargstr (Memc[log])
	    }
	    iferr (fdl = open (Memc[log], APPEND, TEXT_FILE)) {
	        if (reporterr)
		    call erract (EA_ERROR)
		else if (verbose)
		    call erract (EA_WARN)
	    }
	}

	# Enter server loop.
	repeat {

	    # Check for new request streams.
	    do j = 1, nconlist {
		sbcon = connect[j]
		n = SB_NFDI(sbcon)
		repeat {
		    fd = msg_fd (SB_MSG(sbcon))
		    if (fd != NULL) {
		        if (n > 0 && mod (n, 10) == 0)
			    call realloc (SB_FDI(sbcon), n+10, TY_INT)
			n = n + 1
			SB_OID(sbcon) = SB_OID(sbcon) + 1
			SB_NFDI(sbcon) = n
			Memi[SB_FDI(sbcon)+n-1] = fd
		    }
		} until (fd == NULL)
	    }

	    # Check request streams for new connections.
	    do j = 1, nconlist {
		sbcon = connect[j]
		fdi = SB_FDI(sbcon) - 1
		for (i=1; i <= SB_NFDI(sbcon); i=i+1) {
		    do l = 1, nlines {
			iferr (n = getline (Memi[fdi+i], Memc[line]))
			    break
			if (n == EOF) {
			    call close (Memi[fdi+i])
			    do k = i, SB_NFDI(sbcon)-1
				Memi[fdi+k] = Memi[fdi+k+1]
			    Memi[fdi+k] = NULL
			    SB_NFDI(sbcon) = k - 1
			    i = i - 1
			    break
			}

			# Parse request and add connections.
			iferr {
			    call sscan (Memc[line])
			    call gargwrd (Memc[input], SZ_FNAME)
			    call gargwrd (Memc[output], SZ_FNAME)
			    call sb_stripnode (Memc[input])
			    call sb_stripnode (Memc[output])

			    switch (nscan()) {
			    case 1:
				call sb_add (stp, Memi[fdi+i], SB_OID(sbcon),
				    Memc[input], "", "", verbose)
			    case 2:
				if (fdl != NULL)
				    call flush (fdl)
				call sb_add (stp, Memi[fdi+i], SB_OID(sbcon),
				    Memc[input], Memc[output], Memc[log],
				    verbose)
			    }
			} then {
			    if (reporterr)
				call erract (EA_WARN)
			}
		    }
		}
	    }

	    # Copy messages from inputs to outputs.
	    m = 0
	    for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
		sbin = Memi[sym]
	        if (SB_MODE(sbin) != READ_ONLY ||
		   (SB_NFDO(sbin) == 0 && fdl == NULL))
		    next

		# Check for new connections.
		n = SB_NFDI(sbin)
		repeat {
		    fd = msg_fd (SB_MSG(sbin))
		    if (fd != NULL) {
		        if (n > 0 && mod (n, 10) == 0)
			    call realloc (SB_FDI(sbin), n+10, TY_INT)
			Memi[SB_FDI(sbin)+n] = fd
			n = n + 1
			SB_NFDI(sbin) = n
		    }
		} until (fd == NULL)

		# Read from input and write to output.
		fdi = SB_FDI(sbin) - 1
		n = SB_NFDI(sbin)
		for (i=1; i<=n; i=i+1) {
		    do j = 1, nlines {
			iferr (k = getline (Memi[fdi+i], Memc[line]))
			    break
			if (k == EOF) {
			    call close (Memi[fdi+i])
			    do k = i, n-1
				Memi[fdi+k] = Memi[fdi+k+1]
			    Memi[fdi+n] = NULL
			    n = n - 1
			    SB_NFDI(sbin) = n
			    i = i - 1
			    break
			}
			m = m + 1

			# Write to log file and all the associated outputs.
			if (fdl != NULL) {
			    call fprintf (fdl, "%s %s")
			        call pargstr (Memc[stname(stp,sym)])
				call pargstr (Memc[line])
			}
			do k = 1, SB_NFDO(sbin) {
			    sbout = Memi[SB_FDO(sbin)+k-1]
			    if (sbout == NULL)
			        next
			    fdo = SB_FDO(sbout) - 1
			    do l = 1, SB_NFDO(sbout) {
				fd = Memi[fdo+l]
				if (fd == NULL)
				    next
				iferr {
				    call putline (fd, Memc[line])
				    call flush (fd)
				} then {
				    if (reporterr)
					call erract (EA_WARN)
				    call close (Memi[fdo+l])
				    if (SB_MODE(sbout) == WRITE_ONLY)
				        SB_NFDO(sbout) = 0
				}
			    }
			}

			# Respond to client request for acknowledgement.
			if (streq (Memc[line], "EOF\n")) {
			    iferr {
			        fd = reopen (Memi[fdi+i], WRITE_ONLY)
				call putline (fd, "STATUS = '0 SWITCHBD'\n")
				call putline (fd, "EOF\n")
				call close (fd)
			    } then
			        ;
			    break
			}
		    }
		}
	    }

	    # Here is where we sleep waiting for new client input.
	    if (m == 0)
		call tsleep (sleep)
	}

done_
	# Close the connections and free the structures.
	# Note that as a server, this section will only be called on an error.

	if (stp != NULL) {
	    for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym))
		call sb_close (Memi[sym])
	    call stclose (stp)
	}

	if (fdl != EOF)
	    call close (fdl)

	if (conlist != NULL)
	    call clpcls (conlist)

	call sfree (sp)
end



# SB_ADD -- Add a connection.
#
# Enter input and output connection requests in symbol table as needed.
# Open the connections if not already open.
# Make the connection between the them.

procedure sb_add (stp, fdi, oid, input, output, log, verbose)

pointer	stp			# Symbol table
int	fdi			#I Input request connection FD
int	oid			#O Output ID on request connection
char	input[ARB]		#I Input connection name
char	output[ARB]		#I Output connection name
char	log[ARB]		#I Log file
bool	verbose			#I Verbose?

int	i, n, fdout, fdl
pointer	sym, sbin, sbout, msg
pointer	sp, fname, line

bool	strne()
int	msg_fd(), reopen(), open(), fscan()
pointer	stfind(), msg_open(), sb_alloc()
errchk	stfind, msg_open, msg_fd, sb_alloc, realloc, open

begin
	call smark (sp)
	call salloc (fname, SZ_FNAME, TY_CHAR)
	call salloc (line, SZ_LINE, TY_CHAR)

	# Open the output connection if needed.
	# Check that it is not also an input connection.

	sbout = NULL
	fdout = NULL
	if (output[1] != EOS) {
	    sym = stfind (stp, output)
	    if (sym != NULL)
	        sbout = Memi[sym]

	    if (sym == NULL) {
		if (verbose) {
		    call printf ("Open output (%s)\n")
			call pargstr (output)
		}
		msg = msg_open (output, WRITE_ONLY, TEXT_FILE)
		fdout = msg_fd (msg)
		sbout = sb_alloc (stp, output, WRITE_ONLY)
		SB_MSG(sbout) = NULL
		SB_MODE(sbout) = WRITE_ONLY
		SB_NFDI(sbout) = 0
		SB_NFDO(sbout) = 1
		Memi[SB_FDO(sbout)] = fdout
		call msg_close (msg)

	    } else if (SB_MODE(sbout) == READ_ONLY) {
		call sprintf ( Memc[line], SZ_LINE,
		    "Specified as both input and output (%s)")
		    call pargstr (output)
		call error (1, Memc[line])

	    } else if (SB_MODE(sbout) == READ_WRITE) {
		call sprintf (Memc[line], SZ_LINE, "%s:%d")
		    call pargstr (output)
		    call pargi (oid)
		sym = stfind (stp, Memc[line])
		if (sym == NULL) {
		    sbout = sb_alloc (stp, Memc[line], WRITE_ONLY)
		    SB_MSG(sbout) = NULL
		    SB_MODE(sbout) = WRITE_ONLY
		    SB_NFDI(sbout) = 0
		    SB_NFDO(sbout) = 1

		    fdout = reopen (fdi, READ_WRITE)
		    Memi[SB_FDO(sbout)] = fdout
		    if (verbose) {
			call printf ("Add client (%s)\n")
			    call pargstr (Memc[line])
		    }
		} else
		    call error (2, "Attempt to add existing connection")

	    }
	}

	# Open the input connection if needed.
	# Check that it is not also an output connection.

	sym = stfind (stp, input)
	if (sym != NULL)
	    sbin = Memi[sym]
	    
	if (sym == NULL) {
	    if (verbose) {
		call printf ("Open input (%s)\n")
		    call pargstr (input)
	    }
	    msg = msg_open (input, NEW_FILE, TEXT_FILE)
	    sbin = sb_alloc (stp, input, READ_ONLY)
	    SB_MSG(sbin) = msg
	    SB_MODE(sbin) = READ_ONLY

	} else if (SB_MODE(sbin) != READ_ONLY) {
	    call sprintf ( Memc[line], SZ_LINE,
		"Specified as both input and output (%s)")
		call pargstr (input)
	    call error (1, Memc[line])

	} else if (SB_MSG(sbin) == NULL) {
	    if (verbose) {
		call printf ("Reopen input (%s)\n")
		    call pargstr (input)
	    }
	    msg = msg_open (input, NEW_FILE, TEXT_FILE)
	    SB_MSG(sbin) = msg
	}

	# Connect the input to the output as needed.
	if (sbout != NULL) {
	    n = SB_NFDO(sbin)
	    do i = 1, n
		if (Memi[SB_FDO(sbin)+i-1] == sbout)
		    break
	    if (i > n) {
		if (verbose) {
		    call printf ("Connect (%s) to (%s)\n")
			call pargstr (input)
			call pargstr (output)
		}
		if (mod (i, 10) == 0)
		    call realloc (SB_FDO(sbin), n+10, TY_INT)
		Memi[SB_FDO(sbin)+i-1] = sbout
		SB_NFDO(sbin) = i
	    }

	    if (log[1] != EOS && fdout != NULL) {
		if (verbose) {
		    call printf ("Send (%s) to (%s)\n")
			call pargstr (log)
			call pargstr (output)
		}
		fdl = open (log, READ_ONLY, TEXT_FILE)
		while (fscan (fdl) != EOF) {
		    call gargwrd (Memc[fname], SZ_LINE)
		    if (strne (input, Memc[fname]))
			next
		    call gargstr (Memc[line], SZ_LINE)
		    call fprintf (fdout, "%s\n")
			call pargstr (Memc[line+1])
		}
		call flush (fdout)
		call close (fdl)
	    }
	}

	call sfree (sp)
end


# SB_ALLOC -- Allocate switchboard structure and enter in symbol table.
# Note that the all that is stored in the symbol table is the pointer to
# the externally allocated structure.  This is because we store these
# pointers in the structure and if the memory is allocated in the symbol
# table they may be moved.  Therefore, the memory needs to be deallocated
# explicitly.

pointer procedure sb_alloc (stp, name, mode)

pointer	stp			#I Symbol table
char	name[ARB]		#I Symbol table entry name
int	mode			#I Mode for descriptor
pointer	sb			#O Structure

pointer	sym, stenter()
errchk	calloc, stenter

begin
	# Allocate initial memory.
	call calloc (sb, SB_LEN, TY_STRUCT)
	switch (mode) {
	case WRITE_ONLY:
	    call calloc (SB_FDO(sb), 1, TY_INT)
	case READ_ONLY:
	    call calloc (SB_FDI(sb), 10, TY_INT)
	    call calloc (SB_FDO(sb), 10, TY_INT)
	case READ_WRITE:
	    call calloc (SB_FDI(sb), 10, TY_INT)
	}

	# Enter in symbol table.
	sym = stenter (stp, name, 1)
	Memi[sym] = sb

	return (sb)
end


# SB_CLOSE -- Close switchboard structure and file descriptors it references.

procedure sb_close (sbin)

pointer	sbin		#U Switchboard structure.

int	i
pointer	fdi, fdo

begin
	switch (SB_MODE(sbin)) {
	case READ_WRITE:
	    fdi = SB_FDI(sbin)
	    do i = 1, SB_NFDI(sbin)
		call close (Memi[fdi+i-1])
	    call mfree (fdi, TY_INT)
	    call msg_close (SB_MSG(sbin))
	case WRITE_ONLY:
	    fdo = SB_FDO(sbin)
	    call close (Memi[fdo])
	    call mfree (fdo, TY_INT)
	case READ_ONLY:
	    fdi = SB_FDI(sbin)
	    do i = 1, SB_NFDI(sbin)
		call close (Memi[fdi+i-1])
	    call mfree (fdi, TY_INT)
	    call mfree (SB_FDO(sbin), TY_INT)
	    call msg_close (SB_MSG(sbin))
	}
	call mfree (sbin, TY_STRUCT)
end


# SB_STRIPNODE -- Strip node name.

procedure sb_stripnode (name)

char	name[ARB]		#I Connection

int	i, j, stridxs()

begin
	i = stridxs (":", name)
	if (i > 0)
	    j = stridxs (":", name[i+1])
	if (j > 0)
	    name[i+j] = EOS
end
