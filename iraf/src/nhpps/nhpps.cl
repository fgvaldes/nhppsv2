#{ NHPPS - NHPPS_IRAF package

cl < "nhpps$lib/zzsetenv.def"
package nhpps, bin = nhppsbin$

# Define some Unix commands.
# Use the Bourne shell which is faster to start up in the pipeline environment.
task	$ed		= "$!vi $(*)"
task	$find		= "$!find $(*)"
task	$grep		= "$!grep $(*)"
task	$ls		= "$!ls $(*)"
task	$mv		= "$!mv $(*)"
task	$rm		= "$!rm $(*)"
task	$rsync		= "$!rsync $(*)"
task	$scp		= "$!scp $(*)"
task	$vi		= "$!vi $(*)"

task	nhppsargs	= "nhppssrc$nhppsargs.cl"
cache	nhppsargs
task	dataset		= "nhppssrc$dataset.cl"
cache	dataset

task	$plconfig	= "nhppssrc$plconfig.cl"

task	osf_exitinfo	= "$!osf_exitinfo -p $1 -i id $2"
task	plexit		= "nhppssrc$plexit.cl"
cache	plexit

task	sendmsg		= "nhppssrc$sendmsg.cl"
sendmsg.node		= envget("NHPPS_NODE_NAME")
sendmsg.server		= envget("NHPPS_NODE_MM")
sendmsg.port		= envget("NHPPS_PORT_MM")

task	sendtrig	= "nhppssrc$sendtrig.cl"

task	fscopy		= "nhppssrc$fscopy.cl"
task	fspathnames	= "nhppssrc$fspathnames.cl"
task	fsfunpack	= "nhppssrc$fsfunpack.cl"

clbye
