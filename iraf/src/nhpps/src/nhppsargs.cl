#{ NHPPSARGS.CL -- Parse args for execution parameters.
#
# This parses standard execution parameters from the command line
# and if not found uses NHPPS environment variables.  Any remaining
# arguments are pushed back into the args variable.  This uses the same
# option flags as in other NHPPS tasks as much as possible; e.g. -d for
# dataset name.
#
#	-a application			-> nhppsargs.application
#	-d dataset name			-> nhppsargs.dataset
#	-m module name			-> nhppsargs.module
#	-p pipeline name		-> nhppsargs.pipeline
#	-s status flag			-> nhppsargs.flag
#	-t trigger directory		-> nhppsargs.trigdir
#	-i input directory		-> nhppsargs.indir
#	-o output directory		-> nhppsargs.outdir
#	-w dataset working directory	-> nhppsargs.datadir
#	-c call working directory	-> nhppsargs.calldir
#	-b source code directory	-> nhppsargs.srcdir
#	-r staging directory		-> nhppsargs.stage

# Application name.
i = strstr ("-a", args)
if ((i==1 && substr(args,i,i+2) == "-a ") ||
    substr(args,i-1,i+2) == " -a ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, application, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_SYS_NAME"))
    application = envget ("NHPPS_SYS_NAME")
else
    application = "None"
nhppsargs.application = application

# Pipeline name.
i = strstr ("-p", args)
if ((i==1 && substr(args,i,i+2) == "-p ") ||
    substr(args,i-1,i+2) == " -p ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, pipeline, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_SUBPIPE_NAME"))
    pipeline = envget ("NHPPS_SUBPIPE_NAME")
else
    pipeline = "None"
nhppsargs.pipeline = pipeline

# Module name.
i = strstr ("-m", args)
if ((i==1 && substr(args,i,i+2) == "-m ") ||
    substr(args,i-1,i+2) == " -m ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, module, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_MODULE_NAME"))
    module = envget ("NHPPS_MODULE_NAME")
else
    module = "None"
nhppsargs.module = module

# Dataset name.
i = strstr ("-d", args)
if ((i==1 && substr(args,i,i+2) == "-d ") ||
    substr(args,i-1,i+2) == " -d ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, dataset, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("OSF_DATASET"))
    dataset = envget ("OSF_DATASET")
else if (defvar("EVENT_NAME"))
    dataset = envget ("EVENT_NAME")
else
    dataset = "None"
nhppsargs.dataset = dataset

# Flag.
i = strstr ("-s", args)
if ((i==1 && substr(args,i,i+2) == "-s ") ||
    substr(args,i-1,i+2) == " -s ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, flag, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("OSF_FLAG"))
    flag = envget ("OSF_FLAG")
else
    flag = "None"
nhppsargs.flag = flag

# Trigger directory for pipeline.
i = strstr ("-t", args)
if ((i==1 && substr(args,i,i+2) == "-t ") ||
    substr(args,i-1,i+2) == " -t ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, indir, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_DIR_TRIG"))
    trigdir = envget ("NHPPS_DIR_TRIG")
else
    trigdir = "None"
i = strlen (trigdir)
if (trigdir != "None" && substr(trigdir,i,i) != '/')
    trigdir += '/'
nhppsargs.trigdir = trigdir

# Input directory for pipeline.
i = strstr ("-i", args)
if ((i==1 && substr(args,i,i+2) == "-i ") ||
    substr(args,i-1,i+2) == " -i ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, indir, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_DIR_INPUT"))
    indir = envget ("NHPPS_DIR_INPUT")
else
    indir = "None"
i = strlen (indir)
if (indir != "None" && substr(indir,i,i) != '/')
    indir += '/'
nhppsargs.indir = indir

# Output directory for pipeline.
i = strstr ("-o", args)
if ((i==1 && substr(args,i,i+2) == "-o ") ||
    substr(args,i-1,i+2) == " -o ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, outdir, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_DIR_OUTPUT"))
    outdir = envget ("NHPPS_DIR_OUTPUT")
else
    outdir = "None"
i = strlen (outdir)
if (outdir != "None" && substr(outdir,i,i) != '/')
    outdir += '/'
nhppsargs.outdir = outdir

# Dataset working directory.
i = strstr ("-w", args)
if ((i==1 && substr(args,i,i+2) == "-w ") ||
    substr(args,i-1,i+2) == " -w ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, datadir, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_DIR_DATASET"))
    datadir = envget ("NHPPS_DIR_DATASET")
else
    datadir = "None"
nhppsargs.datadir = datadir

# Call working directory.  This may be structured by dataset or not.
# If defined in the dataset working directory it is created as needed.
i = strstr ("-c", args)
if ((i==1 && substr(args,i,i+2) == "-c ") ||
    substr(args,i-1,i+2) == " -c ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, calldir, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (datadir != "None")
    calldir = datadir // ".calldir"
else
    calldir = "None"
i = strlen (calldir)
if (calldir != "None" && substr(calldir,i,i) != '/')
    calldir += '/'
nhppsargs.calldir = calldir

# Source code directory for pipeline.
i = strstr ("-b", args)
if ((i==1 && substr(args,i,i+2) == "-b ") ||
    substr(args,i-1,i+2) == " -b ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, srcdir, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_PIPEAPPSRC") && pipeline != "None")
    srcdir = envget ("NHPPS_PIPEAPPSRC")
else
    srcdir = "None"
i = strlen (srcdir)
if (srcdir != "None") {
    if (substr(srcdir,i,i) != '/')
	srcdir += '/'
    if (pipeline != "None")
	srcdir += pipeline // "/"
}
nhppsargs.srcdir = srcdir

# Staging directory for pipeline.
i = strstr ("-r", args)
if ((i==1 && substr(args,i,i+2) == "-r ") ||
    substr(args,i-1,i+2) == " -r ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, stage, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_STAGE"))
    stage = envget ("NHPPS_STAGE")
else
    stage = "None"
i = stridx (':', stage)
if (i > 0)
    stage = substr(stage,1,i-1) // '!' // substr(stage,i+1,999)
i = strlen (stage)
if (stage != "None" && substr(stage,i,i) != '/')
    stage += '/'
nhppsargs.stage = stage

keep
