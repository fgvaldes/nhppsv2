#{ DATASET.CL -- Pipeline module/dataset initialization.
#
## =======
## dataset
## =======
## -------------------------------------------------
## set dataset variables and perform startup actions
## -------------------------------------------------
## 
## :Manual Group: nhpps
## :Manual Section: 1
## 
## Description
## ===========
## 
## This command is executed if an NHPPS module and dataset are defined.
## It does standard things for all pipeline modules run by NHPPS or
## for running modules outside of NHPPS with the NHPPS environment
## variables defined.  It defines the IRAF variables:
## 
##     dataset.name: 
##         the dataset name (duplicates ``nhppsargs.dataset``)
##     dataset.sname:
##         a "short" name created by removing the pipeline names for
## 	use in creating dataset filenames
##     dataset.ifile:
##         the dataset input file
##     dataset.rfile:
##         the dataset return file
##     dataset.dlist:
##         the dataset result list
##     dataset.lfile:
##         the dataset log file
##     dataset.parent:
##         the dataset parent name form by dropping the last hyphen
## 	delimited part
##     dataset.child:
##         the child component of the dataset which is the part after
## 	following the last hypen and pipeline name
##     dataset.frestore:
##         the file for commands to restore files used when a module
## 	is restarted; ``.<module>_restore.tmp``
##     dataset.fdelete
##         the file containing files to be deleted when a module
## 	is restarted; ``.<module>_delete.tmp``
## 
## Any variables whose values can't be defined are given the value ``None``.
## 
## In addition the following actions are performed if the dataset
## working directory (``nhppsargs.datadir``) is defined.
## 
## #.  Create the dataset working directory if needed.
## #.  Change the current directory to the working directory.  This
##     means a module always starts in its working directory.
## #.  Print a standard timestamp banner to the logfile and
##     standard output.  This includes the uppercase module name,
##     the dataset name, and the time.
## #.  Reset the standard ``tmp``, ``uparm`` and ``pipedata`` logical
##     directory names to be in the working directory.  In particular
##     ``tmp`` is the working directory, ``uparm`` is the subdirectory
##     ``.uparm`` and ``pipedata`` is the subdirectory ``.pipedata``.
## #.  if the delete file is present in the working directory the
##     referenced files are deleted and then the file is deleted for
##     use by the module when it is rerun
## #.  if the restore file is present in the owrking directory the
##     commands are executed and then the file is deleted for use by
##     the module when it is rerun
## 
## Note that the delete and restore files are named and applied here but
## it is up to the module to create them with the appropriate content.
## 
## See Also
## ========
## 
## pipecl, nhppsargs, plconfig

# The following are derived from the dataset name.

# Full dataset name.
name = nhppsargs.dataset

# Short dataset name.
sname = ""; s1 = name
for (i=stridx("-",s1); i!=0; i=stridx("-",s1)) {
    if (i > 1)
        sname += substr (s1, 1, i)
    s1 = substr (s1, i+4, 1000)
}
if (s1 == "") 
    sname = substr (sname, 1, strlen(sname)-1)
else
    sname += s1
dataset.sname = sname

# Dataset base.
s1 = ""
s1 = substr( name, 1, stridx("-",name)-1 )
dataset.bname = s1

# Default dataset input file.
dataset.ifile = nhppsargs.indir//name//"."//nhppsargs.pipeline
if (access(dataset.ifile) == NO)
    dataset.ifile = "None"
;

# Default dataset return file.
dataset.rfile = nhppsargs.indir//"."//name//"."//nhppsargs.pipeline// "rtn"
if (access(dataset.rfile) == NO) {
    #dataset.rfile = "None"
    # Temporary code when using CallPipe.
    s1 = nhppsargs.indir//name//"."//nhppsargs.pipeline// "rtn"
    if (access(s1)) {
	translit (s1, ":", "!", > dataset.rfile)
        delete (s1)
    } else
	dataset.rfile = "None"
}
;

# Dataset result list.
dataset.dlist = name // ".list"

# Dataset logfile name.
dataset.lfile = name // ".log"

# Parent/child names.
i = strstr ("-"//nhppsargs.pipeline, name)
if (i > 0) {
    parent = substr (name, 1, i-1)
    child = substr (name, i+4, 999)
} else {
    parent = "None"
    child = name
}
dataset.parent = parent
dataset.child = child

# Do things that depend on the datadir being present.
if (access(nhppsargs.datadir)) {
    cd (nhppsargs.datadir)

    # Point data list and logfile to the dataset directory.
    dataset.dlist = nhppsargs.datadir // dataset.dlist
    dataset.lfile = nhppsargs.datadir // dataset.lfile

    # Log module.
    printf ("\n%s (%s): ", strupr(nhppsargs.module), name, >> lfile)
    time (>> lfile)

    # Set tmp, uparm, and pipedata.
    reset (tmp = nhppsargs.datadir)
    if (access(nhppsargs.datadir // ".uparm/"))
	reset (uparm = nhppsargs.datadir // ".uparm/")
    ;
    if (access(nhppsargs.datadir // ".pipedata/"))
	reset (pipedata = nhppsargs.datadir // ".pipedata/")
    ;

    # Check for input and return file in the dataset directory.
    s1 = nhppsargs.datadir//name//"."//nhppsargs.pipeline
    if (access (s1))
	dataset.ifile = s1
    ;
    s1 = nhppsargs.datadir//"."//name//"."//nhppsargs.pipeline//"rtn"
    if (access (s1))
	dataset.rfile = s1
    ;

    # Delete and restore files from previous processing.
    # Modules should endeavor to use the standard tmp file patterns
    # below.  Anything else they should include an explicit delete
    # pattern near the start of the module or use the "fdelete" feature.
    # A one-time protection file may be used to avoid this.  This is
    # needed for the expected rare case of one "pipecl" module calling
    # another.

    fprotect = nhppsargs.datadir // "." // nhppsargs.module // "_protect.tmp"
    if (access (fprotect))
        delete (fprotect)

    else {

	# Any files beginning with the module name and ending in tmp
	# tmp.fits, or tmp.pl are automatically deleted.
	delete (nhppsargs.module//"*tmp")
	delete (nhppsargs.module//"*tmp.fits,"//nhppsargs.module//"*tmp.pl")
	if (access("local")) {
	    delete ("local/"//nhppsargs.module//"*tmp")
	    delete ("local/"//nhppsargs.module//"*tmp.fits,"//"local/"//nhppsargs.module//"*tmp.pl")
	}
	;

	# It is the responsibility of the module to add files that need to
	# be deleted to this file.
	fdelete = nhppsargs.datadir // "." // nhppsargs.module // "_delete.tmp"
	if (access(fdelete)) {
	    delete ("@"//fdelete, >& "dev$null")
	    delete (fdelete)
	}
	;

	# The module must save the files to be restored and add
	# restore commands to the fdelete file.
	frestore = nhppsargs.datadir // "." // nhppsargs.module // "_restore.tmp"
	if (access(frestore)) {
	    cl (< frestore)
	    delete (frestore)
	}
	;
    }
}
;

keep
