#!/bin/env pipecl
#
## ========
## start.cl
## ========
## ------------------------------
## standard pipeline start module
## ------------------------------
## 
## :Manual Group: NHPPS/iraf/bin
## :Manual Section: 1
## 
## Usage
## =====
## 
## ``start.cl [idir [mkuparm [dup]]]``
## 
## | ..
## |    idir: input file option (indir|dadir)
## |    mkuparm: Uparm option (mkdir|copy|link)
## |    dup: Input duplication option (ignore|error|warn|rmdup)
## 
## Description
## ===========
## 
## The command performs standard pipeline dataset initialization
## functions.  Its functions are:
## 
##    #. create the dataset working directory as needed
##    #. setup uparm in the working directory as directed
##    #. move the input and return files as directed
##    #. log input file contents to the dataset logfile
##    #. check for duplicates in the input file and handle as directed
##
## The dataset working directory, as defined by ``nhppsargs.datadir``, is
## created as needed.
## 
## A ``.uparm`` directory is created in the working directory as needed.
## The option for creating the directory is specified by the ``mkuparm``
## parameter which may take the values:
## 
##     mkdir
##         create an empty directory
##     copy
##         create the directory and copy the contents of
## 	``$NHPPS_PIPEAPPSRC/uparm``
##     link
##         link to the currently defined ``uparm$``
##
## The default is "mkdir"; i.e. make an empty ``.uparm`` directory.
## 
## The dataset input file (``dataset.ifile``) and return file
## (``dataset.rfile``) are moved as specifed by the ``idir`` parameter.
## If the value is "indir" the files are moved to the dataset input directory
## (``nhppsargs.indir``) and if it is "datadir" they are moved to the dataset
## working directory (``nhppsargs.datadir``).
##
## The contents of the input file are concatenated to the dataset
## logfile (``dataset.lfile``).
##
## The contents of the input may be checked for duplicates if directed
## by the ``dup`` argument.  The values are:
## 
##     ignore
##         don't check the contents of the input file
##     error
##         check for duplicates and send an ERROR message and exit with
## 	an ERROR status if there are duplicates
##     warn
##         check for duplicates and send an WARN message and exit with
## 	an WARN status if there are duplicates
##     rmdup
##         check for duplicates and send an WARN message, remove
## 	duplicates, and exit with a COMPLETED status
## 
## The default is "ignore".

string	idir = "datadir"	# Input file option (indir|datadir)
string	mkuparm = "mkdir"	# Uparm option (mkdir|copy|link)
string	dup = "ignore"		# Input file dups (ignore|error|warn|rmdup)

string	status = "COMPLETED"

# Tasks and packages.
task $ln       = "$!ln -s $1 $2"

# Get input options.
i = fscan (cl.args, idir, mkuparm, dup)

# Create dataset directory if needed.
if (nhppsargs.datadir == "None")
    plexit ENODATA
;
if (access (nhppsargs.datadir) == NO) {
    mkdir (nhppsargs.datadir)
    sleep 1
}
;
cd (nhppsargs.datadir)

# Create the uparm directory if needed.
s1 = osfn (nhppsargs.datadir // ".uparm")
if (access (s1) == NO) {
    if (mkuparm == "mkdir") {
        mkdir (s1)
    } else if (mkuparm == "copy") {
        mkdir (s1)
	copy ("NHPPS_PIPEAPPSRC$/uparm/*", s1)
    } else if (mkuparm == "link") {
	s2 = osfn ("uparm$")
	if (substr (s2, strlen (s2), strlen (s2)) == '/')
	    s2 = substr (s2, 1, strlen (s2)-1)
	;
	ln (s2, s1)
    }
    reset (uparm = s1 // "/")
    sleep 1
}
;

# Check the input file.
if (access(dataset.ifile)) {
    # Place in the desired locations as needed.
    i = strldx ("/", dataset.ifile)
    if (i > 0) {
        s1 = substr (dataset.ifile, 1, i)
	s2 = substr (dataset.ifile, i+1, 999)
    } else {
        s1 = nhppsargs.datadir
	s2 = dataset.ifile
    }
    if (idir == "indir" && s1 != nhppsargs.indir) {
        move (dataset.ifile, nhppsargs.indir)
	dataset.ifile = nhppsargs.indir // s2
    } else if (idir == "datadir" && s1 != nhppsargs.datadir) {
        move (dataset.ifile, nhppsargs.datadir)
	dataset.ifile = nhppsargs.datadir // s2
    }
    ;

    # Record the contents of the input file.
    concat (dataset.ifile) | tee (dataset.lfile)
}
;

# Check the return file.
if (access(dataset.rfile)) {
    # Place in the desired locations as needed.
    i = strldx ("/", dataset.rfile)
    if (i > 0) {
        s1 = substr (dataset.rfile, 1, i)
	s2 = substr (dataset.rfile, i+1, 999)
    } else {
        s1 = nhppsargs.datadir
	s2 = dataset.rfile
    }
    if (idir == "indir" && s1 != nhppsargs.indir) {
        move (dataset.rfile, nhppsargs.indir)
	dataset.rfile = nhppsargs.indir // s2
    } else if (idir == "datadir" && s1 != nhppsargs.datadir) {
        move (dataset.rfile, nhppsargs.datadir)
	dataset.rfile = nhppsargs.datadir // s2
    }
    ;
}
;

# Check for duplicate entries if desired.
count (dataset.ifile) | scan (i)
sort (dataset.ifile) | uniq | count | scan (j)
if (dup != "ignore" && i != j) {
    if (dup == "error") {
	status = "ERROR"
	sendmsg ("ERROR", "Duplicate entries", dataset.ifile, "VRFY")
    } else if (dup == "warn") {
	status = "WARN"
	sendmsg ("WARNING", "Duplicate entries", dataset.ifile, "VRFY")
    } else if (dup == "remove") {
	status = "COMPLETED"
	sendmsg ("WARNING", "Duplicate entries removed", dataset.ifile, "VRFY")
	list = dataset.ifile
	for (i=1; fscan(list,line)!=EOF; i+=1)
	    printf ("%d %s\n", i, line, >> "start.tmp")
	list = ""; delete (dataset.ifile)
	sort ("start.tmp", col=2,  > dataset.ifile); delete ("start.tmp")
	list = dataset.ifile; s1 = "@BEGIN@"
	while (fscan (list, i, line) != EOF) {
	    if (line != s1) {
		if (s1 != "@BEGIN@")
		    printf ("%d %s\n", j, s1, >> "start.tmp")
		;
		j = i; s1 = line
	    } else {
		j = min (i, j)
	        printf( "Removing duplicate entry:\n%s\n", s1 )
	    } 
	}
	printf ("%d %s\n", j, s1, >> "start.tmp")
	list = ""; delete (dataset.ifile)
	sort ("start.tmp", num+) | fields ("STDIN", "2-", > dataset.ifile)
	delete ("start.tmp")
    }
    ;
}
;

plexit (status)
