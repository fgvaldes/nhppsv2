======
pipecl
======
----------------------------
NHPPS pipeline IRAF CL shell
----------------------------

:Manual Group: NHPPS/bin
:Manual Section: 1

Usage
=====

Interactively:
    ``pipecl``
CL scripts:
    ``#!/bin/env pipecl``

Description
============

The ``pipecl`` command starts an IRAF CL shell which is customized for NHPPS
pipelines.  This document describes the various customizations and,
especially, the CL and NHPPS pipeline environment initialization.

Note that that ``pipecl`` can be used both interactively
and as a shell for host callable IRAF scripts.  Therefore it is useful
as a way to start a general user IRAF interactive session.  This
has the important use of checking what is defined and available when
an IRAF host script starts interpreting the script file.

Steps
=====

#.  The ``hlib$zzsetenv.def`` file is read.  It defines packages,
    logical directories, and variables.  It is largely the same as the
    standard version distributed with IRAF with customization to
    add or override definitions.  The importan customizations are:

    -	reading ``hlib$extern.pkg`` and $iraf/unix/hlib/extern.pkg which
	define external packages available to pipeline scripts
    - 	resetting ``clobber``, ``imtype``, ``fkinit``, ``pmmatch``, and
	``cache`` 

#.  The ``hlib$login.cl`` file is read.  This defines additional
    variables, loads the ``nhpps`` package, executes the ``nhppsargs``
    command, loads standard packages which will be available to all scripts
    (``images``, ``utilities``, ``proto``, and ``servers``),  sets hidden
    parameters from common tasks which differ from the task defaults in
    pipeline usage, and then does application and module initializations if
    the NHPPS application and module evironment variables are defined.

``hlib$login.cl``
=================

This is the fundamental file define the pipeline IRAF environment.
The things that happen are:

#.  CL variables ``telescope``, ``instrument``, ``cm``, and ``dm``
    are defined.  The values of these variables are set later.  Recall that
    the IRAF variable search path, when an explicit task is not specified,
    is first a local variable in the current script/task, then a package
    variable in current package, then finally a CL variable; i.e. from the
    ``clpackage``.

#.  The ``nhpps`` package is loaded and the ``nhpps.nhppsargs`` command
    is executed.  This maps NHPPS environment variables or command line
    arguments to task parameters.  These parameters may then be referenced
    in scripts with ``nhppsargs.<parameter>`` rather than having to be done
    in the script.  Note that parameters which are undefined are set as
    "None".

#.  The prefix for IRAF ``pipes`` is set.

#.  Common standard package are loaded, and heavily used tasks are
    cached with default parameters differing from the task defaults set.
    The loaded packages are ``images``, ``utilities``, ``proto``, and the
    NHPPS/IRAF ``servers``.  The last package loaded is the standard
    ``clpackage`` which then loads the ``system`` package.  The cached
    parameters sets and modified defaults that are:

    ::

	cache dir delete tee hedit nhedit
	dir.max = 0
	delete.verify = no
	tee.append = yes
	hedit.verify = no
	hedit.show = no
	hedit.update = yes 
	nhedit.verify = no
	nhedit.show = no
	nhedit.update = yes

#.  If the pipeline application is defined then an IRAF application
    package is declared and loaded if present.  The package must have
    the name ``nhppsargs.application//"pkg"``

#.  The ``nhpps.plconfig`` task is run.

#.  The ``nhpps.dataset`` task is run if a module is define.

``nhpps.plconfig``
==================

This command reads pipeline application configuration files.  While
these files can contain CL executable commands they are primarily
intend for defining CL variables for the application.  These variables
have default values which may then be overridden by operator configuration
files.

The application configuration is in the directory ``config`` of the
aplication source directory given by ``nhppsargs.srcdir``.  The
configuration file has the name defined by ``nhppsargs.application``
with the extension ``.cl``; e.g. ODI.cl.  If this file is not found
then nothing further is done.

If the file is found its contents are sourced.  Then a file in the
operator pipeline configuration directory defined by the environment
variable ``$PipeConfig`` with the same ``<application>.cl`` name is
sought.  If found then it is sourced.  Normally this file contains
overrides to the default variable values rather than defining any new
variables.

``nhpps.dataset``
=================

This command is executed if an NHPPS module and dataset are defined.
It does standard things for all pipeline modules run by NHPPS or
for running modules outside of NHPPS with the NHPPS environment
variables defined.  It defines the IRAF variables:

    dataset.name: 
        the dataset name (duplicates ``nhppsargs.dataset``)
    dataset.sname:
        a "short" name created by removing the pipeline names for
	use in creating dataset filenames
    dataset.ifile:
        the dataset input file
    dataset.rfile:
        the dataset return file
    dataset.dlist:
        the dataset result list
    dataset.lfile:
        the dataset log file
    dataset.parent:
        the dataset parent name form by dropping the last hyphen
	delimited part
    dataset.child:
        the child component of the dataset which is the part after
	following the last hypen and pipeline name
    dataset.frestore:
        the file for commands to restore files used when a module
	is restarted; ``.<module>_restore.tmp``
    dataset.fdelete
        the file containing files to be deleted when a module
	is restarted; ``.<module>_delete.tmp``

Any variables whose values can't be defined are given the value ``None``.

In addition the following actions are performed if the dataset
working directory (``nhppsargs.datadir``) is defined.

#.  Create the dataset working directory if needed.
#.  Change the current directory to the working directory.  This
    means a module always starts in its working directory.
#.  Print a standard timestamp banner to the logfile and
    standard output.  This includes the uppercase module name,
    the dataset name, and the time.
#.  Reset the standard ``tmp``, ``uparm`` and ``pipedata`` logical
    directory names to be in the working directory.  In particular
    ``tmp`` is the working directory, ``uparm`` is the subdirectory
    ``.uparm`` and ``pipedata`` is the subdirectory ``.pipedata``.
#.  if the delete file is present in the working directory the
    referenced files are deleted and then the file is deleted for
    use by the module when it is rerun
#.  if the restore file is present in the owrking directory the
    commands are executed and then the file is deleted for use by
    the module when it is rerun

Note that the delete and restore files are named and applied here but
it is up to the module to create them with the appropriate content.

See Also
========

nhppsargs, plconfig, dataset
