procedure fsfunpack( infile )

string   infile            {prompt="Input file name"}

bool     fdelete = yes     {prompt="Use fdelete method"}
string   outfile = ""      {prompt="Output file name"}

begin

	bool lcp
	int li, li2
	string lobj, ls1

	task $funpackO = "$!funpack -O $(2) $(1)"
	task $funpackD = "$!funpack -D $(1)"

	lobj = infile

	li = strstr (".fz", lobj)
        li2 = li

	# Default is to not copy
	lcp = NO
	# Find the location of "!", if any
        j = stridx ("!", lobj)
	# If there is a "!" for a shared file system, simply
	# strip out the host name and the "!". If there is a "!"
	# for a local file system, set lcp to yet if the data
	# are not on the current host.

	if (j > 0) {
            if (envget("NHPPS_FSTYPE") == "shared") {
                lobj = substr (lobj, j+1, 999)
                li -= j
            } else
                lcp = (substr(lobj,1,j-1)!=envget("HOST"))
        }
        ;

	if ( li2 > 0 ) {

	    # Define the name of the funpack-ed file
            ls1 = substr (lobj, strldx ("/",lobj)+1, li-1)

	    # Copy and funpack, or straight funpack as needed
            if (lcp) {
                copy (lobj, ls1//".fz")
                funpackD (ls1//".fz")
            } else {
                funpackO (lobj, ls1)
	    }
            lobj = ls1

	    if ( fdelete ) {
                # Use the fdelete method to delete lobj upon restart
                print (lobj, >> dataset.fdelete)
	    }
	    ;

	} else {

	    # No .fz file found, so only copy the file if needed

	    # Define the name of the local file
            ls1 = substr (lobj, strldx ("/",lobj)+1, 999)

	    # Copy as needed
            if (lcp) {
                copy (lobj, ls1)
	        lobj = ls1
            }
	    ;

	}

	outfile = lobj

end

