#{ PLCONFIG.CL -- Pipeline configuration.

struct	params

# Read pipeline application configuration files.

line = "NHPPS_PIPEAPPSRC$/config/"
s2 = line // nhppsargs.application // ".cl"
if (access(s2)) {
    cl (< s2)

    line = "PipeConfig$/"
    s2 = line // nhppsargs.application // ".cl"
    if (access(s2))
	cl (< s2)
    ;

    if (nhppsargs.dataset != "None") {
	s1 = nhppsargs.dataset

	# Look for matches against elements of root dataset name.
	s3 = ""
	i = stridx ("-", s1)
	if (i > 0) {
	    s3 = substr (s1, i+1, 1000)
	    s1 = substr (s1, 1, i-1)
	    i = stridx ("-", s3)
	    if (i > 0)
		s3 = substr (s3, 1, i-1)
	    ;
	    s3 = "-" // s3
	}
	;
	for (i=stridx("_",s1); i>0; i=stridx("_",s1)) {
	    s2 = line // substr (s1, 1, i-1) // ".cl"
	    if (access(s2))
		cl (< s2)
	    ;
	    s2 = line // substr (s1, 1, i-1) // s3 // ".cl"
	    if (access(s2))
		cl (< s2)
	    ;
	    s1 = substr (s1, i+1, 1000)
	}
	s2 = line // s1 // ".cl"
	if (access(s2))
	    cl (< s2)
	;
	s2 = line // s1 // s3 // ".cl"
	if (access(s2))
	    cl (< s2)
	;

	# Look for mapped files.
	s1 = nhppsargs.dataset

	line = "NHPPS_PIPEAPPSRC$/config/"
	s2 = line // "ConfigMap.list"
	if (access(s2)) {
	    list = s2
	    while (fscan (list, s3, params) != EOF) {
		if (substr(s3,1,1) == '#')
		    next
		;
		print (s1) | match (s3, meta+, stop-) | scan (s3)
		if (s3==s1) {
		    if (access(line//params))
			cl (<line//params)
		    else
			print (params) | cl
		}
		;
	    }
	    list = ""
	}
	;

	line = "PipeConfig$/"
	s2 = line // "ConfigMap.list"
	if (access(s2)) {
	    list = s2
	    while (fscan (list, s3, params) != EOF) {
		if (substr(s3,1,1) == '#')
		    next
		;
		print (s1) | match (s3, meta+, stop-) | scan (s3)
		if (s3==s1) {
		    if (access(line//params))
			cl (<line//params)
		    else
			print (params) | cl
		}
		;
	    }
	    list = ""
	}
	;
    }
    ;
}
;

# Now set global environment and CL variables that depend on the
# pipeline application.

set MC = "CalCache$/"
set DM = "DMData$/"

#dm = envget (nhppsargs.application//"_DM_PORT") // ":" // envget (nhppsargs.application//"_DM_NODE")
#cm = envget (nhppsargs.application//"_CM_PORT") // ":" // envget (nhppsargs.application//"_CM_NODE")
dm = envget ("NHPPS_PORT_DM") // ":" // envget ("NHPPS_NODE_DM")
cm = envget ("NHPPS_PORT_CM") // ":" // envget ("NHPPS_NODE_CM")

keep
