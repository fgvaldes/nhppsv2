#!/bin/env pipecl
#
## =======
## call.cl
## =======
## ----------------------------
## call instances of a pipeline
## ----------------------------
## 
## :Manual Group: NHPPS/iraf/bin
## :Manual Section: 1
## 
## Usage
## =====
## 
## ``call.cl fname opt pipe [ntrig[del[rmodule]]]``
## 
## | ..
## |    ``fname`` - source file (``inputfile|datalist|<fname>``)
## |	``inputfile`` - dataset input file
## |	``datalist``  - dataset list file
## |	``<fname>``   - filename in dataset working directory
## |    ``opt``   - calling option (``cat|copy|split``)
## |	``cat``   - concatenate lists in input file
## |	``copy``  - copy input file as input to pipeline
## |	``split`` - split list in input file into separate datasets
## |    ``pipe``  - target (called) pipeline
## |    ``ntrig`` - no. of trigggered datasets per node (``<number>|INDEF``) [``0``]
## |    ``del``   - delete input source upon completion [``no``]
## |    ``rmodule``  - return module name
## 
## Description
## ===========
## 
## A source input dataset file defines datasets for instances of a
## target pipeline.  The datasets are triggered on the available target
## pipelines which consists of generating input, trigger and return files.
## There is a feature to limit the number of datasets triggered, stage
## remaining datasets, and allow retriggering if needed (see ``Staged
## Triggering``).  The number of datasets successfully submitted or staged
## and to be eventually be returned is the exit status with an offset of 20.
## This encoding of the number of called pipelines, which allows exit status
## values below 20 for problems, is specially handled by NHPPS where the
## number is stored in the NHPPS blackboard.
## 
## Parameter Options
## =================
## 
## fname
## -----
## 
## inputfile
##     The input file for the calling dataset is the source input file.
##     This is the file in the working directory with the name
##     <dataset>.<pipe>, where <pipe> is the calling pipeline.
## 
## datalist
##     The data list for the calling dataset is the source input file.
##     This is the file in the working directory with the name
##     <dataset>.list.
## 
## <fname>
##     The specified file name in the working directory is the source
##     input file.
## 
## opt
## ---
## 
## cat
##     The input file consists of a list of files whose contents are
##     concatenated to form the input file for one instance of the target
##     pipeline.  The target dataset is the calling dataset with ``-<pipe>``
##     appended.  The names of the input file and the files in input are
##     not important.
## 
## copy
##     The input file is simply used as the input file for one instance of
##     the target pipeline.  The target dataset is the calling dataset with
##     ``-<pipe>`` appended.  The name of the input file is not important.
## 
## split
##     The input file consists of a list of files, each of which is used to
##     define a dataset for an instance of the target pipeline.  The contents
##     of each file is the input file for the target pipeline.  The target
##     dataset is the calling dataset with ``-pipe`` appended, followd by the
##     part of the file name in the input list from the last hypen to the last
##     period (i.e. excluding any extension).  The rest of the input file name
##     is not important.  It is ok if there is no period delimited extension.
##     If there is no hypen the full basename of the file is appended.
## 
## rmodule
## _______
## 
## The return module name.  This is used when the number of called datasets
## is larger than the status value can return and this module then has to
## set the wait counter.  If this is not specified then the immediately
## following module in the stages output is used.
## 
## ntrig
## -----
## 
## The number of target pipeline datasets triggered per compute node is
## controlled by the ``ntrig`` parameter.  If the value is ``INDEF`` then
## there is no limit and all datasets are distributed to the available nodes
## evenly.  If the value is less than 1, for instance 0 which is the default,
## the number is set to the environment variable ``$CoresPerNode`` if defined
## otherwise it is set to 4.  Finally any positive number directly specifies
## the number of datasets to trigger per compute node.  When there are
## more datasets than are triggered based on this parameter the remaining
## datasets are triggered in response to completions of the triggered
## datasets as described in ``Staged Triggering``.
## 
## Staged Triggering
## =================
## 
## In staged triggering, only a limited number of datasets from the `split`
## option are triggered in instances of the target pipeline on the compute
## nodes.  The remaining datasets are held by the calling pipeline.  When
## a target dataset completes and a return event is handled by the calling
## pipeline with ``return.cl`` then another dataset is sent to the
## pipeline on the same compute node (see ``return.cl`` for special
## discussion of changes in the pool of compute nodes).
## 
## Staged triggering is useful for several reasons.
## 
##     #.	Avoid contentions by too many datasets trying to make progress
##     	on a node.
##     #.	Provide balancing by faster node getting more subsequent
##         datasets.
##     #.  Allow retriggering in the event of a node, Node Manager, or Pipeline
##     	Manager going down.
##     #.  Gracefully take a node out of service by letting it finish its
##     	current work but not submitting new datasets to it.
## 
## The dataset information held is maintained in the directory ``.calldir``
## in the working directory of the calling dataset.  The files are:
## 
##     <dataset>-pipes.<pipe>
##         A sorted list of available target pipelines consisting of
## 	the trigger and input directories.
##     <target dataset>.<pipe>
##         The input data for a staged dataset.
##     <target dataset>.tbd
##         File containing the name of the staged input data file.
##     <target dataset>.proc
##         File containing the name of the staged input data file that
## 	has been submitted to a target pipeline.  This is simply
## 	a change of extension from "tbd" to "proc".
##     <target dataset>.<pipe>.node
##         When a dataset has been triggered this file is created with
## 	the node on which the target pipeline is running.
## 
## The notation <dataset> is the dataset name of the calling pipeline and
## <target dataset> is the dataset name for the target pipeline as described
## under the ``opt`` subsection.  Note that not only the dataset waiting to
## be processed are maintained but also the ones submitted and processing
## but not completed.  This allows resubmission to other nodes in case a
## compute node dies before returning.
## 
## Exit Status Values
## ==================
## 
## The possible status values are:
## 
##
##     COMPLETED
##         Used when the number of returns is greater than 235
##     FATAL
##         Error in arguments or other fatal problem
##     NODATA
##         The specified input has not data
##     NOPIPE
##         No instance of the target pipeline found
##     <nreturn>+20
##         The number of datasets to be returned offset by 20
##
## See Also
## ========
##
## return.cl


string	fname = "inputfile"	# Filename (inputfile|datalist|<filename>)
string	option			# Option (cat|copy|split)
string	tpipe			# Target pipeline
string	rmodule = ""		# Return module
bool	delete = no		# Delete input?
int	ntrig = 0               # Number to trigger

bool    retry = yes
int	ncall, nretry
string	ifile, temp
string	pipes, calldir, idir, tdir, node
string	tname, tfile, rfile
struct	*fd1

# Tasks and packages.
task $pipeselect = "$!pipeselect -ti"
task $stages = "$!stages"
task $osf_wait = "$!osf_wait -f $1 -p $2 -m $3 -s w:$4"

# Set the arguments.
if (fscan (cl.args, fname, option, tpipe, ntrig, delete, rmodule) < 3) {
    printf ("Usage: call.cl fname option tpipe [ntrig delete rmodule]\n")
    plexit FATAL
}
;

# Set number of triggers per node.
if (!isindef(ntrig) &&  ntrig < 1 ) {
    if (defvar("CoresPerNode"))
	ntrig = int (envget("CoresPerNode"))
    else
        ntrig = 4
}
;
printf ("ntrig=%d\n", ntrig)

# Set data file. 
if (fname == "inputfile")
    ifile = dataset.ifile
else if (fname == "datalist")
    ifile = dataset.dlist
else if (access(nhppsargs.datadir//fname))
    ifile= nhppsargs.datadir//fname
else
    ifile= fname
printf ("File: %s\n", ifile)

# Check for data.
i = 0
if (access (ifile))
    count (ifile) | scan (i)
;
if (i == 0) {
    sendmsg ("WARNING", "No data for pipeline", tpipe, "VRFY")
    print (ifile)
    plexit NODATA
}
;

# Check for pipelines.
pipes = dataset.name // "-pipes." // tpipe

nretry = 0
while ( retry ) {
    touch (pipes); pipeselect (nhppsargs.application, tpipe, > pipes)
    i=0; count (pipes) | scan (i)
    if ( i > 0 ) {
        retry = no
    } else {
	if ( nretry > 5 ) {
	    retry = no
	} else {
	    sleep( 2**nretry )
	}
	nretry = nretry + 1
    }	
}

if (i > 0) {
    # Work in the call directory.
    if (access(nhppsargs.calldir) == NO) {
	mkdir (nhppsargs.calldir)
	sleep 1
    }
    ;
    move (pipes, nhppsargs.calldir)
    cd (nhppsargs.calldir)

    # We want to assign datasets in the precedence from pipeselect.
    # But for return.cl we need the pipeline list sorted.

    temp = pipes // ".tmp"
    rename (pipes, temp)
    sort (temp, > pipes)
    fd1 = temp
} else {
    sendmsg ("WARNING", "Pipeline is not running", tpipe, "PIPE")
    delete (pipes)
    ntrig = 0
}
;

# Set ncall.
if (option == "split") {
    ncall = 0; count (ifile) | scan (ncall)
} else
    ncall = 1

# Now loop through the input and trigger or cache datasets.
i = 1; node = ""
list = ifile
for (j=1; j<=ncall; j+=1) {

    # Set filename.
    if (option == "split") {
	if (fscan (list, s1) == EOF)
	    break
	;
	s2 = s1
	k = strldx ("/", s2)
	if (k > 0)
	    s2 = substr (s2, k+1, 1000)
	;
	k = strldx (".", s2)
	if (k > 0)
	    s2 = substr (s2, 1, k-1)
	;
	k = strldx ("-", s2)
	if (k > 0) {
	    s2 = substr (s2, k+1, 1000)
	    if (s2 == "none")
	        s2 = ""
	    ;
	    node = s2
	}
	;

	tfile = dataset.name // "-" // tpipe // s2 // "." // tpipe
    } else {
	tfile = dataset.name // "-" // tpipe // "." // tpipe
    }

    # Create @file.
    if (option == "split")
	rename (s1, tfile)
    else if (option == "cat") {
        concatenate ("@"//ifile, tfile)
	delete ("@"//ifile)
    } else
        copy (ifile, tfile)

    # Trigger subpipeline.
    if (!isindef(ntrig) && i > ntrig) {
	print (tfile, > tfile//".tbd")
        next
    }
    ;

    # Get next pipeline unless the split extension matches an available node.
    idir = ""
    if (node != "")
	match ("^"//node//"!", temp, meta+, stop-, print-) | scan (tdir, idir)
    ;
    if (idir == "") {
	if (fscan (fd1, tdir, idir) == EOF) {
	    i += 1
	    if (isindef(ntrig)==NO && access("PipeConfig$/maxtrig.dat")==YES) {
	        fd1 = temp; touch (pipes//"1.tmp")
		while (fscan (fd1, line) != EOF) {
		    s2 = substr (line, 1, stridx("!",line)-1)
		    x = 1
		    match (s2, "PipeConfig$/maxtrig.dat") | scan (s2, x)
		    k = nint (x * ntrig)
		    if (i <= k)
		        print (line, >> pipes // "1.tmp")
		    ;
		}
		fd1 = ""
		rename (pipes//"1.tmp", temp)
	    }
	    ;

	    fd1 = temp
	    if (fscan (fd1, tdir, idir) == EOF) {
		print (tfile, > tfile//".tbd")
		next
	    }
	    ;
	}
	;

	# Call later.
	if (!isindef(ntrig) && i > ntrig) {
	    print (tfile, > tfile//".tbd")
	    next
	}
	;
    }
    ;

    # Trigger pipeline and record node in calldir directory.
    print (substr(idir,1,stridx("!",idir)-1), > tfile//".node")
    sendtrig (tfile, nhppsargs.trigdir, nhppsargs.indir, idir//tfile,
	idir//tfile//"com", idir//"."//tfile//"rtn", tdir//tfile//"trig")
    print (tfile, > tfile//".proc")

    # Check if we don't want to use the calldir mechanism.
    if (isindef(ntrig))
	delete (tfile//"*")
    ;

}
list = ""; fd1 = ""

if (access(pipes)) {
    delete (temp)

    # Clean up if not triggering.
    if (i == 1 || isindef(ntrig)) {
	delete (tfile//"*")
	delete (pipes)
    }
    ;

    # Delete input file if desired.
    if (delete && access(ifile))
	delete (ifile)
    ;

} else {
    # For nhppsapp execution.
    fspath ("*.tbd", > "tbd.tmp")
    list = "tbd.tmp"
    for (i=0; fscan(list,s1)!=EOF; i+=1) {
        s2 = substr (s1, 1, strstr(".tbd",s1)-1)
	printf ("CallPipeList=%s\n", s2)
    }
    list = ""; delete ("tbd.tmp")

    # Output commands to update the blackboard.

    stages (nhppsargs.pipeline, > "stages.tmp")
    list = "stages.tmp"
    while (fscan (list, j, s1) != EOF) {
        if (s1 == nhppsargs.module) {
	    printf ("osf_update -f %s -p %s -m %s -s c\n",
	        nhppsargs.dataset, nhppsargs.pipeline, s1)
	    if (rmodule != "") {
		printf ("osf_wait -f %s -p %s -m %s -s w:%d\n",
		    nhppsargs.dataset, nhppsargs.pipeline, rmodule, ncall)
	    } else {
		k = fscan (list, j, s1)
		printf ("osf_wait -f %s -p %s -m %s -s w:%d\n",
		    nhppsargs.dataset, nhppsargs.pipeline, s1, ncall)
	    }
	    break
	}
    }
    list = ""; delete ("stages.tmp")

    plexit NOPIPE
}

if (ncall < 235) {
    ncall += 20
    logout (ncall)
} else if (rmodule != "") {
    printf ("osf_wait -f %s -p %s -m %s -s w:%d\n",
	nhppsargs.dataset, nhppsargs.pipeline, rmodule, ncall)
    osf_wait (nhppsargs.dataset, nhppsargs.pipeline, rmodule, ncall)
    plexit COMPLETED
} else {
    s2 = nhppsargs.dataset // "_stages.tmp"
    stages (nhppsargs.pipeline, > s2)
    list = s2
    while (fscan (list, j, s1) != EOF) {
        if (s1 == nhppsargs.module) {
	    k = fscan (list, j, s1)
	    printf ("osf_wait -f %s -p %s -m %s -s w:%d\n",
		nhppsargs.dataset, nhppsargs.pipeline, s1, ncall)
	    osf_wait (nhppsargs.dataset, nhppsargs.pipeline, s1, ncall)
	    break
	}
    }
    list = ""; delete (s2)
    plexit COMPLETED
}
