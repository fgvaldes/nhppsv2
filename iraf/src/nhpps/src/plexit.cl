# PLEXIT -- Logout Interface Procedure
#
## PLEXIT
## """"""
## module exit interface
## '''''''''''''''''''''
## 
## :Manual group:   nhpps/iraf 
## :Manual section: nhpps
## 
## USAGE
## =====
## 
## | cl> plexit id
## 
## PARAMETERS
## ==========
## 
## id
##   Exit identifier as defined for the pipeline.
## 
## defcode = 1
##   Exit code if the ID is not defined for the pipeline.
## 
## defdesc = 'unknown id'
##   Description if the ID is not defined for the pipeline.
## 
## output = 'STDOUT'
##  The output file to which the exit line will be appended.  The special
##  name 'STDOUT' is used to write to the standard output (which may be
##  redirected at a higher level by the calling procedure).  A null string
##  will not produce output.
##
## 
## DESCRIPTION
## ===========
## 
## The task provides a logout for IRAF pipeline modules based on logical
## identifiers defined in the pipeline definition files.  If no pipeline
## is defined and the id is a number then a logout with that value is
## taken and there is no other output; in other words it is the same as
## ``logout id``.  When a pipeline is defined, the ``NHPPS_SUBPIPE_NAME``
## environment variable, the identifier is looked up using the command
## ``osf_exitinfo`` to determine the exit code, type, and description.
## If the look up fails the default code and description are used.
## 
## The exit information, when a pipeline is defined, is printed to the
## specified ``output`` which may be ``STDOUT``, a file, or the null string.
## In the last case no output is produced.  After printing the output the IRAF
## ``logout`` is called with the exit code, thus exiting the CL interpreter.
## 
## Note that is any exit identifier, such as ``ERROR`` which is a reserved
## IRAF word must be explicitly quoted even in command mode.
## 
## EXAMPLES
## ========
## 
## | cl> show nhppsargs.pipeline
## | None
## | cl> plexit 0
## | csh> echo $status
## | 0
## |
## | cl> show nhppsargs.pipeline
## | ftr
## | cl> plexit COMPLETED
## | OK: COMPLETED=3   "normal completion"
## | csh> echo $status
## | 3
## |
## | cl> plexit "ERROR"
## | ERROR: ERROR=5 "nonfatal error"
## | csh> echo $status
## | 5


procedure plexit (id)

string	id			{prompt="Exit ID"}
int	defcode = 1		{prompt="Default exit code"}
string	defdesc = "unknown id"	{prompt="Default exit descriptions"}
file	output = "STDOUT"	{prompt="Output file"}

begin
	int	code
	string	id1, iden, flag, type, desc
	struct	desc1

	# Set defaults.
	id1 = id

	iden = id1
	code = defcode
	type = iden
	desc = defdesc
	desc1 = ""

	# Get exit info.
	if (nhppsargs.pipeline != "None")
	    osf_exitinfo (nhppsargs.pipeline, id1) |
		scan (iden, code, flag, type, desc, desc1)
	else if (fscan (id1, code) == 1) {
	    #printf ("logout %s\n", code)
	    logout (code)
	    return
	}

	# Handle description formating.
	if (desc1 != "") {
	    printf ("%s %s\n", desc, desc1) | translit ("STDOUT", '"', del+) |
	        scan (desc1)
	    desc = desc1
	}

	# Output exit info.
	if (output == "STDOUT")
	    printf ('%s: %s=%d %s\n', type, id1, code, desc)
	else if (output != "")
	    printf ('%s: %s=%d %s\n', type, id1, code, desc, >> output)

	# Exit.
	#reset uparm = "dev$null/"
	logout (code)
end
