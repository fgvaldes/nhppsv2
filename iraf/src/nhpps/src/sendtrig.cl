# SENDTRIG -- Send trigger data.
#
## ========
## sendtrig
## ========
## -------------------------------------
## send a file trigger to NHPPS pipeline 
## -------------------------------------
## 
## :Manual Group: NHPPS/iraf/bin
## :Manual Section: 1
## 
## Usage
## =====
## 
## ``sendtrig input rtdir ridir output comments rtn trigger``
## 
## | ..
## |    input:
## |    rtdir:
## |    ridir:
## |
## |    output:
## |    comments:
## |    rtn:
## |    trigger:
## 
## Description
## ===========
## 
## This command encapsulates a standard file trigger event for NHPPS
## pipelines.  Basically it places files in the standard directories
## for a pipeline.  Note that setting these standard directories must
## be done externally with the ``pipeselect`` command.
## 
## There are up to four files which define the NOAO convention
## for triggering a pipeline.  These are the input data file,
## an optional comment file, a return file, and the empty trigger
## file.
## 
## The trigger file is created by a touch command as the last step.  While in
## principle the trigger file could contain data, the NHPPS pipeline manager
## and module associate with the trigger can respond to a new trigger file
## faster than the contents can be written resulting in the module not getting
## all the expected data.  Thus, the secure approach is to first write input
## data, and any other data, to separate files prior to the trigger file.
## 
## Note that in NHPPS V2.0 the the input and trigger directory independent
## though application developers can choose to use the same directory.
## 
## The steps are:
## 
## #.  Write the non-comment data from the input file to the specified
##     output data file.
## #.  Write any comment data from the input file to the specified
##     output comments file.
## #.  Write the return trigger and input directories, in full IRAF path,
##     to the specified output return file.
## #.  Touch the specified trigger file.
## 
## 
## See Also
## ========
## 
## pipeselect

procedure sendtrig (input, rtdir, ridir, output, comments, rtn, trigger)

file	input			{prompt="Input data file"}
file	rtdir			{prompt="Return trigger directory"}
file	ridir			{prompt="Return input directory"}
file	output			{prompt="Output data file"}
file	comments		{prompt="Output comments file"}
file	rtn			{prompt="Output return file"}
file	trigger			{prompt="Output trigger file"}
bool	verbose = yes		{prompt="Verbose?"}

begin
	file	fsinput, fsrtdir, fsridir, fsoutput, fsrtn, fstrigger
	string	rtdir1, ridir1, rtn1, trigger1
	int	ncomments

	fspath (input) | scan (fsinput)
	fspath (rtdir) | scan (fsrtdir)
	fspath (ridir) | scan (fsridir)
	fspath (output) | scan (fsoutput)
	fspath (rtn) | scan (fsrtn)
	fspath (trigger) | scan (fstrigger)

	if (fsinput != "") {
	    # Check for comments.
	    match ("\#", fsinput) | count | scan (ncomments)

	    # Send output data file.
	    if (ncomments == 0) {
		if (verbose)
		    printf ("  %s -> %s\n", fsinput, fsoutput)
		copy (fsinput, fsoutput)
	    } else {
		if (verbose)
		    printf ("  %s -> %s\n", fsinput, fsoutput)
		match ("\#", fsinput, stop+, > fsoutput)
		if (comments != "") {
		    if (verbose)
			printf ("  %s -> %s\n", fsinput, fscomments)
		    match ("\#", fsinput, > comments)
		}
	    }
	}

	# Send return directories file.
	if (fsrtn != "") {
	    if (verbose)
		printf ("  rtn dirs: %s %s > %s\n", fsrtdir, fsridir, fsrtn)
	    printf ("%s %s\n", fsrtdir, fsridir, > fsrtn)
	}

	# Send trigger file.
	if (fstrigger != "") {
	    if (verbose)
		printf ("  touch %s\n", fstrigger)
	    touch (fstrigger)
	}
end
