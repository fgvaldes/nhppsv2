procedure fspathnames( template )

string  template         {prompt="file template"}

bool    sort = yes       {prompt="sort file list"}
struct  *fslist          {prompt="internal"}

begin

    string tmplt, fspath, ls1
    
    tmplt = template

#    if ( envget( "NHPPS_FSTYPE" ) == "local" ) {
#        pathnames( tmplt, sort=sort )
#    } else {
	fspath = mktemp ("fspath")
        pathnames( tmplt, sort=sort) | concat ( >> fspath )
	fslist = fspath
	while ( fscan( fslist, ls1 ) != EOF ) {
	    print( substr( ls1, stridx("!",ls1)+1, 999 )
	}
	fslist = ""
	delete( fspath )
#    }

end
