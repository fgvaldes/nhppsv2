procedure fscopy( infile )

string   infile           {prompt="Input file name"}

bool     copy = yes       {prompt="If FS is shared, actually copy?"}
bool     short = no       {prompt="Print only file name, not IRAF path?"}

begin

    string fil, lfile

    fil = infile

    if ( (envget("NHPPS_FSTYPE")=="shared") && (copy==no) ) {
	print( fil )
    } else {
        lfile = substr( fil, strldx("/",fil)+1, 999 )
        copy( fil, lfile )
	if ( short )
	    print( lfile )
	else
            fspathnames( lfile )
    }

end
